<?php

class Model {

	protected $dbTable = NULL;
	protected $pKey = NULL;

	public function __construct( $ID=NULL ) {
		if( $ID ) {
			$this->getByID( $ID );
		}
	}

	protected function getByID( $ID ) {
		if( !$this->dbTable || !$this->pKey ) {
			return;
		}
		$db = DB::getInstance();
		$row = $db->row( "SELECT t.* FROM {$this->dbTable} t WHERE t.{$this->pKey}=:ID", ['ID'=>(int)$ID] );
		if( $row && isset( $row['ID'] ) && $row['ID'] == $ID ) {
			$this->assign( $row );
		}
	}

	public function assign( Array $data ) {
		if( is_array( $data ) ) {
			foreach( $data as $property => $value ) {
				if( property_exists( $this, $property ) ) {
					$this->{$property} = $value;
				}
			}
		}
	}

	public function getValues() {
		$cast = [];
		$ref = new ReflectionObject( $this );
		foreach( $ref->getProperties( ReflectionProperty::IS_PUBLIC ) as $property ) {
			$cast[$property->getName()] = $property->getValue( $this );
		}
		return $cast;
	}
}
