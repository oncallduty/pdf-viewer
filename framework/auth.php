<?php

class Auth {

	const BACKEND_TYPE_ADMIN = 1;
	const BACKEND_TYPE_RESELLER = 2;
	//const BACKEND_TYPE_CLIENT = 3;
	const BACKEND_TYPE_COURT_REPORTER = 4;
	const BACKEND_TYPE_COURTCLIENT = 5;

	public static function validateUser() {
		$cfg = Config::getInstance();
		$URI = $cfg->URI;
		Log::add( "Auth::validateUser; URI: " . print_r( $URI, true ) );
		if( property_exists( $URI, 'platform' ) && in_array( $URI->platform, ['webmgr','webapp'] ) ) {
			Log::add( "Auth::validateUser; platform: {$URI->platform}" );
			if( !isset( $_SESSION[$URI->platform] ) || !is_array( $_SESSION[$URI->platform] ) ) {
				$_SESSION[$URI->platform] = [];
			}
			if( $URI->platform === 'webmgr' ) {
				return self::authByWebMgr();
			} elseif( $URI->platform === 'webapp' ) {
				return self::authBySKey();
			}
		} else {
			Log::add( "Auth::validateUser; missing platform in URI! -- {$URI->request}" );
		}
		Log::add( 'Auth::validateUser; authentication failed' );
		return FALSE;
	}

	public static function validateFile( $route ) {
		$cfg = Config::getInstance();
		$URI = $cfg->URI;
		$session =& $_SESSION[$URI->platform];
		if( !$URI->routeID ) {
			Log::add( "Auth::validateFile; invalid fileID: {$URI->routeID}" );
			return FALSE;
		}
		$file = new Files( $URI->routeID );
		if( !$file || !$file->ID || $file->ID != $URI->routeID ) {
			Log::add( "Auth::validateFile; file not found by ID: {$URI->routeID}" );
			return FALSE;
		}
		$hasAccess = FALSE;
		if( isset( $session['user']['ID'] ) && isset( $session['user']['role'] ) ) {
			Log::add( "Auth::validateFile; validate by user role: {$session['user']['role']}" );
			switch( $session['user']['role'] ) {
				case DepositionAttendees::ROLE_MEMBER:
					$user = new Users( $session['user']['ID'] );
					$hasAccess = $file->checkUserAccess( $user );
					break;
				case DepositionAttendees::ROLE_GUEST:
				default:
					$attendee = new DepositionAttendees( $session['user']['ID'] );
					$hasAccess = $file->checkAttendeeAccess( $attendee );
					break;
			}
		} elseif( isset( $session['backendType'] ) && isset( $session['email'] ) && isset( $session['depositionID'] ) ) {
			$deposition = new Depositions( $session['depositionID'] );
			if( $deposition && $deposition->ID == $session['depositionID'] && !$deposition->deleted ) {
				$folder = new Folders( $file->folderID );
				if( $folder && $folder->class == Folders::CLASS_EXHIBIT && $folder->depositionID == $session['depositionID'] ) {
					$hasAccess = TRUE;
				}
			}
		}
		if( $hasAccess ) {
			Log::add( "Auth::validateFile; authenticated file: {$file->ID}, {$file->name}" );
			switch( $route ) {
				case 'viewer':
				case 'presenter':
				case 'presentation':
//					$session['fileID'] = $fileID;
//					unset( $session['docInfo'] );
					$_SESSION['fileHashes'][$file->ID] = $file->hash();
					break;
				default:
					// do not modify session data
					break;
			}
			Config::$fileHash = $_SESSION['fileHashes'][$file->ID];
			$file->loadInfo();
		} else {
			Log::add( "Auth::validateFile; authentication for file failed: {$file->ID}, {$file->name}" );
		}
		return $hasAccess;
	}

	public static function validatePresentation( $route ) {
		$cfg = Config::getInstance();
		$URI = $cfg->URI;
		$session =& $_SESSION[$URI->platform];
		$sessionID = (isset( $session['inPresentation'] ) && $session['inPresentation'] == $URI->routeID) ? $URI->routeID : $URI->sessionID;
		if( !$sessionID ) {
			Log::add( 'Auth::validatePresentation; cannot validate presentation; missing Session ID' );
			return FALSE;
		}
		$user = Cache::jsonGet( "user:{$session['user']['ID']}" );
		if( !property_exists( $user, 'depositionID' ) || $user->depositionID != $sessionID ) {
			Log::add( "Auth::validatePresentation; user is not attending session: {$sessionID}" );
			return FALSE;
		}
		$pInfo = Cache::jsonGet( "presentation:{$sessionID}" );
//		$fileID = filter_input(INPUT_GET, 'fileID', FILTER_SANITIZE_NUMBER_INT);
		if( $pInfo && property_exists( $pInfo, 'fileID' ) ) {
			if( $URI->route == 'presenter' && $URI->routeID ) {
				Log::add( "Auth::validatePresentation; presenter fileID: {$URI->routeID}, was requested explicitly!" );
				$depo = new Depositions( $sessionID );
				if( $depo->speakerID == $session['user']['ID'] ) {
					$pInfo->fileID = $URI->routeID;
				}
			}
			$file = new Files( $pInfo->fileID );
			if( !$file || !$file->ID || $file->ID != $pInfo->fileID ) {
				Log::add( "Auth::validatePresentation; presentation fileID: {$pInfo->fileID}, is not found!" );
				return FALSE;
			}
			Log::add( "Auth::validatePresentation; authenticated presentation file: {$file->ID}, {$file->name}" );
			switch( $route ) {
				case 'presenter':
				case 'presentation':
					$_SESSION['fileHashes'][$file->ID] = $file->hash();
					$session['inPresentation'] = $URI->sessionID;
					$URI->routeID = $pInfo->fileID;
					break;
				default:
					// do not modify session data
					$URI->sessionID = $URI->routeID;
					$URI->routeID = $pInfo->fileID;
					break;
			}
			Config::$fileHash = $_SESSION['fileHashes'][$file->ID];
			$file->loadInfo();
			return TRUE;
		}
		return FALSE;
	}

	protected static function authByWebMgr() {
		$cfg = Config::getInstance();
		$sessionID = filter_input( INPUT_COOKIE, $cfg->WebMgr->cookieName );
		if( $sessionID ) {
			$auth = self::getWebMgrSessionUser( $sessionID );
			$session =& $_SESSION[$cfg->URI->platform];
			if( $auth && $auth instanceof Users ) {
				Log::add( "Auth::authByWebMgr; authenticated user by webmgr session: ({$auth->ID}) '{$auth->firstName} {$auth->lastName}'" );
				$session['user'] = ['ID' => $auth->ID, 'role' => DepositionAttendees::ROLE_MEMBER];
				return TRUE;
			} elseif( $auth && is_array ( $auth ) && isset( $auth['backendType'] ) && $auth['backendType'] === self::BACKEND_TYPE_COURT_REPORTER ) {
				Log::add( "Auth::authByWebMgr; authenticated user by webmgr portal: {$auth['depositionID']}, {$auth['user']}" );
				$session['backendType'] = $auth['backendType'];
				$session['email'] = $auth['user'];
				$session['depositionID'] = $auth['depositionID'];
				return TRUE;
			}
		}
		return FALSE;
	}

	protected static function authBySKey() {
		$URI = Config::getInstance()->URI;
		$session =& $_SESSION[$URI->platform];
		Log::add( 'Auth::authBySKey; session: ' . print_r( $session, true ) );
		$sKey = property_exists( $URI, 'sKey' ) ? $URI->sKey : NULL;
		if( !$sKey && isset( $session['sKey'] ) ) {
			Log::add( 'using Session sKey' );
			$sKey = $session['sKey'];
		}
		Log::add( 'Auth::authBySKey; sKey: ' . print_r( $sKey, true ) );
		if( $sKey ) {
			Log::add( 'attempting authentication by sKey' );
			$api = new APISessions();
			$api->getBySKey( $sKey );
			if( $api && is_object( $api ) && $api->ID ) {
				$session['sKey'] = $sKey;
				if( $api->userID ) {
					$user = new Users( $api->userID );
					if( $user && $user->ID == $api->userID && !$user->deleted ) {
						$session['user'] = ['ID' => $user->ID, 'role' => DepositionAttendees::ROLE_MEMBER];
					} else {
						Log::add( "Auth::authBySKey; invalid user; ID: {$api->userID}" );
						return FALSE;
					}
					Log::add( "Auth::authBySKey; authenticated user by sKey: ({$user->ID}) '{$user->firstName} {$user->lastName}'" );
					Log::add( 'Auth::authBySKey; session: ' . print_r( $session, true ) );
					Log::add( 'Auth::authBySKey; _SESSION: ' . print_r( $_SESSION, true ) );
				} elseif( $api->attendeeID ) {
					$attendee = new DepositionAttendees( $api->attendeeID );
					if( $attendee && $attendee->ID == $api->attendeeID && !$attendee->userID && !$attendee->banned ) {
						$session['user'] = ['ID' => $attendee->ID, 'role' => $attendee->role];
					} else {
						Log::add( "Auth::authBySKey; invalid attendee; ID: {$api->attendeeID}" );
						return FALSE;
					}
					Log::add( "Auth::authBySKey; authenticated attendee by sKey: ({$attendee->ID}) '{$attendee->name}' {$attendee->email}" );
				} else {
					Log::add( "Auth::authBySKey; invalid user or attendee; userID: {$api->userID} attendeeID: {$api->attendeeID}" );
					return FALSE;
				}
				return TRUE;
			}
		}
		return FALSE;
	}

	protected static function getWebMgrSessionUser( $sessionID ) {
		$cfg = Config::getInstance();
		if( mb_strlen( $sessionID ) !== 26 ) {
			Log::add( "Auth::getWebMgrSessionUser; Invalid Session ID: {$sessionID}" );
			return FALSE;
		}
		$sessionData = Cache::get( "{$cfg->WebMgr->sessionPrefix}{$sessionID}" );
		if( !$sessionData ) {
			Log::add( "Auth::getWebMgrSessionUser; No data for session ID: {$sessionID}" );
			return FALSE;
		}
		$beforeSession = $_SESSION;
		$didDecode = session_decode( $sessionData );
		if( $didDecode && isset( $_SESSION[$cfg->WebMgr->ns] ) ) {
			foreach( $_SESSION[$cfg->WebMgr->ns] as $key ) {
				if( isset( $key['userID'] ) && isset( $key['userTypeID'] ) ) {
					$user = new Users( $key['userID'] );
					if( $user && is_object( $user ) && $user->ID == $key['userID'] ) {
						self::restoreSession( $beforeSession );
						return $user;
					}
				} elseif( isset( $key['backendType'] ) && isset( $key['user'] ) && isset( $key['depositionID'] ) ) {
					self::restoreSession( $beforeSession );
					return $key;
				}
			}
			self::restoreSession( $beforeSession );
		}
		return FALSE;
	}

	protected static function restoreSession( $sessionData ) {
		if( isset( $sessionData ) && is_array( $sessionData ) ) {
			session_unset();
			foreach( $sessionData as $key => $data ) {
				$_SESSION[$key] = $data;
			}
		}
	}
}
