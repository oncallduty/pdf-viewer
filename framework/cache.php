<?php

class Cache extends Singleton {

	/**
	 * @var Redis
	 */
	protected static $redis;
	protected static $config;

	protected static function init()
	{
		static::$redis = new Redis();
		static::$config = Config::getInstance()->cache;
		try {
			$didConnect = static::$redis->pconnect( static::$config->host, static::$config->port, static::$config->timeout, static::$config->pid, static::$config->retryInterval );
			if( !$didConnect ) {
				$lastErr = static::$redis->getLastError();
				throw new Exception( "Unable to connect to cache: {$lastErr}" );
			}
		} catch( RedisException $re ) {
			throw new Exception( $re->getMessage(), $re->getCode(), $re->getPrevious() );
		}
	}

	public static function get( $key ) {
		if( !is_string( $key ) ) {
			return FALSE;
		}
		$value = static::$redis->get( $key );
//		if( $value === FALSE ) {
//			Log::add( ['get'=>$key, 'lastError'=>static::$redis->getLastError()] );
//		}
		return $value;
	}

	public static function set( $key, $value ) {
		if( !is_string( $key ) || !is_scalar( $value ) ) {
			return FALSE;
		}
		$success = static::$redis->set( $key, $value );
		if( $success === FALSE ) {
			Log::add( ['set'=>$key, 'lastError'=>static::$redis->getLastError()] );
		}
		return $success;
	}

	public static function jsonGet( $key ) {
		$json = json_decode( static::get( $key ) );
		return $json;
	}

	public static function jsonSet( $key, $json ) {
		$jsonStr = json_encode( $json, JSON_UNESCAPED_SLASHES );
		Log::add( "Cache::jsonSet; key: {$key}, '{$jsonStr}'" );
		return static::set( $key, $jsonStr );
	}

	public static function keys( $mask ) {
		if( !is_string( $mask ) ) {
			return FALSE;
		}
		return static::$redis->keys( $mask );
	}

	public static function del( $key ) {
		if( !is_string( $key ) ) {
			return FALSE;
		}
		static::$redis->del( $key );
	}
}
