<?php

class PDFSocket extends Singleton
{
	protected static $config;
	protected static $errNo;
	protected static $errMsg;

	protected static function init()
	{
		self::$config = Config::getInstance()->pdfsocket;
	}

	/**
	 * Send command to daemon
	 * @param string $command
	 * @return boolean|string
	 */
	private static function request( $command )
	{
		$socket = stream_socket_client( self::$config->path, self::$errNo, self::$errMsg, self::$config->timeout );
		if( !$socket || !is_resource( $socket ) ) {
			Log::add( 'PDFSocket::request; unable to open socket!' );
			return FALSE;
		}
		stream_set_timeout( $socket, self::$config->timeout );

		Log::add( "PDFSocket::request; sending message '{$command}' ..." );
		if( !fwrite( $socket, $command ) ) {
			Log::add( 'PDFSocket::request; error writing to socket!' );
			fclose( $socket );
			return FALSE;
		}
		$resMsg = stream_get_contents( $socket );
		fclose( $socket );
		if( !$resMsg ) {
			Log::add( 'PDFSocket::request; error while reading from socket!' );
			return FALSE;
		}
		Log::add( "PDFSocket::request; response: {$resMsg}" );
		return json_decode( $resMsg );
	}

	/**
	 * Cache command
	 * @param string $inPath
	 * @param string $fileHash
	 * @param int $page
	 * @param string $scale
	 * @return boolean|string
	 */
	public static function cache( $inPath, $fileHash, $page=0, $scale=PDFCache::SCALE_ZOOM4 ) {
		//{"cmd":"cache","params":{"inPath":"/path/doc.pdf","hash":"xyz123","page":0,"scale": "zoom4"}}
		$cmd = [
			'cmd' => 'cache',
			'params' => [
				'inPath' => $inPath,
				'hash' => $fileHash,
				'page' => (int)$page,
				'scale' => (in_array( $scale, PDFCache::$scales )) ? $scale : PDFCache::SCALE_ZOOM4,
				'skipSeed' => FALSE
			]
		];
		$cmdMsg = json_encode( $cmd, JSON_UNESCAPED_SLASHES );
		$result = self::request( $cmdMsg );
		return ($result && property_exists( $result, 'success' )) ? $result->success : FALSE;
	}

	/**
	 * Search command
	 * @param string $inPath
	 * @param int $page page index
	 * @param string $query
	 * @return boolean|string
	 */
	public static function search( $inPath, $page, $query, $getCount=false )
	{
		$pageIndex = (int)$page;
		$okayToGo = TRUE;
		if( !file_exists( $inPath ) || !($page >= 0) || !is_string( $query ) || !$query ) {
			$okayToGo = FALSE;
		}
		if( !$okayToGo ) {
			return json_encode( ['success'=>FALSE, 'error'=>'Error: missing required parameters'] );
		}

		// {"cmd":"search","params":{"inPath":"/path/doc.pdf","page":0,"query":"<query string>","getCount":false}}
		$cmd = [
			'cmd' => 'search',
			'params' => [
				'inPath' => $inPath,
				'page' => $pageIndex,
				'query' => $query,
				'getCount' => $getCount
			]
		];

		$cmdMsg = json_encode( $cmd, JSON_UNESCAPED_SLASHES );
		return self::request( $cmdMsg );
	}

	/**
	 * Annotate command
	 * @param string $inPath
	 * @param string $outPath
	 * @param string $annotationsPath
	 * @param array $flags
	 * @return boolean|string
	 */
	public static function annotate( $inPath, $outPath, $annotationsPath, $flags )
	{
		// {"cmd":"annotate","params":{"inPath":"/path/in.pdf","outPath":"/path/out.pdf","annotationsPath":"/path/annotations.json","flags":["wm","flat","clean"]}}
		return FALSE;
	}
}
