<?php

class DB extends Singleton
{
	const DB_EXEC = 0;
	const DB_COLUMN = 1;
	const DB_COLUMNS = 2;
	const DB_ROW = 3;
	const DB_ROWS = 4;
	const DB_COUPLE = 5;

	protected static $dbConn;
	protected static $affectedRows;
	protected static $config;

	protected static function init()
	{
		$cfg = static::$config = Config::getInstance()->db;
		$dsn = "mysql:host={$cfg->host};port={$cfg->port};dbname={$cfg->database};charset={$cfg->charset}";
		static::$dbConn = new PDO( $dsn, static::$config->user, static::$config->passwd, [PDO::ATTR_PERSISTENT => FALSE] );
	}

	public function execute( $sql, array $data=[], $type=self::DB_EXEC, $style=PDO::FETCH_BOTH )
	{
		$res = NULL;
		$st = static::$dbConn->prepare( $sql );
		if( !$st->execute( $data ) ) {
			$sqlError = $st->errorInfo();
			throw new Exception( $sqlError[2] );
		}
		$sqlError = $st->errorInfo();
		self::$affectedRows = $st->rowCount();
		switch( $type ) {
			case self::DB_COLUMN:
				$res = $st->fetchColumn();
				if( is_array( $res ) ) {
					$res = end( $res );
				}
				break;
			case self::DB_COLUMNS:
				$res = [];
				while( $row = $st->fetch( $style ) ) {
					$res[] = array_shift( $row );
				}
				break;
			case self::DB_ROW:
				$res = $st->fetch( $style );
				if( $res === FALSE ) {
					$res = [];
				}
				break;
			case self::DB_ROWS:
				$res = $st->fetchAll( $style );
				break;
			case self::DB_COUPLE:
				$rows = $st->fetchAll( PDO::FETCH_NUM );
				$res = [];
				if( is_array( $rows[0] ) ) {
					foreach( $rows as $v ) {
						$res[$v[0]] = $v[1];
					}
				}
				break;
			case self::DB_EXEC:
			default:
				$res = self::$affectedRows;
				break;
		}
		return $res;
	}

	public function rows( $sql, Array $data=[], $style=PDO::FETCH_ASSOC )
	{
		return $this->execute( $sql, $data, self::DB_ROWS, $style );
	}

	public function row( $sql, Array $data=[], $style=PDO::FETCH_ASSOC )
	{
		return $this->execute( $sql, $data, self::DB_ROW, $style );
	}

	public function cols( $sql, Array $data=[] )
	{
		return $this->execute( $sql, $data, self::DB_COLUMNS );
	}

	public function col( $sql, Array $data=[] )
	{
		return $this->execute( $sql, $data, self::DB_COLUMN );
	}

	public function couples( $sql, Array $data=[] )
	{
		return $this->execute( $sql, $data, self::DB_COUPLE );
	}
}
