<?php

class Config extends Singleton {

	protected static $cfg = NULL;
	protected static $URI;
	protected static $uriSegments;

	public static $fileHash = '';
	public static $docInfo = NULL;

	protected static function init() {
		$confPath = realpath( pathinfo( __FILE__, PATHINFO_DIRNAME ) . '/../conf' );
		$conf = parse_ini_file( "{$confPath}/config.ini", TRUE );
		if( file_exists( "{$confPath}/local.ini" ) ) {
			$local = parse_ini_file( "{$confPath}/local.ini", TRUE );
			$conf = array_replace_recursive( $conf, $local );
		}
		foreach( $conf as &$section ) {
			$section = (object)$section;
		}
		self::$cfg = $conf;

		self::$URI = new stdClass();
		self::$URI->request = $_SERVER['REQUEST_URI'];

		if( isset( $_SERVER['REQUEST_URI'] ) ) {
			self::$uriSegments = explode( '/', $_SERVER['REQUEST_URI'] );
			if( is_array( self::$uriSegments ) && self::$uriSegments ) {
				array_shift( self::$uriSegments ); // trim first, empty segment
				self::$URI->sessionID = 0;
				self::$URI->sKey = NULL;
				self::$URI->pageIndex = 0;
				self::$URI->scale = NULL;
				self::$URI->query = NULL;
				self::$URI->getCount = NULL;
				self::$URI->platform = mb_strtolower( array_shift( self::$uriSegments ) );
				self::$URI->routeID = (int)array_shift( self::$uriSegments );
				self::$URI->route = mb_strtolower( array_shift( self::$uriSegments ) );
				switch( self::$URI->route ):
					case 'viewer': // /[webapp|webmgr]/rID/viewer/[sKey]
						self::getSessionKey();
						break;
					case 'presenter':
					case 'presentation': // /webapp/rID/[presenter|presentation]/sessionID/sKey
						self::getSessionID();
						self::getSessionKey();
						break;
					case 'image':
						self::getPageIndex();
						self::getScale();
						break;
					case 'annotations':
						self::getPageIndex();
						break;
					case 'search':
						self::getPageIndex();
						self::getSearchParams();
						break;
				endswitch;
			}
		}
//		var_dump( self::$URI );
		static::$cfg['URI'] = self::$URI;
	}

	public function __get( $property ) {
		if( isset( static::$cfg[$property] ) ) {
			return static::$cfg[$property];
		}
		return NULL;
	}

	protected static function getSessionKey() {
		$sessionKey = preg_replace( '/[^0-9a-z]/i', '', array_shift( self::$uriSegments ) );
		if( mb_strlen( $sessionKey ) == 128 ) {
			self::$URI->sKey = $sessionKey;
		}
	}

	protected static function getSessionID() {
		$sessionID = (int)array_shift( self::$uriSegments );
		if( $sessionID >= 0 ) {
			self::$URI->sessionID = $sessionID;
		}
	}

	protected static function getPageIndex() {
		$pageIndex = (int)array_shift( self::$uriSegments );
		if( $pageIndex >= 0 ) {
			self::$URI->pageIndex = $pageIndex;
		}
	}

	protected static function getScale() {
		$scale = array_shift( self::$uriSegments );
		if( in_array( $scale, [PDFCache::SCALE_THUMB, PDFCache::SCALE_ACTUAL, PDFCache::SCALE_ZOOM2, PDFCache::SCALE_ZOOM4] ) ) {
			self::$URI->scale = $scale;
		}
	}

	protected static function getSearchParams() {
		$postData = file_get_contents( 'php://input' );
		$jsonData = json_decode( $postData );
		if( $jsonData && is_object( $jsonData ) && property_exists( $jsonData, 'query' ) ) {
			self::$URI->query = Util::sanitizeString( $jsonData->query );
			self::$URI->getCount = (bool)$jsonData->getCount ;
		}
	}
}
