<?php

class Log extends Singleton {

	protected static $logPath;

	protected static function init() {
		$logDir = realpath( pathinfo( __FILE__, PATHINFO_DIRNAME ) . DIRECTORY_SEPARATOR . Config::getInstance()->paths->logs );
		static::$logPath = "{$logDir}/pdfviewer.log";
		if( !file_exists( static::$logPath ) ) {
			touch( static::$logPath );
			chmod( static::$logPath, 0664 );
		}
		ini_set( 'log_errors', 1 );
		ini_set( 'display_errors', 0 );
		ini_set( 'error_log', self::$logPath );
	}

	public static function add( $entry ) {
		$t = microtime( TRUE );
		$micro = sprintf( '%06d', ($t - floor( $t )) * 1000000 );
		$dt = new DateTime( date( 'Y-m-d H:i:s.' . $micro, $t) );
		$URI = Config::getInstance()->URI;
		$route = $URI->route ? "[{$URI->route}] ": '';
		$msg = $dt->format( 'Y-m-d H:i:s.u' ) . " {$route}" . ((is_string( $entry )) ? $entry : print_r( $entry, TRUE )) . "\n";
		error_log( $msg, 3, static::$logPath );
	}

}