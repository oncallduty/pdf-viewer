<?php

class Util
{
	public static function sanitizeString( $inString )
	{
		if( !is_string( $inString ) ) {
			return FALSE;
		}
		$str = iconv( 'UTF-8', 'UTF-8//IGNORE', $inString );
		return preg_replace( '/(?>[\x00-\x1F]|\xC2[\x80-\x9F]|\xE2[\x80-\x8F]{2}|\xE2\x80[\xA4-\xA8]|\xE2\x81[\x9F-\xAF])/', ' ', $str );
	}
}
