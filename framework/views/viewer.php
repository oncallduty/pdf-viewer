<?php
$cfg = Config::getInstance();
$URI = $cfg->URI;
Config::$docInfo->fileHash = Config::$fileHash;
?>
<!doctype html>
<html ng-app="pdfapp">
<head>
	<title>PDF Viewer</title>
	<meta charset="utf-8">
	<meta http-equiv="content-type" content="text/html;charset=utf-8">
	<link rel="icon" href="/favicon.png" type="image/png">
	<link rel="stylesheet" href="/css/jquery-ui.min.css">
	<link rel="stylesheet" href="/css/style.css">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
	<link href="/css/fonts.css" rel="stylesheet" type="text/css">
	<script src="/js/jquery.min.js"></script>
	<script src="/js/jquery-ui.min.js"></script>
	<script src="/js/angular.js"></script>
	<script src="/js/angular-ui-router.min.js"></script>
	<script src="/js/angular-webstorage.min.js"></script>
	<script src="/js/angular-touch.min.js"></script>
	<script src="/js/pdfviewer.js"></script>
	<script src="/js/pdfapp.js"></script>
	<script src="/js/DrawObject.js"></script>
	<script src="/js/DrawPipeline.js"></script>
	<script src="/js/DrawColor.js"></script>
	<script src="/js/DrawStash.js"></script>
	<script src="/js/DrawCallout.js"></script>
	<base href="/">
</head>
<body data-arrow-keys="">
	<script type="text/javascript">window.document.domain="edepoze.com";</script>
	<div id="pdf-viewer">
		<div data-ui-view="init">
			<div data-ng-init="_docInfo=<?=htmlspecialchars( json_encode( Config::$docInfo, JSON_UNESCAPED_SLASHES ) );?>"></div>
		</div>
		<div id="pdf-container" style="text-align:left;">
			<div ng-controller="PageCtrl">
				<div id="pdf-page-box" data-ui-view="page"></div>
				<div data-ui-view="pdfToolbar"></div>
				<div data-ui-view="pageNumDialog"></div>
				<div data-ui-view="presentPaused"></div>
				<div data-ui-view="pageArrowBtns"></div>
				<div data-ui-view="annotateNoteDialog"></div>
				<div data-ui-view="annotateClear"></div>
				<div data-ui-view="stampDialog"></div>
				<div data-ui-view="callouts"></div>
			</div>
			<div ng-controller="ThumbnailsCtrl">
				<div data-ui-view="thumbnails"></div>
			</div>
		</div>
	</div>
	<div id="scrollbar-measure" style="width:100px;height:100px;overflow:scroll;position:absolute;top:-9999px;"></div>
</body>
</html>
