<?php

$search = function() {
	$cfg = Config::getInstance();
	$URI = $cfg->URI;
	$session =& $_SESSION[$URI->platform];
	$pageIndex = $URI->pageIndex;
	if( !Config::$docInfo || !property_exists( Config::$docInfo, 'pages' ) || !isset( Config::$docInfo->pages[$pageIndex] ) ) {
		require_once( '404.php' );
		exit;
	}
	$pageInfo = PDFCache::getPage( $pageIndex );
	$okayToGo = TRUE;
	if( !$pageInfo || !is_object( $pageInfo ) || !property_exists( $pageInfo, 'state' ) || $pageInfo->state != PDFCache::STATE_COMPLETE ) {
		$okayToGo = FALSE;
	}
	$file = new Files( $URI->routeID );
	if( !$file || !$file->ID || $file->ID != $URI->routeID ) {
		$okayToGo = FALSE;
	}

	if( !$okayToGo ) {
		require_once( '500.php' );
		exit;
	}

	while( ob_get_level() > 0 ) {
		ob_end_clean();
	}

	Log::add( "[Search] Searching page {$pageIndex} for: '{$URI->query}'" );
	$results = PDFSocket::search( $file->fullPath(), $pageIndex, $URI->query, $URI->getCount );
	$jsonResult = json_encode( $results, JSON_UNESCAPED_SLASHES );
	Log::add( "[Search] Result: '{$jsonResult}'" );

	header( "Content-Type: application/json" );
	header( 'Content-Length: ' . strlen( $jsonResult ) );
	echo $jsonResult;
};

$search();
unset( $search );
