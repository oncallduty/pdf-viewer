<?php

$image = function() {
	$cfg = Config::getInstance();
	$URI = $cfg->URI;
	$pageIndex = $URI->pageIndex;
	if( !Config::$docInfo || !property_exists( Config::$docInfo, 'pages' ) || !isset( Config::$docInfo->pages[$pageIndex] ) ) {
		require_once( '404.php' );
		exit;
	}
	$fHash = Config::$fileHash;
	$pageInfo = PDFCache::getPage( $pageIndex, $URI->scale );
	$okayToGo = TRUE;
	if( !$pageInfo || !is_object( $pageInfo ) || !property_exists( $pageInfo, 'state' ) || $pageInfo->state != PDFCache::STATE_COMPLETE ) {
		$okayToGo = FALSE;
	}
	$imgPath = realpath( implode( DIRECTORY_SEPARATOR, [$cfg->paths->cache, $fHash[0], $fHash, ''] ) . $URI->scale . '_' . $pageIndex . '.jpg' );
	Log::add( "Image; imgPath: {$imgPath}" );
	if( !$imgPath || !file_exists( $imgPath ) ) {
		$okayToGo = FALSE;
	}
	if( !$okayToGo ) {
		require_once( '500.php' );
		exit;
	}

	while( ob_get_level() > 0 ) {
		ob_end_clean();
	}

	$readChunked = function( $filePath )
	{
		$chunkSize = 1 * (1024 * 1024); // bytes per chunk -- 1 MB
		$buffer = '';
		$handle = fopen( $filePath, 'rb' );
		if ( $handle === FALSE ) {
			Log::add( "Image; unable to open file handle to: {$filePath}" );
			return FALSE;
		}
		while( !feof( $handle ) ) {
			$buffer = fread( $handle, $chunkSize );
			echo $buffer;
			flush();
		}
		flush();
		$status = fclose( $handle );
		return $status;
	};

	$lastModified = filemtime( $imgPath );
	$fSize = filesize( $imgPath );
	$eTag = hash( 'md5', "{$fHash}{$pageIndex}{$URI->scale}{$lastModified}{$fSize}" );
	$contentType = finfo_file( finfo_open( FILEINFO_MIME_TYPE ), $imgPath );
	header( "Content-Type: {$contentType}" );
	header( "Content-Length: {$fSize}" );
	header( 'Cache-Control: must-revalidate' );
	header( 'Last-Modified: ' . gmdate( 'D, d M Y H:i:s', $lastModified ) . ' GMT' );
	header( "Etag: {$eTag}" );
	header( 'Expires: -1' );
	if( isset( $_SERVER['HTTP_IF_MODIFIED_SINCE'] ) && isset( $_SERVER['HTTP_IF_NONE_MATCH'] ) ) {
		if( strtotime( $_SERVER['HTTP_IF_MODIFIED_SINCE'] ) == $lastModified && trim( $_SERVER['HTTP_IF_NONE_MATCH'] ) == $eTag ) {
			header( 'HTTP/1.1 304 Not Modified' );
			exit;
		}
	}

	$readChunked( $imgPath, TRUE );
};

$image();
unset( $image );
