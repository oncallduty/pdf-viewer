<?php

class PDFCache {

	const STATE_REQUESTED = 'requested';
	const STATE_PENDING = 'pending';
	const STATE_COMPLETE = 'complete';
	const STATE_ERROR = 'error';

	const SCALE_THUMB = 'thumb';
	const SCALE_ACTUAL = 'actual';
	const SCALE_ZOOM2 = 'zoom2';
	const SCALE_ZOOM4 = 'zoom4';

	static $scales = [self::SCALE_THUMB, self::SCALE_ACTUAL, self::SCALE_ZOOM2, self::SCALE_ZOOM4];

	public static function getDoc() {
		Log::add( 'PDFCache::getDoc;' );
		$cfg = Config::getInstance();
		$cache = Cache::getInstance();
		$file = new Files( $cfg->URI->routeID );
		$fHash = Config::$fileHash;
		$scale = self::SCALE_ZOOM4;
		$pageIndex = 0;
		$docKey = "PdfViewer:doc:{$fHash}";
		$pageKey = "PdfViewer:{$scale}:{$fHash}:{$pageIndex}";
		$filePath = $file->fullPath();
		self::request( $pageKey, $filePath, $fHash, $pageIndex, $scale );
		$docInfo = $cache->jsonGet( $docKey );
		return $docInfo;
	}

	public static function getPage( $pageIndex=0, $scale=self::SCALE_ZOOM4 ) {
		Log::add( "PDFCache::getPage; pageIndex: {$pageIndex}" );
		if( !($pageIndex >= 0) || !in_array( $scale, self::$scales ) ) {
			Log::add( "PDFCache::getPage; invalid request to getPage; pageIndex: {$pageIndex}, scale: {$scale}" );
			return;
		}
		$cfg = Config::getInstance();
		$file = new Files( $cfg->URI->routeID );
		$fHash = Config::$fileHash;
		$pgKey = "PdfViewer:{$scale}:{$fHash}:{$pageIndex}";
		$filePath = $file->fullPath();
		return self::request( $pgKey, $filePath, $fHash, $pageIndex, $scale );
	}

	protected static function request( $pgKey, $filePath, $fileHash, $pageIndex=0, $scale=FALSE ) {
		Log::add( "PDFCache::request; pgKey: {$pgKey}" );
		$cfg = Config::getInstance();
		$cache = Cache::getInstance();
		$retries = (int)$cfg->cache->retries;
		$docKey = "PdfViewer:doc:{$fileHash}";
		do {
			$attempt = ($cfg->cache->retries - $retries) + 1;
			Log::add( "PDFCache::request; ({$attempt}) doc: {$docKey}" );
			$docInfo = $cache->jsonGet( $docKey );
			if( !$docInfo ) {
				$docInfo = new PDFCacheRequest();
				$docInfo->state = self::STATE_REQUESTED;
			} elseif( $docInfo->state !== self::STATE_COMPLETE && $docInfo->state !== self::STATE_PENDING ) {
				$docInfo->state = self::STATE_REQUESTED;
			}
			if( $docInfo->state === self::STATE_REQUESTED ) {
				$docInfo->atime = time();
				$cache->jsonSet( $docKey, $docInfo );
			}
			Log::add( "PDFCache::request; ({$attempt}) page: {$pgKey}" );
			$pgInfo = $cache->jsonGet( $pgKey );
			if( !$pgInfo ) {
				$pgInfo = new PDFCacheRequest();
				$pgInfo->state = self::STATE_REQUESTED;
			}
			Log::add( "PDFCache::request; ({$attempt}) page state: {$pgInfo->state} -- {$pgKey}" );
			switch( $pgInfo->state ) {
				case self::STATE_COMPLETE:
					$pgInfo->atime = time();
					$cache->jsonSet( $pgKey, $pgInfo );
					if( $pageIndex > 0 && $cfg->URI->route === 'image' ) {
						// ask daemon regardless, so it can seed page cache
						if( !PDFSocket::cache( $filePath, $fileHash, $pageIndex, $scale ) ) {
							Log::add( 'PDFCache::request; failed to make request to daemon' );
						}
					}
					return $pgInfo;
				case self::STATE_PENDING:
					--$retries;
					usleep( $cfg->cache->retryDelay );
					continue;
				case self::STATE_ERROR:
					$pgInfo->state = self::STATE_REQUESTED;
					break;
				case self::STATE_REQUESTED:
				default:
					break;
			}
			// do NOT update cache if state isn't REQUESTED (or might break pending)
			if( $pgInfo->state === self::STATE_REQUESTED ) {
				$pgInfo->atime = time();
				$cache->jsonSet( $pgKey, $pgInfo );
			}
			if( !PDFSocket::cache( $filePath, $fileHash, $pageIndex, $scale ) ) {
				Log::add( 'PDFCache::request; failed to make request to daemon' );
			}
			--$retries;
			usleep( $cfg->cache->retryDelay );
		}
		while( $pgInfo->state !== self::STATE_COMPLETE && $retries > 0 );
		Log::add( "PDFCache::request; retries exceeded for {$fileHash}, pageIndex: {$pageIndex}, scale: {$scale}" );
	}
}

class PDFCacheRequest {

	public $state;
	public $atime;

	public function __construct() {
		$this->state = PDFCache::STATE_REQUESTED;
		$this->atime = time();
	}

}
