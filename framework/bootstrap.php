<?php

function __autoload( $_className ) {
	$fwPath = realpath( pathinfo( __FILE__, PATHINFO_DIRNAME ) );
	$className = mb_strtolower( preg_replace( '/[^\da-z_]/i', '', $_className ) );
	$classPath = "{$fwPath}/{$className}.php";
	$modelPath = "{$fwPath}/models/{$className}.php";
	if( file_exists( $classPath ) ) {
		require_once( $classPath ) ;
	} elseif( file_exists( $modelPath ) ) {
		require_once( $modelPath );
	}
}

$init = function() {
	date_default_timezone_set( 'UTC' );
	ob_start( 'ob_gzhandler' );
	// init singleton(s)
	$cfg = Config::getInstance();
	DB::getInstance();
	Cache::getInstance();
	PDFSocket::getInstance();
	Log::getInstance();
	ini_set( 'session.save_handler', $cfg->session->saveHandler );
	ini_set( 'session.save_path', $cfg->session->savePath );
	ini_set( 'session.gc_maxlifetime', $cfg->session->maxLifetime );
	ini_set( 'session.cookie_domain', $cfg->session->cookieDomain );
	session_name( $cfg->session->name );
	session_start();
};

$init();
unset( $init );
