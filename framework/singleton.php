<?php

class Singleton
{
	public static function getInstance()
	{
		static $instance = NULL;
		if( $instance === NULL ) {
			$instance = new static();
			static::init();
		}
		return $instance;
	}

	/**
     * Protected constructor to prevent creating a new instance of the singleton via the `new` operator from outside of this class.
     */
	protected function __construct() {}

	/**
	 * Private clone method to prevent cloning of the instance of the singleton instance.
	 * @return void
	 */
	private function __clone() {}

	/**
	 * Private unserialize method to prevent unserializing of the singleton instance.
	 */
	private function __wakeup() {}

	/**
	 * Protected method for subclasses to override for initialization
	 */
	protected static function init() {}
}
