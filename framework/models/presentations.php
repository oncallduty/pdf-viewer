<?php

/**
 * @property bigint $depositionID
 * @property bigint $fileID
 */
class Presentations extends Model {

	public $depositionID;
	public $fileID;

	protected $dbTable = 'Presentations';
	protected $pKey = 'depositionID';
}
