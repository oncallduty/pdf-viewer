<?php

/**
 * @property bigint $ID
 * @property char $typeID
 * @property bigint $clientID
 * @property varchar $firstName
 * @property varchar $lastName
 * @property varchar $email
 * @property varchar $username
 * @property varchar $phone
 * @property varchar $address1
 * @property varchar $address2
 * @property varchar $city
 * @property char $state
 * @property varchar $ZIP
 * @property timestamp $created
 * @property timestamp $deleted
 * @property bool $clientAdmin
 */
class Users extends Model {

	public $ID;
	public $typeID;
	public $clientID;
	public $firstName;
	public $lastName;
	public $email;
	public $username;
	public $phone;
	public $address1;
	public $address2;
	public $city;
	public $state;
	public $ZIP;
	public $created;
	public $deleted;
	public $clientAdmin;

	protected $dbTable = 'Users';
	protected $pKey = 'ID';

	const TYPE_CLIENT_ADMIN = 'C';
	const TYPE_CLIENT_USER = 'CU';

	public function isClientAdmin() {
		return ($this->typeID == self::TYPE_CLIENT_ADMIN || $this->clientAdmin);
	}
}
