<?php

/**
 * @property bigint $ID
 * @property bigint $caseID
 * @property bigint $depositionID
 * @property bigint $createdBy
 * @property varchar $name
 * @property timestamp $created
 * @property timestamp $lastModified
 * @property enum $class
 */
class Folders extends Model {

	public $ID;
	public $caseID;
	public $depositionID;
	public $createdBy;
	public $name;
	public $created;
	public $lastModified;
	public $class;

	protected $dbTable = 'Folders';
	protected $pKey = 'ID';

	const CLASS_FOLDER = 'Folder';
	const CLASS_PERSONAL = 'Personal';
	const CLASS_TRUSTED = 'Trusted';
	const CLASS_EXHIBIT = 'Exhibit';
	const CLASS_TRANSCRIPT = 'Transcript';
	const CLASS_COURTESY_COPY = 'CourtesyCopy';
	const CLASS_WITNESS_ANNOTATIONS = 'WitnessAnnotations';

	public function checkUserAccess( Users $user, $fileID=0 ) {
		if( !$this->ID || !$this->class || !$user || !$user->ID ) {
			return FALSE;
		}
		switch( $this->class ) {
			case self::CLASS_FOLDER:
			case self::CLASS_PERSONAL:
			case self::CLASS_WITNESS_ANNOTATIONS:
				return ($this->createdBy == $user->ID);
			case self::CLASS_EXHIBIT:
			case self::CLASS_TRANSCRIPT:
				if( $this->isTrustedUser( $user ) ) {
					return TRUE;
				}
				if( $fileID && $this->acccessToFileInCaseFolder( $user, $fileID ) ) {
					return TRUE;
				}
				if( $this->depositionID ) {
					$attendee = DepositionAttendees::getAttendeeForSession( $user->ID, $this->depositionID );
					if( $attendee ) {
						return $this->checkAttendeeAccess( $attendee, $fileID );
					}
				}
				break;
			case self::CLASS_TRUSTED:
				return $this->isTrustedUser( $user );
		}
		return FALSE;
	}

	public function checkAttendeeAccess( DepositionAttendees $attendee, $fileID=0 ) {
		if( !$this->ID || !$this->class || !$attendee || !$attendee->ID ) {
			return FALSE;
		}
		if( $this->depositionID ) {
			$session = new Depositions( $this->depositionID );
			if( $attendee->depositionID != $this->depositionID && $attendee->depositionID != $session->parentID ) {
				if( $fileID && $this->acccessToFileInCaseFolder( $attendee, $fileID ) ) {
					return TRUE;
				}
				Log::add( "Folder::checkAttendeeAccess; invalid Session ID: {$attendee->depositionID} != {$this->depositionID}" );
				return FALSE;
			}
		} else {
			if( !in_array( $attendee->depositionID, $this->getLinkedSessions() ) ) {
				Log::add( "Folder::checkAttendeeAccess; invalid Session ID: {$attendee->depositionID}" );
				return FALSE;
			}
		}
		if( $attendee->banned ) {
			Log::add( 'Folder::checkAttendeeAccess; attendee is banned from the session' );
			return FALSE;
		}
		$createdBy = new Users( $this->createdBy );
		Log::add( "Folder::checkAttendeeAccess; class: {$this->class}" );
		switch( $this->class ) {
			case self::CLASS_EXHIBIT:
			case self::CLASS_TRANSCRIPT:
				if( $attendee->role === DepositionAttendees::ROLE_MEMBER ) {
					$user = new Users( $attendee->userID );
					if( $user && $user->ID && $user->ID == $attendee->userID ) {
						return ($user->clientID == $createdBy->clientID);
					}
				}
				if( $this->class === self::CLASS_EXHIBIT ) {
					return in_array( $attendee->role, [DepositionAttendees::ROLE_WITNESS, DepositionAttendees::ROLE_WITNESSMEMBER, DepositionAttendees::ROLE_TEMPWITNESS] );
				}
			case self::CLASS_COURTESY_COPY:
				return ($attendee->role === DepositionAttendees::ROLE_GUEST);
			case self::CLASS_WITNESS_ANNOTATIONS:
			case self::CLASS_TRUSTED:
				return ($attendee->role === DepositionAttendees::ROLE_WITNESSMEMBER);
		}
		return FALSE;
	}

	protected function getCase() {
		$case = new Cases( $this->caseID );
		if( !$case || !$case->ID || $case->ID != $this->caseID ) {
			return NULL;
		}
		return $case;
	}

	protected function getDeposition() {
		$deposition = new Depositions( $this->depositionID );
		if( !$deposition || !$deposition->ID || $deposition->ID != $this->depositionID ) {
			return NULL;
		}
		return $deposition;
	}

	protected function isTrustedUser( Users $user ) {
		if( $this->caseID ) {
			$case = $this->getCase();
			if( $case && $case->isTrustedUser( $user ) ) {
				return TRUE;
			}
			$sessions = $this->getLinkedSessions();
			if( $sessions && is_array( $sessions ) ) {
				foreach( $sessions as $sessionID ) {
					$session = new Depositions( $sessionID );
					if( $session->isTrustedUser( $user ) ) {
						return TRUE;
					}
				}
			}
		} else {
			$deposition = $this->getDeposition();
			return ($deposition && $deposition->isTrustedUser( $user ));
		}
	}

	protected function getLinkedSessions() {
		$db = DB::getInstance();
		return $db->cols( 'SELECT cf.sessionID FROM SessionCaseFolders cf WHERE cf.folderID=:folderID', ['folderID'=>$this->ID] );
	}

	protected function getFoldersForFile( $fileID ) {
		$db = DB::getInstance();
		$sql = '(SELECT fl.* FROM Files fi
			INNER JOIN Folders fl ON (fl.ID=fi.folderID)
			WHERE fi.ID=:fileID)
			UNION
			(SELECT cf.* FROM Files fi
			INNER JOIN Folders fl ON (fl.ID=fi.folderID)
			INNER JOIN Depositions d ON (d.ID=fl.depositionID)
			INNER JOIN Cases c ON (c.ID=d.caseID)
			INNER JOIN Folders cf ON (cf.caseID=c.ID AND cf.class=fl.class)
			WHERE fi.ID=:fileID)';
		return $db->rows( $sql, ['fileID' => (int)$fileID] );
	}

	protected function acccessToFileInCaseFolder( $user, $fileID ) {
		$caseFolders = $this->getFoldersForFile( $fileID );
		//Log::add( "Folder::caseFoldersAccessToFile; fileID: {$fileID}" );
		if( $caseFolders && is_array( $caseFolders ) ) {
			foreach( $caseFolders as $folderInfo ) {
				//Log::add( "Folder::caseFoldersAccessToFile; folderID: {$folderInfo['ID']}" );
				$folder = new Folders( $folderInfo['ID'] );
				if( $folder && $folder->ID && $folder->ID == $folderInfo['ID'] ) {
					if( (get_class( $user ) == 'Users' && $folder->checkUserAccess( $user )) || (get_class( $user ) == 'DepositionAttendees' && $folder->checkAttendeeAccess( $user )) ) {
						Log::add( "Folder::acccessToFileInCaseFolder; has access to folderID: {$folder->ID} with fileID: {$fileID}" );
						return TRUE;
					}
				}
			}
		}
		return FALSE;
	}

}
