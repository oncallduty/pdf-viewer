<?php

/**
 * @property bigint $ID
 * @property varchar $sKey
 * @property bigint $userID
 * @property bigint $attendeeID
 * @property timestamp $created
 */
class APISessions extends Model {

	public $ID;
	public $sKey;
	public $userID;
	public $attendeeID;
	public $created;

	protected $dbTable = 'APISessions';
	protected $pKey = 'ID';

	public function getBySKey( $_sKey ) {
		$sKey = preg_replace( '/[^\da-z]/i', '', $_sKey );
		if( mb_strlen( $sKey ) != 128 ) {
			return FALSE;
		}
		$db = DB::getInstance();
		$sql = 'SELECT * FROM APISessions api WHERE api.sKey=:sKey';
		$row = $db->row( $sql, ['sKey'=>$sKey] );
		if( !$row || !isset( $row['ID'] ) || !$row['ID'] ) {
			return FALSE;
		}
		$this->assign( $row );
	}

}
