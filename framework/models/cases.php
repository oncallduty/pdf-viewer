<?php

/**
 * @property bigint $ID
 * @property bigint $clientID
 * @property bigint $createdBy
 * @property varchar $name
 * @property varchar $number
 * @property varchar $jurisdiction
 * @property timestamp $created
 * @property timestamp $deleted
 * @property enum $class
 */
class Cases extends Model {
	public $ID;
	public $clientID;
	public $createdBy;
	public $name;
	public $number;
	public $jurisdiction;
	public $created;
	public $deleted;
	public $class;

	protected $dbTable = 'Cases';
	protected $pKey = 'ID';

	const CLASS_CASE = 'Case';
	const CLASS_DEMO = 'Demo';

	public function isTrustedUser( Users $user ) {
		if( $user->clientID == $this->clientID ) {
			if( $user->isClientAdmin() || $this->isCaseManager( $user ) || $this->class == self::CLASS_DEMO ) {
				return TRUE;
			}
		}
		return FALSE;
	}

	public function isCaseManager( Users $user ) {
		$db = DB::getInstance();
		$sql = 'SELECT cm.userID FROM CaseManagers cm WHERE cm.caseID=:caseID AND cm.userID=:userID';
		return (bool)$db->col( $sql, ['caseID'=>(int)$this->ID, 'userID'=>(int)$user->ID] );
	}

	public function fileIsSourceToExhibit( $fileID ) {
		$db = DB::getInstance();
		$sql = 'SELECT COUNT(sourceFileID) as numSources FROM ExhibitHistory WHERE caseID=:caseID AND sourceFileID=:fileID';
		return (bool)$db->col( $sql, ['caseID'=>(int)$this->ID, 'fileID'=>(int)$fileID] );
	}
}
