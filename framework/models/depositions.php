<?php

/**
 * @property bigint $ID
 * @property bigint $caseID
 * @property bigint $createdBy
 * @property bigint $ownerID
 * @property bigint $speakerID
 * @property char $statusID
 * @property datetime $openDateTime
 * @property varchar $depositionOf
 * @property varchar $volume
 * @property varchar $title
 * @property varchar $location
 * @property varchar $oppositionNotes
 * @property text $notes
 * @property timestamp $created
 * @property timestamp $deleted
 * @property timestamp $started
 * @property timestamp $finished
 * @property bigint $parentID
 * @property char $exhibitTitle
 * @property char $exhibitSubTitle
 * @property decimal $exhibitXOrigin
 * @property decimal $exhibitYOrigin
 * @property bigint $enterpriseResellerID
 * @property enum $class
 * @property enum $liveTranscript
 * @property varchar $jobNumber
 */
class Depositions extends Model {
	public $ID;
	public $caseID;
	public $createdBy;
	public $ownerID;
	public $speakerID;
	public $statusID;
	public $openDateTime;
	public $depositionOf;
	public $volume;
	public $title;
	public $location;
	public $oppositionNotes;
	public $notes;
	public $created;
	public $deleted;
	public $started;
	public $finished;
	public $parentID;
	public $exhibitTitle;
	public $exhibitSubTitle;
	public $exhibitXOrigin;
	public $exhibitYOrigin;
	public $enterpriseResellerID;
	public $class;
	public $liveTranscript;
	public $jobNumber;

	protected $dbTable = 'Depositions';
	protected $pKey = 'ID';

	const CLASS_DEPOSITION = 'Deposition';
	const CLASS_DEMO_DEPOSITION = 'Demo';
	const CLASS_WITNESSPREP = 'WitnessPrep';
	const CLASS_DEMO_WITNESSPREP = 'WPDemo';
	const CLASS_TRIAL = 'Trial';
	const CLASS_DEMOTRIAL = 'Demo Trial';
	const CLASS_MEDIATION = 'Mediation';
	const CLASS_ARBITRATION = 'Arbitration';
	const CLASS_HEARING = 'Hearing';

	protected function getByID( $ID ) {
		parent::getByID( $ID );
		if( !$this->speakerID && !$this->parentID ) {
			$this->speakerID = $this->ownerID;
		}
	}

	public function isTrustedUser( Users $user ) {
		if( !$this->ID || !$this->caseID ) {
			return FALSE;
		}
		$case = $this->getCase();
		if( $case && $user->clientID == $case->clientID ) {
			if( $user->isClientAdmin() ) {
				return TRUE;
			}
			if( $user->ID == $this->ownerID ) {
				return TRUE;
			}
			if(	$case->isCaseManager( $user ) || $case->isTrustedUser( $user ) ) {
				return TRUE;
			}
			return ($this->isSessionAssistant( $user ));
		}
		return FALSE;
	}

	protected function getCase() {
		$case = new Cases( $this->caseID );
		if( !$case || !$case->ID || $case->ID != $this->caseID ) {
			return NULL;
		}
		return $case;
	}

	protected function isSessionAssistant( Users $user ) {
		$db = DB::getInstance();
		return (bool)$db->col( 'SELECT da.userID FROM DepositionAssistants da WHERE da.depositionID=:sessionID AND da.userID=:userID', ['sessionID'=>$this->ID, 'userID'=>$user->ID] );
	}
}
