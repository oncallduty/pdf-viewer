<?php

class DepositionAttendees extends Model {

	/**
	 * @var bigint ID, primary key
	 */
	public $ID;

	/**
	 * @var bigint Session ID (fk)
	 */
	public $depositionID;

	/**
	 * @var bigint User ID (fk)
	 */
	public $userID;

	/**
	 * @var varchar Non-member Name
	 */
	public $name;

	/**
	 * @var varchar Non-member email
	 */
	public $email;

	/**
	 * @var enum Role [M|G|W|WM|TW]
	 */
	public $role;

	/**
	 * @var timestamp Banned on Date
	 */
	public $banned;

	protected $dbTable = 'DepositionAttendees';
	protected $pKey = 'ID';

	const ROLE_MEMBER = 'M';
	const ROLE_GUEST = 'G';
	const ROLE_WITNESS = 'W';
	const ROLE_WITNESSMEMBER = 'WM';
	const ROLE_TEMPWITNESS = 'TW';

	public static function getAttendeeForSession( $userID, $sessionID ) {
		$db = DB::getInstance();
		$sql = 'SELECT ID FROM DepositionAttendees WHERE depositionID=:sessionID AND (userID=:userID OR ID=:userID)';
		$attendeeID = (int)$db->col( $sql, ['sessionID'=>(int)$sessionID,'userID'=>(int)$userID] );
		if( $attendeeID ) {
			return new DepositionAttendees( $attendeeID );
		}
	}
}
