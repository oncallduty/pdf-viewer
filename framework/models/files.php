<?php

/**
 * @property bigint $ID
 * @property bigint $folderID
 * @property bigint $createdBy
 * @property varchar $name
 * @property timestamp $created
 */
class Files extends Model {

	public $ID;
	public $folderID;
	public $createdBy;
	public $name;
	public $created;

	protected $dbTable = 'Files';
	protected $pKey = 'ID';

	public function checkUserAccess( Users $user ) {
		if( !$this->folderID || !$user || !$user->ID ) {
			return FALSE;
		}
		$folder = new Folders( $this->folderID );
		if( !$folder || !$folder->ID || $folder->ID != $this->folderID ) {
			return FALSE;
		}
		$isOwner = $folder->checkUserAccess( $user, $this->ID );
		if( $isOwner ) {
			return TRUE;
		}
		$caseID = 0;
		if( $folder->caseID ) {
			$caseID = $folder->caseID;
		} else {
			$session = new Depositions( $folder->depositionID );
			$caseID = $session->caseID;
		}
		$case = new Cases( $caseID );
		if( !$case->isTrustedUser( $user ) ) {
			return FALSE;
		}
		return $case->fileIsSourceToExhibit( $this->ID );
	}

	public function checkAttendeeAccess( DepositionAttendees $attendee ) {
		if( !$this->folderID || !$attendee || !$attendee->ID ) {
			return FALSE;
		}
		$folder = new Folders( $this->folderID );
		if( !$folder || !$folder->ID || $folder->ID != $this->folderID ) {
			return FALSE;
		}
		return $folder->checkAttendeeAccess( $attendee, $this->ID );
	}

	public function fullPath() {
		if( !$this->ID ) {
			return;
		}
		$sql = 'SELECT c.clientID, c.ID as caseID, fo.caseID as folderCaseID, d.ID as depositionID, fo.ID as folderID, fo.createdBy as ownerID, fo.name as folderName FROM Files fi
			INNER JOIN Folders fo ON (fo.ID=fi.folderID)
			LEFT JOIN Depositions d ON (d.ID=fo.depositionID)
			LEFT JOIN Cases c ON (c.ID=d.caseID OR c.ID=fo.caseID)
			WHERE fi.ID=:fileID';
		$db = DB::getInstance();
		$fi = $db->row( $sql, ['fileID'=>$this->ID] );
		if( !$fi || !is_array( $fi ) ) {
			return;
		}
		$casesPath = implode( DIRECTORY_SEPARATOR, ['', 'clients', $fi['clientID'], 'cases', $fi['caseID']] );
		$folderPath = ($fi['folderCaseID']) ? implode( DIRECTORY_SEPARATOR, ['', $fi['folderID']] ) : implode( DIRECTORY_SEPARATOR, ['', 'depositions', $fi['depositionID'], 'users', $fi['ownerID'], $fi['folderName']] );
		$basePath = '.' . DIRECTORY_SEPARATOR . Config::getInstance()->paths->media . $casesPath . $folderPath;
		$mediaPath = realpath( $basePath );
		if( !$mediaPath ) {
			Log::add( "Files::fullPath; missing directory: {$basePath}, casesPath: {$casesPath}, folderPath: {$folderPath}, mediaPath: {$mediaPath}" );
		}
		$fullPath = $mediaPath . DIRECTORY_SEPARATOR . $this->name;
		return $fullPath;
	}

	public function hash() {
		$fullPath = $this->fullPath();
		$fileHash = hash_file( 'md5', $fullPath );
		if( !$fileHash ) {
			Log::add( "File::hash; unable to hash: {$fullPath}" );
		}
		return $fileHash;
	}

	public function loadInfo() {
		$URI = Config::getInstance()->URI;
		if( !Config::$fileHash ) {
			Log::add( "Files::loadInfo; missing fileHash: {$URI->routeID}" );
			return;
		}
		if( !Config::$docInfo ) {
			Config::$docInfo = PDFCache::getDoc();
		}
	}
}
