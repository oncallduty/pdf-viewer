/**********************************************
 * Copyright by eDepoze 2015
 * All rights reserved.
 */



/**
 * UndoStashFactory makes new UndoStashClass object
 * @returns UndoStashClass
 */
function StashFactory() {



	// make my new factory instance
	var Stash = new StashClass();

	// return the instance
	return Stash;
};


var StashClass = function StashClass() {

	/**
	 * The pipeline of draw objects
	 * @type Array|Array
	 */
	this.pages = {};
	this.stashes = {};

};


StashClass.prototype.stash = function stash(page, pipeline, undoStack, callouts) {
	var stackCopy = [];
	for(var idx in undoStack) {
		stackCopy.push(undoStack[idx]);
	}
	//console.log('Stash Page:', page, pipeline, stackCopy);
	this.pages[page] = true;
	this.stashes[page] = {'pipeline': pipeline.clone(), 'undo': stackCopy, 'callouts': callouts.clone()};
	//this.stashes[page] = undo;
	//console.log(this.hasChanges(100), JSON.stringify(this.getChangeState(100).payload));
};

StashClass.prototype.unstash = function unstash(page) {
//	if(this.pages[page]===true) {
//		this.stashes[page].callouts.restoreDom();
//	}
	return (this.pages[page]===true) ? this.stashes[page] : {'pipeline':DrawPipelineFactory(),'undo':[],'callouts':DrawCalloutListFactory()};
};

StashClass.prototype.isStash = function isStash(page) {
	return (this.pages[page]===true);
};

StashClass.prototype.undoAll = function undoAll() {
	angular.forEach(this.stashes, function(stash, page) {
		var undoCount = stash.undo.length;
		// do the stuff to unwind this item
		for(var i=0; i<undoCount; ++i) {
			var undoObj = stash.undo.pop();
			undoObj.undo(stash.pipeline);
		}
	});
};

StashClass.prototype.hasChanges = function hasChanges(pageCount, requestedState) {
	var state = this.getChangeState(pageCount, requestedState);
	return (state.counter > 0);
};

StashClass.prototype.hasCallouts = function hasCallouts(pageCount) {
	var state = this.getChangeState(pageCount);
	return (state.callouts.length > 0);
};

StashClass.prototype.getChangeState = function getChangeState(pageCount, requestedState) {

	// Init the page count
	if(typeof pageCount === "undefined") {
		pageCount = 300;
	}

	// make sure the values are defined
	if(typeof PDF_STATE_ANNOTATE === "undefined" || PDF_STATE_ANNOTATE === null) {
		PDF_STATE_ANNOTATE = 'annotate';
		PDF_STATE_PRESENTATION = 'presentation';
		PDF_STATE_INTRODUCE = 'introduce';
	}

	// Init the requested state
	if(typeof requestedState === "undefined" || requestedState === null || requestedState === '') {
		requestedState = PDF_STATE_ANNOTATE;
	}
	// State will be comprised of a:
	// 1. payload - the json data for saving
	// 2. state - the undo objects the payload is made of.
	// Terms:
	// * Lineage: The draw object and any parent draw objects that this may have been cloned from.
	// * Draw Cache: The sum of the draw objects (including lineage draw objects) that have been used.
	var result = {payload:[], state:{}, counter:0, callouts:[]}; // we'll use result as our final returnable object comprising payload and state

	// Review each stashes undo objects
	// IMPORTANT: Rick wants these in page order, so we iterate all pages in order, and just loop if no stash is present for a page.
	//angular.forEach(this.stashes, function(stash, page) {
	for(page=0; page<pageCount; ++page) {
		stash = this.stashes[page];
		if(!stash)
			continue;

		// Setup pageObj in the format expected for save:
		// pageObj desired format: {"0":[{"op":"add","type":"pencil",...},{"op":"add","type":"marker",...}]}
		// which will allow us to do:
		//		payload.push(pageObj);
		//		::Final target format: [{"0":[{"op":"add","type":"pencil",...},{"op":"add","type":"marker",...}]},{"1":[{"op":"del","type":"note",...},{"op":"mod","type":"arrow",...}]}]
		var pageObj = {};
		pageObj[page] = [];

		// Setup pageState, which will track all undo objects that our payload is comprised of.
		var pageState = [];

		// Because we chain the draw objects together, it is possible for a lineage of draw objects
		// to be spread in multiple undo objects. Since we essentially flatten the draw object into
		// one operation, we need to make sure that older lineage draw objects aren't re-processed
		// by storying them in our cache of draw objects.
		var pageDrawCache = [];

		// we want to review the undo stack element by element. newest to oldest.
		for(var i=stash.undo.length-1; i>=0; --i) {
			// dereference the current stash undo object into variable "item": convenience
			var item = stash.undo[i];

			// Add the item to the page state regardles of whether it presents new items to our payload.
			// upon success, our pageState will be used to clean our undo stack, so we don't undo something saved.
			pageState.push(item);

			// the composition is the lineage of draw objects comprising the full scope of this undo from start to end.
			//var composition = item.getComposition();

			// the origin keeps track of the very first original draw object
			var origin = item.getOrigin();

			// test if our origin is new to us.
			if(jQuery.inArray(origin, pageDrawCache) > -1) {
				continue;
			}

			// add this page to the cache for future reference.
			pageDrawCache.push(origin);

			// we found a new undo item that needs to be processed
			var opCode = requestedState;
			if(requestedState === PDF_STATE_INTRODUCE) {
				opCode = PDF_STATE_PRESENTATION;
			}
			var operation = item.getOp(opCode);

			// test if this is an operation that is valid
			if(operation !== false) {
				// Populate page into presentation state operations
				if(requestedState === PDF_STATE_PRESENTATION) {
					operation.Page = page;
				}

				// We found an undo item that needs to be added to page.
				if(requestedState === PDF_STATE_PRESENTATION) {
					var DrawObject = item.getDrawObject();
					operation.zIndex = stash.pipeline.pipeline.indexOf(DrawObject);
					pageObj[page].unshift(operation);
				} else {
					pageObj[page].unshift(operation);
				}
				++result.counter;
			}
		}

		// Add the appropriate page state to the page index of state.
		result.state[page] = pageState;

		if(requestedState !== PDF_STATE_INTRODUCE) {
			// Add the callouts to the result.callouts
			var arrayCallouts = [];
			for(var j in stash.callouts.callouts) {
				var DrawCallout = stash.callouts.callouts[j];
				if(!DrawCallout.isDestroyed()) {
					if(DrawCallout.isDirty()) {
						++result.counter;
					}
					var json = DrawCallout.composeAddJson();
					result.callouts.push(json);
					arrayCallouts.push(json);
				} else {
					if(DrawCallout.getOrigin() === 'disk') {
						++result.counter;
					}
				}
			}

			if(arrayCallouts.length > 0) {
				pageObj[page].push({"op":"add", "type":"callout", "data":arrayCallouts});
			}
		}

		// Add the appropriate page object to the payload for json
		if(pageObj[page].length > 0) {
			result.payload.push(pageObj);
		}
	}//);

	// Return the result to the calling method.
	return result;
};

StashClass.prototype.applyState = function applyState(pageCount) {
	for(page=0; page<pageCount; ++page) {
		stash = this.stashes[page];
		if(stash && stash.undo)
			stash.undo = []; // clear the stack
		if(stash && stash.callouts) {
			for(var j in stash.callouts.callouts) {
				var DrawCallout = stash.callouts.callouts[j];
				DrawCallout.setDirty(false);
				if(DrawCallout.isDestroyed()) {
					DrawCallout.setOrigin('draw');
				}
			}
		}
	}
};

StashClass.prototype.toString = function toString() {
	return 'UndoStash';
};
