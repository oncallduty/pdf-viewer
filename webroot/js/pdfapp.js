/* global angular,PDFViewerFactory */
window.document.domain = "edepoze.com";

// polyfill
if( !window.hasOwnProperty( "isset" ) || typeof window.isset !== "function" ) (function() {
	"use strict";
	/**
	 * is set
	 * @param {*} v
	 * @returns {boolean}
	 */
	function isset( v ) {
		return (typeof v !== "undefined" && v !== null);
	}
	window.isset = isset;
} )();

(function () {
	var pdfapp = angular.module( "pdfapp", ["ui.router","webStorageModule","ngTouch"] );

	pdfapp.factory( "PDFViewer", ["$rootScope", "$http", "$timeout", "$q", PDFViewerFactory] );

	pdfapp.directive( 'showFocus', ['$timeout', function( $timeout ) {
		return {
			"restrict": "A",
			"link": function( scope, element, attrs ) {
				scope.$watch( attrs.showFocus, function( willShow ) {
					$timeout( function() {
						willShow && element[0].focus();
					} );
				} );
			}
		};
	}] );

	pdfapp.directive( 'bindLoad', ['$parse', function( $parse ) {
		return {
			"restrict": 'A',
			"link": function( scope, elem, attrs ) {
				var fn = $parse( attrs.bindLoad );
				elem.on( 'error', function( event ) {
					console.log( 'error!', event );
					scope.$apply( function() {
						fn( scope, {$event: event} );
					} );
				} );
				elem.on( 'load', function( event ) {
					scope.$apply( function() {
						fn( scope, {$event: event} );
					} );
				} );
			}
		};
	}] );

	pdfapp.directive( 'arrowKeys', [function() {
		return {
			"restrict": "A",
			"link": function( scope, elem, attrs ) {
				elem.bind( "keydown", function( event ) {
					event = event || window.event;
					var keyCode = "keyCode" in event ? event.keyCode : event.which;
					if( keyCode === 37 ) {
						scope.$root.$emit( 'ArrowLeft', event );
						if(typeof scope.$root.arrowKeyLeft === "function") {
							scope.$root.arrowKeyLeft(event);
						}
					} else if( keyCode === 39 ) {
						scope.$root.$emit( 'ArrowRight', event );
						if(typeof scope.$root.arrowKeyRight === "function") {
							scope.$root.arrowKeyRight(event);
						}
					} else if( keyCode === 38 ) {
						if(typeof scope.$root.arrowKeyUp === "function") {
							scope.$root.arrowKeyUp(event);
						}
					} else if( keyCode === 40 ) {
						if(typeof scope.$root.arrowKeyDown === "function") {
							scope.$root.arrowKeyDown(event);
						}
					} else if( keyCode === 33 ) {
						if(typeof scope.$root.arrowPgUp === "function") {
							scope.$root.arrowPgUp(event);
						}
					} else if( keyCode === 34 ) {
						if(typeof scope.$root.arrowPgDown === "function") {
							scope.$root.arrowPgDown(event);
						}
					}
				} );
			}
		};
	}] );

	pdfapp.directive("scroll", function ($window,$timeout) {
		return function(scope, element, attrs) {
			angular.element($window).bind("scroll", function() {
				var handler = this;
				var x = this.pageXOffset;
				var y = this.pageYOffset;
				if(this.promise !== undefined) {
					$timeout.cancel(this.promise);
				}
				this.promise = $timeout(function(){
					if(x===this.pageXOffset && y===this.pageYOffset) {
						// fire off page message
						scope.PDFViewer.presenterPageUpdate();
					}
					delete handler.promise;
				},400);
				scope.$apply();
			});
		};
	});

	pdfapp.directive('date', function() {
		return {
			restrict: 'A',
			require: '^ngModel',
			link: function(scope, elm, attrs, ctrl) {
				var dp = $(elm);
				dp.datepicker({
					constrainInput: true,
					onSelect: function(dateText) {
						scope.$apply(function() { // Apply cause it is outside angularjs
							ctrl.$setViewValue(dateText); // Set new value of datepicker to scope
						});
					}
				});
				scope.$watch(attrs.ngModel, function(nv) {
					dp.datepicker('setDate', nv); // Update datePicker date when scope change
				});
				scope.$watch(attrs.date, function(ops) {
					if(typeof ops === "undefined" || ops === null) {
						return;
					}
					if(typeof ops.minDate !== "undefined" && ops.minDate !== null) {
						if(ops.minDate.substr(4,1)==="-" && ops.minDate.substr(7,1)==="-") {
							ops.minDate = ops.minDate.substr(5,2) + "/" + ops.minDate.substr(8,2) + "/" + ops.minDate.substr(0,4);
						}
					}
					dp.datepicker('option', ops);
				});
			}
		};
	});

	pdfapp.directive("ngTouchstart", function () {
		return {
			controller: ["$scope", "$element", function ($scope, $element) {
				$element.bind("touchstart", onTouchStart);
				function onTouchStart(event) {
					//event.preventDefault();
					//console.log("ngTouchstart", event.originalEvent.touches[0].clientX, event.originalEvent.touches[0].clientY);
					var method = $element.attr("ng-touchstart");
					$scope.$event = event;
					$scope.$apply(method);
				}
			}]
		};
	});

	pdfapp.directive("ngTouchmove", function () {
		return {
			controller: ["$scope", "$element", function ($scope, $element) {
				$element.bind("touchstart", onTouchStart);
				function onTouchStart(event) {
					//event.preventDefault();
					//console.log("ngTouchstart", event.originalEvent.touches[0].clientX, event.originalEvent.touches[0].clientY);
					$element.bind("touchmove", onTouchMove);
					$element.bind("touchend", onTouchEnd);
				}
				function onTouchMove(event) {
					//event.preventDefault();
					//console.log("ngTouchmove", event.originalEvent.touches[0].clientX, event.originalEvent.touches[0].clientY);
					var method = $element.attr("ng-touchmove");
					$scope.$event = event;
					$scope.$apply(method);
				}
				function onTouchEnd(event) {
					//event.preventDefault();
					//console.log("nbTouchend", event.originalEvent.touches[0].clientX, event.originalEvent.touches[0].clientY);
					$element.unbind("touchmove", onTouchMove);
					$element.unbind("touchend", onTouchEnd);
				}
			}]
		};
	});

	pdfapp.directive("ngTouchend", function () {
		return {
			controller: ["$scope", "$element", function ($scope, $element) {
				$element.bind("touchend", onTouchEnd);
				function onTouchEnd(event) {
					//event.preventDefault();
					//console.log("nbTouchend", event);
					var method = $element.attr("ng-touchend");
					$scope.$event = event;
					$scope.$apply(method);
				}
			}]
		};
	});

	pdfapp.directive("ngDragdrop", function () {
		return {
			controller: ["$scope", "$element", function ($scope, $element) {
				$element.bind("drop", onDragdrop);
				function onDragdrop(event) {
					var method = $element.attr("ng-dragdrop");
					$scope.$event = event;
					$scope.$apply(method);
				}
			}]
		};
	});

	pdfapp.directive("ngDragover", function () {
		return {
			controller: ["$scope", "$element", function ($scope, $element) {
				$element.bind("dragover", onDragover);
				function onDragover(event) {
					var method = $element.attr("ng-dragover");
					$scope.$event = event;
					$scope.$apply(method);
				}
			}]
		};
	});

	pdfapp.directive('ngRepeatFinish', ['$timeout', '$parse', function ($timeout, $parse) {
		return {
			restrict: 'A',
			link: function (scope, element, attr) {
				var forExec = attr.ngRepeatFinish;
				var fn = $parse( forExec );
				if (scope.$last === true) {
					$timeout(fn);
				}
			}
		};
	}]);

	pdfapp.controller( "ViewerCtrl", ["$scope", "$state", "PDFViewer", function ViewerCtrl( $scope, $state, PDFViewer ) {
		console.log( "ViewerCtrl; state:", $state.current.name, $state.current.url );
		var Page = PDFViewer.Page;
		$scope.PDFViewer = PDFViewer;
		$scope.Page = Page;
		$scope.thumbnailsVisible = false;
		$scope.me = function(){return 'ViewerCtrl';};

		if( angular.isDefined( $scope._docInfo ) ) {
			for(var idx in $scope._docInfo.pages) {
				if(!PDFViewer.pages[idx])
					PDFViewer.pages[idx] = {};
				PDFViewer.pages[idx].size = $scope._docInfo.pages[idx];
				PDFViewer.pages[idx].annotations = [];
			}
			PDFViewer.fileHash = (typeof $scope._docInfo.fileHash === "string" ? $scope._docInfo.fileHash : "err");
			Page.size = PDFViewer.pages[0].size;
			Page.zoomScale = (Page.bounds.width / Page.size.width);
			Page.scale.width = Math.round( (Page.size.width * Page.zoomScale ) * 10) / 10;
			Page.scale.height = Math.round( (Page.size.height * Page.zoomScale) * 10 ) / 10;

			if( !Page.imagePath ) {
				PDFViewer.selectPage( Page.number );
			}
		}
	}] );

	pdfapp.controller( "PageCtrl", ["$scope", "$state", "$timeout", "PDFViewer", function PageCtrl( $scope, $state, $timeout, PDFViewer ) {
		console.log( "PageCtrl; state:", $state.current.name, $state.current.url );
		var Page = PDFViewer.Page;
		var Toolbar = PDFViewer.Toolbar;
		var Canvas = PDFViewer.Canvas;
		var Stamp = PDFViewer.Stamp;
		$scope.PDFViewer = PDFViewer;
		$scope.Page = Page;
		$scope.Stamp = Stamp;
		$scope.Search = {"keywords":""};
		$scope.showSearchToolbar = false; // do we need this?
		$scope.drawTool = '';
		$scope.me = function(){return 'PageCtrl';};
		$scope.Math = window.Math;

		if( !angular.isDefined( $scope.initialized ) ) {
			$scope.showPageNumDialog = false;
			Page.canvasStyle = Page.spinnerStyle();
			Page.pageStyle.width = Page.bounds.width;
			Page.pageStyle.height = Page.bounds.height;
			Page.zoomMode();
			if( $state.current.name === "Page" || $state.current.name === "Thumbnails" ) {
				PDFViewer.ignoreArrowKeys = false;
			}

			$scope.$root.arrowKeyLeft = function arrowKeyLeft( event ) {
				if( PDFViewer.annotations.mode === 'presentation') {
					event.preventDefault();
					PDFViewer.safeApply();
				}
				if( !PDFViewer.ignoreArrowKeys ) {
					PDFViewer.previousPage();
					event.preventDefault();
					PDFViewer.safeApply();
				}
			};

			$scope.$root.arrowKeyRight = function arrowKeyRight( event ) {
				if( PDFViewer.annotations.mode === 'presentation') {
					event.preventDefault();
					PDFViewer.safeApply();
				}
				if( !PDFViewer.ignoreArrowKeys ) {
					PDFViewer.nextPage();
					event.preventDefault();
					PDFViewer.safeApply();
				}
			};

			$scope.$root.arrowKeyUp = function arrowKeyUp( event ) {
				if( PDFViewer.ignoreArrowKeys ) {
					event.preventDefault();
					PDFViewer.safeApply();
				}
			};

			$scope.$root.arrowKeyDown = function arrowKeyDown( event ) {
				if( PDFViewer.ignoreArrowKeys ) {
					event.preventDefault();
					PDFViewer.safeApply();
				}
			};

			$scope.$root.arrowPgUp = function arrowPgUp( event ) {
				if( PDFViewer.ignoreArrowKeys ) {
					event.preventDefault();
					PDFViewer.safeApply();
				}
			};

			$scope.$root.arrowPgDown = function arrowPgDown( event ) {
				if( PDFViewer.ignoreArrowKeys ) {
					event.preventDefault();
					PDFViewer.safeApply();
				}
			};

			$scope.initialized = true;
		}

		//Page.calcPageScale();

		$scope.didLoad = PDFViewer.didLoad = function didLoad( event ) {
//			console.log( "PageCtrl; didLoad:", event );
			if( angular.isDefined( event.type ) && event.type === "error" ) {
				window.location.replace( window.location.href );
				return;
			}
			//console.log("   QQ", "*DidLoad*");
			PDFViewer.didComplete(Page.imagePath);
		};

		$scope.toggle = function toggle() {
			$scope.showPageNumDialog = !$scope.showPageNumDialog;
			PDFViewer.ignoreArrowKeys = !!$scope.showPageNumDialog;
			$scope.pageNumDialogError = false;
		};

		$scope.selectPage = function selectPage() {
			if( (Page.jumpToPage * 1) === (Page.number * 1) ) {
				$scope.toggle();
				Page.jumpToPage = "";
				return;
			}
			if( PDFViewer.selectPage( Page.jumpToPage ) ) {
				if( $scope.showPageNumDialog ) {
					$scope.toggle();
				}
				Page.jumpToPage = "";
				$scope.pageNumDialogError = false;
				$('#pgNum .dialog').css({'height':220});
			} else {
				$scope.pageNumDialogError = true;
				$('#pgNum .dialog').css({'height':240});
			}
		};
		$scope.pageNumDialogError = false;

		$scope.previousPage = function previousPage( $event ) {
			PDFViewer.previousPage();
			blurTarget( $event );
		};

		$scope.nextPage = function nextPage( $event ) {
			PDFViewer.nextPage();
			blurTarget( $event );
		};

		$scope.swipePreviousPage = function swipePreviousPage(event) {
			event.preventDefault();
			if(!$scope.toolbar.drawToolSelected() && !$scope.toolbar.modifyToolSelected() && Page.bounds.width >= Page.scale.width) {
				$scope.previousPage(event);
			}
		};

		$scope.swipeNextPage = function swipeNextPage(event) {
			event.preventDefault();
			if(!$scope.toolbar.drawToolSelected() && !$scope.toolbar.modifyToolSelected() && Page.bounds.width >= Page.scale.width) {
				$scope.nextPage(event);
			}
		};

		$scope.zoomOut = function zoomOut( $event ) {
			Page.zoomOut();
			$scope.toolbar.refresh();
			blurTarget( $event );
		};

		$scope.zoomIn = function zoomIn( $event ) {
			Page.zoomIn();
			$scope.toolbar.refresh();
			blurTarget( $event );
		};

		$scope.selectScale = function selectScale( $event ) {
			Page.selectScale();
			$scope.toolbar.refresh();
			blurTarget( $event );
		};

		$scope.ignoreArrowKeys = function ignoreArrowKeys( ignore ) {
			PDFViewer.ignoreArrowKeys = !!(ignore);
			//console.log( "Ignore Arrow Keys:", PDFViewer.ignoreArrowKeys );
		};

		$scope.portraitOrientation = function portraitOrientation( $event ) {
			Page.portraitOrientation();
			//console.log( "$scope.portraitOrientation; isPortrait:", this.isPortrait, "isLandscape:", this.isLandscape );
			if( isset( window.top.webApp ) ) {
				window.top.webApp.setItem( 'stickyOrientation', "portrait" );
			}
			blurTarget( $event );
		};

		$scope.landscapeOrientation = function landscapeOrientation( $event ) {
			Page.landscapeOrientation();
			//console.log( "$scope.landscapeOrientation; isPortrait:", this.isPortrait, "isLandscape:", this.isLandscape );
			if( isset( window.top.webApp ) ) {
				window.top.webApp.setItem( 'stickyOrientation', "landscape" );
			}
			blurTarget( $event );
		};

		$scope.getShowOrientation = function getShowOrientation() {
			return (["presenter","presentation"].indexOf( PDFViewer.annotations.mode ) === -1);
		};


		//**************************************************
		// DRAW CANVAS SETUP
		//PDFViewer.Canvas = PDFViewer.Canvas;
		$scope.counterTemp = 0;
		$scope.$on('ToolChange',function ToolChange(event) {
			//console.log('ToolChange', $state.current.url, $scope.toolbar.getTool(), PDFViewer.annotations.mode);
			var tool = $scope.toolbar.getTool();
			switch(tool) {
				case $scope.toolbar.draw.arrow.toolCode:
					PDFViewer.Canvas.events.mouse.handlers = [];
					if(PDFViewer.annotations.mode === 'draw' || PDFViewer.annotations.mode === 'presenter') {
						PDFViewer.Canvas.events.mouse.handlers.push($scope.toolbar.draw.arrow.mouseHandler);
						PDFViewer.annotations.callouts.setDrawReady(true);
					}
					break;
				case $scope.toolbar.draw.highlight.toolCode:
					PDFViewer.Canvas.events.mouse.handlers = [];
					if(PDFViewer.annotations.mode === 'draw' || PDFViewer.annotations.mode === 'presenter' || PDFViewer.annotations.mode === 'annotator') {
						PDFViewer.Canvas.events.mouse.handlers.push($scope.toolbar.draw.highlight.mouseHandler);
						PDFViewer.annotations.callouts.setDrawReady(true);
					}
					break;
				case $scope.toolbar.draw.pencil.toolCode:
					PDFViewer.Canvas.events.mouse.handlers = [];
					if(PDFViewer.annotations.mode === 'draw' || PDFViewer.annotations.mode === 'presenter' || PDFViewer.annotations.mode === 'annotator') {
						PDFViewer.Canvas.events.mouse.handlers.push($scope.toolbar.draw.pencil.mouseHandler);
						PDFViewer.annotations.callouts.setDrawReady(true);
					}
					break;
				case $scope.toolbar.draw.note.toolCode:
					PDFViewer.Canvas.events.mouse.handlers = [];
					if(PDFViewer.annotations.mode === 'draw' || PDFViewer.annotations.mode === 'presenter') {
						PDFViewer.Canvas.events.mouse.handlers.push($scope.toolbar.draw.note.mouseDrawHandler);
						PDFViewer.annotations.callouts.setDrawReady(true);
					}
					break;
				case $scope.toolbar.draw.modify.toolCode:
					PDFViewer.Canvas.events.mouse.handlers = [];
					PDFViewer.annotations.callouts.setDrawReady(true);
					if(PDFViewer.annotations.mode === 'draw' || PDFViewer.annotations.mode === 'presenter' || PDFViewer.annotations.mode === 'annotator') {
						PDFViewer.Canvas.events.mouse.handlers.push($scope.toolbar.draw.modify.mouseMoveHandler);
					}
					break;
				case $scope.toolbar.draw.erase.toolCode:
					PDFViewer.Canvas.events.mouse.handlers = [];
					if(PDFViewer.annotations.mode === 'draw' || PDFViewer.annotations.mode === 'presenter') {
						PDFViewer.Canvas.events.mouse.handlers.push($scope.toolbar.draw.erase.mouseHandler);
						PDFViewer.annotations.callouts.setDrawReady(true);
					}
					break;
				case $scope.toolbar.draw.stamp.toolCode:
					PDFViewer.Canvas.events.mouse.handlers = [];
					PDFViewer.annotations.callouts.setDrawReady(false);
					if(PDFViewer.annotations.mode === 'stamp') {
						PDFViewer.Canvas.events.mouse.handlers.push(Stamp.mouseHandler);
					}
					break;
				case $scope.toolbar.draw.callout.toolCode:
					PDFViewer.Canvas.events.mouse.handlers = [];
					PDFViewer.annotations.callouts.setDrawReady(false);
					if(PDFViewer.annotations.mode === 'draw' || PDFViewer.annotations.mode === 'presenter' || PDFViewer.annotations.mode === 'annotator') {
						PDFViewer.Canvas.events.mouse.handlers.push($scope.toolbar.draw.callout.mouseHandler);
					}
					break;
				case $scope.toolbar.startupTool:
				case $scope.toolbar.noneTool:
					PDFViewer.Canvas.events.mouse.handlers = [];
					PDFViewer.annotations.callouts.setDrawReady(false);
					if(PDFViewer.annotations.mode === 'draw' || PDFViewer.annotations.mode === 'presenter') { // No Annotator Select
					//if(PDFViewer.annotations.mode === 'draw' || PDFViewer.annotations.mode === 'presenter' || PDFViewer.annotations.mode === 'annotator') { // Annotator Select Mine
						PDFViewer.Canvas.events.mouse.handlers.push($scope.toolbar.draw.modify.mouseSelectHandler);
					} else {
						PDFViewer.Canvas.events.mouse.handlers = [];
					}
					break;
				case $scope.toolbar.presentationTool:
					PDFViewer.Canvas.events.mouse.handlers = [];
					PDFViewer.annotations.callouts.setDrawReady(false);
					break;
			}
		});
		$scope.$root.$on('Redraw',function Redraw(event) {
			PDFViewer.Canvas.refresh();
		});


		//**************************************************
		// TOOLBAR SETUP
		$scope.toolbar = PDFViewer.Toolbar;
		$scope.toolbar.noneTool = 'none';
		$scope.toolbar.presentationTool = 'presentation';
		$scope.toolbar.startupTool = 'startup';
		$scope.toolbar.activeTool = $scope.toolbar.startupTool;
		$scope.toolbar.modal = false;
		$scope.toolbar.setTool = function setTool(tool, active) {
			if(!$scope.toolbar.modal) {
				if(!active && tool === $scope.toolbar.activeTool) {
					$scope.toolbar.activeTool = $scope.toolbar.noneTool;
				}
				else if(active && tool !== $scope.toolbar.activeTool) {
					$scope.toolbar.activeTool = tool;
				}
				$scope.$emit('ToolChange', $scope.toolbar.activeTool);
			}
		};
		$scope.toolbar.setModalTool = function setModalTool(tool, active) {
			if(!$scope.toolbar.modal || ($scope.toolbar.modal && tool === $scope.toolbar.activeTool)) {
				$scope.toolbar.setTool(tool, active);
				$scope.toolbar.modal = active;
			}
		};
		$scope.toolbar.getTool = function getTool() {
			return $scope.toolbar.activeTool;
		};
		$scope.toolbar.hideAll = function hideAll() {
			$scope.toolbar.setTool($scope.toolbar.presentationTool, true);
		};
		$scope.toolbar.refresh = function refresh() {
			$scope.toolbar.draw.refresh();
		};
		$scope.toolbar.drawToolSelected = function drawToolSelected() {
			if($scope.toolbar.activeTool === $scope.toolbar.draw.arrow.toolCode
				|| $scope.toolbar.activeTool === $scope.toolbar.draw.highlight.toolCode
				|| $scope.toolbar.activeTool === $scope.toolbar.draw.pencil.toolCode
				|| $scope.toolbar.activeTool === $scope.toolbar.draw.note.toolCode
				|| $scope.toolbar.activeTool === $scope.toolbar.draw.callout.toolCode)
				return true;
			return false;
		};
		$scope.toolbar.modifyToolSelected = function modifyToolSelected() {
			if($scope.toolbar.activeTool === $scope.toolbar.draw.modify.toolCode
				|| $scope.toolbar.activeTool === $scope.toolbar.draw.erase.toolCode)
				return true;
			return false;
		};
		$scope.toolbar.noneToolSelected = function noneToolSelected() {
			if($scope.toolbar.activeTool === $scope.toolbar.noneTool
				|| $scope.toolbar.activeTool === $scope.toolbar.startupTool)
				return true;
			return false;
		};
		$scope.toolbar.resetTools = function resetTools() {
			$scope.toolbar.setTool($scope.toolbar.noneTool, true);
		};



		//**************************************************
		// PAGE TOOLBAR SETUP
		$scope.toolbar.page = {};
		$scope.toolbar.page.showNavBtns = true;


		//**************************************************
		// ROTATE TOOLBAR SETUP
		$scope.toolbar.rotate = {};
		$scope.toolbar.rotate.orientation = 0;
		$scope.toolbar.rotate.orientationStyle = {};
		$scope.$root.$on( "PageChange", function PageChange() {
			$scope.toolbar.rotate.reset();
		});
		$scope.toolbar.rotate.reset = function reset() {
			$scope.toolbar.rotate.orientation = 0;
			Page.orientation.reset();
		};
		$scope.toolbar.rotate.right = function right($event) {
			$scope.toolbar.rotate.orientation = Page.orientation.right();
			$scope.toolbar.refresh();
			blurTarget( $event );
		};
		$scope.toolbar.rotate.left = function left($event) {
			$scope.toolbar.rotate.orientation = Page.orientation.left();
			$scope.toolbar.refresh();
			blurTarget( $event );
		};


		//**************************************************
		// SEARCH TOOLBAR SETUP
		$scope.toolbar.search = {};
		$scope.toolbar.search.toolCode = 'search';
		$scope.toolbar.search.show = false;
		$scope.toolbar.search.showStyle = {};
		$scope.toolbar.search.keywords = '';
		$scope.toolbar.search.isDoneBtn = true;
		$scope.toolbar.search.isActive = false;

		$scope.toolbar.search.toggle = function toggle() {
			$scope.toolbar.search.show = !$scope.toolbar.search.show;
			$scope.toolbar.search.showStyle = $scope.toolbar.search.show ? {'background-color':'#eee'} : {'background-color':'transparent'};
			$scope.toolbar.setTool($scope.toolbar.search.toolCode, $scope.toolbar.search.show);
			$scope.toolbar.search.keywords = '';
			PDFViewer.clearSearch();
		};
		$scope.toolbar.search.hide = function hide(event) {
			$scope.toolbar.search.show = false;
			$scope.toolbar.search.showStyle = {'background-color':'transparent'};
			$scope.toolbar.setTool($scope.toolbar.search.toolCode, $scope.toolbar.search.show);
			$scope.toolbar.search.keywords = '';
			PDFViewer.clearSearch();
		};
		$scope.toolbar.search.next = function next($event) {
			var search = $scope.toolbar.search.searchParseString($scope.toolbar.search.keywords);
			if( PDFViewer.isSearchSame(search) || PDFViewer.isSearchEmpty(search) ) {
				PDFViewer.nextSearch();
			} else {
				PDFViewer.search( search, true );
			}
			blurTarget( $event );
		};
		$scope.toolbar.search.previous = function previous($event) {
			var search = $scope.toolbar.search.searchParseString($scope.toolbar.search.keywords);
			if( PDFViewer.isSearchSame(search) || PDFViewer.isSearchEmpty(search) ) {
				PDFViewer.prevSearch();
			} else {
				PDFViewer.search( search, true );
			}
			blurTarget( $event );
		};
		$scope.toolbar.search.position = function position() {
			return (PDFViewer.searchResults.countBase + PDFViewer.searchResults.cursor);
		};
		$scope.toolbar.search.matches = function matches() {
			return PDFViewer.searchResults.documentHits;
		};
		$scope.toolbar.search.results = function results() {
			PDFViewer.showSearchResults();
		};
		$scope.toolbar.search.keyup = function keyup($event) {
			if( $event && angular.isDefined( $event.keyCode ) && $event.keyCode === 13 ) {
				PDFViewer.search( [$scope.toolbar.search.keywords], true );
			}
		};
		$scope.toolbar.search.searchParseString = function searchParseString( termsString ) {
			var origString = termsString;
			var keywordRegex = /("[^"]+"|[\w]+)/gi;
			var nextExec = [];
			var termsArray = [];
			while( (nextExec = keywordRegex.exec( termsString )) ) {
				console.log("find:", nextExec, nextExec[0], nextExec[0].replace(/"/g,''));
				termsArray.push(nextExec[0].replace(/"/g,''));
			}
			return termsArray;
		};


		//**************************************************
		// DRAW SELECTION TOOLBAR SETUP
		$scope.toolbar.draw = {};
		//$scope.toolbar.draw.enable = false;
		$scope.toolbar.draw.colorArray = [ "#FF0000", "#00FF00", "#0080FF", "#FF00FF", "#FCB643", "#FFFF00", "#000000", "#8000FF" ];
		$scope.toolbar.draw.colorMarkerArray = [ "#FF0000", "#00FF00", "#0080FF", "#FF00FF", "#FCB643", "#FFFF00", "#00FFFF", "#8000FF" ];
		$scope.toolbar.draw.clear = function clear() {
			PDFViewer.annotations.clearPage();
			PDFViewer.Canvas.refresh();
			$scope.toolbar.draw.clearConfirm = false;
			PDFViewer.triggerViewerAPIPresenter('ClearAnnotations',{});
		};
		$scope.toolbar.draw.clearPrompt = function clearPrompt() {
			$scope.toolbar.draw.clearConfirm = true;
		};
		$scope.toolbar.draw.save = function save() {
			console.log('ChangeState:', PDFViewer.annotations.getChangeState());
		};
		$scope.toolbar.draw.reset = function reset(){
			console.log('ResetState:', PDFViewer.annotations.getChangeState());
		};
		$scope.toolbar.draw.scroll = function scroll(scrollable){
			if(!!scrollable) {
				$('body').css('overflow', 'auto');
			}
			else {
				$('body').css('overflow', 'hidden');
			}
		};
		$scope.toolbar.draw.refresh = function refresh() {
			if(typeof $scope.toolbar.draw.modify.refreshShowFn === "function") {
				$.when($scope.toolbar.draw.modify.refreshShowFn()).done(function(start){
					PDFViewer.scrollTo(((start.left-100)/1.5), ((start.top-100)/1.5));
				});
			}
		};
		$scope.toolbar.draw.modify = {};
		$scope.toolbar.draw.modify.mode = false;
		$scope.toolbar.draw.modify.toolCode = 'draw-modify';
		$scope.toolbar.draw.modify.showMenu = false;
		$scope.toolbar.draw.modify.DrawObject = null;
		$scope.toolbar.draw.modify.DrawObjectOrigin = [0, 0];
		$scope.toolbar.draw.modify.modeStyle = {};
		$scope.toolbar.draw.modify.positionMenu = function positionMenu() {
			//reposition menu
		};
		$scope.toolbar.draw.modify.setMode = function setMode(mode) {
			$scope.toolbar.setTool($scope.toolbar.draw.modify.toolCode, mode);
		};
		$scope.toolbar.draw.modify.setDrawObject = function setDrawObject(DrawObject) {
			$scope.toolbar.draw.modify.DrawObject = DrawObject;
			$scope.toolbar.draw.modify.menu.category = DrawObject.getCategory();
			$scope.toolbar.draw.modify.setMode(true);
		};
		$scope.toolbar.draw.modify.calcMenuPos = function calcMenuPos(modifyId) {
			//console.log('Modify::calcMenuPos()::Orientation', Page.orientation.getOrientation());
			if(typeof $scope.toolbar.draw.modify.DrawObject === "undefined" || $scope.toolbar.draw.modify.DrawObject === null) {
				return {top: -400, left: -400};
			}
			var boundingBox = $scope.toolbar.draw.modify.DrawObject.getBoundingBox();
			var bbleft = boundingBox[0][0] * Page.zoomScale;
			var bbright = boundingBox[1][0] * Page.zoomScale;
			//var bbcenter = ((bbleft+bbright)>>1);
			var bbtop = boundingBox[0][1] * Page.zoomScale;
			var bbbottom = boundingBox[1][1] * Page.zoomScale;
			var temp = 0;
			var canvasWidth = $('#pdf-page-canvas').width();
			var canvasHeight = $('#pdf-page-canvas').height();
			switch(Page.orientation.getOrientation()) {
				case 1:
					canvasWidth = $('#pdf-page-canvas').height();
					canvasHeight = $('#pdf-page-canvas').width();
					temp = bbtop;
					bbtop = bbleft;
					bbleft = canvasWidth-bbbottom;
					bbbottom = bbright;
					bbright = canvasWidth-temp;
					break;
				case 2:
					temp = canvasHeight-bbtop;
					bbtop = canvasHeight-bbbottom;
					bbbottom = temp;
					temp = canvasWidth-bbleft;
					bbleft = canvasWidth-bbright;
					bbright = temp;
					//bbcenter = ((bbleft+bbright)>>1);
					break;
				case 3:
					canvasWidth = $('#pdf-page-canvas').height();
					canvasHeight = $('#pdf-page-canvas').width();
					temp = bbtop;
					bbtop = canvasHeight-bbright;
					bbright = bbbottom;
					bbbottom = canvasHeight-bbleft;
					bbleft = temp;
					//bbcenter = ((bbleft+bbright)>>1);
					break;
				case 0:
				default:
					// do nothing
					break;
			}
			var bbmiddle = (bbtop+bbbottom)>>1;
			var bbcenter = (bbleft+bbright)>>1;
			//var bodyScrollY = $('body').scrollTop();
			//var bodyScrollX = $('body').scrollLeft();
			var docScrollX = $(document).scrollLeft(); // better than the body scroll above
			var docScrollY = $(document).scrollTop(); // better than the body scroll above
			//console.log('Document Scroll X:', docScrollX);
			//console.log('Document Scroll Y:', docScrollY);
			var pageScrollY = $('#'+PDFViewer.Canvas.idPage).scrollTop();
			var pageScrollX = $('#'+PDFViewer.Canvas.idPage).scrollLeft();
			var position = $('#'+PDFViewer.Canvas.idCanvas).position();
			var offset = $('#'+PDFViewer.Canvas.idCanvas).offset();
			//console.log('POSITION', position, offset, [pageScrollX, pageScrollY], [bodyScrollX, bodyScrollY]);
			var menuWidth = $('#'+modifyId).width();
			var menuHeight = $('#'+modifyId).height();
			var left = bbcenter-(menuWidth>>1);
			var top = bbtop - (menuHeight + 25);
			if(top+offset.top < 100) {
				top = bbbottom + 25;
			}
			if(left+menuWidth > canvasWidth) {
				left -= (left+menuWidth)-canvasWidth;
			}
			if(top < 0) {
				top = 0;
			}
			if(left < 0) {
				left = 0;
			}
			//console.log('Modify::calcMenuPos()::X', 'left', left, 'offset.left', offset.left, 'pageScrollX', pageScrollX, 'bodyScrollX', bodyScrollX);
			//console.log('Modify::calcMenuPos()::Y', 'top', top, 'offset.top', offset.top, 'pageScrollY', pageScrollY, 'bodyScrollY', bodyScrollY);
			return {top: top+offset.top+pageScrollY, left: left+offset.left+pageScrollX};
		};
		$scope.toolbar.draw.modify.refreshShowFn = null;
		$scope.toolbar.draw.modify.menu = {};
		$scope.toolbar.draw.modify.menu.category = 'none';
		$scope.toolbar.draw.modify.menu.style = {'display':'none','top':0,'left':0,'position':'absolute','zIndex':100};
		$scope.toolbar.draw.modify.menu.show = function show() {
			var dfd = jQuery.Deferred();
			$scope.toolbar.draw.modify.refreshShowFn = $scope.toolbar.draw.modify.menu.show;
			$scope.toolbar.draw.modify.menu.style.display = 'block';
			$scope.toolbar.draw.modify.menu.style.top = -1000; //start off screen for calculation of size
			$scope.toolbar.draw.modify.menu.style.left = -1000;
			$timeout( function() {
				var start = $scope.toolbar.draw.modify.calcMenuPos('modifyMainMenu');
				$scope.toolbar.draw.modify.menu.style.top = start.top; //PDFViewer.Canvas.coord.eventY;
				$scope.toolbar.draw.modify.menu.style.left = start.left; //PDFViewer.Canvas.coord.eventX;
				dfd.resolve(start);
			}, 100);
			return dfd;
		};
		$scope.toolbar.draw.modify.menu.hide = function hide() {
			$scope.toolbar.draw.modify.refreshShowFn = null;
			$scope.toolbar.draw.modify.menu.style.display = 'none';
		};
		$scope.toolbar.draw.modify.menu.hideAll = function hideAll() {
			$scope.toolbar.draw.modify.menu.hide();
			$scope.toolbar.draw.modify.colorMenu.hide();
			$scope.toolbar.draw.modify.thicknessMenu.hide();
			$scope.toolbar.draw.modify.opacityMenu.hide();
		};
		$scope.toolbar.draw.modify.menu.click = function click(event, button) {
			switch(button) {
				case 'color':
					$scope.toolbar.draw.modify.menu.hide();
					$scope.toolbar.draw.modify.colorMenu.show();
					break;
				case 'thickness':
					$scope.toolbar.draw.modify.menu.hide();
					$scope.toolbar.draw.modify.thicknessMenu.show();
					break;
				case 'opacity':
					$scope.toolbar.draw.modify.menu.hide();
					$scope.toolbar.draw.modify.opacityMenu.show();
					break;
				case 'note':
					$scope.toolbar.draw.modify.menu.hide();
					break;
				case 'delete':
					$scope.toolbar.draw.erase.eraseDrawObject($scope.toolbar.draw.modify.DrawObject);
					$scope.toolbar.draw.modify.menu.hide();
					$scope.toolbar.draw.modify.setMode(false);
					break;
			}
			if(typeof PDFViewer.Toolbar.activityCallback === "function") {
				PDFViewer.Toolbar.activityCallback(true);
				return true;
			}
		};
		$scope.toolbar.draw.modify.menu.clickEditNote = function clickEditNote(event) {
			$scope.toolbar.draw.modify.menu.hideAll();
			$scope.toolbar.draw.note.setDrawObject($scope.toolbar.draw.modify.DrawObject);
		};
		$scope.toolbar.draw.modify.colorMenu = {};
		$scope.toolbar.draw.modify.colorMenu.current = '#000000';
		$scope.toolbar.draw.modify.colorMenu.colorArray =  [ "#FF0000", "#00FF00", "#0080FF", "#FF00FF", "#FCB643", "#FFFF00", "#000000", "#8000FF" ];
		$scope.toolbar.draw.modify.originX = 0;
		$scope.toolbar.draw.modify.originY = 0;
		$scope.toolbar.draw.modify.latestX = 0;
		$scope.toolbar.draw.modify.latestY = 0;
		$scope.toolbar.draw.modify.colorMenu.style = {'display':'none','top':0,'left':0,'position':'absolute','zIndex':100};
		$scope.toolbar.draw.modify.colorMenu.show = function show() {
			var dfd = jQuery.Deferred();
			$scope.toolbar.draw.modify.refreshShowFn = $scope.toolbar.draw.modify.colorMenu.show;
			$scope.toolbar.draw.modify.colorMenu.current = $scope.toolbar.draw.modify.DrawObject.color;
			$scope.toolbar.draw.modify.colorMenu.colorArray = $scope.toolbar.draw.modify.DrawObject.colorArray;
			$scope.toolbar.draw.modify.colorMenu.style.display = 'block';
			$scope.toolbar.draw.modify.colorMenu.style.top = -1000; //start off screen for calculation of size
			$scope.toolbar.draw.modify.colorMenu.style.left = -1000;
			$timeout( function() {
				var start = $scope.toolbar.draw.modify.calcMenuPos('modifyColorMenu');
				$scope.toolbar.draw.modify.colorMenu.style.top = start.top; //PDFViewer.Canvas.coord.eventY;
				$scope.toolbar.draw.modify.colorMenu.style.left = start.left; //PDFViewer.Canvas.coord.eventX;
				dfd.resolve(start);
			}, 100);
			return dfd;
		};
		$scope.toolbar.draw.modify.colorMenu.hide = function hide() {
			$scope.toolbar.draw.modify.refreshShowFn = null;
			$scope.toolbar.draw.modify.colorMenu.style.display = 'none';
		};
		$scope.toolbar.draw.modify.colorMenu.click = function click(event, color) {
			var DrawObject = $scope.toolbar.draw.modify.DrawObject;
			$scope.toolbar.draw.undo.add($scope.toolbar.draw.undo.factory.modify(DrawObject));
			DrawObject.setColor(color);
			$scope.toolbar.draw.modify.setStickyColor(color);
			$scope.toolbar.draw.modify.colorMenu.hide();
			$scope.toolbar.draw.modify.setMode(false);
			PDFViewer.triggerViewerAPIPresenter('ModifyAnnotation', DrawObject.sendModify(Page.index));
			if(typeof PDFViewer.Toolbar.activityCallback === "function") {
				PDFViewer.Toolbar.activityCallback(true);
				return true;
			}
		};
		$scope.toolbar.draw.modify.thicknessMenu = {};
		$scope.toolbar.draw.modify.thicknessMenu.current = 1;
		$scope.toolbar.draw.modify.thicknessMenu.thicknessArray = [1,2,3,4];
		$scope.toolbar.draw.modify.thicknessMenu.style = {'display':'none','top':0,'left':0,'position':'absolute','zIndex':100};
		$scope.toolbar.draw.modify.thicknessMenu.show = function show() {
			var dfd = jQuery.Deferred();
			$scope.toolbar.draw.modify.refreshShowFn = $scope.toolbar.draw.modify.thicknessMenu.show;
			$scope.toolbar.draw.modify.thicknessMenu.current = $scope.toolbar.draw.modify.DrawObject.lineWidth;
			$scope.toolbar.draw.modify.thicknessMenu.thicknessArray = $scope.toolbar.draw.modify.DrawObject.thicknessArray;
			$scope.toolbar.draw.modify.thicknessMenu.style.display = 'block';
			$scope.toolbar.draw.modify.thicknessMenu.style.top = -1000; //start off screen for calculation of size
			$scope.toolbar.draw.modify.thicknessMenu.style.left = -1000;
			$timeout( function() {
				var start = $scope.toolbar.draw.modify.calcMenuPos('modifyThicknessMenu');
				$scope.toolbar.draw.modify.thicknessMenu.style.top = start.top; //PDFViewer.Canvas.coord.eventY;
				$scope.toolbar.draw.modify.thicknessMenu.style.left = start.left; //PDFViewer.Canvas.coord.eventX;
				dfd.resolve(start);
			}, 100);
			return dfd;
		};
		$scope.toolbar.draw.modify.thicknessMenu.hide = function hide() {
			$scope.toolbar.draw.modify.refreshShowFn = null;
			$scope.toolbar.draw.modify.thicknessMenu.style.display = 'none';
		};
		$scope.toolbar.draw.modify.thicknessMenu.click = function click(event, width) {
			var DrawObject = $scope.toolbar.draw.modify.DrawObject;
			$scope.toolbar.draw.undo.add($scope.toolbar.draw.undo.factory.modify(DrawObject));
			DrawObject.setLineWidth(width);
			$scope.toolbar.draw.modify.setStickyWidth(width);
			$scope.toolbar.draw.modify.thicknessMenu.hide();
			$scope.toolbar.draw.modify.setMode(false);
			PDFViewer.triggerViewerAPIPresenter('ModifyAnnotation', DrawObject.sendModify(Page.index));
			if(typeof PDFViewer.Toolbar.activityCallback === "function") {
				PDFViewer.Toolbar.activityCallback(true);
				return true;
			}
		};
		$scope.toolbar.draw.modify.opacityMenu = {};
		$scope.toolbar.draw.modify.opacityMenu.current = 1.0;
		$scope.toolbar.draw.modify.opacityMenu.opacityArray = [0.25, 0.50, 0.75, 1.0];
		$scope.toolbar.draw.modify.opacityMenu.style = {'display':'none','top':0,'left':0,'position':'absolute','zIndex':100};
		$scope.toolbar.draw.modify.opacityMenu.show = function show() {
			var dfd = jQuery.Deferred();
			$scope.toolbar.draw.modify.refreshShowFn = $scope.toolbar.draw.modify.opacityMenu.show;
			$scope.toolbar.draw.modify.opacityMenu.current = $scope.toolbar.draw.modify.DrawObject.opacity;
			$scope.toolbar.draw.modify.opacityMenu.opacityArray = $scope.toolbar.draw.modify.DrawObject.opacityArray;
			$scope.toolbar.draw.modify.opacityMenu.style.display = 'block';
			$scope.toolbar.draw.modify.opacityMenu.style.top = -1000; //start off screen for calculation of size
			$scope.toolbar.draw.modify.opacityMenu.style.left = -1000;
			$timeout( function() {
				var start = $scope.toolbar.draw.modify.calcMenuPos('modifyOpacityMenu');
				$scope.toolbar.draw.modify.opacityMenu.style.top = start.top; //PDFViewer.Canvas.coord.eventY;
				$scope.toolbar.draw.modify.opacityMenu.style.left = start.left; //PDFViewer.Canvas.coord.eventX;
				dfd.resolve(start);
			}, 100);
			return dfd;
		};
		$scope.toolbar.draw.modify.opacityMenu.hide = function hide() {
			$scope.toolbar.draw.modify.refreshShowFn = null;
			$scope.toolbar.draw.modify.opacityMenu.style.display = 'none';
		};
		$scope.toolbar.draw.modify.opacityMenu.click = function click(event, opacity) {
			var DrawObject = $scope.toolbar.draw.modify.DrawObject;
			$scope.toolbar.draw.undo.add($scope.toolbar.draw.undo.factory.modify(DrawObject));
			DrawObject.setOpacity(opacity);
			$scope.toolbar.draw.modify.setStickyOpacity(opacity);
			$scope.toolbar.draw.modify.opacityMenu.hide();
			$scope.toolbar.draw.modify.setMode(false);
			PDFViewer.triggerViewerAPIPresenter('ModifyAnnotation', DrawObject.sendModify(Page.index));
			if(typeof PDFViewer.Toolbar.activityCallback === "function") {
				PDFViewer.Toolbar.activityCallback(true);
				return true;
			}
		};
		$scope.toolbar.draw.modify.setStickyWidth = function setStickyWidth(lineWidth) {
			var DrawObject = $scope.toolbar.draw.modify.DrawObject;
			switch(DrawObject.drawType) {
				case "arrow":
					$scope.toolbar.draw.arrow.lineWidth = lineWidth;
					break;
				case "marker":
					$scope.toolbar.draw.highlight.lineWidth = lineWidth;
					break;
				case "pencil":
					$scope.toolbar.draw.pencil.lineWidth = lineWidth;
					break;
			}
		};
		$scope.toolbar.draw.modify.setStickyOpacity = function setStickyOpacity(opacity) {
			var DrawObject = $scope.toolbar.draw.modify.DrawObject;
			switch(DrawObject.drawType) {
				case "arrow":
					$scope.toolbar.draw.arrow.opacity = opacity;
					break;
				case "marker":
					$scope.toolbar.draw.highlight.opacity = opacity;
					break;
				case "pencil":
					$scope.toolbar.draw.pencil.opacity = opacity;
					break;
			}
		};
		$scope.toolbar.draw.modify.setStickyColor = function setStickyColor(color) {
			var DrawObject = $scope.toolbar.draw.modify.DrawObject;
			switch(DrawObject.drawType) {
				case "arrow":
					$scope.toolbar.draw.arrow.colorStyle.backgroundColor = color;
					$scope.toolbar.draw.arrow.color = color;
					PDFViewer.transmitArrowColorCallback(color);
					break;
				case "marker":
					$scope.toolbar.draw.highlight.colorStyle.backgroundColor = color;
					$scope.toolbar.draw.highlight.color = color;
					PDFViewer.transmitHighlightColorCallback(color);
					break;
				case "pencil":
					$scope.toolbar.draw.pencil.colorStyle.backgroundColor = color;
					$scope.toolbar.draw.pencil.color = color;
					PDFViewer.transmitPencilColorCallback(color);
					break;
				case "note":
					$scope.toolbar.draw.note.colorStyle.backgroundColor = color;
					PDFViewer.transmitNoteColorCallback(color);
					break;
			}
		};
		$scope.toolbar.draw.modify.validClick = function validClick() {
			if(Math.abs($scope.toolbar.draw.modify.originX-$scope.toolbar.draw.modify.latestX) < 5 &&
				Math.abs($scope.toolbar.draw.modify.originY-$scope.toolbar.draw.modify.latestY) < 5) {
				return true;
			}
			return false;
		};
		$scope.toolbar.draw.modify.mouseSelectState = 'select';
		$scope.toolbar.draw.modify.reset = function reset() {
			$scope.toolbar.draw.modify.mouseSelectState = 'select';
			$scope.toolbar.draw.modify.originX = 0;
			$scope.toolbar.draw.modify.latestX = 0;
			$scope.toolbar.draw.modify.originY = 0;
			$scope.toolbar.draw.modify.latestY = 0;
		};
		$scope.toolbar.draw.modify.mouseSelectHandler = new PDFViewer.Canvas.events.mouse.delegate(this);
		$scope.toolbar.draw.modify.mouseSelectHandler.down = function down(event) {
			if($scope.toolbar.getTool() !== $scope.toolbar.draw.erase.toolCode) {
				$scope.toolbar.draw.modify.originX = PDFViewer.Canvas.coord.scaleX; // to track the movement
				$scope.toolbar.draw.modify.originY = PDFViewer.Canvas.coord.scaleY; // to track the movement
				$scope.toolbar.draw.modify.latestX = PDFViewer.Canvas.coord.scaleX; // to track the movement
				$scope.toolbar.draw.modify.latestY = PDFViewer.Canvas.coord.scaleY; // to track the movement
				var DrawObject = PDFViewer.annotations.pipeline.getObj(PDFViewer.Canvas.coord.scaleX, PDFViewer.Canvas.coord.scaleY);
				if(DrawObject && DrawObject.drawType === 'note' && PDFViewer.annotations.mode !== "presenter" && PDFViewer.annotations.mode !== "annotator") {
					event.preventDefault();
					$scope.toolbar.draw.modify.DrawObject = DrawObject;
					$scope.toolbar.draw.modify.DrawObjectOrigin = [PDFViewer.Canvas.coord.scaleX, PDFViewer.Canvas.coord.scaleY];
					PDFViewer.Canvas.refresh();
				}
			}
		};
		$scope.toolbar.draw.modify.mouseSelectHandler.move = function move(event) {
			if($scope.toolbar.getTool() !== $scope.toolbar.draw.erase.toolCode) {
				$scope.toolbar.draw.modify.latestX = PDFViewer.Canvas.coord.scaleX; // to track the movement
				$scope.toolbar.draw.modify.latestY = PDFViewer.Canvas.coord.scaleY; // to track the movement
				if($scope.toolbar.draw.modify.DrawObject) {
					if($scope.toolbar.draw.modify.DrawObject.drawType !== 'note') {
						return;
					}
					if($scope.toolbar.draw.modify.mouseSelectState === 'select') {
						if(Math.abs(PDFViewer.Canvas.coord.scaleX-$scope.toolbar.draw.modify.DrawObjectOrigin[0])>2
								|| Math.abs(PDFViewer.Canvas.coord.scaleY-$scope.toolbar.draw.modify.DrawObjectOrigin[1])>2) {
							event.preventDefault();
							$scope.toolbar.draw.modify.mouseSelectState = 'drag';
							$scope.toolbar.draw.modify.menu.hide();
							$scope.toolbar.draw.modify.colorMenu.hide();
							$scope.toolbar.draw.modify.thicknessMenu.hide();
							$scope.toolbar.draw.modify.opacityMenu.hide();
							$scope.toolbar.page.showNavBtns = false;
							$scope.toolbar.draw.undo.add($scope.toolbar.draw.undo.factory.modify($scope.toolbar.draw.modify.DrawObject));
							$scope.toolbar.draw.modify.DrawObject.startDrag(PDFViewer.Canvas.coord.scaleX, PDFViewer.Canvas.coord.scaleY);
						}
					} else {
						event.preventDefault();
						$scope.toolbar.draw.modify.DrawObject.drag(PDFViewer.Canvas.coord.scaleX, PDFViewer.Canvas.coord.scaleY);
					}
					PDFViewer.Canvas.refresh();
				}
			}
		};
		$scope.toolbar.draw.modify.mouseSelectHandler.click = function click(event) {
			// Generic Canvas Annotation Selector Listener
			if($scope.toolbar.getTool() !== $scope.toolbar.draw.erase.toolCode) {
				if($scope.toolbar.draw.modify.mouseSelectState === 'drag') {
					$scope.toolbar.draw.modify.mouseSelectState = 'select';
					PDFViewer.annotations.pipeline.deselectAll();
					$scope.toolbar.draw.modify.DrawObject.endDrag();
					$scope.toolbar.draw.modify.DrawObject.refresh();
					PDFViewer.Canvas.refresh();
					$scope.toolbar.page.showNavBtns = true;
					$scope.toolbar.draw.modify.DrawObject = null;
				} else if($scope.toolbar.draw.modify.validClick()) {
					PDFViewer.annotations.pipeline.deselectAll();
					var DrawObject = PDFViewer.annotations.pipeline.getObj(PDFViewer.Canvas.coord.scaleX, PDFViewer.Canvas.coord.scaleY);
					if(DrawObject && DrawObject.drawType === 'note') {
						$scope.toolbar.draw.note.setDrawObject(DrawObject);
						if(typeof PDFViewer.Toolbar.activityCallback === "function") {
							PDFViewer.Toolbar.activityCallback(true);
							PDFViewer.Canvas.refresh();
							return true;
						}
					} else if(DrawObject) {
						$scope.toolbar.draw.modify.setDrawObject(DrawObject);
						if(typeof PDFViewer.Toolbar.activityCallback === "function") {
							PDFViewer.Toolbar.activityCallback(true);
							PDFViewer.Canvas.refresh();
							return true;
						}
					} else {
						if(typeof PDFViewer.Toolbar.activityCallback === "function") {
							PDFViewer.Toolbar.activityCallback(false);
							PDFViewer.Canvas.refresh();
							return true;
						}
					}
				}
				PDFViewer.Canvas.refresh();
			}
		};
		$scope.toolbar.draw.modify.mouseMoveHandler = new PDFViewer.Canvas.events.mouse.delegate(this);
		$scope.toolbar.draw.modify.mouseMoveHandler.click = function click(event) {
			// Generic Canvas Annotation Selector Listener
			if(typeof PDFViewer.Toolbar.activityCallback === "function") {
				PDFViewer.Toolbar.activityCallback(true);
				return true;
			}
		};
		$scope.toolbar.draw.modify.mouseMoveHandler.down = function down(event) {
			if($scope.toolbar.getTool() === $scope.toolbar.draw.modify.toolCode) {
				event.preventDefault();
				// make sure we hit something
				var DrawObject = PDFViewer.annotations.pipeline.getObj(PDFViewer.Canvas.coord.scaleX, PDFViewer.Canvas.coord.scaleY);
				//console.log('ModifyStart', PDFViewer.Canvas.coord.scaleX, PDFViewer.Canvas.coord.scaleY, !!DrawObject);
				// check the something we hit against our selection DrawObject
				if(DrawObject === $scope.toolbar.draw.modify.DrawObject) {
					$scope.toolbar.draw.modify.menu.hide();
					$scope.toolbar.draw.modify.colorMenu.hide();
					$scope.toolbar.draw.modify.thicknessMenu.hide();
					$scope.toolbar.draw.modify.opacityMenu.hide();
					$scope.toolbar.page.showNavBtns = false;
					$scope.toolbar.draw.undo.add($scope.toolbar.draw.undo.factory.modify(DrawObject));
					$scope.toolbar.draw.modify.DrawObject.startDrag(PDFViewer.Canvas.coord.scaleX, PDFViewer.Canvas.coord.scaleY);
					PDFViewer.Canvas.refresh();
				}
				if(DrawObject !== $scope.toolbar.draw.modify.DrawObject) {
					//console.log('deselect the item', $scope.toolbar.draw.modify.DrawObject);
					$scope.toolbar.setTool($scope.toolbar.draw.modify.toolCode, false);
				}
			}
		};
		$scope.toolbar.draw.modify.mouseMoveHandler.move = function move(event) {
			if($scope.toolbar.getTool() === $scope.toolbar.draw.modify.toolCode) {
				event.preventDefault();
				$scope.toolbar.draw.modify.DrawObject.drag(PDFViewer.Canvas.coord.scaleX, PDFViewer.Canvas.coord.scaleY);
				PDFViewer.Canvas.refresh();
			}
		};
		$scope.toolbar.draw.modify.mouseMoveHandler.up = function up(event) {
			if($scope.toolbar.getTool() === $scope.toolbar.draw.modify.toolCode) {
				event.preventDefault();
				var DrawObject = $scope.toolbar.draw.modify.DrawObject;
				$scope.toolbar.draw.modify.menu.show();
				DrawObject.endDrag();
				DrawObject.refresh();
				//$scope.toolbar.draw.undo.add($scope.toolbar.draw.undo.factory.modify($scope.toolbar.draw.modify.DrawObject));
				PDFViewer.Canvas.refresh();
				$scope.toolbar.page.showNavBtns = true;
				PDFViewer.triggerViewerAPIPresenter('ModifyAnnotation', DrawObject.sendModify(Page.index));
			}
		};
		$scope.$on('ToolChange',function ToolChange(event) {
			$scope.toolbar.draw.modify.mode = ($scope.toolbar.getTool() === $scope.toolbar.draw.modify.toolCode);
			if($scope.toolbar.draw.modify.mode) {
				if($scope.toolbar.draw.modify.DrawObject) {
					//PDFViewer.Canvas.events.mouse.addHandler($scope.toolbar.draw.modify.mouseMoveHandler);
					$scope.toolbar.draw.modify.DrawObject.select();
					$scope.toolbar.draw.modify.menu.show();
				}
			}
			else {
				//PDFViewer.Canvas.events.mouse.removeHandler($scope.toolbar.draw.modify.mouseMoveHandler);
				$scope.toolbar.draw.modify.menu.hide();
				$scope.toolbar.draw.modify.colorMenu.hide();
				$scope.toolbar.draw.modify.thicknessMenu.hide();
				$scope.toolbar.draw.modify.opacityMenu.hide();
				if($scope.toolbar.draw.modify.DrawObject) {
					// deselect
					$scope.toolbar.draw.modify.DrawObject.deselect();
					$scope.toolbar.draw.modify.DrawObject = null;
				}
			}
			PDFViewer.Canvas.refresh();
		});
		$scope.$root.$on( "PageChange", function PageChange() {
			// any selected item should be unselected.
			$scope.toolbar.draw.modify.mode = ($scope.toolbar.getTool() === $scope.toolbar.draw.modify.toolCode);
			if($scope.toolbar.draw.modify.mode) {
				$scope.toolbar.draw.modify.menu.hide();
				$scope.toolbar.draw.modify.colorMenu.hide();
				$scope.toolbar.draw.modify.thicknessMenu.hide();
				$scope.toolbar.draw.modify.opacityMenu.hide();
				if($scope.toolbar.draw.modify.DrawObject) {
					// deselect
					$scope.toolbar.draw.modify.DrawObject.deselect();
					$scope.toolbar.draw.modify.DrawObject = null;
				}
			}
		});


		//**************************************************
		// DRAW ARROW TOOLBAR SETUP
		$scope.toolbar.draw.arrow = {};
		$scope.toolbar.draw.arrow.mode = false;
		$scope.toolbar.draw.arrow.toolCode = 'draw-arrow';
		$scope.toolbar.draw.arrow.showMenu = false;
		$scope.toolbar.draw.arrow.color = '#ff0000';
		$scope.toolbar.draw.arrow.lineWidth = 3;
		$scope.toolbar.draw.arrow.opacity = 1.0;
		$scope.toolbar.draw.arrow.noDraw = false;
		$scope.toolbar.draw.arrow.modeStyle = {'background-color':'transparent'};
		$scope.toolbar.draw.arrow.DrawInProgress = false;
		$scope.toolbar.draw.arrow.DrawObject = undefined;
		$scope.toolbar.draw.arrow.DrawObjectOrigin = {x:0,y:0};
		$scope.toolbar.draw.arrow.colorStyle = {'background-color':$scope.toolbar.draw.arrow.color};
		$scope.toolbar.draw.arrow.toggleMode = function toggleMode() {
			$scope.toolbar.draw.arrow.setMode(!$scope.toolbar.draw.arrow.mode);
		};
		$scope.toolbar.draw.arrow.toggleMenu = function toggleMenu() {
			$scope.toolbar.draw.arrow.showMenu = !$scope.toolbar.draw.arrow.showMenu;
			$scope.toolbar.draw.arrow.setMode(true);
		};
		$scope.toolbar.draw.arrow.setMode = function setMode(mode) {
			if(!mode && $scope.toolbar.draw.arrow.mode) {
				$scope.toolbar.draw.arrow.noDraw = true;
				// set something to remember we just let go of this, for swipe prevention
				$timeout(function(){$scope.toolbar.setTool($scope.toolbar.draw.arrow.toolCode, mode);$scope.toolbar.draw.arrow.noDraw=false;},200);
			} else {
				$scope.toolbar.draw.arrow.noDraw=false;
				$scope.toolbar.setTool($scope.toolbar.draw.arrow.toolCode, mode);
			}
		};
		$scope.toolbar.draw.arrow.setColor = function setColor(color) {
			$scope.toolbar.draw.arrow.color = color;
			$scope.toolbar.draw.arrow.colorStyle = {'background-color':$scope.toolbar.draw.arrow.color};
			$scope.toolbar.draw.arrow.showMenu = false;
		};
		$scope.toolbar.draw.arrow.setLineWidth = function setLineWidth(lineWidth) {
			$scope.toolbar.draw.arrow.lineWidth = lineWidth;
		};
		$scope.toolbar.draw.arrow.setOpacity = function setOpacity(pOpacity) {
			if(pOpacity > 2.0) {
				pOpacity = pOpacity / 100.0;
			}
			$scope.toolbar.draw.arrow.opacity = pOpacity;
		};
		$scope.toolbar.draw.arrow.mouseHandler = new PDFViewer.Canvas.events.mouse.delegate(this);
		$scope.toolbar.draw.arrow.mouseHandler.click = function click(event) {
			if($scope.toolbar.getTool() === $scope.toolbar.draw.arrow.toolCode) {
				$scope.toolbar.draw.arrow.setMode(false);
				PDFViewer.Canvas.refresh();
				if(typeof PDFViewer.Toolbar.activityCallback === "function") {
					PDFViewer.Toolbar.activityCallback(true);
					return true;
				}
			}
		};
		$scope.toolbar.draw.arrow.mouseHandler.down = function down(event) {
			if($scope.toolbar.getTool() === $scope.toolbar.draw.arrow.toolCode) {
				event.preventDefault();
				if(!$scope.toolbar.draw.arrow.DrawInProgress && !$scope.toolbar.draw.arrow.noDraw) {
					// new draw arrow annotation.
					var params = DrawObjectDefaultArrowParams();
					params.pointList.push([PDFViewer.Canvas.coord.scaleX, PDFViewer.Canvas.coord.scaleY]);
					params.pointList.push([PDFViewer.Canvas.coord.scaleX, PDFViewer.Canvas.coord.scaleY]);
					params.color = $scope.toolbar.draw.arrow.color;
					params.lineWidth = $scope.toolbar.draw.arrow.lineWidth;
					params.opacity = $scope.toolbar.draw.arrow.opacity * 100;
					params.pageIndex = Page.index;
					$scope.toolbar.draw.arrow.DrawObjectOrigin.x = PDFViewer.Canvas.coord.scaleX;
					$scope.toolbar.draw.arrow.DrawObjectOrigin.y = PDFViewer.Canvas.coord.scaleY;
					$scope.toolbar.draw.arrow.DrawObject = DrawObjectFactory(params);
					$scope.toolbar.draw.arrow.DrawObject.scale = PDFViewer.getScaleFactor();
				}
			}
		};
		$scope.toolbar.draw.arrow.mouseHandler.move = function move(event) {
			if($scope.toolbar.getTool() === $scope.toolbar.draw.arrow.toolCode) {
				event.preventDefault();
				if(!$scope.toolbar.draw.arrow.DrawInProgress
					&& !$scope.toolbar.draw.arrow.noDraw
					&&( $scope.toolbar.draw.arrow.DrawObjectOrigin.x !== PDFViewer.Canvas.coord.scaleX
					||  $scope.toolbar.draw.arrow.DrawObjectOrigin.y !== PDFViewer.Canvas.coord.scaleY)) {
					//console.log('Added draw of an arrow');
					$scope.toolbar.page.showNavBtns = false;
					$scope.toolbar.draw.scroll(false);
					PDFViewer.annotations.pipeline.add($scope.toolbar.draw.arrow.DrawObject);
					$scope.toolbar.draw.arrow.DrawObjectOrigin.x = 0;
					$scope.toolbar.draw.arrow.DrawObjectOrigin.y = 0;
					$scope.toolbar.draw.arrow.DrawInProgress = true;
				}
				if($scope.toolbar.draw.arrow.DrawObject && $scope.toolbar.draw.arrow.DrawInProgress) {
					$scope.toolbar.draw.arrow.DrawObject.changeLineEnd(PDFViewer.Canvas.coord.scaleX, PDFViewer.Canvas.coord.scaleY);
					PDFViewer.Canvas.refresh();
				}
			}
		};
		$scope.toolbar.draw.arrow.mouseHandler.up = function up(event) {
			if($scope.toolbar.getTool() === $scope.toolbar.draw.arrow.toolCode) {
				$scope.toolbar.draw.arrow.setMode(false);
				event.preventDefault();
				if($scope.toolbar.draw.arrow.DrawObject && $scope.toolbar.draw.arrow.DrawInProgress) {
					$scope.toolbar.draw.arrow.DrawObject.changeLineEnd(PDFViewer.Canvas.coord.scaleX, PDFViewer.Canvas.coord.scaleY);
					$scope.toolbar.draw.arrow.DrawObject.existing = false;
					$scope.toolbar.draw.arrow.DrawObject.deselect();
					$scope.toolbar.draw.arrow.DrawObject.refresh();
					$scope.toolbar.draw.arrow.DrawObject.setTargetHostPageRotation(PDFViewer.getHostPageRotation(Page.index));
					PDFViewer.triggerViewerAPIPresenter('AddAnnotation', $scope.toolbar.draw.arrow.DrawObject.sendDraw(Page.index));
					$scope.toolbar.draw.undo.add($scope.toolbar.draw.undo.factory.draw($scope.toolbar.draw.arrow.DrawObject));
					$scope.toolbar.draw.arrow.DrawObject = undefined;
					$scope.toolbar.draw.arrow.DrawInProgress = false;
				}
				$scope.toolbar.page.showNavBtns = true;
				$scope.toolbar.draw.scroll(true);
				PDFViewer.Canvas.refresh();
			}
		};
		$scope.toolbar.draw.arrow.exitCallback = 0;
		$scope.toolbar.draw.arrow.addExitCallback = function addExitCallback(callback) {
			if(typeof callback === "function") {
				$scope.toolbar.draw.arrow.exitCallback = callback;
			}
		};
		$scope.$on('ToolChange',function ToolChange(event) {
			$scope.toolbar.draw.arrow.mode = ($scope.toolbar.getTool() === $scope.toolbar.draw.arrow.toolCode);
			//$scope.toolbar.draw.arrow.modeStyle = $scope.toolbar.draw.arrow.mode ? {'background-color':'#eee'} : {'background-color':'transparent'};
			if(!$scope.toolbar.draw.arrow.mode) {
				$scope.toolbar.draw.arrow.showMenu = false;
				if($scope.toolbar.draw.arrow.exitCallback) {
					$scope.toolbar.draw.arrow.exitCallback();
					$scope.toolbar.draw.arrow.exitCallback = 0;
				}
				//PDFViewer.Canvas.events.mouse.removeHandler($scope.toolbar.draw.arrow.mouseHandler);
			}
			else {
				//PDFViewer.Canvas.events.mouse.addHandler($scope.toolbar.draw.arrow.mouseHandler);
			}
		});


		//**************************************************
		// DRAW HIGHLIGHT TOOLBAR SETUP
		$scope.toolbar.draw.highlight = {};
		$scope.toolbar.draw.highlight.mode = false;
		$scope.toolbar.draw.highlight.toolCode = 'draw-highlight';
		$scope.toolbar.draw.highlight.showMenu = false;
		$scope.toolbar.draw.highlight.color = '#ffff00';
		$scope.toolbar.draw.highlight.lineWidth = 12;
		$scope.toolbar.draw.highlight.opacity = 0.35;
		$scope.toolbar.draw.highlight.modeStyle = {'background-color':'transparent'};
		$scope.toolbar.draw.highlight.DrawInProgress = false;
		$scope.toolbar.draw.highlight.DrawObject = undefined;
		$scope.toolbar.draw.highlight.DrawObjectOrigin = {x:0,y:0};
		$scope.toolbar.draw.highlight.colorStyle = {'background-color':$scope.toolbar.draw.highlight.color};
		$scope.toolbar.draw.highlight.toggleMode = function toggleMode() {
			$scope.toolbar.draw.highlight.setMode(!$scope.toolbar.draw.highlight.mode);
			if(!$scope.toolbar.draw.highlight.mode) {
				$scope.toolbar.draw.highlight.showMenu = false;
			}
		};
		$scope.toolbar.draw.highlight.toggleMenu = function toggleMenu() {
			$scope.toolbar.draw.highlight.showMenu = !$scope.toolbar.draw.highlight.showMenu;
			$scope.toolbar.draw.highlight.setMode(true);
		};
		$scope.toolbar.draw.highlight.setMode = function setMode(mode) {
			$scope.toolbar.setTool($scope.toolbar.draw.highlight.toolCode, mode);
		};
		$scope.toolbar.draw.highlight.setColor = function setColor(color) {
			$scope.toolbar.draw.highlight.color = color;
			$scope.toolbar.draw.highlight.colorStyle = {'background-color':$scope.toolbar.draw.highlight.color};
			$scope.toolbar.draw.highlight.showMenu = false;
		};
		$scope.toolbar.draw.highlight.setLineWidth = function setLineWidth(lineWidth) {
			$scope.toolbar.draw.highlight.lineWidth = lineWidth;
		};
		$scope.toolbar.draw.highlight.setOpacity = function setOpacity(pOpacity) {
			if(pOpacity > 2.0) {
				pOpacity = pOpacity / 100.0;
			}
			$scope.toolbar.draw.highlight.opacity = pOpacity;
		};
		$scope.toolbar.draw.highlight.mouseHandler = new PDFViewer.Canvas.events.mouse.delegate(this);
		$scope.toolbar.draw.highlight.mouseHandler.click = function click(event) {
			if($scope.toolbar.getTool() === $scope.toolbar.draw.highlight.toolCode) {
				if(typeof PDFViewer.Toolbar.activityCallback === "function") {
					PDFViewer.Toolbar.activityCallback(true);
					return true;
				}
			}
		};
		$scope.toolbar.draw.highlight.mouseHandler.down = function down( event ) {
			if( $scope.toolbar.getTool() === $scope.toolbar.draw.highlight.toolCode ) {
				event.preventDefault();
				// new draw highlight annotation.
				var params = DrawObjectDefaultHighlightParams();
				params.pointList.push( [ PDFViewer.Canvas.coord.scaleX, PDFViewer.Canvas.coord.scaleY ] );
				params.pointList.push( [ PDFViewer.Canvas.coord.scaleX, PDFViewer.Canvas.coord.scaleY ] );
				params.color = $scope.toolbar.draw.highlight.color;
				params.lineWidth = $scope.toolbar.draw.highlight.lineWidth;
				params.opacity = $scope.toolbar.draw.highlight.opacity * 100;
				params.pageIndex = Page.index;
				$scope.toolbar.draw.highlight.DrawObjectOrigin.x = PDFViewer.Canvas.coord.scaleX;
				$scope.toolbar.draw.highlight.DrawObjectOrigin.y = PDFViewer.Canvas.coord.scaleY;
				$scope.toolbar.draw.highlight.DrawObject = DrawObjectFactory( params );
				$scope.toolbar.draw.highlight.DrawObject.scale = PDFViewer.getScaleFactor();
			}
		};
		$scope.toolbar.draw.highlight.mouseHandler.move = function move( event ) {
			if( $scope.toolbar.getTool() === $scope.toolbar.draw.highlight.toolCode ) {
				event.preventDefault();
				var xDidMove = ($scope.toolbar.draw.highlight.DrawObjectOrigin.x !== PDFViewer.Canvas.coord.scaleX);
				var yDidMove = ($scope.toolbar.draw.highlight.DrawObjectOrigin.y !== PDFViewer.Canvas.coord.scaleY);
				if( !$scope.toolbar.draw.highlight.DrawInProgress && (xDidMove || yDidMove) ) {
					$scope.toolbar.page.showNavBtns = false;
					$scope.toolbar.draw.scroll( false );
					PDFViewer.annotations.pipeline.add( $scope.toolbar.draw.highlight.DrawObject );
					$scope.toolbar.draw.highlight.DrawInProgress = true;
				}
				if( $scope.toolbar.draw.highlight.DrawObject && $scope.toolbar.draw.highlight.DrawInProgress ) {
					$scope.toolbar.draw.highlight.DrawObject.changeLineEnd( PDFViewer.Canvas.coord.scaleX, PDFViewer.Canvas.coord.scaleY );
					PDFViewer.Canvas.refresh();
				}
			}
		};
		$scope.toolbar.draw.highlight.mouseHandler.up = function up( event ) {
			if( $scope.toolbar.getTool() === $scope.toolbar.draw.highlight.toolCode ) {
				event.preventDefault();
				if( $scope.toolbar.draw.highlight.DrawObject && $scope.toolbar.draw.highlight.DrawInProgress ) {
					var coord = Object.assign( {}, PDFViewer.Canvas.coord );
					var objOrigin = Object.assign( {}, $scope.toolbar.draw.highlight.DrawObjectOrigin );
					var width = Math.abs( objOrigin.x - coord.scaleX );
					var height = Math.abs( objOrigin.y - coord.scaleY );
					if( width < 12 && height < 12 ) {
						PDFViewer.annotations.pipeline.remove( $scope.toolbar.draw.highlight.DrawObject );
					} else {
						var x1 = Math.min( coord.scaleX, objOrigin.x );
						var y1 = Math.min( coord.scaleY, objOrigin.y );
						var x2 = Math.max( coord.scaleX, objOrigin.x );
						var y2 = Math.max( coord.scaleY, objOrigin.y );
						if( width < 12 ) {
							x2 += 12 - Math.abs( x1 - x2 );
						} else if( height < 12 ) {
							y2 += 12 - Math.abs( y1 - y2 );
						}
						$scope.toolbar.draw.highlight.DrawObject.changeLineStart( x1, y1 );
						$scope.toolbar.draw.highlight.DrawObject.changeLineEnd( x2, y2 );
						$scope.toolbar.draw.highlight.DrawObject.existing = false;
						$scope.toolbar.draw.highlight.DrawObject.deselect();
						$scope.toolbar.draw.highlight.DrawObject.refresh();
						$scope.toolbar.draw.highlight.DrawObject.setTargetHostPageRotation( PDFViewer.getHostPageRotation( Page.index ) );
						PDFViewer.triggerViewerAPIPresenter( "AddAnnotation", $scope.toolbar.draw.highlight.DrawObject.sendDraw( Page.index ) );
						$scope.toolbar.draw.undo.add( $scope.toolbar.draw.undo.factory.draw( $scope.toolbar.draw.highlight.DrawObject ) );
					}
					$scope.toolbar.draw.highlight.DrawObject = undefined;
					$scope.toolbar.draw.highlight.DrawInProgress = false;
					$scope.toolbar.draw.highlight.DrawObjectOrigin.x = 0;
					$scope.toolbar.draw.highlight.DrawObjectOrigin.y = 0;
				}
				$scope.toolbar.page.showNavBtns = true;
				$scope.toolbar.draw.scroll( true );
				PDFViewer.Canvas.refresh();
				PDFViewer.safeApply();
			}
		};
		$scope.toolbar.draw.highlight.exitCallback = 0;
		$scope.toolbar.draw.highlight.addExitCallback = function addExitCallback(callback) {
			if(typeof callback === "function") {
				$scope.toolbar.draw.highlight.exitCallback = callback;
			}
		};
		$scope.$on('ToolChange',function ToolChange(event) {
			$scope.toolbar.draw.highlight.mode = ($scope.toolbar.getTool() === $scope.toolbar.draw.highlight.toolCode);
			//$scope.toolbar.draw.highlight.modeStyle = $scope.toolbar.draw.highlight.mode ? {'background-color':'#eee'} : {'background-color':'transparent'};
			if(!$scope.toolbar.draw.highlight.mode) {
				$scope.toolbar.draw.highlight.showMenu = false;
				if($scope.toolbar.draw.highlight.exitCallback) {
					$scope.toolbar.draw.highlight.exitCallback();
					$scope.toolbar.draw.highlight.exitCallback = 0;
				}
				//PDFViewer.Canvas.events.mouse.removeHandler($scope.toolbar.draw.highlight.mouseHandler);
			}
			else {
				//PDFViewer.Canvas.events.mouse.addHandler($scope.toolbar.draw.highlight.mouseHandler);
			}
		});


		//**************************************************
		// DRAW PENCIL TOOLBAR SETUP
		$scope.toolbar.draw.pencil = {};
		$scope.toolbar.draw.pencil.mode = false;
		$scope.toolbar.draw.pencil.toolCode = 'draw-pencil';
		$scope.toolbar.draw.pencil.showMenu = false;
		$scope.toolbar.draw.pencil.color = '#ff0000';
		$scope.toolbar.draw.pencil.lineWidth = 3;
		$scope.toolbar.draw.pencil.opacity = 1.0;
		$scope.toolbar.draw.pencil.modeStyle = {'background-color':'transparent'};
		$scope.toolbar.draw.pencil.DrawObject = undefined;
		$scope.toolbar.draw.pencil.DrawInProgress = false;
		$scope.toolbar.draw.pencil.DrawObjectOrigin = {x:0,y:0};
		$scope.toolbar.draw.pencil.colorStyle = {'background-color':$scope.toolbar.draw.pencil.color};
		$scope.toolbar.draw.pencil.toggleMode = function toggleMode() {
			$scope.toolbar.draw.pencil.setMode(!$scope.toolbar.draw.pencil.mode);
		};
		$scope.toolbar.draw.pencil.toggleMenu = function toggleMenu() {
			$scope.toolbar.draw.pencil.showMenu = !$scope.toolbar.draw.pencil.showMenu;
			$scope.toolbar.draw.pencil.setMode(true);
		};
		$scope.toolbar.draw.pencil.setMode = function setMode(mode) {
			$scope.toolbar.setTool($scope.toolbar.draw.pencil.toolCode, mode);
		};
		$scope.toolbar.draw.pencil.setColor = function setColor(color) {
			$scope.toolbar.draw.pencil.color = color;
			$scope.toolbar.draw.pencil.colorStyle = {'background-color':$scope.toolbar.draw.pencil.color};
			$scope.toolbar.draw.pencil.showMenu = false;
		};
		$scope.toolbar.draw.pencil.setLineWidth = function setLineWidth(lineWidth) {
			$scope.toolbar.draw.pencil.lineWidth = lineWidth;
		};
		$scope.toolbar.draw.pencil.setOpacity = function setOpacity(pOpacity) {
			if(pOpacity > 2.0) {
				pOpacity = pOpacity / 100.0;
			}
			$scope.toolbar.draw.pencil.opacity = pOpacity;
		};
		$scope.toolbar.draw.pencil.mouseHandler = new PDFViewer.Canvas.events.mouse.delegate(this);
		$scope.toolbar.draw.pencil.mouseHandler.click = function click(event) {
			if($scope.toolbar.getTool() === $scope.toolbar.draw.pencil.toolCode) {
				if(typeof PDFViewer.Toolbar.activityCallback === "function") {
					PDFViewer.Toolbar.activityCallback(true);
					return true;
				}
			}
		};
		$scope.toolbar.draw.pencil.mouseHandler.down = function down(event) {
			if($scope.toolbar.getTool() === $scope.toolbar.draw.pencil.toolCode) {
				event.preventDefault();
				// new draw arrow annotation.
				var params = DrawObjectDefaultPencilParams();
				params.pointList.push([PDFViewer.Canvas.coord.scaleX, PDFViewer.Canvas.coord.scaleY]);
				params.color = $scope.toolbar.draw.pencil.color;
				params.lineWidth = $scope.toolbar.draw.pencil.lineWidth;
				params.opacity = $scope.toolbar.draw.pencil.opacity * 100.0;
				params.pageIndex = Page.index;
				$scope.toolbar.draw.pencil.DrawObjectOrigin.x = PDFViewer.Canvas.coord.scaleX;
				$scope.toolbar.draw.pencil.DrawObjectOrigin.y = PDFViewer.Canvas.coord.scaleY;
				$scope.toolbar.draw.pencil.DrawObject = DrawObjectFactory(params);
				$scope.toolbar.draw.pencil.DrawObject.scale = PDFViewer.getScaleFactor();
			}
		};
		$scope.toolbar.draw.pencil.mouseHandler.move = function move(event) {
			if($scope.toolbar.getTool() === $scope.toolbar.draw.pencil.toolCode) {
				event.preventDefault();
				if(!$scope.toolbar.draw.pencil.DrawInProgress) {
					$scope.toolbar.page.showNavBtns = false;
					$scope.toolbar.draw.scroll(false);
					PDFViewer.annotations.pipeline.add($scope.toolbar.draw.pencil.DrawObject);
					$scope.toolbar.draw.pencil.DrawObjectOrigin.x = 0;
					$scope.toolbar.draw.pencil.DrawObjectOrigin.y = 0;
					$scope.toolbar.draw.pencil.DrawInProgress = true;
					$scope.toolbar.draw.pencil.DrawObject.startDrag(PDFViewer.Canvas.coord.scaleX, PDFViewer.Canvas.coord.scaleY);
				}
				if($scope.toolbar.draw.pencil.DrawObject && $scope.toolbar.draw.pencil.DrawInProgress) {
					$scope.toolbar.draw.pencil.DrawObject.addPoints(PDFViewer.Canvas.coord.scaleX, PDFViewer.Canvas.coord.scaleY);
					PDFViewer.Canvas.refresh();
				}
			}
		};
		$scope.toolbar.draw.pencil.mouseHandler.up = function up(event) {
			if($scope.toolbar.getTool() === $scope.toolbar.draw.pencil.toolCode) {
				event.preventDefault();
				if(!$scope.toolbar.draw.pencil.DrawInProgress) {
					$scope.toolbar.page.showNavBtns = false;
					$scope.toolbar.draw.scroll(false);
					PDFViewer.annotations.pipeline.add($scope.toolbar.draw.pencil.DrawObject);
					$scope.toolbar.draw.pencil.DrawObjectOrigin.x = 0;
					$scope.toolbar.draw.pencil.DrawObjectOrigin.y = 0;
					$scope.toolbar.draw.pencil.DrawInProgress = true;
					$scope.toolbar.draw.pencil.DrawObject.startDrag(PDFViewer.Canvas.coord.scaleX, PDFViewer.Canvas.coord.scaleY);
				}
				if($scope.toolbar.draw.pencil.DrawObject && $scope.toolbar.draw.pencil.DrawInProgress) {
					$scope.toolbar.draw.pencil.DrawObject.addPoints(PDFViewer.Canvas.coord.scaleX, PDFViewer.Canvas.coord.scaleY);
					$scope.toolbar.draw.pencil.DrawObject.assureDotFoxitCompatible();
					$scope.toolbar.draw.pencil.DrawObject.existing = false;
					$scope.toolbar.draw.pencil.DrawObject.deselect();
					$scope.toolbar.draw.pencil.DrawObject.refresh();
					$scope.toolbar.draw.pencil.DrawObject.setTargetHostPageRotation(PDFViewer.getHostPageRotation(Page.index));
					if(PDFViewer.annotations.pipeline.countSigs($scope.toolbar.draw.pencil.DrawObject.getSig()) > 1) {
						//console.log("PDFViewer::Pencil::Duplicate()", $scope.toolbar.draw.pencil.DrawObject);
						PDFViewer.annotations.pipeline.remove($scope.toolbar.draw.pencil.DrawObject);
					} else {
						//console.log("PDFViewer::Pencil::Added()", $scope.toolbar.draw.pencil.DrawObject);
						PDFViewer.triggerViewerAPIPresenter('AddAnnotation', $scope.toolbar.draw.pencil.DrawObject.sendDraw(Page.index));
						$scope.toolbar.draw.undo.add($scope.toolbar.draw.undo.factory.draw($scope.toolbar.draw.pencil.DrawObject));
					}
					$scope.toolbar.draw.pencil.DrawObject = undefined;
					$scope.toolbar.draw.pencil.DrawInProgress = false;
				}
				$scope.toolbar.page.showNavBtns = true;
				$scope.toolbar.draw.scroll(true);
				PDFViewer.Canvas.refresh();
			}
		};
		$scope.toolbar.draw.pencil.exitCallback = 0;
		$scope.toolbar.draw.pencil.addExitCallback = function addExitCallback(callback) {
			if(typeof callback === "function") {
				$scope.toolbar.draw.pencil.exitCallback = callback;
			}
		};
		$scope.$on('ToolChange',function ToolChange(event) {
			$scope.toolbar.draw.pencil.mode = ($scope.toolbar.getTool() === $scope.toolbar.draw.pencil.toolCode);
			//$scope.toolbar.draw.pencil.modeStyle = $scope.toolbar.draw.pencil.mode ? {'background-color':'#eee'} : {'background-color':'transparent'};
			if(!$scope.toolbar.draw.pencil.mode) {
				$scope.toolbar.draw.pencil.showMenu = false;
				if($scope.toolbar.draw.pencil.exitCallback) {
					$scope.toolbar.draw.pencil.exitCallback();
					$scope.toolbar.draw.pencil.exitCallback = 0;
				}
				//PDFViewer.Canvas.events.mouse.removeHandler($scope.toolbar.draw.pencil.mouseHandler);
			}
			else {
				//PDFViewer.Canvas.events.mouse.addHandler($scope.toolbar.draw.pencil.mouseHandler);
			}
		});


		//**************************************************
		// DRAW NOTE TOOLBAR SETUP
		$scope.toolbar.draw.note = {};
		$scope.toolbar.draw.note.mode = false;
		$scope.toolbar.draw.note.toolCode = 'draw-note';
		$scope.toolbar.draw.note.showMenu = false;
		$scope.toolbar.draw.note.color = '#0090ff';
		$scope.toolbar.draw.note.modeStyle = {'background-color':'transparent'};
		$scope.toolbar.draw.note.DrawObject = undefined;
		$scope.toolbar.draw.note.setDrawObject = function setDrawObject(DrawObject) {
			if(PDFViewer.annotations.mode !== 'presenter' && PDFViewer.annotations.mode !== 'presentation') {
				$scope.toolbar.draw.note.DrawObject = DrawObject;
				$scope.toolbar.draw.note.edit.note = DrawObject.getNote();
				$scope.toolbar.draw.note.edit.date = DrawObject.getDate();
				$scope.toolbar.draw.note.edit.username = DrawObject.getAuthor();
				$scope.toolbar.draw.note.edit.style.backgroundColor = DrawColor.colorToPastel(DrawObject.getColor());
				$scope.toolbar.draw.note.setMode(true);
				$scope.toolbar.draw.note.edit.show = true;
				PDFViewer.ignoreArrowKeys = true;
			}
			else {
				console.log(PDFViewer.annotations.mode);
				$scope.toolbar.draw.note.setMode(true);
				$scope.toolbar.draw.note.setMode(false);
			}
		};
		$scope.toolbar.draw.note.colorStyle = {backgroundColor:$scope.toolbar.draw.note.color};
		$scope.toolbar.draw.note.toggleMode = function toggleMode() {
			$scope.toolbar.draw.note.setMode(!$scope.toolbar.draw.note.mode);
		};
		$scope.toolbar.draw.note.toggleMenu = function toggleMenu() {
			$scope.toolbar.draw.note.showMenu = !$scope.toolbar.draw.note.showMenu;
			$scope.toolbar.draw.note.setMode(true);
		};
		$scope.toolbar.draw.note.setMode = function setMode(mode) {
			$scope.toolbar.setTool($scope.toolbar.draw.note.toolCode, mode);
		};
		$scope.toolbar.draw.note.setColor = function setColor(color) {
			$scope.toolbar.draw.note.color = color;
			$scope.toolbar.draw.note.colorStyle = {'background-color':$scope.toolbar.draw.note.color};
			$scope.toolbar.draw.note.showMenu = false;
		};
		$scope.toolbar.draw.note.edit = {};
		$scope.toolbar.draw.note.edit.username = ''; // form data
		$scope.toolbar.draw.note.edit.date = new Date(); // form data
		$scope.toolbar.draw.note.edit.note = ''; // form data
		$scope.toolbar.draw.note.edit.show = false;
		$scope.toolbar.draw.note.edit.style = {backgroundColor:$scope.toolbar.draw.note.color};
		$scope.toolbar.draw.note.edit.toggle = function toggle() {
			$scope.toolbar.draw.note.edit.show = !$scope.toolbar.draw.note.edit.show;
			$scope.toolbar.draw.note.setMode(false);
			PDFViewer.ignoreArrowKeys = false;
		};
		$scope.toolbar.draw.note.edit.save = function save() {
			// save code for notes
			if($scope.toolbar.draw.note.edit.note !== $scope.toolbar.draw.note.DrawObject.getNote()) {
//				$scope.toolbar.draw.undo.add($scope.toolbar.draw.undo.factory.modify($scope.toolbar.draw.note.DrawObject)); // We don't want the note message on the undo stack
				$scope.toolbar.draw.note.DrawObject.setNote($scope.toolbar.draw.note.edit.note);
				$scope.toolbar.draw.note.DrawObject.setDate(new Date());
			}
			$scope.toolbar.draw.note.edit.toggle();
		};
		$scope.toolbar.draw.note.edit.remove = function remove() {
			$scope.toolbar.draw.erase.eraseDrawObject($scope.toolbar.draw.note.DrawObject);
			$scope.toolbar.draw.note.edit.toggle();
		};
		$scope.toolbar.draw.note.mouseDrawHandler = new PDFViewer.Canvas.events.mouse.delegate(this);
		$scope.toolbar.draw.note.mouseDrawHandler.click = function click(event) {
			if($scope.toolbar.getTool() === $scope.toolbar.draw.note.toolCode) {
				event.preventDefault();
				//console.log(PDFViewer.annotations.mode);
				if(PDFViewer.annotations.mode !== 'presenter' && PDFViewer.annotations.mode !== 'presentation') {
					// new draw arrow annotation.
					$scope.toolbar.draw.note.newNote = true;
					var params = DrawObjectDefaultNoteParams();
					params.pointList.push([PDFViewer.Canvas.coord.scaleX, PDFViewer.Canvas.coord.scaleY]);
					params.color = $scope.toolbar.draw.note.color;
					params.note = '';
					params.date = new Date();
					params.pageIndex = Page.index;
					var DrawObject = DrawObjectFactory(params);
					DrawObject.scale = PDFViewer.getScaleFactor();
					DrawObject.existing = false;
					$scope.toolbar.draw.undo.add($scope.toolbar.draw.undo.factory.draw(DrawObject));
					PDFViewer.annotations.pipeline.add(DrawObject);
					$scope.toolbar.draw.note.setDrawObject(DrawObject);
					PDFViewer.Canvas.refresh();
				}
			}
		};
		$scope.toolbar.draw.note.exitCallback = 0;
		$scope.toolbar.draw.note.addExitCallback = function addExitCallback(callback) {
			if(typeof callback === "function") {
				$scope.toolbar.draw.note.exitCallback = callback;
			}
		};
		$scope.$on('ToolChange',function ToolChange(event) {
			$scope.toolbar.draw.note.mode = ($scope.toolbar.getTool() === $scope.toolbar.draw.note.toolCode);
			//$scope.toolbar.draw.note.modeStyle = $scope.toolbar.draw.note.mode ? {'background-color':'#eee'} : {'background-color':'transparent'};
			if(!$scope.toolbar.draw.note.mode) {
				$scope.toolbar.draw.note.showMenu = false;
				if($scope.toolbar.draw.note.exitCallback) {
					$scope.toolbar.draw.note.exitCallback();
					$scope.toolbar.draw.note.exitCallback = 0;
				}
				//$scope.toolbar.draw.note.noteStyle = {'display':'none'};
				//PDFViewer.Canvas.events.mouse.removeHandler($scope.toolbar.draw.note.mouseDrawHandler);
			}
			else {
				//PDFViewer.Canvas.events.mouse.addHandler($scope.toolbar.draw.note.mouseDrawHandler);
			}
		});


		//**************************************************
		// DRAW ARROW TOOLBAR SETUP
		$scope.toolbar.draw.callout = {};
		$scope.toolbar.draw.callout.mode = false;
		$scope.toolbar.draw.callout.toolCode = 'draw-callout';
		$scope.toolbar.draw.callout.showMenu = false;
		$scope.toolbar.draw.callout.color = '#000000';
		$scope.toolbar.draw.callout.lineWidth = 1;
		$scope.toolbar.draw.callout.opacity = 0.8;
		$scope.toolbar.draw.callout.noDraw = false;
		$scope.toolbar.draw.callout.modeStyle = {'background-color':'transparent'};
		$scope.toolbar.draw.callout.DrawInProgress = false;
		$scope.toolbar.draw.callout.DrawObject = undefined;
		$scope.toolbar.draw.callout.DrawObjectOrigin = {x:0,y:0,pixelX:0,pixelY:0,eventX:0,eventY:0};
		$scope.toolbar.draw.callout.colorStyle = {'background-color':$scope.toolbar.draw.callout.color};
		$scope.toolbar.draw.callout.toggleMode = function toggleMode() {
			$scope.toolbar.draw.callout.setMode(!$scope.toolbar.draw.callout.mode);
		};
		$scope.toolbar.draw.callout.toggleMenu = function toggleMenu() {
			$scope.toolbar.draw.callout.showMenu = !$scope.toolbar.draw.callout.showMenu;
			$scope.toolbar.draw.callout.setMode(true);
		};
		$scope.toolbar.draw.callout.setMode = function setMode(mode) {
			if(!mode && $scope.toolbar.draw.callout.mode) {
				$scope.toolbar.draw.callout.noDraw = true;
				// set something to remember we just let go of this, for swipe prevention
				$timeout(function(){$scope.toolbar.setTool($scope.toolbar.draw.callout.toolCode, mode);$scope.toolbar.draw.callout.noDraw=false;},200);
			} else {
				$scope.toolbar.draw.callout.noDraw=false;
				$scope.toolbar.setTool($scope.toolbar.draw.callout.toolCode, mode);
			}
		};
		$scope.toolbar.draw.callout.setColor = function setColor(color) {
			$scope.toolbar.draw.callout.color = color;
			$scope.toolbar.draw.callout.colorStyle = {'background-color':$scope.toolbar.draw.callout.color};
			$scope.toolbar.draw.callout.showMenu = false;
		};
		$scope.toolbar.draw.callout.setLineWidth = function setLineWidth(lineWidth) {
			$scope.toolbar.draw.callout.lineWidth = lineWidth;
		};
		$scope.toolbar.draw.callout.setOpacity = function setOpacity(pOpacity) {
			if(pOpacity > 2.0) {
				pOpacity = pOpacity / 100.0;
			}
			$scope.toolbar.draw.callout.opacity = pOpacity;
		};
		$scope.toolbar.draw.callout.mouseHandler = new PDFViewer.Canvas.events.mouse.delegate(this);
		$scope.toolbar.draw.callout.mouseHandler.click = function click(event) {
			if($scope.toolbar.getTool() === $scope.toolbar.draw.callout.toolCode) {
				$scope.toolbar.draw.callout.toggleMode();
				PDFViewer.Canvas.refresh();
				if(typeof PDFViewer.Toolbar.activityCallback === "function") {
					PDFViewer.Toolbar.activityCallback(true);
					return true;
				}
			}
		};
		$scope.toolbar.draw.callout.mouseHandler.down = function down(event) {
			if($scope.toolbar.getTool() === $scope.toolbar.draw.callout.toolCode) {
				event.preventDefault();
				if(!$scope.toolbar.draw.callout.DrawInProgress && !$scope.toolbar.draw.callout.noDraw) {
					// new draw callout annotation.
					var params = DrawObjectDefaultCalloutParams();
					params.pointList.push([PDFViewer.Canvas.coord.scaleX, PDFViewer.Canvas.coord.scaleY]);
					params.pointList.push([PDFViewer.Canvas.coord.scaleX, PDFViewer.Canvas.coord.scaleY]);
					params.color = $scope.toolbar.draw.callout.color;
					params.lineWidth = $scope.toolbar.draw.callout.lineWidth;
					params.opacity = $scope.toolbar.draw.callout.opacity * 100;
					params.pageIndex = Page.index;
					//var calloutWrapPosition = $(PDFViewer.calloutWrapSelector).position();
					$scope.toolbar.draw.callout.DrawObjectOrigin.x = PDFViewer.Canvas.coord.scaleX;
					$scope.toolbar.draw.callout.DrawObjectOrigin.pixelX = PDFViewer.Canvas.coord.pixelX;
					$scope.toolbar.draw.callout.DrawObjectOrigin.eventX = PDFViewer.Canvas.coord.eventX;
					$scope.toolbar.draw.callout.DrawObjectOrigin.y = PDFViewer.Canvas.coord.scaleY;
					$scope.toolbar.draw.callout.DrawObjectOrigin.pixelY = PDFViewer.Canvas.coord.pixelY;
					$scope.toolbar.draw.callout.DrawObjectOrigin.eventY = PDFViewer.Canvas.coord.eventY;
					$scope.toolbar.draw.callout.DrawObject = DrawObjectFactory(params);
					$scope.toolbar.draw.callout.DrawObject.scale = PDFViewer.getScaleFactor();
				}
			}
		};
		$scope.toolbar.draw.callout.mouseHandler.move = function move(event) {
			if($scope.toolbar.getTool() === $scope.toolbar.draw.callout.toolCode) {
				event.preventDefault();
				if(!$scope.toolbar.draw.callout.DrawInProgress
					&& !$scope.toolbar.draw.callout.noDraw
					&&( $scope.toolbar.draw.callout.DrawObjectOrigin.x !== PDFViewer.Canvas.coord.scaleX
					||  $scope.toolbar.draw.callout.DrawObjectOrigin.y !== PDFViewer.Canvas.coord.scaleY)) {
					//console.log('Added draw of an callout');
					$scope.toolbar.page.showNavBtns = false;
					$scope.toolbar.draw.scroll(false);
					PDFViewer.annotations.pipeline.add($scope.toolbar.draw.callout.DrawObject);
					$scope.toolbar.draw.callout.DrawObjectOrigin.x = 0;
					$scope.toolbar.draw.callout.DrawObjectOrigin.y = 0;
					$scope.toolbar.draw.callout.DrawInProgress = true;
				}
				if($scope.toolbar.draw.callout.DrawObject && $scope.toolbar.draw.callout.DrawInProgress) {
					$scope.toolbar.draw.callout.DrawObject.changeLineEnd(PDFViewer.Canvas.coord.scaleX, PDFViewer.Canvas.coord.scaleY);
					PDFViewer.Canvas.refresh();
				}
			}
		};
		$scope.toolbar.draw.callout.mouseHandler.up = function up( event ) {
			if( $scope.toolbar.getTool() !== $scope.toolbar.draw.callout.toolCode ) {
				return;
			}
			event.preventDefault();
			if( $scope.toolbar.draw.callout.DrawObject && $scope.toolbar.draw.callout.DrawInProgress ) {
				var _callout = Object.assign( {}, $scope.toolbar.draw.callout );
				var _coord = PDFViewer.Canvas.coord;
				var pwScale = PDFViewer.Page.scaleOptionWidth.scale;
				var zoomScale = PDFViewer.Page.getZoomScale();
				var baseScale = 2.0;
				var $window = $( window );
				var $doc = $( document );
				var docScrollX = $doc.scrollLeft();
				var docScrollY = $doc.scrollTop();
				var windowSize = {"width": $window.width(), "height": $window.height()};
				var docSize = {"width": $doc.width(), "height": $doc.height()};
				var pageMargin = 60;
				var maxWidth = Math.min( windowSize.width, docSize.width ) - pageMargin;
				var maxHeight = Math.min( windowSize.height, docSize.height ) - pageMargin;
				var x1 = Math.min( _callout.DrawObject.pointLists[ 0 ][ 0 ], _coord.scaleX );
				var y1 = Math.min( _callout.DrawObject.pointLists[ 0 ][ 1 ], _coord.scaleY );
				var x2 = Math.max( _callout.DrawObject.pointLists[ 0 ][ 0 ], _coord.scaleX );
				var y2 = Math.max( _callout.DrawObject.pointLists[ 0 ][ 1 ], _coord.scaleY );
				var offsetX = _coord.offsetX;
				var offsetY = _coord.offsetY;
				var dragX = Math.min( (_coord.eventX - offsetX), (_callout.DrawObjectOrigin.eventX - offsetX) );
				var dragY = Math.min( (_coord.eventY - offsetX), (_callout.DrawObjectOrigin.eventY - offsetY) );
				var dragW = Math.abs( _coord.eventX - _callout.DrawObjectOrigin.eventX );
				var dragH = Math.abs( _coord.eventY - _callout.DrawObjectOrigin.eventY );
				var viewX = dragX;
				var viewY = dragY;
				var viewW = dragW * baseScale;
				var viewH = dragH * baseScale;
				var bsW = baseScale;
				var bsH = baseScale;
				if( viewW > maxWidth ) {
					var wChange = (maxWidth / viewW);
					bsW *= wChange;
				}
				if( viewH > maxHeight ) {
					var hChange = (maxHeight / viewH);
					bsH *= hChange;
				}
				if( (bsW + bsH) < 4 ) {
					baseScale = Math.min( bsW, bsH );
					viewW = dragW * baseScale;
					viewH = dragH * baseScale;
					// console.log( "up; adjusted baseScale:", baseScale, viewW, viewH );
				}
				if( (viewX + viewW) > maxWidth ) {
					viewX = Math.max( (pageMargin / 2), (maxWidth - viewW) );
				}
				if( (viewY + viewH) > maxHeight ) {
					viewY = Math.max( (pageMargin / 2), (maxHeight - viewH) );
				}
				var pageSize = PDFViewer.Page.size;
				var pwScale = PDFViewer.Page.scaleOptionWidth.scale;
				var pageScale = {
					"width": (pageSize.width * pwScale),
					"height": (pageSize.height * pwScale)
				};
				var canvasOffset = PDFViewer.Canvas.getCanvasOffset();
				_callout.DrawObject.changeLineStart( x1, y1 );
				_callout.DrawObject.changeLineEnd( x2, y2 );
				_callout.DrawObject.existing = false;
				_callout.DrawObject.unhittable = true;
				_callout.DrawObject.deselect();
				_callout.DrawObject.refresh();
				_callout.DrawObject.setTargetHostPageRotation( PDFViewer.getHostPageRotation( Page.index ) );
				_callout.DrawObject.createBoundingBox();
				var nCallout = new DrawCalloutStruct();
				nCallout.Index = PDFViewer.getValidCalloutIndex();
				nCallout.Bounds.left = ((viewX - canvasOffset.left) / pageScale.width);
				nCallout.Bounds.top = ((viewY - canvasOffset.top) / pageScale.height);
				nCallout.Bounds.right = (viewW / pageScale.width);
				nCallout.Bounds.bottom = (viewH / pageScale.height);
				nCallout.Initial.left = ((viewX - canvasOffset.left) / pageScale.width);
				nCallout.Initial.top = ((viewY - canvasOffset.top) / pageScale.height);
				nCallout.Initial.right = (viewW / pageScale.width);
				nCallout.Initial.bottom = (viewH / pageScale.height);
				nCallout.drag.left = (dragX / pageScale.width);
				nCallout.drag.top = (dragY / pageScale.height);
				nCallout.drag.right = (dragW / pageScale.width);
				nCallout.drag.bottom = (dragH / pageScale.height);
				nCallout.Origin.x = nCallout.Initial.left;
				nCallout.Origin.y = nCallout.Initial.top;
				nCallout.pageOrientation = PDFViewer.Page.orientation.orientation;
				nCallout.scale = (zoomScale / pwScale);
				nCallout.zIndex = (PDFViewer.annotations.callouts.callouts.length + 4);
				// console.log( "callout.up; nCallout:", nCallout );
				var params = {
					"Index": nCallout.Index,
					"PDFViewer": PDFViewer,
					"DrawObject": _callout.DrawObject,
					"page": Page.index,
					"viewX": viewX,
					"viewY": viewY,
					"viewW": viewW,
					"viewH": viewH,
					"viewUrl": Page.imagePath,
					"scale": nCallout.scale,
					"drawable": true,
					"resizable": true,
					"moveable": true,
					"closeable": true,
					"origin": "draw",
					"pageOrientation": PDFViewer.Page.orientation.orientation,
					"zIndex": nCallout.zIndex
				};
				_callout.DrawCallout = DrawCalloutFactory( params, nCallout );
				PDFViewer.annotations.callouts.add( _callout.DrawCallout );
				_callout.DrawCallout.setDirty( true );
				_callout.DrawCallout.sendAdd();
				$scope.toolbar.draw.callout.DrawObject = undefined;
				$scope.toolbar.draw.callout.DrawInProgress = false;
			}
			$scope.toolbar.page.showNavBtns = true;
			$scope.toolbar.draw.scroll( true );
			PDFViewer.Canvas.refresh();
		};
		$scope.toolbar.draw.callout.exitCallback = 0;
		$scope.toolbar.draw.callout.addExitCallback = function addExitCallback(callback) {
			if(typeof callback === "function") {
				$scope.toolbar.draw.callout.exitCallback = callback;
			}
		};
		$scope.$on('ToolChange',function ToolChange(event) {
			$scope.toolbar.draw.callout.mode = ($scope.toolbar.getTool() === $scope.toolbar.draw.callout.toolCode);
			//$scope.toolbar.draw.callout.modeStyle = $scope.toolbar.draw.callout.mode ? {'background-color':'#eee'} : {'background-color':'transparent'};
			if(!$scope.toolbar.draw.callout.mode) {
				$scope.toolbar.draw.callout.showMenu = false;
				if($scope.toolbar.draw.callout.exitCallback) {
					$scope.toolbar.draw.callout.exitCallback();
					$scope.toolbar.draw.callout.exitCallback = 0;
				}
			}
		});



		//**************************************************
		// DRAW ERASE TOOLBAR SETUP
		$scope.toolbar.draw.erase = {};
		$scope.toolbar.draw.erase.mode = false;
		$scope.toolbar.draw.erase.toolCode = 'draw-erase';
		$scope.toolbar.draw.erase.modeStyle = {'background-color':'transparent'};
		$scope.toolbar.draw.erase.eraseDrawObject = function eraseDrawObject(DrawObject){
			// remove it from the pipeline
			PDFViewer.annotations.pipeline.remove(DrawObject);
			// add it to the undo stack
			$scope.toolbar.draw.undo.add($scope.toolbar.draw.undo.factory.erase(DrawObject));
			PDFViewer.triggerViewerAPIPresenter('DeleteAnnotation', DrawObject.sendDelete(Page.index));
			// refresh the canvas after erase
			PDFViewer.Canvas.refresh();
		};
		$scope.toolbar.draw.erase.toggleMode = function toggleMode() {
			$scope.toolbar.draw.erase.setMode(!$scope.toolbar.draw.erase.mode);
		};
		$scope.toolbar.draw.erase.setMode = function setMode(mode) {
			$scope.toolbar.setTool($scope.toolbar.draw.erase.toolCode, mode);
		};
		$scope.toolbar.draw.erase.mouseHandler = new PDFViewer.Canvas.events.mouse.delegate(this);
		$scope.toolbar.draw.erase.mouseHandler.click = function click(event) {
			if($scope.toolbar.getTool() === $scope.toolbar.draw.erase.toolCode) {
				var DrawObject = PDFViewer.annotations.pipeline.getObj(PDFViewer.Canvas.coord.scaleX, PDFViewer.Canvas.coord.scaleY);
				if(DrawObject) {
					$scope.toolbar.draw.erase.eraseDrawObject(DrawObject);
				}
				if(typeof PDFViewer.Toolbar.activityCallback === "function") {
					PDFViewer.Toolbar.activityCallback(true);
					return true;
				}
			}
		};
		$scope.toolbar.draw.erase.exitCallback = 0;
		$scope.toolbar.draw.erase.addExitCallback = function addExitCallback(callback) {
			if(typeof callback === "function") {
				$scope.toolbar.draw.erase.exitCallback = callback;
			}
		};
		$scope.$on('ToolChange',function ToolChange(event) {
			$scope.toolbar.draw.erase.mode = ($scope.toolbar.getTool() === $scope.toolbar.draw.erase.toolCode);
			//$scope.toolbar.draw.erase.modeStyle = $scope.toolbar.draw.erase.mode ? {'background-color':'#eee'} : {'background-color':'transparent'};
			if(!$scope.toolbar.draw.erase.mode) {
				if($scope.toolbar.draw.erase.exitCallback) {
					$scope.toolbar.draw.erase.exitCallback();
					$scope.toolbar.draw.erase.exitCallback = 0;
				}
				//PDFViewer.Canvas.events.mouse.addHandler($scope.toolbar.draw.erase.mouseHandler);
			}
			else {
				//PDFViewer.Canvas.events.mouse.removeHandler($scope.toolbar.draw.erase.mouseHandler);
			}
		});



		//**************************************************
		// DRAW UNDO TOOLBAR SETUP
		$scope.toolbar.draw.undo = {};
		$scope.toolbar.draw.undo.click = function click() {
			$scope.toolbar.draw.modify.menu.hideAll();
			$scope.toolbar.resetTools();
			if(angular.isArray(PDFViewer.annotations.undo) && PDFViewer.annotations.undo.length > 0) {
				var item = PDFViewer.annotations.undo.pop();
				item.undo(PDFViewer.annotations.pipeline);
				PDFViewer.triggerViewerAPISavable(PDFViewer.annotations.hasChanges());
			}
			PDFViewer.Canvas.tickCount = 0;
			PDFViewer.Canvas.refresh();
		};
		$scope.toolbar.draw.undo.add = function add(item) {
			PDFViewer.annotations.undo.push(item);
			PDFViewer.triggerViewerAPISavable(PDFViewer.annotations.hasChanges());
		};
		$scope.toolbar.draw.undo.factory = {};
		$scope.toolbar.draw.undo.factory.erase = function erase(DrawObject) {
			// box up the draw object
			var EraseObject = {};
			EraseObject.DrawObject = DrawObject;
			//EraseObject.saveState = 'undo';
			EraseObject.undo = function undo(DrawPipeline) {
				// method to perform the undo
				DrawPipeline.add(EraseObject.DrawObject);
			};
			EraseObject.getOp = function getOp(state) {
				var undoObj = false;
				if(typeof state === "undefined" || state === null) {
					state = 'annotate';
				}
				switch(state) {
					case 'annotate':
						undoObj = EraseObject.getSaveOp();
						break;
					case 'presentation':
						undoObj = EraseObject.getPresentOp();
						break;
				}
				return undoObj;
			};
			EraseObject.getSaveOp = function getSaveOp() {
				var origin = EraseObject.DrawObject.getOrigin();
				if(EraseObject.DrawObject.existing === false) {
					// this is a draw then delete, no need to process.
					return false;
				}
				var undoObj = EraseObject.DrawObject.getJsonData(PDFViewer.getScaleFactor(), PDFViewer.getHostPageRotation(EraseObject.DrawObject.pageIndex));
				if(undoObj === false)
					return undoObj;
				undoObj.op = 'del';
				return undoObj;
			};
			EraseObject.getPresentOp = function getPresentOp() {
				var origin = EraseObject.DrawObject.getOrigin();
				if(EraseObject.DrawObject.existing === false) {
					// this is a draw then delete, no need to process.
					return false;
				}
				var undoObj = EraseObject.DrawObject.sendDelete();
				if(undoObj === false)
					return undoObj;
				undoObj.Message = 'DeleteAnnotation';
				return undoObj;
			};
			EraseObject.getOrigin = function getOrigin() {
				return EraseObject.DrawObject.getOrigin();
			};
			EraseObject.getDrawObject = function getDrawObject() {
				return EraseObject.DrawObject;
			};
			return EraseObject;
		};
		$scope.toolbar.draw.undo.factory.draw = function draw(DrawObject) {
			// box up the draw object
			var BoxedObject = {};
			BoxedObject.DrawObject = DrawObject;
			BoxedObject.undo = function undo(DrawPipeline) {
				// method to perform the undo
				DrawPipeline.remove(BoxedObject.DrawObject);
				delete BoxedObject.DrawObject;
			};
			BoxedObject.getOp = function getOp(state) {
				var undoObj = false;
				if(typeof state === "undefined" || state === null) {
					state = 'annotate';
				}
				switch(state) {
					case 'annotate':
						undoObj = BoxedObject.getSaveOp();
						break;
					case 'presentation':
						undoObj = BoxedObject.getPresentOp();
						break;
				}
				return undoObj;
			};
			BoxedObject.getSaveOp = function getSaveOp() {
				var undoObj = BoxedObject.DrawObject.getJsonData(PDFViewer.getScaleFactor(), PDFViewer.getHostPageRotation(BoxedObject.DrawObject.pageIndex));
				if(undoObj === false)
					return undoObj;
				if(BoxedObject.DrawObject.existing === true)
					undoObj.op = 'mod';
				else
					undoObj.op = 'add';
				return undoObj;
			};
			BoxedObject.getPresentOp = function getPresentOp() {
				var undoObj = {};
				if(BoxedObject.DrawObject.existing === true) {
					BoxedObject.DrawObject.setTargetHostPageRotation(PDFViewer.getHostPageRotation(BoxedObject.DrawObject.pageIndex));
					undoObj = BoxedObject.DrawObject.sendModify();
					if(undoObj === false)
						return undoObj;
					undoObj.Operation = 'mod';
					undoObj.Message = 'ModifyAnnotation';
				} else {
					BoxedObject.DrawObject.setTargetHostPageRotation(PDFViewer.getHostPageRotation(BoxedObject.DrawObject.pageIndex));
					undoObj = BoxedObject.DrawObject.sendDraw();
					if(undoObj === false)
						return undoObj;
					undoObj.Operation = 'add';
					undoObj.Message = 'AddAnnotation';
				}
				return undoObj;
			};
			BoxedObject.getOrigin = function getOrigin() {
				return BoxedObject.DrawObject.getOrigin();
			};
			BoxedObject.getDrawObject = function getDrawObject() {
				return BoxedObject.DrawObject;
			};
			return BoxedObject;
		};
		$scope.toolbar.draw.undo.factory.modify = function modify(DrawObject) {
			// box up the draw object
			var BoxedObject = {};
			BoxedObject.LiveDrawObject = DrawObject;
			BoxedObject.OldDrawObject = DrawObject.clone();
			BoxedObject.undo = function undo(DrawPipeline) {
				BoxedObject.LiveDrawObject.inject(BoxedObject.OldDrawObject);
			};
			BoxedObject.getOp = function getOp(state) {
				var undoObj = false;
				if(typeof state === "undefined" || state === null) {
					state = 'annotate';
				}
				switch(state) {
					case 'annotate':
						undoObj = BoxedObject.getSaveOp();
						break;
					case 'presentation':
						undoObj = BoxedObject.getPresentOp();
						break;
				}
				return undoObj;
			};
			BoxedObject.getSaveOp = function getSaveOp() {
				var undoObj = BoxedObject.LiveDrawObject.getJsonData(PDFViewer.getScaleFactor(), PDFViewer.getHostPageRotation(BoxedObject.LiveDrawObject.pageIndex));
				if(undoObj === false)
					return undoObj;
				if(BoxedObject.LiveDrawObject.existing === true)
					undoObj.op = 'mod';
				else
					undoObj.op = 'add';
				return undoObj;
			};
			BoxedObject.getPresentOp = function getPresentOp() {
				var undoObj = {};
				if(BoxedObject.LiveDrawObject.existing === true) {
					BoxedObject.LiveDrawObject.setTargetHostPageRotation(PDFViewer.getHostPageRotation(BoxedObject.LiveDrawObject.pageIndex));
					undoObj = BoxedObject.LiveDrawObject.sendModify();
					if(undoObj === false)
						return undoObj;
					undoObj.Operation = 'mod';
					undoObj.Message = 'ModifyAnnotation';
				} else {
					BoxedObject.LiveDrawObject.setTargetHostPageRotation(PDFViewer.getHostPageRotation(BoxedObject.LiveDrawObject.pageIndex));
					undoObj = BoxedObject.LiveDrawObject.sendDraw();
					if(undoObj === false)
						return undoObj;
					undoObj.Operation = 'add';
					undoObj.Message = 'AddAnnotation';
				}
				return undoObj;
			};
			BoxedObject.getOrigin = function getOrigin() {
				return BoxedObject.LiveDrawObject.getOrigin();
			};
			BoxedObject.getDrawObject = function getDrawObject() {
				return BoxedObject.LiveDrawObject;
			};
			return BoxedObject;
		};


		//**************************************************
		// DRAW STAMP SETUP
		$scope.toolbar.draw.stamp = Stamp;

		$scope.toolbar.init = function _toolbar_init() {
			$scope.toolbar.setTool($scope.toolbar.noneTool);
		};
		$scope.toolbar.init();

		$scope.$root.$on( "$stateChangeStart", function _scope_root_on_stateChangeStart( event, toState, toParams ) {
			//console.log('State Change Start (Event)',  event.name, toState.name, toState.url, toParams );
			$scope.toolbar.setTool($scope.toolbar.noneTool);
			//if(toParams.sKey) {
				PDFViewer.annotations.setMode('draw');
				$scope.toolbar.setTool($scope.toolbar.noneTool);
			//}
		} );


		//***************************************************
		// THUMBNAILS SETUP
		$scope.showThumbnails = function _PageCtrl_showThumbnails( $event ) {
			console.log("PageCtrl::showThumbnails()", "$event", $event);
			PDFViewer.annotations.closePage(false);
			PDFViewer.viewMode = "thumbnails";
			$state.go( 'Thumbnails', {platform:PDFViewer.platform,fromState:$state.current.name} );
			blurTarget( $event );
			if(typeof PDFViewer.thumbnailModeEventHandler === "function") {
				PDFViewer.thumbnailModeEventHandler();
			}
		};
		PDFViewer.Toolbar.showThumbnails = $scope.showThumbnails;

		$scope.showPage = function showPage( $event ) {
			console.log("PageCtrl::showPage()", "$event", $event);
			PDFViewer.annotations.openPage(PDFViewer.Page.index, true);
			PDFViewer.viewMode = "page";
			$state.go( 'Page' );
			blurTarget( $event );
			if(typeof PDFViewer.pageModeEventHandler === "function") {
				PDFViewer.pageModeEventHandler();
			}
		};
		PDFViewer.Toolbar.showPage = $scope.showPage;
		PDFViewer.Stamp.showPage = $scope.showPage;

		$scope.drawMode = function _scope_drawMode( evt, mode ) {
			switch(mode) {
				case 'arrow':
				case 'highlight':
				case 'pencil':
				case 'note':
				case 'eraser':
					PDFViewer.annotations.mode = 'draw';
					PDFViewer.annotations.drawMode = mode;
					break;
				case 'undo':
					// undo the stuff
				case 'none':
				case '':
				default:
					PDFViewer.annotations.mode = 'draw';
					PDFViewer.annotations.drawMode = mode;
					break;
			}
		};

	}] );

	pdfapp.controller( "ThumbnailsCtrl", ["$scope", "$state", "PDFViewer", function( $scope, $state, PDFViewer ) {
		console.log( "ThumbnailsCtrl; state:", $state.current.name, $state.current.url );
		$scope.PDFViewer = PDFViewer;
		var Page = $scope.Page = PDFViewer.Page;
		var Toolbar = $scope.Toolbar = PDFViewer.Toolbar;

		function Thumbnail() {
			this.pageIndex = null;
			this.pageNumber = null;
			this.style = null;
			this.path = null;
		};

		$scope.boundsStyle = {
			width: Page.bounds.width,
			height: Page.bounds.height-5
		};

		$scope.thumb1 = new Thumbnail();
		$scope.thumb2 = new Thumbnail();
		$scope.thumb3 = new Thumbnail();
		$scope.thumb4 = new Thumbnail();
		//$scope.thumb5 = new Thumbnail();
		$scope.$root.$on( "PageChange", function() {
			buildThumbnails();
		});

		function buildThumbnails() {
			//for( var i=1; i<=5; ++i ) {
			  for( var i=1; i<=4; ++i ) {
				var tn = $scope["thumb" + i];
				tn.path = null;
				tn.pageIndex = ((Page.index + i) - 1);
				if(PDFViewer.pages[tn.pageIndex] && PDFViewer.pages[tn.pageIndex].size) {
					var pg = PDFViewer.pages[tn.pageIndex].size;
					tn.pageNumber = (tn.pageIndex + 1);
					tn.style = spinnerStyle( tn.pageIndex );
					tn.path = ["/", PDFViewer.platform, "/", PDFViewer.routeID, "/image/", tn.pageIndex, "/thumb/thumb.jpg"].join( "" );
				}
			}
		};

		if( $state.current.name === "Thumbnails" ) {
			PDFViewer.ignoreArrowKeys = false;
			buildThumbnails();
		}

		$scope.selectPage = function selectPage( pageNum ) {
			if( PDFViewer.selectPage( pageNum ) ) {
				PDFViewer.Toolbar.showPage();
			}
		};

		$scope.didLoad = function didLoad( $event, thumb ) {
			if( !angular.isDefined( thumb ) || !angular.isDefined( thumb.path ) ) {
				return;
			}
			thumb.style["background-image"] = "url(" + thumb.path + ")";
			thumb.style["background-size"] = "100% 100%";
			thumb.style["background-position"] = "0 0";
		};

		$scope.thumbClick = function thumbClick(event) {
			if(typeof Toolbar.activityCallback === "function") {
				Toolbar.activityCallback(false);
			}
		};

		$scope.swipePreviousPage = function swipePreviousPage(event) {
			event.preventDefault();
			PDFViewer.previousPage();
			PDFViewer.safeApply();
		};

		$scope.swipeNextPage = function swipeNextPage(event) {
			event.preventDefault();
			PDFViewer.nextPage();
			PDFViewer.safeApply();
		};

		function spinnerStyle( pageIndex ) {
			var page = PDFViewer.pages[pageIndex].size;
			if( !angular.isDefined( page ) ) {
				return {};
			}
			var height = Math.round( (page.height / (page.width / 250)) );
			return {
				"background-image": "url('/img/spin64.gif')",
				"background-size": "32px 32px",
				"background-position": "50% 50%",
				"width": "250px",
				"height": height + "px"
			};
		};
	}] );

	// TODO: not being utilized, remove?
	pdfapp.run( ["$rootScope", "$state", "PDFViewer", function( $rootScope, $state, PDFViewer ) {
		$rootScope.$on( "$stateChangeStart", function( event, toState, toParams ) {
			//console.log('State Change Start',  event.name, toState.name, toState.url, toParams );
			if( angular.isDefined( toParams.platform ) ) {
				console.log( "$stateChangeStart; platform:", toParams.platform, toParams.fromState );
				if((typeof PDFViewer.platform === "undefined" || PDFViewer.platform === "err") && typeof toParams.platform !== "undefined" && toParams.platform !== null && toParams.platform !== "") {
					PDFViewer.platform = toParams.platform;
				}
				if(typeof PDFViewer.fromState === "undefined" && typeof toParams.fromState !== "undefined"
						&& toParams.fromState !== null && toParams.fromState !== "") {
					PDFViewer.fromState = toParams.fromState;
				}
			}
			if( angular.isDefined( toParams.routeID ) ) {
				console.log( '$stateChangeStart; routeID:', toParams.routeID );
				var routeID = parseInt( toParams.routeID );
				if( !isNaN( routeID ) && routeID >= 0 ) {
					PDFViewer.routeID = routeID;
				}
			}
			if( angular.isDefined( toParams.sessionID ) ) {
				console.log( '$stateChangeStart; sessionID:', toParams.sessionID );
				var sessionID = parseInt( toParams.sessionID );
				if( !isNaN( sessionID ) && sessionID >= 0 ) {
					PDFViewer.sessionID = sessionID;
				}
			}
			if( PDFViewer.sessionID && !PDFViewer.routeID ) {
				PDFViewer.routeID = PDFViewer.sessionID;
			}
		} );
	}] );

	pdfapp.config( ["$stateProvider", "$urlRouterProvider", "$locationProvider", function( $stateProvider, $urlRouterProvider, $locationProvider ) {
		$locationProvider.html5Mode( {enabled: true} );
		$locationProvider.hashPrefix( '!' );
		$urlRouterProvider.otherwise( '/:platform/:routeID/viewer/:sKey' );
		$stateProvider.state( 'Page', {
			url: '/:platform/:routeID/viewer/:sKey',
			views: {
				'init@': {template: '<ui-view/>', controller: 'ViewerCtrl'},
				'pdfToolbar@': {templateUrl: 'views/pdfToolbar.html'},
				'page@': {templateUrl: 'views/page.html'},
				'pageNumDialog@': {templateUrl: 'views/pageNumDialog.html'},
				'presentPaused@': {templateUrl: 'views/presentPaused.html'},
				'pageArrowBtns@': {templateUrl: 'views/pageArrowBtns.html'},
				'callouts@': {templateUrl: 'views/callouts.html'},
				'annotateNoteDialog@': {templateUrl: 'views/annotateNoteDialog.html'},
				'annotateClear@': {templateUrl: 'views/annotateClear.html'},
				'stampDialog@': {templateUrl: 'views/stampDialog.html'}
			},
			params: {
				platform: {value: null, squash: true},
				routeID: {value: null, squash: true},
				sKey: {value: null, squash: true}
			}
		} );
		$stateProvider.state( 'Thumbnails', {
			url: '/:platform/:routeID/viewer/thumbnails/',
			views: {
				'init@': {template: '<ui-view/>', controller: 'ViewerCtrl'},
				'pdfToolbar@': {templateUrl: 'views/pdfToolbar.html'},
				'thumbnails@': {templateUrl: 'views/thumbnails.html'},
				'pageNumDialog@': {templateUrl: 'views/pageNumDialog.html'},
				'presentPaused@': {templateUrl: 'views/presentPaused.html'},
				'pageArrowBtns@': {templateUrl: 'views/pageArrowBtns.html'}
			},
			params: {
				platform: {value: null, squash: true},
				routeID: {value: null, squash: true},
				fromState: {value: null, squash: true}
			}
		} );
		$stateProvider.state( 'Presentation', {
			url: '/:platform/:routeID/presentation/:sessionID/:sKey',
			views: {
				'init@': {template: '<ui-view/>', controller: 'ViewerCtrl'},
				'pdfToolbar@': {templateUrl: 'views/pdfToolbar.html'},
				'page@': {templateUrl: 'views/page.html'},
				'pageNumDialog@': {templateUrl: 'views/pageNumDialog.html'},
				'presentPaused@': {templateUrl: 'views/presentPaused.html'},
				'pageArrowBtns@': {templateUrl: 'views/pageArrowBtns.html'},
				'callouts@': {templateUrl: 'views/callouts.html'}
				//,'annotateNoteDialog@': {templateUrl: 'views/annotateNoteDialog.html'} // would they want to see notes?
			},
			params: {
				platform: {value: null, squash: true},
				routeID: {value: null, squash: true},
				sessionID: {value: null, squash: true}
			}
		} );
		$stateProvider.state( 'Presenter', {
			url: '/:platform/:routeID/presenter/:sessionID/:sKey',
			views: {
				'init@': {template: '<ui-view/>', controller: 'ViewerCtrl'},
				'pdfToolbar@': {templateUrl: 'views/pdfToolbar.html'},
				'page@': {templateUrl: 'views/page.html'},
				'pageNumDialog@': {templateUrl: 'views/pageNumDialog.html'},
				'presentPaused@': {templateUrl: 'views/presentPaused.html'},
				'pageArrowBtns@': {templateUrl: 'views/pageArrowBtns.html'},
				'callouts@': {templateUrl: 'views/callouts.html'},
				'annotateNoteDialog@': {templateUrl: 'views/annotateNoteDialog.html'},
				'annotateClear@': {templateUrl: 'views/annotateClear.html'},
				'stampDialog@': {templateUrl: 'views/stampDialog.html'}
			},
			params: {
				platform: {value: null, squash: true},
				routeID: {value: null, squash: true},
				sessionID: {value: null, squash: true}
			}
		} );
	}] );

	jQuery( document ).ready( function() {
		//buttons on click should lose focus
		jQuery( "button" ).on( "click", function() {
			console.log( "button.blur()" );
			this.blur();
		} );
	} );

	function blurTarget( e ) {
		if( typeof e === "object" && e !== null && e.hasOwnProperty( "currentTarget" ) && e.currentTarget ) {
			jQuery( e.currentTarget ).blur();
		}
	};

}());
