/**********************************************
 * Copyright by eDepoze 2015
 * All rights reserved.
 */


var DRAWPIPELINE_DRAW_ALL = "all";
var DRAWPIPELINE_DRAW_SELECTED = "selected";
var DRAWPIPELINE_DRAW_UNSELECTED = "unselected";


var DrawPipelineFactory = function DrawPipelineFactory() {

	// make my new factory instance
	var DrawPipeline = new DrawPipelineClass();

	// return the instance
	return DrawPipeline;
};

var DrawPipelineClass = function DrawPipelineClass() {

	/**
	 * The pipeline of draw objects
	 * @type {Array}
	 */
	this.pipeline = [];

	/**
	 * The list of indexes that couldn't be drawn
	 * @type {Array}
	 */
	this.failDraw = [];

	/**
	 * The list of draw calloutList
	 * @type {Array}
	 */
	this.calloutList = DrawCalloutListFactory();

	/**
	 * Avoid the duplicate sigs
	 * @type bool
	 */
	this.avoidDupSigs = true;

};

DrawPipelineClass.prototype.add = function add(DrawObject) {
	if(DrawObject && (DrawObject.toString() === 'DrawObject' || DrawObject.toString() === 'DrawPipeline') && DrawObject !== this) {
		if(DrawObject.toString() === 'DrawPipeline' || !this.avoidDupSigs || this.countSigs(DrawObject.getSig()) === 0) {
			if(DrawObject.zIndex === -1 || DrawObject.toString() === 'DrawPipeline') {
				this.pipeline.push(DrawObject);
			} else {
				var zIndex = parseInt(""+DrawObject.zIndex);
				var inserted = false;
				for(var idx in this.pipeline) {
					if(this.pipeline[idx].zIndex > zIndex) {
						this.pipeline.splice(idx, 0, DrawObject);
						inserted = true;
						break;
					}
				}
				if(!inserted) {
					this.pipeline.push(DrawObject);
				}
			}
			return true;
		}
	}
	return false;
};

DrawPipelineClass.prototype.disableSigChecks = function disableSigChecks() {
	this.avoidDupSigs = false;
	return this;
};

DrawPipelineClass.prototype.enableSigChecks = function enableSigChecks() {
	this.avoidDupSigs = true;
	return this;
};

DrawPipelineClass.prototype.remove = function remove(DrawObject) {
	var index = jQuery.inArray(DrawObject, this.pipeline);
	if(index > -1) {
		this.pipeline.splice(index, 1);
		return true;
	}
	return false;
};

DrawPipelineClass.prototype.countSigs = function countSigs(sig) {
	var count = 0;
	$.each(this.pipeline, function(key, value) {
		if(value.toString() === 'DrawObject') {
			if(value.getSig() === sig)
				++count;
		}
		else if(value.toString() === 'DrawPipeline') {
			count += value.countSigs(sig);
		}
	});
	return count;
};

DrawPipelineClass.prototype.findSig = function findSig(sig) {
	var SigDrawObject = null;
	$.each(this.pipeline, function(key, value) {
		if(value.toString() === 'DrawObject') {
			if(value.sig === sig)
				SigDrawObject = value;
		}
		else if(value.toString() === 'DrawPipeline') {
			var tmp = value.findSig(sig);
			if(tmp !== null)
				SigDrawObject = tmp;
		}
	});
	return SigDrawObject;
};

DrawPipelineClass.prototype.count = function count() {
	return this.pipeline.length;
};

DrawPipelineClass.prototype.clear = function clear() {
	this.pipeline = [];
	return true;
};

DrawPipelineClass.prototype.draw = function draw( canvas, tick, drawType ) {
	// console.log( "DrawPipeline.draw; canvas:", canvas, "tick:", tick, "drawType:", drawType );
	if( typeof drawType === "undefined" ) {
		drawType = DRAWPIPELINE_DRAW_ALL;
	}
	this.failDraw = [];
	for( var idx in this.pipeline ) {
		if( this.pipeline[ idx ].draw( canvas, tick, drawType ) === false) {
			this.failDraw.push( idx );
		}
	}
	this.purgeFail(); // purge the objects that failed to draw
	var dObj = null;
	if( this.calloutList ) {
		for( var i in this.calloutList.callouts ) {
			if( this.calloutList.callouts[ i ].isDestroyed() ) {
				continue;
			}
			if( drawType === DRAWPIPELINE_DRAW_UNSELECTED ) {
				this.calloutList.callouts[ i ].clearCanvas();
			}
			dObj = this.calloutList.callouts[ i ].getCanvasObj();
			if( dObj === null ) {
				//console.log("Null draw object");
				continue;
			}
			for( var p in this.pipeline ) {
				// console.log( "DrawPipline.draw; [", p, "].draw; dObj:", dObj, "i:", i, "Index:", this.calloutList.callouts[ i ].Index, "offsetX:", dObj.offsetX, "offsetY:", dObj.offsetY );
				this.pipeline[ p ].draw( dObj, tick, drawType );
			}
		}
	}
	return true;
};

DrawPipelineClass.prototype.deselectAll = function deselectAll() {
	for(var idx in this.pipeline) {
		this.pipeline[idx].deselect();
	}
};

DrawPipelineClass.prototype.toString = function toString() {
	return 'DrawPipeline';
};

DrawPipelineClass.prototype.purgeFail = function purgeFail() {
	while(this.failDraw.length > 0) {
		var index = this.failDraw.pop();
		this.pipeline.splice(index, 1);
	}
};

DrawPipelineClass.prototype.getObj = function getObj(x, y) {
	for(var idx in this.pipeline) {
		if(this.pipeline[idx].toString() == 'DrawObject') {
			if(this.pipeline[idx].hitSelected(x, y)) {
				return this.pipeline[idx];
			}
		}
		else if(this.pipeline[idx].toString() == 'DrawPipeline') {
			var DrawObject = this.pipeline[idx].getObj(x, y);
			if(DrawObject)
				return DrawObject;
		}
	}
	for(idx in this.pipeline) {
		if(this.pipeline[idx].toString() == 'DrawObject') {
			if(this.pipeline[idx].hit(x, y)) {
				return this.pipeline[idx];
			}
		}
		else if(this.pipeline[idx].toString() == 'DrawPipeline') {
			var DrawObject = this.pipeline[idx].getObj(x, y);
			if(DrawObject)
				return DrawObject;
		}
	}
	return undefined;
};

DrawPipelineClass.prototype.clone = function clone() {
	var DP = DrawPipelineFactory();
	for(var idx in this.pipeline) {
		DP.add(this.pipeline[idx]);
	}
	DP.calloutList = this.calloutList.clone();
	return DP;
};

DrawPipelineClass.prototype.inject = function inject(DP) {
	this.pipeline = [];
	if(DP && DP.toString() === this.toString()) {
		for(var idx in DP.pipeline) {
			this.add(DP.pipeline[idx]);
		};
	}
	if(this.calloutList && DP.toString() === this.calloutList.toString()) {
		this.calloutList.inject(DP.calloutList);
	}
};

DrawPipelineClass.prototype.addCalloutList = function addCalloutList(CalloutList) {
	this.calloutList = CalloutList;
	return true;
};
