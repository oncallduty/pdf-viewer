/* global angular */

var PDF_STATE_ANNOTATE = "annotate";
var PDF_STATE_PRESENTATION = "presentation";
var PDF_STATE_INTRODUCE = "introduce";

function PDFViewerFactory( $rootScope, $http, $timeout, $q ) {


	//viewPort and Scrollbars
	var viewPortSize = {"width":0, "height":0};
	var vp = $(window);
	viewPortSize.width = vp.width();
	viewPortSize.height = vp.height();
	sbm = document.getElementById( "scrollbar-measure" );
	var scrollbarSize = sbm.offsetWidth - sbm.clientWidth;
	document.body.removeChild( sbm );
	vp.resize( function _windowOnResize() {
		var body = $("body");
		body.hide();
		viewPortSize.width = vp.width();
		viewPortSize.height = vp.height();
		appOrientation = (viewPortSize.width > viewPortSize.height) ? LANDSCAPE : PORTRAIT;
		PDFViewer.Page.appOrientationClasses();
		body.show();
		//console.log( "window.body.resize;", viewPortSize );
		if( Page ) {
			Page.updateDimensions();
		}
	} );
	delete sbm;

	//const
	var PORTRAIT = "portrait";
	var LANDSCAPE = "landscape";

	var Page = new PageClass();
	var Toolbar = new ToolbarClass();
	var Canvas = new CanvasClass();
	var Stamp = new StampClass();
	var PDFViewer =	new PDFViewerClass();
	var pdfViewerAPI = new PDFViewerAPIClass();
	var appOrientation = (viewPortSize.width > viewPortSize.height) ? LANDSCAPE : PORTRAIT;
	var appOrientationDelegate = null;

	if( typeof window.top.webApp === "object" && window.top.webApp !== null ) {
		window.top.webApp.bindViewer( pdfViewerAPI );
	}
	if( typeof window.top.webMgr === "object" && window.top.webMgr !== null ) {
		window.top.webMgr.bindViewer( pdfViewerAPI );
	}

	function PDFViewerClass() {
		var PDFViewer = this;
		var scaleFactor = 1.5;
		this.fileHash = "err";
		this.internalReferenceID = Math.floor(Math.random() * 1000.0);
		this.safeApply = function safeApply() {
			$timeout( function() {} );
		};
		this.platform = "err";
		this.routeID = 0;
		this.sessionID = 0;
		this.calloutWrapStyle = {position:'fixed',left:0,top:0};
		var _calloutIndex = 99;
		this.annotations = {
			mode: 'view', // view, draw, presentation, presenter, annotator
			drawMode: 'none', // arrow, highlight, pencil, note, erase
			undo: [], // array of undo objects to step back annotations
			pipeline: DrawPipelineFactory(),
			callouts: DrawCalloutListFactory(),
			page: -1,
			fetchedPages: {},
			firstLoad: true,
			stash: StashFactory(),
			isStash: function isStash(pageIndex) {
				return PDFViewer.annotations.stash.isStash(pageIndex);
			},
			openPage: function openPage(newPage, force) {
				//console.log("PDFViewer::annotations::openPage()", "newPage:", newPage, "force:", force);
				if(typeof force === "undefined") {
					force = false;
				}
				if(newPage === PDFViewer.annotations.page && !force) {
					return;
				}
				PDFViewer.annotations.page = newPage;
				var stash = PDFViewer.annotations.stash.unstash(PDFViewer.annotations.page);
				PDFViewer.annotations.pipeline.inject(stash.pipeline);
				PDFViewer.annotations.undo = stash.undo;
				PDFViewer.annotations.callouts.inject(stash.callouts);
				PDFViewer.drawPipeline.addCalloutList(PDFViewer.annotations.callouts);
				if(PDFViewer.annotations.fetchedPages[PDFViewer.annotations.page] !== true) {
					PDFViewer.annotations.fetchedPages[PDFViewer.annotations.page] = true;
					PDFViewer.fetchAnnotations(PDFViewer.annotations.page);
				}
				PDFViewer.Canvas.tickCount = 0;
				PDFViewer.Canvas.refresh();
				PDFViewer.safeApply();
			},
			closePage: function closePage(redraw) {
				//console.log("PDFViewer::annotations::closePage()", "redraw:", redraw);
				if(PDFViewer.annotations.fetchedPages[PDFViewer.annotations.page] === true
						&& PDFViewer.annotations.page >= 0) {
					PDFViewer.annotations.stash.stash(PDFViewer.annotations.page, PDFViewer.annotations.pipeline, PDFViewer.annotations.undo, PDFViewer.annotations.callouts);
					PDFViewer.annotations.pipeline.clear();
					PDFViewer.annotations.undo = [];
					PDFViewer.annotations.page = -1;
				}
				if(redraw) {
					PDFViewer.Canvas.refresh();
				}
			},
			switchPage: function switchPage(newPage) {
				PDFViewer.annotations.closePage(false);
				PDFViewer.annotations.openPage(newPage);
			},
			clearPage: function clearPage() {
				var pipelineCopy = PDFViewer.annotations.pipeline.pipeline.slice();
				angular.forEach(pipelineCopy, function(DrawObject,index){
					if(isset(DrawObject) && typeof DrawObject.toString === "function" && DrawObject.toString() === "DrawObject") {
						var nextUndo = Toolbar.draw.undo.factory.erase(DrawObject);
						PDFViewer.annotations.undo.push(nextUndo);
						PDFViewer.annotations.pipeline.remove(DrawObject);
					}
				});
			},
			undoAll: function undoAll() {
				// clear all in undo stacks on all pages
				// do not clear out the annotations loaded from the fetch
				PDFViewer.annotations.stash.undoAll();
				var currStash = PDFViewer.annotations.stash.unstash(Page.index);
				PDFViewer.annotations.undo = currStash.undo;
				PDFViewer.annotations.pipeline.inject(currStash.pipeline);
				PDFViewer.annotations.callouts = currStash.callouts;
				PDFViewer.Canvas.refresh();
			},
			hasChanges: function hasChanges(requestedState) {
				// we don't want to re-stash a page that has already been stashed when going into thumbnail view.
				if(typeof requestedState === "undefined" || requestedState === null) {
					requestedState = PDF_STATE_ANNOTATE;
				}
				if(PDFViewer.viewMode !== "thumbnails") {
					PDFViewer.annotations.stash.stash(Page.index, PDFViewer.annotations.pipeline, PDFViewer.annotations.undo, PDFViewer.annotations.callouts);
				}
				return PDFViewer.annotations.stash.hasChanges(PDFViewer.pages.length, requestedState);
			},
			hasCallouts: function hasCallouts() {
				return PDFViewer.annotations.stash.hasCallouts(PDFViewer.pages.length);
			},
			getChangeState: function getChangeState(requestedState) {
				if(typeof requestedState === "undefined" || requestedState === null) {
					requestedState = PDF_STATE_ANNOTATE;
				}
				PDFViewer.annotations.stash.stash(Page.index, PDFViewer.annotations.pipeline, PDFViewer.annotations.undo, PDFViewer.annotations.callouts);
				return PDFViewer.annotations.stash.getChangeState(PDFViewer.pages.length, requestedState);
			},
			catchUp: function catchUp(ChangeState) {
				PDFViewer.annotations.stash.applyState(PDFViewer.pages.length);
				PDFViewer.annotations.openPage(Page.index,true);
			},
			setMode: function setMode(mode) {
				switch(mode) {
					case 'presentation':
						PDFViewer.annotations.mode = mode;
						PDFViewer.annotations.callouts.setMoveable(false);
						PDFViewer.annotations.callouts.setResizable(false);
						PDFViewer.annotations.callouts.setCloseable(false);
						PDFViewer.annotations.callouts.setDrawable(false);
						PDFViewer.Toolbar.rotate.orientationStyle.marginTop = 0;
						$('body').css('overflow', 'hidden');
						break;
					case 'view':
					case 'draw':
					case 'presenter':
						PDFViewer.annotations.callouts.setResizable(true);
						PDFViewer.annotations.callouts.setCloseable(true);
					case 'annotator':
						PDFViewer.annotations.mode = mode;
						PDFViewer.annotations.callouts.setDrawable(true);
						PDFViewer.annotations.callouts.setMoveable(true);
						PDFViewer.Toolbar.rotate.orientationStyle.marginTop = 31;
						$('body').css('overflow', 'auto');
						break;
				}
				PDFViewer.annotations.pipeline.deselectAll();
				PDFViewer.Toolbar.setTool('none',true);
				PDFViewer.safeApply();
				Canvas.refresh();
			}
		};
		this.presentation = {
			serverZoom: 1.0,
			firstLoad: {},
			presenterState: {
				firstLoad: true,
				preventPageUpdate: false,
				pageIndex: -1,
				orientation: -1,
				pageOrientation: -1,
				pageScale: -1,
				pageOffset: -1,
				pageOffsetX: -1,
				pageOffsetY: -1,
				rectSizeWidth: -1,
				rectSizeHeight: -1
			}
		};
		var searchKeywordsArray = [];
		this.viewMode = "page";
		//console.log("   QQ", "*Block*");
		this.Page = Page;
		this.Canvas = Canvas;
		this.Toolbar = Toolbar;
		this.Stamp = Stamp;
		this.pages = [];
		this.isLoaded = false;
		this.loadQueue = [];
		this.elementLoadImg = document.createElement("img");
		this.ignoreArrowKeys = false;
		this.pauseIndicator = false;
		this.searchResults = {
			pipeline: DrawPipelineFactory().disableSigChecks(),
			cursor: -1,
			rects: [],
			pageIndex: -1,
			countBase: 1,
			keywords: "",
			hits: [],
			documentHits: 0,
			cursorLast: false,
			eventPromise: undefined,
			active: false
		};
		this.preventPageChange = false;

		this.drawPipeline = DrawPipelineFactory();
		this.drawPipeline.add(this.annotations.pipeline);
		this.drawPipeline.add(this.searchResults.pipeline);
		this.drawPipeline.add(this.Stamp.pipeline);
		this.drawPipeline.addCalloutList(this.annotations.callouts);

		function loadPage() {
			//console.log("   QQ", "*LoadPage*", "Page:", Page.index+1);


			
			if(PDFViewer.Page.orientation.getOrientation() == 0)
			{
			 // $("#pdf-toolbar-wrap").css({"background-color":"yellow","font-size":"300%"});
			  //setAppOrientation("portrait");
			  Page.setZoomScale( 1 );
			  Page.updateDimensions();

			}
			else
			{
			  //$("#pdf-toolbar-wrap").css({"background-color":"pink","font-size":"200%"});
			  //setAppOrientation("landscape");
			  Page.setZoomScale( 0.5 );
			  Page.updateDimensions();
			}







			PDFViewer.lastLoadStarted = Date.now();
			PDFViewer.searchResults.pipeline.clear();
			PDFViewer.annotations.closePage(true);
			Toolbar.draw.arrow.setMode(false);
			Toolbar.draw.highlight.setMode(false);
			Toolbar.draw.pencil.setMode(false);
			Toolbar.draw.note.setMode(false);
			Toolbar.draw.erase.setMode(false);
			Toolbar.draw.callout.setMode(false);
			Page.size = PDFViewer.pages[Page.index].size;
			Page.number = Page.index + 1;
			Page.calcPageScale();
			Page.zoomMode();
			loadImage(getImageSrc(Page.index));
			$rootScope.$emit( 'PageChange', Page );
			PDFViewer.scrollTo(0,0);
			if( !PDFViewer.isSearchEmpty(searchKeywordsArray) ) {
				PDFViewer.search( searchKeywordsArray, false );
			}
			if(typeof PDFViewer.pageModeEventHandler === "function" && PDFViewer.viewMode === "page") {
				PDFViewer.pageModeEventHandler();
			}
		};

		function getImageSrc(pageIndex) {
			return ["/", PDFViewer.platform, "/", PDFViewer.routeID, "/image/", pageIndex, "/", Page.scaleType, "/", PDFViewer.fileHash, "/img.jpg"].join( "" );
		};

		var loadImageLastSrc = '';
		function loadImage(src) {
			//console.log("   QQ", "*Block*");
			PDFViewer.isLoaded = false;
			if(src !== loadImageLastSrc) {
				Page.canvasStyle = Page.spinnerStyle();
			}
			PDFViewer.elementLoadImg.src = Page.imagePath = src;
			loadImageLastSrc = src;
			didValidate();
		};

		function didValidate(left) {
			if(typeof left === "undefined") {
				left = 7;
			}
			if(left === 0) {
				//console.log("   QQ", "*End*");
				return;
			}
			$timeout(function() {
				if(PDFViewer.elementLoadImg.complete || PDFViewer.elementLoadImg.width+PDFViewer.elementLoadImg.height > 0) {
					//console.log("   QQ", "*Valid*", 8-left, "attempt");
					PDFViewer.didComplete(PDFViewer.elementLoadImg.src);
				} else {
					didValidate(left-1);
				}
			}, 1000);
		};

		this.didComplete = function didComplete(src) {
			var path = src.replace(/^.*\/\/[^\/]+/, '');//remove domain
			if(this.didComplete.lastSrc === path) {
				return;
			} else {
				this.didComplete.lastSrc = path;
			}
			//console.log("   QQ", "*UnBlock*", src);
			PDFViewer.isLoaded = true;
			PDFViewer.triggerViewerOnLoad();
			Page.updateDimensions();
			if($.inArray(PDFViewer.annotations.mode, ['presentation', 'presenter', 'annotator']) === -1) {
				Page.zoomMode();
			} else {
				Page.zoomPresentation();
			}
			Page.pageStyle.width = Page.scale.width;
			Page.pageStyle.height = Page.scale.height;
			Page.canvasStyle = Page.pageImgStyle(src);
			//Page.imagePath = "";
			PDFViewer.annotations.openPage(Page.index);
			if(PDFViewer.annotations.mode === 'presentation') {
				PDFViewer.presentationAlign();
			} else {
				PDFViewer.presenterPageUpdate();
			}
		}

		function processLoadQueue() {
			while(PDFViewer.loadQueue.length > 0) {
//				if(!PDFViewer.isLoaded) {
//					console.log("   QQ", "*Stop*", "Left:", PDFViewer.loadQueue.length);
//					return;
//				}
				var element = PDFViewer.loadQueue.shift();
				//console.log("   QQ", "*Process*", element);
				switch(element.action) {
					case 'PresentationAdd':
						PDFViewer.presentationAdd(element.data);
						break;
					case 'PresentationModify':
						PDFViewer.presentationModify(element.data);
						break;
					case 'PresentationDelete':
						PDFViewer.presentationDelete(element.data);
						break;
					case 'PresentationClearAll':
						PDFViewer.presentationClearAll(element.data);
						break;
					case 'PresentationPage':
						PDFViewer.presentationPageAdd(element.data);
						break;
					case 'PresentationAlign':
						PDFViewer.presentationAlign();
						break;
				}
			}
		};

		this.triggerViewerOnLoad = function triggerViewerOnLoad() {
			processLoadQueue();
		};

		this.selectPage = function selectPage( pageNum ) {
			var pageIndex = parseInt( pageNum ) - 1;
			if( !isNaN( pageIndex ) && PDFViewer.pages[pageIndex] && PDFViewer.pages[pageIndex].size ) {
				Page.index = pageIndex;
				Page.setPresentationServerZoom(Page.calcPageWidthFit());
				loadPage();
				return true;
			}
			return false;
		};

		this.nextPage = function nextPage() {
			if(PDFViewer.preventPageChange) {
				return false;
			}
			var pageIndex = parseInt( Page.index ) + 1;
			if( !isNaN( pageIndex ) && PDFViewer.pages[pageIndex] && PDFViewer.pages[pageIndex].size ) {
				Page.index = pageIndex;
				Page.setPresentationServerZoom(Page.calcPageWidthFit());
				loadPage();
				return true;
			}
			return false;
		};

		this.previousPage = function previousPage() {
			if(PDFViewer.preventPageChange) {
				return false;
			}
			var pageIndex = parseInt( Page.index ) - 1;
			if( !isNaN( pageIndex ) && PDFViewer.pages[pageIndex] && PDFViewer.pages[pageIndex].size ) {
				Page.index = pageIndex;
				Page.setPresentationServerZoom(Page.calcPageWidthFit());
				loadPage();
				return true;
			}
			return false;
		};

		this.fetchAnnotations = function fetchAnnotations( pageIndex ) {
			//console.log('PDFViewer.fetchAnnotations');
			if( pageIndex >= 0 ) {
				var annotsURL = [ "/", PDFViewer.platform, "/", PDFViewer.routeID, "/annotations/", pageIndex, "/page.json" ].join( "" );
				$http.get( annotsURL ).success( function( response ) {
					if( angular.isArray( response ) ) {
						var _page = PDFViewer.pages[ pageIndex ];
						var rotation = !!_page.size.rotation ? _page.size.rotation : 0;
						var width = !!_page.size.width ? _page.size.width : Page.size.width;
						var height = !!_page.size.height ? _page.size.height : Page.size.height;
						for( var idx in response ) {
							if( response[ idx ].type === "callout" ) {
								if( PDFViewer.annotations.mode === "stamp" ) {
									continue;
								}
								//console.log(" -- ", "PDFViewer.fetchAnnotations", "Callouts", JSON.stringify(response[idx].data));
								// code to setup the callout
								// var callouts = response[ idx ].data;
								// if( callouts && callouts.length > 0 ) {
								// 	for( var i in callouts ) {
								// 		var calloutParams = callouts[ i ];
								// 		console.log( "PDFViewer.fetchAnnotations;", "callout:", Object.assign( {}, calloutParams ) );
								// 		var DrawCallout = PDFViewer.presentationAddCallout( calloutParams );
								// 		if( isset( DrawCallout ) ) {
								// 			DrawCallout.setOrigin( "disk" );
								// 		}
								// 	}
								// }
							} else {
								//console.log('FetchAnnotations', pageIndex, idx, JSON.stringify(response[idx]));
								var DrawObject = getAnnotateDrawObj( response[ idx ] );
								DrawObject.pageIndex = pageIndex;
								DrawObject.zIndex = 0;
								DrawObject.transformHostPageRotation( rotation, width, height );
								PDFViewer.annotations.pipeline.add( DrawObject );
							}
						}
						PDFViewer.triggerViewerAPIOnLoad();
						PDFViewer.drawCanvas();
					}
				} );
			}
		};

		var getAnnotateDrawObj = function getAnnotateDrawObj(params) {
			if(params) {
				if(params.type === 'note') {
					//console.log('Note Annotation', params.pointList[0], params.pointList[1], params);
					params.width = Math.min(Math.max(Math.abs(params.pointList[0][0]-params.pointList[1][0]),DEFAULT_NOTE_ICON_MIN_WIDTH),DEFAULT_NOTE_ICON_MAX_WIDTH);
					params.height = Math.min(Math.max(Math.abs(params.pointList[0][0]-params.pointList[1][0]),DEFAULT_NOTE_ICON_MIN_WIDTH),DEFAULT_NOTE_ICON_MAX_WIDTH);
					params.pointList[0][1] += params.height;
					params.pointList[1][1] = params.pointList[0][1] - params.height;
				}
				params.canvasWidth = Page.size.width;
				params.canvasHeight = Page.size.height;
				params.scale = scaleFactor;
				var DrawObject = DrawObjectFactory(params);
				DrawObject.setInitial(true);
				if(PDFViewer.annotations.mode === 'annotator') {
					DrawObject.setUnhittable(true);
					DrawObject.setUnselectable(true);
				}
				if(params.type === 'marker') {
					DrawObject.convertMarker2Highlight();
				}
				return DrawObject;
			}
			return null;
		};

		this.getHostPageRotation = function getHostPageRotation(pageIndex) {
			return PDFViewer.pages[pageIndex].size.rotation ? PDFViewer.pages[pageIndex].size.rotation : 0;
		};

		this.clearSearch = function clearSearch() {
			PDFViewer.resetSearchResults( [ '' ] );
			searchKeywordsArray = [ '' ];
			PDFViewer.drawCanvas();
		};

		this.resetSearchResults = function resetSearchResults(keywords) {
			PDFViewer.searchResults.cursor = -1;
			PDFViewer.searchResults.rects = [];
			PDFViewer.searchResults.countBase = 1;
			PDFViewer.searchResults.pipeline.clear();
			PDFViewer.searchResults.pageIndex = Page.index;
			PDFViewer.searchResults.keywords = keywords;
			PDFViewer.searchResults.active = false;
			PDFViewer.searchResults.documentHits = 0;
			PDFViewer.searchResults.hits = [];
		};

		this.showSearchResults = function showSearchResults() {
			var tbSearch = this.Toolbar.search;
			var isEmpty = (tbSearch.keywords.trim().length === 0);
			var isSameNotEmpty = PDFViewer.isSearchSameAndNotEmpty( [tbSearch.keywords] );
			tbSearch.isActive = !!PDFViewer.searchResults.active;
			tbSearch.isDoneBtn = (isEmpty || (!tbSearch.isActive && isEmpty) || (tbSearch.isActive && isSameNotEmpty)) ;
		};

		this.setSearchPromise = function setSearchPromise(dfd) {
			PDFViewer.searchResults.eventPromise = dfd;
		};

		this.searchPromiseResolve = function searchPromiseResolve(status) {
			if(PDFViewer.searchResults.eventPromise !== undefined) {
				PDFViewer.searchResults.eventPromise.resolve(status);
			}
		};

		this.search = function search(keywordsArray, getCount) {
			if( angular.isDefined( keywordsArray ) && angular.isArray(keywordsArray) && !PDFViewer.isSearchEmpty(keywordsArray) ) {
				searchKeywordsArray = keywordsArray;
				PDFViewer.searchInProgress = true;
				var promises = [];
				var grandTotalHits = 0;
				PDFViewer.resetSearchResults(searchKeywordsArray);
				if(!!(getCount)) {
					PDFViewer.searchResults.hits = [];
				}
				angular.forEach(searchKeywordsArray, function(termString){
					var postData = {
						query: termString,
						getCount: angular.isDefined( getCount ) ? !!(getCount) : false
					};
					promises.push($http.post( ["/", PDFViewer.platform, "/", PDFViewer.routeID, "/search/" + Page.index + "/"].join(""), postData )
						.success(function( response ) {
							var totalHits = 0;
							// save the page hit counts
							if(angular.isArray(response.result.hitCounts)) {
								for(var idx in response.result.hitCounts) {
									PDFViewer.searchResults.hits[idx] = isset(PDFViewer.searchResults.hits[idx]) ?
											PDFViewer.searchResults.hits[idx] + response.result.hitCounts[idx] :
											response.result.hitCounts[idx];
									totalHits += response.result.hitCounts[idx];
									grandTotalHits += response.result.hitCounts[idx];
								}
							}

							if(angular.isArray(response.result.hitRects)) {
								for(var idx in response.result.hitRects) {
									var DrawObject = PDFViewer.createSearchDrawRect(response.result.hitRects[idx]);

									if(PDFViewer.searchResults.pipeline.add(DrawObject)) {
										PDFViewer.searchResults.rects.push(DrawObject);
									}
								}
							}
							console.log("Multi-Term Search:", "Complete!", termString, "Found:", totalHits);
						}));
				});
				$q.all(promises).finally(function(){
					PDFViewer.searchResults.rects.sort(function(item, compareTo){
						if(Math.abs(item.pointLists[1] - compareTo.pointLists[1]) <= 5) {
							if(item.pointLists[0] < compareTo.pointLists[0])
								return -1;
						} else {
							if(item.pointLists[1] < compareTo.pointLists[1])
								return -1;
						}
						return 1;
					});
					PDFViewer.calcSearchHitCount(getCount);
					PDFViewer.searchInProgress = false;

					console.log("Multi-Term Search:", "Finished!", "Found:", grandTotalHits, getCount);
					if(PDFViewer.searchResults.documentHits > 0) {
						PDFViewer.nextSearch();
						PDFViewer.searchResults.active = true;
					} else {
						PDFViewer.searchPromiseResolve('No results');
					}
					if(!PDFViewer.Toolbar.webAppHide) {
						Toolbar.search.show = true;
						Toolbar.search.showStyle = {'background-color':'#eee'};
						Toolbar.setTool(Toolbar.search.toolCode, Toolbar.search.show);
						Toolbar.search.keywords = searchKeywordsArray.join(' ');
					}
				});
			}
		};

		this.calcSearchHitCount = function calcSearchHitCount(getCount) {
			if(!!getCount) {
				PDFViewer.searchResults.documentHits = 0;
			}
			PDFViewer.searchResults.countBase = 1;
			if( PDFViewer.searchResults.hits ) {
				for( var idx in PDFViewer.searchResults.hits ) {
					if(!!getCount) {
						PDFViewer.searchResults.documentHits += PDFViewer.searchResults.hits[idx];
					}
					if( idx < Page.index ) {
						PDFViewer.searchResults.countBase += PDFViewer.searchResults.hits[idx];
					}
				}
			}
		};

		this.createSearchDrawRect = function createSearchDrawRect(coord) {
			if(angular.isArray(coord)) {
				var params = DrawObjectDefaultRectParams();
				params.canvasHeight = Page.size.height;
				params.scale = scaleFactor;
				params.pointList = [coord[1]-2,coord[0]+2,coord[3]+4,coord[2]+4];
				params.color = '#ffff00';
				params.opacity = 35;
				return DrawObjectFactory(params);
			}
			return null;
		};

		this.prevSearch = function prevSearch() {
			if( PDFViewer.isSearchSame(searchKeywordsArray) && PDFViewer.searchResults.pageIndex === Page.index ) {
				// check page bounds of search results
				if( (PDFViewer.searchResults.cursor - 1) < 0 ) {
					PDFViewer.searchResults.cursorLast = true;
					return PDFViewer.prevDocumentSearch();
				}

				// reset old search value
				var current = getSearchCurrent();
				if(PDFViewer.searchResults.cursor >= 0 && current.toString() === 'DrawObject') {
					current.setColor('#ffff00');
					current.setOpacity(0.35);
					current.setBorderColor('#ffff00');
					current.setBorderOpacity(0.35);
				}

				// advance cursor previous
				--PDFViewer.searchResults.cursor;

				// set new current properties
				current = getSearchCurrent();
				current.setColor('#ff7f00')
				current.setOpacity(0.3);
				current.setBorderColor('#ff7f00');
				current.setBorderOpacity(0.7);

				// draw
				PDFViewer.drawCanvas();

				// scroll
				scrollSearchHighlight(current.pointLists);

				// promise
				PDFViewer.searchPromiseResolve(Toolbar.search.position()+' of '+Toolbar.search.matches());

			} else if( !PDFViewer.isSearchEmpty(searchKeywordsArray) ) {
				PDFViewer.search( searchKeywordsArray, false );
			}
		};

		this.nextSearch = function nextSearch() {
			if( PDFViewer.isSearchSame(searchKeywordsArray) && PDFViewer.searchResults.pageIndex === Page.index ) {
				// check page bounds of search restuls
				if( (PDFViewer.searchResults.cursor + 1) >= PDFViewer.searchResults.rects.length ) {
					return PDFViewer.nextDocumentSearch();
				}

				// check if we came from a prev document search
				if(PDFViewer.searchResults.cursorLast) {
					PDFViewer.searchResults.cursorLast = false;
					PDFViewer.searchResults.cursor = PDFViewer.searchResults.rects.length - 2;
				}

				// reset old search value
				var current = getSearchCurrent();
				if( PDFViewer.searchResults.cursor >= 0 && current.toString() === "DrawObject" ) {
					current.setColor( "#ffff00" );
					current.setOpacity( 0.35 );
					current.setBorderColor( "#ffff00" );
					current.setBorderOpacity( 0.35 );
				}

				// advance cursor next
				++PDFViewer.searchResults.cursor;

				// set the new current properties
				current = getSearchCurrent();
				current.setColor( "#ff7f00" );
				current.setOpacity( 0.3 );
				current.setBorderColor( "#ff7f00" );
				current.setBorderOpacity( 0.7 );

				// draw
				PDFViewer.drawCanvas();

				// scroll
				scrollSearchHighlight(current.pointLists);

				// promise
				PDFViewer.searchPromiseResolve(Toolbar.search.position()+' of '+Toolbar.search.matches());

			} else if( !PDFViewer.isSearchEmpty(searchKeywordsArray) && PDFViewer.searchResults.documentHits > 0 ) {
				PDFViewer.search( searchKeywordsArray, false );
			}
		};

		this.isSearchSame = function isSearchSame(searchArray) {
			var same = (angular.isArray(searchArray) && angular.isArray(PDFViewer.searchResults.keywords)
						&& searchArray.length === PDFViewer.searchResults.keywords.length);
			for(var idx in searchArray) {
				if(searchArray[idx] !== PDFViewer.searchResults.keywords[idx]) {
					same = false;
					break;
				}
			}
			return same;
		}

		this.isSearchEmpty = function isSearchEmpty(searchArray) {
			var empty = true;
			for(var idx in searchArray) {
				if(searchArray[idx].trim().length > 0) {
					empty = false;
					break;
				}
			}
			return empty;
		}

		this.isSearchSameAndNotEmpty = function isSearchSameAndNotEmpty( searchArray ) {
			var same = (angular.isArray( searchArray ) && angular.isArray( PDFViewer.searchResults.keywords ) && searchArray.length === PDFViewer.searchResults.keywords.length);
			var empty = true;
			for( var idx in searchArray ) {
				if( searchArray[ idx ].trim().length > 0 ) {
					empty = false;
				}
				if( searchArray[ idx ] !== PDFViewer.searchResults.keywords[ idx ] ) {
					same = false;
				}
			}
			return (same && !empty);
		}

		this.prevDocumentSearch = function prevDocumentSearch() {
			var totalPages = PDFViewer.searchResults.hits.length;
			var found = false;
			for( var i = 1; i <= totalPages; ++i ) {
				var pageIndex = ((PDFViewer.searchResults.pageIndex - i) + totalPages) % totalPages;
				if( PDFViewer.searchResults.hits[pageIndex] > 0 ) {
					found = true;
					break;
				}
			}
			if(pageIndex !== Page.index) {
				PDFViewer.searchResults.pageIndex = Page.index;
				return PDFViewer.selectPage( pageIndex + 1 );
			} else if(found && pageIndex === Page.index) {
				PDFViewer.searchPromiseResolve(Toolbar.search.position()+' of '+Toolbar.search.matches());
				return 0;
			} else {
				if(!found)
					PDFViewer.searchPromiseResolve('No Results');
				return 0;
			}
		};

		this.nextDocumentSearch = function nextDocumentSearch() {
			var totalPages = PDFViewer.searchResults.hits.length;
			var found = false;
			for( var i = 1; i <= totalPages; ++i ) {
				var pageIndex = (PDFViewer.searchResults.pageIndex + i) % totalPages;
				if( PDFViewer.searchResults.hits[pageIndex] > 0 ) {
					found = true;
					break;
				}
			}
			if(pageIndex !== Page.index) {
				PDFViewer.searchResults.pageIndex = Page.index;
				return PDFViewer.selectPage( pageIndex + 1 );
			} else if(found && pageIndex === Page.index) {
				PDFViewer.searchPromiseResolve(Toolbar.search.position()+' of '+Toolbar.search.matches());
				return 0;
			} else {
				if(!found)
					PDFViewer.searchPromiseResolve('No Results');
				return 0;
			}
		};

		var getSearchCurrent = function getSearchCurrent() {
			if(angular.isArray(PDFViewer.searchResults.rects) && PDFViewer.searchResults.rects[PDFViewer.searchResults.cursor])
				return PDFViewer.searchResults.rects[PDFViewer.searchResults.cursor];
		};

		var _canvas = {
			"idCanvas": "pdf-page-canvas",
			"context": null,
			"htmlObj": null,
			"isPrimary": true,
			"scale": 1.0,
			"offsetX": 0,
			"offsetY": 0,
			"orientation": 0
		};
		var initCanvas = function initCanvas() {
			var obj = document.getElementById( _canvas.idCanvas );
			if( angular.isDefined( obj ) && typeof obj === "object" && obj ) {
				_canvas.context = obj.getContext( "2d" );
				_canvas.htmlObj = obj;
			}
		};
		this.clearCanvas = function clearCanvas() {
			initCanvas();
			if( isset( _canvas.context ) && _canvas.context ) {
				_canvas.context.clearRect( 0,0, Page.size.width, Page.size.height );
			}
		};
		this.drawCanvas = function drawCanvas( idCanvas, tickCount ) {
			if( idCanvas ) {
				_canvas.idCanvas = idCanvas;
			}
			this.clearCanvas();
			this.drawPipeline.draw( _canvas, tickCount, DRAWPIPELINE_DRAW_UNSELECTED );
			this.drawPipeline.draw( _canvas, tickCount, DRAWPIPELINE_DRAW_SELECTED );
		};
		this.getCanvasWidth = function getCanvasWidth() {
			return _canvas.htmlObj.width;
		};
		this.getCanvasHeight = function getCanvasHeight() {
			return _canvas.htmlObj.height;
		};

		var scrollSearchHighlight = function scrollSearchHighlight(coord) {
			var scrollLeft = (coord[0] * Page.getZoomScale())-200;
			var scrollTop = (coord[1] * Page.getZoomScale())-200;
			var pageWidth = PDFViewer.Page.scale.width;
			var pageHeight = PDFViewer.Page.scale.height;
			if( scrollTop < 0 ) {
				scrollTop = 0;
			}
			if( scrollLeft < 0 ) {
				scrollLeft = 0;
			}
			var temp;
			switch( PDFViewer.Page.orientation.getOrientation() ) {
				case 0:
					break;
				case 1:
					temp = scrollTop;
					scrollTop = scrollLeft;
					scrollLeft = temp;
					scrollLeft = (pageWidth - scrollLeft) - 400;
					break;
				case 2:
					scrollTop = (pageHeight - scrollTop) - 400;
					scrollLeft = (pageWidth - scrollLeft) - 400;
					break;
				case 3:
					temp = scrollTop;
					scrollTop = scrollLeft;
					scrollLeft = temp;
					scrollTop = (pageHeight - scrollTop) - 500;
					break;
			}
			$( "html, body" ).animate( {"scrollTop": scrollTop + "px", "scrollLeft": scrollLeft + "px"} );
		};

		this.scrollTo = function scrollTo(left, top) {
			var scrollLeft = (left * Page.getZoomScale());
			var scrollTop = (top * Page.getZoomScale());
			scrollLeft = Math.min(Math.max(Math.floor(scrollLeft),0),$(document).width());
			scrollTop = Math.min(Math.max(Math.floor(scrollTop),0),$(document).height());
			$("html, body").animate({ scrollTop: scrollTop+"px", scrollLeft: scrollLeft+"px" });
		};

		this.scrollToXY = function scrollToXY(left, top) {
			var scrollLeft = Math.min(Math.max(Math.floor(left),0),$(document).width());
			var scrollTop = Math.min(Math.max(Math.floor(top),0),$(document).height());
			$("html, body").animate({ scrollTop: scrollTop+"px", scrollLeft: scrollLeft+"px" });
		};

		this.getScaleFactor = function getScaleFactor() {
			return scaleFactor;
		};

		this.introduceStart = function introduceStart(title, subtitle, date, first) {
			console.log('Introduce Start (PDFViewer)');
			var lastNumRegEx = /(\d+)(?!.*\d)/;

			result1 = lastNumRegEx.exec(subtitle);
			result0 = lastNumRegEx.exec(title);

			if(result1 && result1[0] !== '') {
				oldInt = parseInt(result1[0]);
				newInt = oldInt + 1;
				subtitle = subtitle.replace(lastNumRegEx, newInt);
			}
			else if(result0 && result0[0] !== '') {
				oldInt = parseInt(result0[0]);
				newInt = oldInt + 1;
				title = title.replace(lastNumRegEx, newInt);
			}

			// disable arrow keys
			PDFViewer.ignoreArrowKeys = true;
			PDFViewer.preventPageChange = true;

			// set the mode
			if(PDFViewer.annotations.mode !== 'stamp')
				PDFViewer.annotations._saveMode = PDFViewer.annotations.mode;
			PDFViewer.annotations.mode = 'stamp';

			// set the attributes of the stamp object
			Stamp.title.myValue = Stamp.title.defaultValue = title;
			Stamp.subtitle.myValue = Stamp.subtitle.defaultValue = subtitle;
			Stamp.date.myValue = Stamp.date.defaultValue = date;
			//Stamp.enableFn = enableFn;
			//Stamp.disableFn = disableFn;

			// Load page 1
			PDFViewer.selectPage(1);
			PDFViewer.Stamp.showPage();
			Page.orientation.reset();
			Page.zoomMode();

			// clear search results
			PDFViewer.searchResults.pipeline.clear();

			Toolbar.draw.modify.menu.hideAll();
			Toolbar.rotate.orientationStyle.marginTop = 0;

			if(first)
				Stamp.dialog();
			else
				Stamp.place();
		};

		this.introduceStop = function introduceStop() {
			console.log('Stopping Introduce');

			// hide the dialog
			Stamp.show = false;
			Stamp.showPage();

			// remove the modal tool
			Toolbar.setModalTool(Stamp.toolCode, false);
			Toolbar.setTool(Toolbar.noneTool, true);

			// hide stamp
			Stamp.pipeline.clear();

			// set the mode
			PDFViewer.annotations.setMode("draw");
			//PDFViewer.annotations.mode = "";
			console.log('Toolbar Modal:', Toolbar.modal);
			//console.log('Annotation Save Mode Restored:', PDFViewer.annotations._saveMode);

			// set the attributes of the stamp object
			Toolbar.rotate.orientationStyle.marginTop = 30;

			// Load page 1
			PDFViewer.selectPage(0);
			//PDFViewer.fetchAnnotations(0);
			Page.orientation.reset();
			Page.scaleOption.id = 0;
			Page.zoomMode();

			// enable arrow keys
			PDFViewer.ignoreArrowKeys = false;
			PDFViewer.preventPageChange = false;
		};

		this.introduceSave = function introduceSave(callback) {
			// save the introduce
			if(Stamp.isDrawObject()) {
				if(typeof callback === "function") {
					callback(Stamp.getSaveJson());
				} else {
					return Stamp.getSaveJson();
				}
			} else {
				if(typeof callback === "function") {
					Stamp.delayedGetSaveJson(callback);
				} else {
					return Stamp.getSaveJson();
				}
			}
		};

		this.introduceClear = function introduceClear() {
			return Stamp.clearDrawObject();
		};

		this.modifyDrawObject = function modifyDrawObject(DrawObject, modifyData) {
			if(typeof modifyData.Color !== "undefined") {
				DrawObject.color = DrawColor.intToHex( modifyData.Color );
			}
			if(modifyData.Opacity) {
				DrawObject.opacity = modifyData.Opacity/100.0;
			}
			if(modifyData.LineWidth) {
				DrawObject.lineWidth = modifyData.LineWidth;
			}
			switch(DrawObject.drawType) {
				case 'arrow':
				case 'marker':
				case 'highlight':
					if(modifyData.StartPoint) {
						DrawObject.pointLists[0] = PDFViewer.stringToPixelPoint( modifyData.StartPoint ); // Pixels because already exists
					}
					if(modifyData.EndPoint) {
						DrawObject.pointLists[1] = PDFViewer.stringToPixelPoint( modifyData.EndPoint ); // Pixels because already exists
					}
					DrawObject.transformHostPageRotation( DrawObject.hostPageRotation, DrawObject.canvasWidth, DrawObject.canvasHeight);
					DrawObject.createBoundingBox();
					break;
				case 'pencil':
					if(modifyData.Bounds) {
						DrawObject.drawPresentationBounds( modifyData.Bounds );
					}
					break;
				case 'note':
				case 'stamp':
					break;
			}
		};

		this.presenterOrientation = function presenterOrientation(myOrientation) {
			myOrientation = Math.abs(myOrientation+4)%4; // ensure the range is right for myOrientation
			var myPresenterOrientation = 0;
			switch(myOrientation) {
				case 1: // Right 90 my 1, their 2
					myPresenterOrientation = 3;
					break;
				case 2: // 180 my 2, their 3
					myPresenterOrientation = 2;
					break;
				case 3: // Left 90 my 3, their 1
					myPresenterOrientation = 1;
					break;
				default: // upright, same on both
					myPresenterOrientation = 0;
					break;
			}
			return myPresenterOrientation;
		};

		this.presenterPageUpdate = function presenterPageUpdate() {
			if(PDFViewer.presentation.presenterState.preventPageUpdate) {
				return;
			}
			// initialize the return object
			var pageData = {}; // start empty
			var pState = PDFViewer.presentation.presenterState;

			// construct current values
			var cPage = Page.index;
			//if(cPage !== PDFViewer.presentation.presenterState.pageIndex) {
			pageData.pageIndex = cPage;
			PDFViewer.presentation.presenterState.pageIndex = cPage;
			//}

			if( appOrientation !== PDFViewer.presentation.presenterState.orientation ) {
				pageData.orientation = appOrientation;
				PDFViewer.presentation.presenterState.orientation = appOrientation;
			}

			var cOrientation = PDFViewer.presenterOrientation( Page.orientation.orientation );
			if( cOrientation !== pState.pageOrientation ) {
				pageData.pageOrientation = cOrientation;
				PDFViewer.presentation.presenterState.pageOrientation = cOrientation;
			}

			// console.log( "pState.pageScale:", pState.pageScale, "scaleOption:", Page.scaleOption );
			if( Page.scaleOption.pageScale != pState.pageScale || isset( pageData.pageIndex ) ) {
				pageData.pageScale = Page.scaleOption.pageScale;
			}

			var cRectW = Page.bounds.width / Page.scale.width;
			var cRectH = Page.bounds.height / Page.scale.height;
			if( typeof pageData.pageOrientation !== "undefined" || Math.abs( cRectW - pState.rectSizeWidth ) > 0.001 || Math.abs( cRectH - pState.rectSizeHeight ) > 0.001 ) {
				pageData.rectSizeWidth = cRectW;
				pageData.rectSizeHeight = cRectH;
				PDFViewer.presentation.presenterState.rectSizeWidth = cRectW;
				PDFViewer.presentation.presenterState.rectSizeHeight = cRectH;
			}

			var $doc = $(document);
			var boundsX = Math.max( Page.bounds.width, 0 );
			var boundsY = Math.max( Page.bounds.height, 0 );
			var sizeX = Math.max( Page.scale.width, 0 );
			var sizeY = Math.max( Page.scale.height, 0 );
			var scrollX = Math.max( $doc.scrollLeft(), 0 );
			var scrollY = Math.max( $doc.scrollTop(), 0 );
			var scrInvX, scrInvY, cOffsetX, cOffsetY;
			switch(Page.orientation.getOrientation()) {
				case 1: // Right 90
					sizeX = Math.max( Page.scale.height, 0 ); // swap x and y
					sizeY = Math.max( Page.scale.width, 0 ); // swap x and y
					scrInvX = Math.round( sizeX - (scrollX + boundsX) );
					scrInvY = Math.round( sizeY - (scrollY + boundsY) );
					cOffsetX = (scrollY / sizeY);
					cOffsetY = (scrInvX / sizeX); // scroll inverse scrInvX
					break;
				case 2: // 180
					scrInvX = Math.round( sizeX - (scrollX + boundsX) );
					scrInvY = Math.round( sizeY - (scrollY + boundsY) );
					cOffsetX = (scrInvX / sizeX); // scroll inverse scrInvX
					cOffsetY = (scrInvY / sizeY); // scroll inverse scrInvY
					break;
				case 3: // Left 90
					sizeX = Math.max( Page.scale.height, 0 ); // swap x and y
					sizeY = Math.max( Page.scale.width, 0 ); // swap x and y
					scrInvX = Math.round( sizeX - (scrollX + boundsX) );
					scrInvY = Math.round( sizeY - (scrollY + boundsY) );
					cOffsetX = (scrInvY / sizeY); // scroll inverse scrInvY
					cOffsetY = (scrollX / sizeX);
					break;
				default: // 0
					scrInvX = Math.round( sizeX - (scrollX + boundsX) );
					scrInvY = Math.round( sizeY - (scrollY + boundsY) );
					cOffsetX = (scrollX / sizeX);
					cOffsetY = (scrollY / sizeY);
					break;
			}
			// console.log( "presenterPageUpdate; sizeX:", sizeX, "sizeY:", sizeY );
			// console.log( "presenterPageUpdate; boundsX:", boundsX, "boundsY:", boundsY );
			// console.log( "presenterPageUpdate; scrollX:", scrollX, "scrollY:", scrollY );
			// console.log( "presenterPageUpdate; scrInvX:", scrInvX, "scrInvY:", scrInvY );
			// console.log( "presenterPageUpdate; cOffsetX:", cOffsetX, "cOffsetY:", cOffsetY );
			var xDidChange = (Math.abs( cOffsetX - pState.pageOffsetX ) > 0.001);
			var yDidChange = (Math.abs( cOffsetY - pState.pageOffsetY ) > 0.001);
			if( isset( pageData.pageOrientation ) || xDidChange || yDidChange || isset( pageData.pageScale ) ) {
				pageData.pageOffset = 1;
				pageData.pageOffsetX = cOffsetX;
				pageData.pageOffsetY = cOffsetY;
				PDFViewer.presentation.presenterState.pageOffset = 1;
				PDFViewer.presentation.presenterState.pageOffsetX = cOffsetX;
				PDFViewer.presentation.presenterState.pageOffsetY = cOffsetY;
			}

			//override on first load
			//console.log( "PDFViewer.presenterPageUpdate; presentation.firstLoad:", PDFViewer.presentation.firstLoad );
			if( isset( PDFViewer.presentation.firstLoad ) && typeof PDFViewer.presentation.firstLoad.pageScale === "number" ) {
				//console.log( "PDFViewer.presenterPageUpdate; override pageScale:", PDFViewer.presentation.firstLoad.pageScale );
				pageData.pageScale = (PDFViewer.presentation.firstLoad.pageScale + 0);
				PDFViewer.presentation.presenterState.pageScale = pageData.pageScale;
			}
			if( typeof pageData.pageOffset && pageData.pageOffset && isset( PDFViewer.presentation.firstLoad ) && typeof PDFViewer.presentation.firstLoad.pageOffsetX === "number" ) {
				//console.log( "PDFViewer.presenterPageUpdate; override pageOffset:", PDFViewer.presentation.firstLoad );
				pageData.pageOffsetX = (PDFViewer.presentation.firstLoad.pageOffsetX + 0);
				pageData.pageOffsetY = (PDFViewer.presentation.firstLoad.pageOffsetY + 0);
				PDFViewer.presentation.presenterState.pageOffsetX = pageData.pageOffsetX;
				PDFViewer.presentation.presenterState.pageOffsetY = pageData.pageOffsetY;
			}

			//var testString = JSON.stringify(pageData);
			if(PDFViewer.annotations.mode !== "stamp") {
				if( !!PDFViewer.presentation.presenterState.firstLoad ) {
					// console.log( "PDFViewer::presenterPageUpdate()", PDFViewer.internalReferenceID, "First load, discarding:", pageData );
					PDFViewer.presentation.presenterState.firstLoad = false;
					PDFViewer.presentation.firstLoad = null;
				} else {
					//console.log("PDFViewer::presenterPageUpdate()", PDFViewer.internalReferenceID, pageData);
					PDFViewer.triggerViewerAPIPresenter( 'PageInfo', pageData );
				}
			}
		};

		function synchronizePresentation(actionString, syncData) {
			if(typeof syncData === "undefined") {
				syncData = null;
			}
			if(!PDFViewer.isLoaded) {
				//console.log("   QQ", "*Queue*", "Action:", actionString);
				PDFViewer.loadQueue.push({action:actionString,data:syncData});
				return true;
			}
			return false;
		}

		this.presentationPageAdd = function presentationPageAdd( page ) {
			 console.log( "PDFViewerAPI.presentationPageAdd;", page );
//			if(synchronizePresentation('PresentationPage', page)) {
//				console.log("   QQ", "*Wait*", "Action:", "PresentationPage");
//				return;
//			}
			if( typeof page === "undefined" || page === null) {
				return;
			}
			PDFViewer.presentation.presenterState.preventPageUpdate = true;
			if( typeof page.pageIndex !== "undefined" ) {
				if( Page.index !== page.pageIndex ) {
					PDFViewer.presentation.presenterState.pageIndex = page.pageIndex;
					PDFViewer.selectPage( page.pageIndex + 1 );
				}
			}
			if( typeof page.orientation !== "undefined" ) {
				PDFViewer.presentation.presenterState.orientation = page.orientation;
				Page.setPresentationOrientation( page.orientation );
			}
			page.pageOffsetX = (typeof page.pageOffsetX === "undefined") ? 0 : page.pageOffsetX;
			page.pageOffsetY = (typeof page.pageOffsetY === "undefined") ? 0 : page.pageOffsetY;
			if( typeof page.pageOrientation !== "undefined" ) {
				var newRotation;
				switch ( page.pageOrientation ) {
					case 1: // Left 90 their 1, my 3
						newRotation = 3;
						break;
					case 2: // Right 90 their 2, my 1
						newRotation = 2;
						break;
					case 3: // 180 their 3, my 2
						newRotation = 1;
						break;
					default: // upright.
						newRotation = 0;
						break;
				}
				PDFViewer.presentation.presenterState.pageOrientation = newRotation;
				Page.orientation.setOrientation( newRotation );
			}
			if( typeof page.pageScale === "string" ) {
				console.log( "PDFViewerAPI.presentationPageAdd; pageScale string:", page.pageScale );
				page.pageScale = parseFloat( page.pageScale );
				if (page.pageScale == -1){page.pageScale = 0.5;console.log('pageScale came as -1, fixing it to 1');}
			}
			if( typeof page.pageScale === "number" ) {
				console.log( "PDFViewerAPI.presentationPageAdd; pageScale number:", page.pageScale );
				if (page.pageScale == -1){page.pageScale = 0.5;console.log('pageScale came as -1, fixing it to 0.5');}
				 if( page.pageScale === 1 ) {
					 console.log('pageScale came as 1, fixing it to 0.5',Page.scaleOptionWidth.scale);
				 	page.pageScale = 0.5;
				 }
				PDFViewer.presentation.presenterState.pageScale = page.pageScale;
				Page.setPresentationServerZoom( page.pageScale );
			}
			Page.zoomPresentation();
			switch( Page.orientation.getOrientation() ) {
				case 1: // Right 90
					sizeX = Math.max(Page.scale.height,0);
					sizeY = Math.max(Page.scale.width,0);
					boundsX = Math.max(Page.bounds.width,0);
					boundsY = Math.max(Page.bounds.height,0);
					directionX = -1;
					directionY = 1;
					originX = 1;
					originY = 0;
					rangeX = Math.max(sizeX - boundsX,0);
					rangeY = Math.max(sizeY - boundsY,0);
					rangeXP = rangeX / sizeX;
					rangeYP = rangeY / sizeY;
					pageOriginX = page.pageOffsetX;
					pageOriginY = page.pageOffsetY;
					page.pageOffsetX = Math.max(Math.min(rangeXP, pageOriginY),0);
					page.pageOffsetY = Math.max(Math.min(rangeYP, pageOriginX),0);
					finalX = (originX * (sizeX - boundsX)) + (page.pageOffsetX * sizeX * directionX);
					finalY = (originY * (sizeY - boundsY)) + (page.pageOffsetY * sizeY * directionY);
					break;
				case 2: // 180
					sizeX = Math.max(Page.scale.width,0);
					sizeY = Math.max(Page.scale.height,0);
					boundsX = Math.max(Page.bounds.width,0);
					boundsY = Math.max(Page.bounds.height,0);
					directionX = -1;
					directionY = -1;
					originX = 1;
					originY = 1;
					rangeX = Math.max(sizeX - boundsX,0);
					rangeY = Math.max(sizeY - boundsY,0);
					rangeXP = rangeX / sizeX;
					rangeYP = rangeY / sizeY;
					pageOriginX = page.pageOffsetX;
					pageOriginY = page.pageOffsetY;
					page.pageOffsetX = Math.max(Math.min(rangeXP, pageOriginX),0);
					page.pageOffsetY = Math.max(Math.min(rangeYP, pageOriginY),0);
					finalX = (originX * (sizeX - boundsX)) + (page.pageOffsetX * sizeX * directionX);
					finalY = (originY * (sizeY - boundsY)) + (page.pageOffsetY * sizeY * directionY);
					break;
				case 3: // Left 90
					sizeX = Math.max(Page.scale.height,0);
					sizeY = Math.max(Page.scale.width,0);
					boundsX = Math.max(Page.bounds.width,0);
					boundsY = Math.max(Page.bounds.height,0);
					directionX = 1;
					directionY = -1;
					originX = 0;
					originY = 1;
					rangeX = Math.max(sizeX - boundsX,0);
					rangeY = Math.max(sizeY - boundsY,0);
					rangeXP = rangeX / sizeX;
					rangeYP = rangeY / sizeY;
					pageOriginX = page.pageOffsetX;
					pageOriginY = page.pageOffsetY;
					page.pageOffsetX = Math.max(Math.min(rangeXP, pageOriginY),0);
					page.pageOffsetY = Math.max(Math.min(rangeYP, pageOriginX),0);
					finalX = (originX * (sizeX - boundsX)) + (page.pageOffsetX * sizeX * directionX);
					finalY = (originY * (sizeY - boundsY)) + (page.pageOffsetY * sizeY * directionY);
					break;
				default: // 0
					sizeX = Math.max(Page.scale.width,0);
					sizeY = Math.max(Page.scale.height,0);
					boundsX = Math.max(Page.bounds.width,0);
					boundsY = Math.max(Page.bounds.height,0);
					directionX = 1;
					directionY = 1;
					originX = 0;
					originY = 0;
					rangeX = Math.max(sizeX - boundsX,0);
					rangeY = Math.max(sizeY - boundsY,0);
					rangeXP = rangeX / sizeX;
					rangeYP = rangeY / sizeY;
					pageOriginX = page.pageOffsetX;
					pageOriginY = page.pageOffsetY;
					page.pageOffsetX = Math.max(Math.min(rangeXP, pageOriginX),0);
					page.pageOffsetY = Math.max(Math.min(rangeYP, pageOriginY),0);
					finalX = (originX * (sizeX - boundsX)) + (page.pageOffsetX * sizeX * directionX);
					finalY = (originY * (sizeY - boundsY)) + (page.pageOffsetY * sizeY * directionY);
					break;
			}
			PDFViewer.presentation.presenterState.pageOffsetX = finalX;
			PDFViewer.presentation.presenterState.pageOffsetY = finalY;
			if(Toolbar.getTool() !== Stamp.toolCode) {
				PDFViewer.scrollToXY( finalX, finalY );
			}
			PDFViewer.safeApply();
			PDFViewer.presentation.firstLoad = {};
			PDFViewer.presentation.presenterState.preventPageUpdate = false;
		};

		this.presentationAlign = function presentationAlign() {
//			console.log("PDFViewer.presentationAlign()");
			Page.zoomPresentation();
			if(Toolbar.getTool() !== Stamp.toolCode) {
				PDFViewer.scrollToXY(PDFViewer.presentation.presenterState.pageOffsetX, PDFViewer.presentation.presenterState.pageOffsetY);
			}
			window.top.dismissLoadingOverlay();
		};

		this.presentationAdd = function presentationAdd(annotation) {
			if(synchronizePresentation('PresentationAdd', annotation)) {
				//console.log("   QQ", "*Wait*", "Action:", "PresentationAdd");
				return;
			}
			if(typeof annotation.Type === 'undefined') {
				return;
			}
			var DrawObject = PDFViewer.annotations.pipeline.findSig(annotation.Index);
			if(DrawObject !== null) {
				PDFViewer.presentationModify(annotation);
			} else if (annotation.Operation === 'add' || annotation.Operation === 'mod' || !isset(annotation.Operation)) {
				var page = annotation.Page;
				var params = presentationAddDefaultParams(annotation.Type);
				params.color = DrawColor.intToHex( annotation.Color );
				params.opacity = annotation.Opacity;
				params.lineWidth = annotation.LineWidth;
				params.sig = annotation.Index;
				params.canvasWidth = PDFViewer.pages[page].size.width;
				params.canvasHeight = PDFViewer.pages[page].size.height;
				params.pageIndex = page;
				params = presentationAddCustomizeParams(annotation, params);
				var DrawObject = DrawObjectFactory(params);
				DrawObject.existing = false; // when saved will save as add instead of mod
				DrawObject.zIndex = typeof annotation.zIndex !== "undefined" ? annotation.zIndex : -1;
				DrawObject.transformHostPageRotation(PDFViewer.pages[page].size.rotation, params.canvasWidth, params.canvasHeight);
				DrawObject.createBoundingBox();
				DrawObject.updateDragPoints();
				if(annotation.Bounds && annotation.Type === 1) { // Pencil
					console.log("PDFViewer::presentationAdd()", annotation.Bounds);
					DrawObject.drawPresentationBounds(annotation.Bounds);
					DrawObject.updateDragPoints();
				}
				if(annotation.Type === 5) {
					DrawObject.convertMarker2Highlight();
				}
				var UndoObj = PDFViewer.Toolbar.draw.undo.factory.draw(DrawObject); // create an undo object for the presentation object
				if(page === Page.index) {
					PDFViewer.annotations.pipeline.add(DrawObject);
					PDFViewer.Toolbar.draw.undo.add(UndoObj); // add the item to the undo queue
					PDFViewer.annotations.stash.stash(page,PDFViewer.annotations.pipeline,PDFViewer.annotations.undo,PDFViewer.annotations.callouts);
				} else {
					var stash = PDFViewer.annotations.stash.unstash(page);
					stash.pipeline.add(DrawObject);
					stash.undo.push(UndoObj); // add the item to the undo queue for the page
					PDFViewer.annotations.stash.stash(page,stash.pipeline,stash.undo,stash.callouts);
				}
			}
			PDFViewer.safeApply();
			PDFViewer.Canvas.refresh();
		};

		function presentationAddDefaultParams(annotationType) {
			switch(annotationType) {
				case 1: // Pencil
					return DrawObjectDefaultPencilParams();
				case 3: // Arrow
					return DrawObjectDefaultArrowParams();
				case 5: // Marker (Highlight)
					return DrawObjectDefaultMarkerParams();
				case 8: // Note
					return DrawObjectDefaultNoteParams();
			}
		};

		function presentationAddCustomizeParams(annotation, params) {
			switch(annotation.Type) {
				case 1: // Pencil
					for(var i = 0; i < annotation.Points.length; i++) {
						point = PDFViewer.stringToPdfPoint( annotation.Points[i] ); // PDF Points b/c DrawObject constructor
						params.pointList.push( point );
					}
					if (annotation.Points.length === 1) {
						params.pointList.push( point );
					}
					break;
				case 3: // Arrow
				case 5: // Marker (Highlight)
					var startPoint = PDFViewer.stringToPdfPoint( annotation.StartPoint ); // PDF Points b/c DrawObject constructor
					params.pointList.push( startPoint );
					var endPoint = PDFViewer.stringToPdfPoint( annotation.EndPoint ); // PDF Points b/c DrawObject constructor
					params.pointList.push( endPoint );
					break;
				case 8: // Note
					params.date = Date.now();
					params.note = annotation.Content;
					var startPoint = PDFViewer.stringToPdfPoint( annotation.Bounds ); // PDF Points b/c DrawObject constructor
					params.pointList.push( startPoint );
					break;
			}
			return params;
		};

		this.presentationClearAll = function presentationClearAll( clearData ) {
			if(synchronizePresentation('PresentationClearAll', clearData)) {
				//console.log("   QQ", "*Wait*", "Action:", "PresentationClearAll");
				return;
			}
			//console.log('PDFViewerAPI::presentationClearAll()',clearData);
			PDFViewer.annotations.pipeline.clear();
			PDFViewer.annotations.stash = StashFactory();
			PDFViewer.safeApply();
			PDFViewer.Canvas.refresh();
		};

		this.presentationDelete = function presentationDelete( deleteData ) {
			if(synchronizePresentation('PresentationDelete', deleteData)) {
				//console.log("   QQ", "*Wait*", "Action:", "PresentationDelete");
				return;
			}
			//console.log('PDFViewerAPI::presentationDelete()',deleteData);
			var page = deleteData.Page;
			var sig = deleteData.Index;
			if(page === Page.index) {
				var DrawObject = PDFViewer.annotations.pipeline.findSig(sig);
				if(DrawObject !== null && DrawObject.toString() === 'DrawObject') {
					var UndoObj = PDFViewer.Toolbar.draw.undo.factory.erase(DrawObject); // create an undo object for the presentation object
					PDFViewer.Toolbar.draw.undo.add(UndoObj);
					PDFViewer.annotations.pipeline.remove(DrawObject);
				}
			} else {
				var pStash = PDFViewer.annotations.stash.unstash(page);
				var DrawObject = pStash.pipeline.findSig(sig);
				if(DrawObject !== null && DrawObject.toString() === 'DrawObject') {
					var UndoObj = PDFViewer.Toolbar.draw.undo.factory.erase(DrawObject); // create an undo object for the presentation object
					PDFViewer.Toolbar.draw.undo.add(UndoObj);
					pStash.pipeline.remove(DrawObject);
					PDFViewer.annotations.stash.stash(page,pStash.pipeline,pStash.undo,pStash.callouts);
				}
			}
			PDFViewer.safeApply();
			PDFViewer.Canvas.refresh();
		};

		this.presentationModify = function presentationModify( modifyData ) {
			if(synchronizePresentation('PresentationModify', modifyData)) {
				//console.log("   QQ", "*Wait*", "Action:", "PresentationModify");
				return;
			}
			//console.log('PDFViewerAPI::presentationModify()',modifyData);
			var page = modifyData.Page;
			var sig = modifyData.Index;
			if(page === Page.index) {
				var DrawObject = PDFViewer.annotations.pipeline.findSig(sig);
				if(DrawObject !== null && DrawObject.toString() === 'DrawObject') {
					var ModifyObj = PDFViewer.Toolbar.draw.undo.factory.modify(DrawObject); // create an undo object for the presentation object
					// PDFViewer.Toolbar.draw.undo.add(ModifyObj); // ED-3879
					PDFViewer.modifyDrawObject(DrawObject, modifyData);
				}
			} else {
				// presentation pipeline
				var pStash = PDFViewer.annotations.stash.unstash(page);
				var DrawObject = pStash.pipeline.findSig(sig);
				if(DrawObject !== null && DrawObject.toString() === 'DrawObject') {
					var ModifyObj = PDFViewer.Toolbar.draw.undo.factory.modify(DrawObject); // create an undo object for the presentation object
					// PDFViewer.Toolbar.draw.undo.add(ModifyObj); // ED-3879
					PDFViewer.modifyDrawObject(DrawObject, modifyData);
				}
			}
			PDFViewer.safeApply();
			PDFViewer.Canvas.refresh();
		};

		var _viewerAPIOnEvents = {firstLoad:[],everyLoad:[],savable:[],presenter:[]};
		var _onEventsFirstLoad = true;
		this.addOnFirstLoadCallback = function addOnFirstLoadCallback(callback) {
			_viewerAPIOnEvents.firstLoad.push(callback);
		};
		this.addOnEveryLoadCallback = function addOnEveryLoadCallback(callback) {
			_viewerAPIOnEvents.everyLoad.push(callback);
		};
		this.addOnSavableCallback = function addOnSavableCallback(callback) {
			_viewerAPIOnEvents.savable.push(callback);
		};
		this.addOnPresenterCallback = function addOnPresenterCallback(callback) {
			_viewerAPIOnEvents.presenter.push(callback);
		};
		this.triggerViewerAPIOnLoad = function triggerViewerAPIOnLoad() {
			if(_onEventsFirstLoad) {
				_onEventsFirstLoad = false;
				angular.forEach(_viewerAPIOnEvents.firstLoad, function(val) {
					if(typeof val === "function") {
						val();
					}
				});
			}
			angular.forEach(_viewerAPIOnEvents.everyLoad, function(val) {
				if(typeof val === "function") {
					val();
				}
			});
		};
		this.triggerViewerAPISavable = function triggerViewerAPISavable(isSavable) {
			angular.forEach(_viewerAPIOnEvents.savable, function(val) {
				if(typeof val === "function") {
					val(isSavable);
				}
			});
		};
		this.triggerViewerAPIPresenter = function triggerViewerAPIPresenter(presenterMessage, presenterData) {
			angular.forEach(_viewerAPIOnEvents.presenter, function(val) {
				if(typeof val === "function") {
					val(presenterMessage, presenterData);
				}
			});
		};
		this.stringToPixelPoint = function stringToPixelPoint(stringPoint) {
			var canvasHeight = Page.size.height;
			var point = PDFViewer.stringToPdfPoint(stringPoint);
			point[1] = canvasHeight - point[1];
			console.log(" --- pdf viewer string to ctx point:", Page.size, point);
			return point;
		};
		this.stringToPdfPoint = function stringToPdfPoint(stringPoint) {
			var canvasWidth = Page.size.width;
			var canvasHeight = Page.size.height;
			//var scale = scaleFactor;
			var splitPoint = stringPoint.replace( /{/g, '' ).replace( /}/g, '' ).split( ',' );
			var x = parseFloat( splitPoint[0] ) * canvasWidth;
			var y = parseFloat( splitPoint[1] ) * canvasHeight;
			var point = [x, y];
			//console.log(" --- pdf viewer string to pdf point:", Page.size, splitPoint, point);
			return point;
		};

		this.appOrientationDidChange = function appOrientationDidChange( eventData ) {
			if( typeof eventData.orientation === "string" && [PORTRAIT, LANDSCAPE].indexOf( eventData.orientation ) >= 0 ) {
				appOrientation = eventData.orientation;
			}
			if( typeof appOrientationDelegate === "function" ) {
				appOrientationDelegate( eventData );
			}
		};

		this.arrowColorCallback = jQuery.noop;
		this.setArrowColorCallback = function setArrowColorCallback(callback) {
			if(typeof callback === "function") {
				this.arrowColorCallback = callback;
				return true;
			}
			return false;
		};
		this.transmitArrowColorCallback = function transmitArrowColorCallback(color) {
			this.arrowColorCallback(color);
		};

		this.highlightColorCallback = jQuery.noop;
		this.setHighlightColorCallback = function setHighlightColorCallback(callback) {
			if(typeof callback === "function") {
				this.highlightColorCallback = callback;
				return true;
			}
			return false;
		};
		this.transmitHighlightColorCallback = function transmitHighlightColorCallback(color) {
			this.highlightColorCallback(color);
		};

		this.pencilColorCallback = jQuery.noop;
		this.setPencilColorCallback = function setPencilColorCallback(callback) {
			if(typeof callback === "function") {
				this.pencilColorCallback = callback;
				return true;
			}
			return false;
		};
		this.transmitPencilColorCallback = function transmitPencilColorCallback(color) {
			this.pencilColorCallback(color);
		};

		this.noteColorCallback = jQuery.noop;
		this.setNoteColorCallback = function setNoteColorCallback(callback) {
			if(typeof callback === "function") {
				this.noteColorCallback = callback;
				return true;
			}
			return false;
		};
		this.transmitNoteColorCallback = function transmitNoteColorCallback(color) {
			this.noteColorCallback(color);
		};

		this.presentationAddCallout = function presentationAddCallout( args ) {
			// console.log( "PDFViewer.presentationAddCallout; args:", Object.assign( {}, args ), "mode:", PDFViewer.annotations.mode );
			var ExistingCallout = null;
			if( args.Page === Page.index ) {
				ExistingCallout = PDFViewer.annotations.callouts.getByIndex( args.Index );
			} else {
				var stash = PDFViewer.annotations.stash.unstash( args.Page );
				ExistingCallout = stash.callouts.getByIndex( args.Index );
			}
			if( isset( ExistingCallout ) && ExistingCallout.toString() === "DrawCallout" ) {
				return PDFViewer.presentationModifyCallout( args );
			}
			var page = PDFViewer.pages[ args.Page ];
			var pwScale = Page.scaleOptionWidth.scale;
			var pageWidth = page.size.width;
			var pageHeight = page.size.height;
			var scaledWidth = (args.Page !== Page.index) ? pageWidth : (pageWidth * pwScale);
			var scaledHeight = (args.Page !== Page.index) ? pageHeight : (pageHeight * pwScale);

			var coords = {
				"x1": Math.floor( args.Origin.x * pageWidth ),
				"y1": Math.floor( args.Origin.y * pageHeight ),
				"x2": Math.ceil( (args.Origin.x + args.drag.right) * pageWidth ),
				"y2": Math.ceil( (args.Origin.y + args.drag.bottom) * pageHeight )
			};
			// console.log( "PDFViewer.presentationAddCallout; coords:", Object.assign( {}, coords ) );

			var params = DrawObjectDefaultCalloutParams();
			params.pointList.push( [ coords.x1, coords.y1 ] );
			params.pointList.push( [ coords.x2, coords.y2 ] );
			params.color = Toolbar.draw.callout.color;
			params.lineWidth = Toolbar.draw.callout.lineWidth;
			params.opacity = (Toolbar.draw.callout.opacity * 100);
			params.pageIndex = args.Page;

			var DrawObject = DrawObjectFactory( params );
			DrawObject.scale = PDFViewer.getScaleFactor();
			DrawObject.baseScale = 2.0;
			DrawObject.pageWidthScale = pwScale;
			DrawObject.canvasWidth = pageWidth;
			DrawObject.canvasHeight = pageHeight;
			// DrawObject.ICallout = args;

			var coParams = {
				"PDFViewer": PDFViewer,
				"DrawObject": DrawObject,
				"page": args.Page,
				"viewX": (args.Initial.left * pageWidth),
				"viewY": (args.Initial.top * pageHeight),
				"viewW": (args.Initial.right * scaledWidth),
				"viewH": (args.Initial.bottom * scaledHeight),
				"viewUrl": getImageSrc( args.Page ),
				"scale": args.scale,
				"pageOrientation": args.pageOrientation,
				"viewOrientation": args.viewOrientation,
				"Index": args.Index,
				"zIndex": args.zIndex,
				"closeable": !(PDFViewer.annotations.mode === "presentation" || PDFViewer.annotations.mode === "annotator"),
				"resizable": !(PDFViewer.annotations.mode === "presentation" || PDFViewer.annotations.mode === "annotator"),
				"moveable": !(PDFViewer.annotations.mode === "presentation"),
				"drawable": true
			};
			var DrawCallout = DrawCalloutFactory( coParams, args );

			if( args.Page === Page.index ) {
				PDFViewer.annotations.pipeline.add( DrawObject );
				PDFViewer.annotations.callouts.add( DrawCallout );
				DrawCallout.receiveModify( args ); // we don't want to stomp the initial values
				PDFViewer.annotations.stash.stash( args.Page, PDFViewer.annotations.pipeline, PDFViewer.annotations.undo, PDFViewer.annotations.callouts );
			} else {
				var stash = PDFViewer.annotations.stash.unstash( args.Page );
				stash.pipeline.add( DrawObject );
				stash.callouts.add( DrawCallout );
				DrawCallout.receiveModify( args ); // we don't want to stomp the initial values
				PDFViewer.annotations.stash.stash( args.Page, stash.pipeline, stash.undo, stash.callouts );
			}
			PDFViewer.Canvas.refresh();
			PDFViewer.safeApply();
			return DrawCallout;
		};

		this.presentationModifyCallout = function presentationModifyCallout(args) {
			console.log("PDFViewer::presentationModifyCallout()", JSON.stringify(args));
			var DrawCallout;
			if(args.Page === Page.index) {
				DrawCallout = PDFViewer.annotations.callouts.getByIndex(args.Index);
				DrawCallout.receiveModify(args);
			} else {
				// stash to another page.
				var stash = PDFViewer.annotations.stash.unstash(args.Page);
				DrawCallout = stash.callouts.getByIndex(args.Index);
				DrawCallout.receiveModify(args);
			}
			PDFViewer.Canvas.refresh();
			PDFViewer.safeApply();
		};

		this.presentationDeleteCallout = function presentationDeleteCallout(args) {
			console.log("PDFViewer::presentationDeleteCallout()", JSON.stringify(args));
			if(args.Page === Page.index) {
				var DrawCallout = PDFViewer.annotations.callouts.getByIndex(args.Index);
				DrawCallout.receiveDelete();
			} else {
				// stuff for stashing to another page.
				var stash = PDFViewer.annotations.stash.unstash(args.Page);
				DrawCallout = stash.callouts.getByIndex(args.Index);
				DrawCallout.receiveDelete();
			}
			PDFViewer.Canvas.refresh();
			PDFViewer.safeApply();
		};

		this.presentationSetCalloutZIndex = function presentationSetCalloutZIndex(args) {
			console.log("PDFViewer::presentationSetCalloutZIndex()", JSON.stringify(args));
			if(args.Page === Page.index) {
				PDFViewer.annotations.callouts.setZIndexes(args);
			} else {
				var stash = PDFViewer.annotations.stash.unstash(args.Page);
				var callouts = stash.callouts;
				callouts.setZIndexes(args);
			}
			PDFViewer.Canvas.refresh();
			PDFViewer.safeApply();
		};

		this.sendCalloutsZIndexes = function sendCalloutsZIndexes() {
			var result = {Page: Page.index, Indices: PDFViewer.annotations.callouts.getZIndexes()};
			//console.log("PDFViewer::sendCalloutsZIndexes()", JSON.stringify(result));
			PDFViewer.triggerViewerAPIPresenter('SetCalloutZIndexes', result);
		};

		this.getValidCalloutIndex = function getValidCalloutIndex(existing) {
			if(typeof existing === "string") {
				existing = parseInt(existing);
			}
			if(typeof existing !== "undefined" && existing > 0) {
				if(existing > _calloutIndex) {
					_calloutIndex = existing;
				}
				return existing;
			} else {
				return ++_calloutIndex;
			}
		};

		this.calloutContainerCss = function calloutContainerCss(obj) {
			angular.forEach(obj, function(value, key) {
				PDFViewer.calloutWrapStyle[key] = value;
			});
		};
	};

	function PageClass() {
		var Page = this;
		this.number = 1;
		this.index = 0;
		this.zoomScale = 0.5;
		this.zoomModeScale = 0.5;
		this.scale = defaultSize();
		this.jumpToPage = "";
		this.size = defaultSize();
		this.bounds = defaultSize();
		this.margin = {
			top: 0,
			right: 0,
			bottom: 0,
			left: 0
		};
		this.canvasStyle = {};
		this.pageStyle = defaultSize();
		this.wrapStyle = defaultSize();
		this.imagePath = "";
		this.scaleType = "zoom4";
//		this.scaleOptionFit = {"id": "page-fit", "label": "Page Fit", "show": "yes", "scale": null};
//		var showOrientationOptions = ( typeof window.top.webApp === "object" && window.top.webApp !== null ) ? "yes" : "no";
		this.scaleOptionWidth = {"id":"0.5", "label":"100%", "show":"yes", "scale":0.5, "pageScale":0.5, "pageWidthScale":0.5};
		this.scaleOptions = [
			Page.scaleOptionWidth,
			// {"id":"0.5",  "label":"50%",  "show":"no",  "scale":0.5},
			// {"id":"0.75", "label":"75%",  "show":"no",  "scale":0.75},
			// {"id":"1",    "label":"100%", "show":"yes", "scale":1, "pageScale":1, "pageWidthScale":1},
			{"id":"1.25", "label":"125%", "show":"no",  "scale":0.75, "pageScale":0.75, "pageWidthScale":0.75},
			{"id":"1.50",  "label":"150%", "show":"yes", "scale":1, "pageScale":1, "pageWidthScale":1},
			{"id":"1.75", "label":"175%", "show":"no",  "scale":1.25, "pageScale":1.25, "pageWidthScale":1.25},
			{"id":"2",    "label":"200%", "show":"yes", "scale":1.5, "pageScale":1.5, "pageWidthScale":1.5},
			{"id":"2.25", "label":"225%", "show":"no",  "scale":1.75, "pageScale":1.75, "pageWidthScale":1.75},
			{"id":"2.5",  "label":"250%", "show":"yes",  "scale":2, "pageScale":2, "pageWidthScale":2},
			{"id":"2.75", "label":"275%", "show":"no",  "scale":2.25, "pageScale":2.25, "pageWidthScale":2.25},
			{"id":"3",    "label":"300%", "show":"yes", "scale":2.5, "pageScale":2.5, "pageWidthScale":2.5},
			{"id":"3.25", "label":"325%", "show":"no",  "scale":2.75, "pageScale":2.75, "pageWidthScale":2.75},
			{"id":"3.5",  "label":"350%", "show":"yes",  "scale":3, "pageScale":3, "pageWidthScale":3},
			{"id":"3.75", "label":"375%", "show":"no",  "scale":3.25, "pageScale":3.25, "pageWidthScale":3.25},
			{"id":"4",    "label":"400%", "show":"yes", "scale":3.5, "pageScale":3.5, "pageWidthScale":3.5}
		];
		this.scaleMode = 0; // index value of page-width (default)
		this.scaleOption = this.scaleOptions[ this.scaleMode ]; //page-width (default)

		this.orientation = {
			degrees: 0,
			orientation: 0,
			format: "landscape",
			right: function _right() {
				Page.orientation.setOrientation( this.orientation + 1 );
				return this.orientation;
			},
			left: function _left() {
				Page.orientation.setOrientation( this.orientation - 1 );
				return this.orientation;
			},
			getOrientation: function _getOrientation() {
				return Page.orientation.orientation;
			},
			setOrientation: function _setOrientation(orientation) {
				console.log("Page.orientation.setOrientation;", orientation);
				orientation = (orientation+4) % 4;
				switch(orientation) {
					case 0:
					case 1:
					case 2:
					case 3:
						Page.orientation.orientation = orientation;
						Page.orientation.degrees = orientation*90;
						break;
				}
				Page.updateDimensions();
				PDFViewer.presenterPageUpdate();
			},
			reset: function _reset() {
				Page.orientation.setOrientation(0);
			}
		};

		//initial viewport bounds
		function defaultSize() {
			return {"width": viewPortSize.width, "height": viewPortSize.height};	//clone viewPortSize
		};

		this.setZoomScale = function setZoomScale( scale ) {
			if( typeof scale === "number" && !isNaN( scale ) ) {
				// console.log( "Page.setZoomScale;", scale );
				Page.zoomScale = scale;
			}
		};

		this.getZoomScale = function getZoomScale() {
			//console.log( "Page.getZoomScale:", Page.zoomScale );
			return Page.zoomScale;
		};

		this.selectScale = function selectScale() {
//			console.log( "Page::selectScale;", Page.scaleOption );
			Page.zoomPage( Page.scaleOption );
			PDFViewer.presenterPageUpdate();
		};

		this.zoomIn = function zoomIn() {
			var newZoom = 0;
			var idx = Page.scaleOptions.indexOf( Page.scaleOption );
			var precision = 100;

			for( var i=0; i < Page.scaleOptions.length; ++i ) {
				// continue if scale less than or equal to current
				if( Page.scaleOptions[ i ].pageWidthScale <= Page.zoomScale ) {
					continue;
				}

				// first zoom found
				if( !newZoom ) {
					idx = i;
					newZoom = Math.round( Page.scaleOptions[ i ].pageWidthScale * precision );
					continue;
				}

				// find a more refined value
				if( newZoom > Math.round( Page.scaleOptions[ i ].pageWidthScale * precision ) ) {
					console.log( "TODO: ?" );
					idx = i;
					newZoom = Math.round( Page.scaleOptions[ i ].pageWidthScale * precision );
				}
			}

			// assign the value
			if( angular.isDefined( Page.scaleOptions[ idx ] ) ) {
				Page.scaleOption = Page.scaleOptions[ idx ];
				Page.zoomPage();
				PDFViewer.presenterPageUpdate();
			}
		};

		this.zoomOut = function zoomOut() {
			var newZoom = 0;
			var idx = Page.scaleOptions.indexOf( Page.scaleOption );
			var precision = 100;

			for( var i=0; i < Page.scaleOptions.length; ++i ) {

				// continue if scale greater than or equal to current
				if( Page.scaleOptions[ i ].pageWidthScale >= Page.zoomScale ) {
					continue;
				}

				// make sure scale is larger than small threshold
				if( Math.round( Page.scaleOptions[ i ].pageWidthScale * precision ) < Math.round( 0.05 * precision ) ) {
					continue;
				}

				// first zoom found
				if( !newZoom ) {
					idx = i;
					newZoom = Math.round( Page.scaleOptions[ i ].pageWidthScale * precision );
					continue;
				}

				// find a more refined value
				if( newZoom < Math.round( Page.scaleOptions[ i ].pageWidthScale * precision ) ) {
					idx = i;
					newZoom = Math.round( Page.scaleOptions[ i ].pageWidthScale * precision );
				}
			}

			if( angular.isDefined( Page.scaleOptions[ idx ] ) ) {
				Page.scaleOption = Page.scaleOptions[ idx ];
				Page.zoomPage();
				PDFViewer.presenterPageUpdate();
			}
		};

		this.setScaleMode = function setScaleMode( fit ) {
			console.log( "Page::setScaleMode;", fit );
			PDFViewer.appOrientationDidChange( {"orientation": fit} );
		};

		this.getScaleMode = function getScaleMode() {
			return Page.scaleMode;
		};

		this.getModeZoomScale = function getModeZoomScale() {
			return Page.calcPageWidthFit();
		};

		this.zoomMode = function zoomMode() {
			Page.scaleOption = Page.scaleOptions[Page.getScaleMode()];
			Page.zoomPage();
		};

		this.zoomPage = function zoomPage( args ) {
			var idx = Page.scaleOptions.indexOf( Page.scaleOption );
			if( angular.isDefined( Page.scaleOptions[idx] ) ) {
				Page.resetScaleOptions();
				if( Page.scaleOptions[idx].show === "no" ) {
					Page.scaleOptions[idx].show = "maybe";
				}
			}

			switch( Page.scaleOption.id ) {
				case '0.5':
				case '0.75':
				case '1':
				case '1.25':
				case '1.5':
				case '1.75':
				case '2':
				case '2.25':
				case '2.5':
				case '2.75':
				case '3':
				case '3.25':
				case '3.5':
				case '3.75':
				case '4':
					if( isset( Page.scaleOption.pageWidthScale ) ) {
						Page.setZoomScale( Page.scaleOption.pageWidthScale );
					} else {
						var scale = parseFloat( Page.scaleOption.id );
						if( !isNaN( scale ) ) {
							scale = (Math.round( scale * 100 ) / 100);
							Page.setZoomScale( scale );
						}
					}
					break;
				case PORTRAIT: 
				case LANDSCAPE:
				case 'page-width':
				default:
					Page.setZoomScale( Page.pageWidthScale );
					break;
			}
			Page.updateDimensions();
			if( typeof args === 'object' && args.hasOwnProperty( 'show' ) && args.show === 'yes' ) {
				Page.resetScaleOptions();
			}
		};

		this.updateDimensions = function updateDimensions() {
			//console.log( "Page.updateDimensions; <--", arguments.callee.caller.name );
			//console.log("===UPDATE-DIMENSIONS===", Page.scale, Page.canvasStyle, Page.zoomScale);
			Page.calcPageScale();
			if( [ "presenter", "presentation" ].indexOf( PDFViewer.annotations.mode ) === -1 && Page.scaleOption.id === "page-width" && Page.zoomScale !== Page.scaleOption.scale ) {
				console.log( "Page.updateDimensions;", Page.zoomScale, "!=", Page.scaleOption.scale, "mode:", PDFViewer.annotations.mode );
				console.log( "Page.updateDimensions; changing page-width to:", Page.scaleOption.scale );
				Page.selectScale();
			}
			var pgOrientation = Page.orientation.getOrientation();
			Page.scale.width = (Math.round( (Page.size.width * Page.zoomScale) * 10 ) / 10);
			Page.scale.height = (Math.round( (Page.size.height * Page.zoomScale) * 10 ) / 10);
			Page.wrapStyle.width = Page.pageStyle.width = Page.canvasStyle.width = Page.scale.width;
			Page.wrapStyle.height = Page.pageStyle.height = Page.canvasStyle.height = Page.scale.height;
			//console.log( "Page.updateDimensions; Page.scale:", Page.scale, Page.zoomScale );
			console.log( "Page.updateDimensions; orientation:", pgOrientation );
			Page.canvasStyle.transform = 'rotate(' + (pgOrientation * 90) + 'deg)';
			Page.wrapStyle.textAlign = Page.pageStyle.textAlign = 'left';
			Page.wrapStyle.backgroundColor = Page.pageStyle.backgroundColor = 'white';
			Page.canvasStyle.marginTop = Page.wrapStyle.marginTop = Page.pageStyle.marginTop = Page.canvasStyle.marginLeft = Page.wrapStyle.marginLeft = Page.pageStyle.marginLeft = 0;
			Page.canvasStyle.transformOrigin = '';
			switch( pgOrientation ) {
				case 1:
				case 3:
					Page.wrapStyle.width = Page.pageStyle.width = Page.scale.height;
					Page.wrapStyle.height = Page.pageStyle.height = Page.scale.width;
					Page.canvasStyle.transform = 'translate' + (pgOrientation === 3 ? 'X' : 'Y') + '(-100%) ' + Page.canvasStyle.transform;
					Page.canvasStyle.transformOrigin = (pgOrientation === 3 ? 'right top' : 'left bottom') + ' 0px';
					break;
				case 2:
					Page.canvasStyle.transformOrigin = '50% 50%';
					break;
				case 0:
				default:
					break;
			}
			Page.canvasStyle['-webkit-transform'] = Page.canvasStyle.transform;
			Page.canvasStyle['-moz-transform'] = Page.canvasStyle.transform;
			Page.canvasStyle['-ms-transform'] = Page.canvasStyle.transform;
			Page.canvasStyle['-webkit-transform-origin'] = Page.canvasStyle.transformOrigin;
			Page.canvasStyle['-moz-transform-origin'] = Page.canvasStyle.transformOrigin;
			Page.canvasStyle['-ms-transform-origin'] = Page.canvasStyle.transformOrigin;
			$('#pdf-page-canvas').css({
				'-webkit-transform':Page.canvasStyle.transform,
				'-moz-transform':Page.canvasStyle.transform,
				'-ms-transform':Page.canvasStyle.transform,
				'transform':Page.canvasStyle.transform,
				'-webkit-transform-origin':Page.canvasStyle.transformOrigin,
				'-moz-transform-origin':Page.canvasStyle.transformOrigin,
				'-ms-transform-origin':Page.canvasStyle.transformOrigin,
				'transform-origin':Page.canvasStyle.transformOrigin
			});
			$('#pdf-page').css({
				'width':Page.pageStyle.width,
				'height':Page.pageStyle.height
			});
			Page.pageStyle.marginLeft = (Page.bounds.width > Page.wrapStyle.width) ? Math.floor( (Page.bounds.width - Page.wrapStyle.width) / 2 ) : 0;
			//console.log( "wrapStyle:", Page.wrapStyle );
			//console.log( "pageStyle:", Page.pageStyle );
			//console.log( "canvasStyle:", Page.canvasStyle );
			PDFViewer.safeApply();
		};

		this.resetScaleOptions = function resetScaleOptions() {
			for( var optIdx in Page.scaleOptions ) {
				var opt = Page.scaleOptions[optIdx];
				if( opt.show === "maybe" ) {
					opt.show = "no";
				}
			}
		};

		this.calcPageScale = function calcPageScale() {
			//console.log("===CALC-PAGE-SCALE===", arguments.callee.caller.toString());
			//console.log("===CALC-PAGE-SCALE===", "Width:", $(window).width(), "Height", $(window).height(), "IWidth:", $(window).innerWidth(), "IHeight:", $(window).innerHeight(), "wWidth:", window.innerWidth, "wHeight:", window.innerHeight, "ZoomScale:", Page.zoomScale);
			var pdfToolbar = document.getElementById( "pdf-toolbar-wrap" );
			var boundsHeight = viewPortSize.height - (pdfToolbar ? pdfToolbar.offsetHeight : 0);
			switch( Page.orientation.getOrientation() ) {
				case 0:
				case 2:
				default:
					Page.bounds.width = ((Page.size.height * Page.zoomScale) > boundsHeight ? (viewPortSize.width - scrollbarSize) : viewPortSize.width);
					Page.bounds.height = ((Page.size.width * Page.zoomScale) > viewPortSize.width ? (boundsHeight - scrollbarSize) : boundsHeight);
					break;
				case 1:
				case 3:
					Page.bounds.width = ((Page.size.width * Page.zoomScale) > boundsHeight ? (viewPortSize.width - scrollbarSize) : viewPortSize.width);
					Page.bounds.height = ((Page.size.height * Page.zoomScale) > viewPortSize.width ? (boundsHeight - scrollbarSize) : boundsHeight);
					break;
			}
			//console.log( "Page.calcPageScale; bounds:", Page.bounds, viewPortSize );
			if( PDFViewer.pages[Page.index] ) {
				Page.size = PDFViewer.pages[Page.index].size;
			}
			Page.scaleOptionWidth.scale = Page.calcPageWidthFit();	//page-width scale
			if( Page.orientation.orientation === 0 ) {
				Page.pageWidthScale = Page.scaleOptionWidth.scale;
			}
		};

		Page.pageWidthScale = 1;
		this.calcPageWidthFit = function calcPageWidthFit() {
			var precision = 100000; // 5 decimal places
			var isRotated = !([0,2].indexOf( Page.orientation.getOrientation() ) >= 0);
			var pdfToolbar = document.getElementById( "pdf-toolbar-wrap" );
			var boundsHeight = (viewPortSize.height - (pdfToolbar ? pdfToolbar.offsetHeight : 0));
			var factorWidth = (isRotated ? boundsHeight : viewPortSize.width);
			var viewY = (isRotated ? viewPortSize.width : boundsHeight);
			var pageWidthScale = Math.round( (factorWidth / Page.size.width) * precision ) / precision;
			// console.log( "pageWidthScale:", pageWidthScale );
			if( (Page.size.height * pageWidthScale) > viewY ) {
				pageWidthScale = Math.round( ((factorWidth - scrollbarSize) / Page.size.width) * precision) / precision;
				// console.log( "pageWidthScale w/ scrollbar:", pageWidthScale, scrollbarSize );
				if( !((Page.size.height * pageWidthScale) > viewY) ) {
					for( var i=1; i<=scrollbarSize; ++i ) {
						var pgScale = (factorWidth - i) / Page.size.width;
						if( (Page.size.height * pgScale) <= viewY ) {
							// console.log( "adjusted scale:", i,  pgScale, factorWidth - i );
							pageWidthScale = pgScale;
							break;
						}
					}
				}
			}
			pageWidthScale = Math.round( pageWidthScale * precision ) / precision;
			// console.log( "Page.calcPageWidthFit;", Page.zoomScale, pageWidthScale, Page.size, viewPortSize, Page.bounds );
			var scaleKeys = Object.keys( this.scaleOptions );
			for( var k in scaleKeys ) {
				if( scaleKeys.hasOwnProperty( k ) ) {
					var scaleOption = this.scaleOptions[ scaleKeys[ k ] ];
					scaleOption.pageWidthScale = Math.round( scaleOption.pageScale * pageWidthScale * precision) / precision;
				}
			}
			// console.log( "calcPageWidthFit; pageWidthScale:", pageWidthScale, Page.scaleOptions );
			return pageWidthScale;
		};

		this.spinnerStyle = function spinnerStyle() {
			var width = Page.scale.width;
			var height = Page.scale.height;
			if( Page.bounds.width < width ) {
				width = Page.bounds.width;
			}
			if( Page.bounds.height < height ) {
				height = Page.bounds.height;
			}
			var hInt = (Page.bounds.height / Page.scale.height) * 50;
			return {
				"background-image": "url('/img/spin64.gif')",
				"background-size": "64px 64px",
				"background-position": "50% " + hInt + "%",
				"width": width + 'px',
				"height": height + 'px'
			};
		};

		this.pageImgStyle = function pageImgStyle(src) {
			return {
				"background-image": "url('" + src + "')",
				"background-size": "100% 100%",
				"background-position": "0 0",
				"width": Page.scale.width + 'px',
				"height": "95%"
			};
		};

		this.setPresentationOrientation = function setPresentationOrientation( orientation ) {
			Page.orientation.format = orientation;
			Page.scaleMode = 0;
		};

		this.setPresentationServerZoom = function setPresentationServerZoom( serverZoom ) {
			if( typeof PDFViewer.presentation.serverZoom === "number" && !isNaN( serverZoom ) ) {
				//console.log( "Page.setPresentationServerZoom:", serverZoom );
				PDFViewer.presentation.serverZoom = serverZoom;
			}
		};

		this.getPresentationServerZoom = function getPresentationServerZoom() {
			//console.log( "Page.getPresentationServerZoom:", PDFViewer.presentation.serverZoom );
			return PDFViewer.presentation.serverZoom;
		};

		this.getPresentationOrientationFormatZoom = function getPresentationOrientationFormatZoom() {
			//console.log( "Page.getPresentationOrientationFormatZoom:", (viewPortSize.width / Page.size.width), viewPortSize.width, Page.size.width );
			//return (viewPortSize.width / Page.size.width);
			return 1;
		};

		this.getPresentationZoom = function getPresentationZoom() {
			//console.log( "Page.getPresentationZoom:", Page.getPresentationServerZoom() * Page.getPresentationOrientationFormatZoom() );
			return Page.getPresentationServerZoom() * Page.getPresentationOrientationFormatZoom();
		};

		this.zoomPresentation = function zoomPresentation() {
			var pZoom = Page.getPresentationZoom();
			var zoomScale = Math.round( pZoom * Page.pageWidthScale * 100000 ) / 100000;
			 console.log( "zoomPresentation;", pZoom, zoomScale );
			Page.setZoomScale( zoomScale );
			Page.updateDimensions();
		};

	$(window).on("load",function(){
			if(window.orientation == 0)
			{
			 // $("#pdf-toolbar-wrap").css({"background-color":"yellow","font-size":"300%"});
			  setAppOrientation("portrait");
			  Page.setZoomScale( 1 );
			  Page.updateDimensions();

			}
			else
			{
			  //$("#pdf-toolbar-wrap").css({"background-color":"pink","font-size":"200%"});
			  setAppOrientation("landscape");
			  Page.setZoomScale( 0.5 );
			  Page.updateDimensions();
			}
	});

		$(window).on("load orientationchange",function(){
			if(window.orientation == 0)
			{
			 // $("#pdf-toolbar-wrap").css({"background-color":"yellow","font-size":"300%"});
			  setAppOrientation("portrait");
			  Page.setZoomScale( 1 );
			  Page.updateDimensions();

			}
			else
			{
			  //$("#pdf-toolbar-wrap").css({"background-color":"pink","font-size":"200%"});
			  setAppOrientation("landscape");
			  Page.setZoomScale( 0.5 );
			  Page.updateDimensions();
			}
	});

	

		this.isPortrait = "";
		this.isLandscape = "active";

		function setAppOrientation( orientation ) {
			//console.log( "setAppOrientation; orientation:", orientation );
			orientation = (orientation + '').toLowerCase();
			if( !([ PORTRAIT, LANDSCAPE ].indexOf( orientation ) >= 0) ) {
				//console.log( "setAppOrientation; unknown orientation:", orientation );
				orientation = LANDSCAPE;
			}
			appOrientationClasses();
			PDFViewer.appOrientationDidChange( {"orientation": orientation} );
			Page.scaleOption = Page.scaleOptions[ 0 ];
			Page.selectScale();
			window.setTimeout( redrawCallouts, 0 );
			PDFViewer.Canvas.tickCount = 0;
			PDFViewer.Canvas.refresh();
			PDFViewer.safeApply();

			function redrawCallouts() {
				for( var i in PDFViewer.annotations.callouts.callouts ) {
					var callout = PDFViewer.annotations.callouts.callouts[ i ];
					if( !isset( callout ) ) {
						continue;
					}
					callout.dCallout = callout.denormalizeCallout( callout.nCallout );
					callout.DrawObject.baseScale = callout.dCallout.baseScale;
					callout.State.zoomScale = PDFViewer.Page.scaleOptionWidth.scale;
					callout.View.width = callout.View.Initial.width = Math.round( callout.dCallout.Initial.right );
					callout.View.height = callout.View.Initial.height = Math.round( callout.dCallout.Initial.bottom );
					callout.receiveModify( callout.nCallout );
				}
				PDFViewer.Canvas.refresh();
				PDFViewer.safeApply();
			}
		}

		function appOrientationClasses() {
			//console.log( "Page.appOrientationClasses;", appOrientation );
			this.isPortrait = (appOrientation === PORTRAIT ? "active" : "");
			this.isLandscape = (appOrientation === LANDSCAPE ? "active" : "");
			//console.log( "Page.appOrientationClasses; isPortrait:", this.isPortrait, "isLandscape:", this.isLandscape );
		}
		this.appOrientationClasses = appOrientationClasses;

		function portraitOrientation() {
			// console.log( "Page.portraitOrientation;" );
			setAppOrientation( PORTRAIT );
		}
		this.portraitOrientation = portraitOrientation;

		function landscapeOrientation() {
			// console.log( "Page.landscapeOrientation;" );
			setAppOrientation( LANDSCAPE );
		}
		this.landscapeOrientation = landscapeOrientation;

	};

	function ToolbarClass() {
		var _s = {};
	};

	function CanvasClass() {
		var Canvas = this;
		this.idCanvas = 'pdf-page-canvas';
		this.idPage = 'pdf-page';
		this.coord = {pixelX:0,pixelY:0,scaleX:0,scaleY:0,pdfX:0,pdfY:0,eventX:0,eventY:0};
		this.mode = 'Select'; // Select, Draw, Modify
		this.orientation = 0;
		this.pdfScale = 1.5;
		this.canvasScale = 1.0;
		this.tickCount = 0;
		var tickInterval;

		this.setIdString = function setIdString(idString) {
			Canvas.idCanvas = idString;
		};
		this.tick = function tick() {
			// console.log( "Canvas.tick;", Canvas.tickCount );
			++Canvas.tickCount;
			Canvas.refresh();
			if( Canvas.tickCount > 5 && tickInterval ) {
				window.clearInterval( tickInterval );
			}
		};
		this.getCanvasOffsetX = function getCanvasOffsetX() {
			var offset = $('#'+PDFViewer.Canvas.idCanvas).offset();
			return offset.left;
		};
		this.getCanvasOffsetY = function getCanvasOffsetY() {
			var offset = $('#'+PDFViewer.Canvas.idCanvas).offset();
			return offset.top;
		};
		this.getCanvasOffset = function getCanvasOffset() {
			return $( "#" + PDFViewer.Canvas.idCanvas ).offset();
		};
		this.refresh = function refresh() {
			PDFViewer.drawCanvas( Canvas.idCanvas, Canvas.tickCount );
			if( Canvas.tickCount === 0 ) {
				++Canvas.tickCount;
				window.clearInterval( tickInterval );
				tickInterval = window.setInterval( this.tick, 300 );
			}
		};

		window.clearInterval( tickInterval );
		tickInterval = window.setInterval( this.tick, 300 );

		function EventClass() {
			var Event = this;
			Event.simpleHandlerStatus = -1;
			function MouseClass() {
				var Mouse = this;
				this.DRAW_MOUSE_STATE_UP = 'up';
				this.DRAW_MOUSE_STATE_DOWN = 'down';
				this.DRAW_MOUSE_STATE_CLICK = 'click';
				this.DRAW_MOUSE_PRESENCE_FOCUS = 'focus';
				this.DRAW_MOUSE_PRESENCE_BLUR = 'blur';
				this.state = this.DRAW_MOUSE_STATE_UP;
				this.presence = this.DRAW_MOUSE_PRESENCE_BLUR;
				this.originX = 0; // to track the movement
				this.originY = 0; // to track the movement
				this.latestX = 0; // to track the movement
				this.latestY = 0; // to track the movement
				this.handlers = [];
				this.delegate = function delegate( myThis ) {
					var self = isset( myThis ) ? myThis : this;
					//this.self = self;
					this.click = function click( event ) {
						event.preventDefault(); // do nothing by default
					};
					this.dblclick = function dblclick( event ) {
						event.preventDefault(); // do nothing by default
					};
					this.down = function down( event ) {
						event.preventDefault(); // do nothing by default
					};
					this.up = function up( event ) {
						event.preventDefault(); // do nothing by default
					};
					this.move = function move( event ) {

						event.preventDefault(); // do nothing by default
					};
					this.hover = function hover( event ) {
						event.preventDefault(); // do nothing by default
					};
					this.touchup = function touchup( event ) {
						return true;
					};
					this.touchdown = function touchdown( event ) {
						return true;
					};
					this.touchmove = function touchmove( event ) {
						return true;
					};
					this.toString = function toString() {
						return "MouseDelegate";
					};
					this.getThis = function getThis() {
						return self;
					};
				};
				this.isMainCanvas = false;
				this._event = null;

				this.addHandler = function addHandler( mouseDelegate ) {
					if( mouseDelegate && mouseDelegate.toString() === "MouseDelegate" ) {
						this.removeHandler( mouseDelegate );
						this.handlers.push( mouseDelegate );
						return true;
					}
					return false;
				};
				this.removeHandler = function removeHandler( mouseDelegate ) {
					var index = jQuery.inArray( mouseDelegate, this.handlers );
					if( index > -1 ) {
						this.handlers.splice( index, 1 );
						return true;
					}
					return false;
				};
				this.click = function click( event ) {
					this._event = event;
					this.update( event.clientX, event.clientY );
					angular.forEach( this.handlers, function( delegate ) {
						if( delegate && delegate.toString() == "MouseDelegate" ) {
							var obj = delegate.getThis();
							if( !!delegate.click.call( obj, event ) ) {
								Event.simple.ignore( event );
							}
						}
					} );
				};
				this.dblclick = function dblclick( event ) {
					this._event = event;
					this.update( event.clientX, event.clientY );
					angular.forEach( this.handlers, function( delegate ) {
						if( delegate && delegate.toString() == "MouseDelegate" ) {
							var obj = delegate.getThis();
							delegate.dblclick.call( obj, event );
						}
					} );
				};
				this.down = function down( event ) {
					this._event = event;
					this.state = this.DRAW_MOUSE_STATE_DOWN;
					//console.log( "down; event.clientX:", event.clientX, "event.clientY:", event.clientY );
					this.update( event.clientX, event.clientY, true );
					if( this.isMainCanvas && !event.vicarious ) {
						$( ".pdf-capture" ).css( {"z-index": 1000} );
					}
					angular.forEach( this.handlers, function( delegate ) {
						if( delegate && delegate.toString() === "MouseDelegate" ) {
							var obj = delegate.getThis();
							// console.log( "delegate:", obj );
							delegate.down.call( obj, event );
						}
					} );
				};
				this.up = function up( event ) {
					this._event = event;
					var previousState = this.state;
					this.state = this.DRAW_MOUSE_STATE_UP;
					this.update( event.clientX, event.clientY );
					if( this.isMainCanvas && !event.vicarious ) {
						$( ".pdf-capture" ).css( {"z-index": ""} );
					}
					if( previousState === this.DRAW_MOUSE_STATE_DOWN ) {
						angular.forEach( this.handlers, function( delegate ) {
							if( delegate && delegate.toString() === "MouseDelegate" ) {
								var obj = delegate.getThis();
								delegate.up.call( obj, event );
							}
						} );
					}
				};
				this.enter = function enter( event ) {
					this._event = event;
					this.presence = this.DRAW_MOUSE_PRESENCE_FOCUS;
					this.state = this.DRAW_MOUSE_STATE_UP;
					this.update( event.clientX, event.clientY );
				};
				this.leave = function leave( event ) {
					this._event = event;
					this.presence = this.DRAW_MOUSE_PRESENCE_BLUR;
					this.update( event.clientX, event.clientY );
					if( this.state === this.DRAW_MOUSE_STATE_DOWN ) {
						this.up( event );
					}
				};
				this.move = function move( event ) {
					this._event = event;
					this.update( event.clientX, event.clientY );
					if( this.state === this.DRAW_MOUSE_STATE_DOWN ) {
						angular.forEach( this.handlers, function( delegate ) {
							if( delegate && delegate.toString() === "MouseDelegate" ) {
								var obj = delegate.getThis();
								delegate.move.call( obj, event );
							}
						} );
					} else {
						angular.forEach( this.handlers, function( delegate ) {
							var obj1 = delegate.getThis();
							if( obj1 && obj1.toString() == "MouseDelegate" ) {
								var obj2 = obj1.getThis();
								obj1.hover.call( obj2, event );
							}
						} );
					}
				};
				this.getTouchX = function getTouchX( event ) {
					this._event = event;
					if( isset( event ) && isset( event.originalEvent ) && isset( event.originalEvent.touches ) && isset( event.originalEvent.touches[ 0 ] ) ) {
						return event.originalEvent.touches[ 0 ].clientX;
					}
					return -1;
				};
				this.getTouchY = function getTouchY( event ) {
					this._event = event;
					if( isset( event ) && isset( event.originalEvent ) && isset( event.originalEvent.touches ) && isset( event.originalEvent.touches[ 0 ] ) ) {
						return event.originalEvent.touches[ 0 ].clientY;
					}
					return -1;
				};
				this.touchdown = function touchdown( event ) {
					this._event = event;
					//console.log("touchdown:", Mouse.getTouchX(event), Mouse.getTouchY(event));
					this.state = this.DRAW_MOUSE_STATE_DOWN;
					this.originX = this.latestX = this.getTouchX( event );
					this.originY = this.latestY = this.getTouchY( event );
					this.update( this.getTouchX( event ), this.getTouchY( event ) );
					angular.forEach( this.handlers, function( delegate ) {
						if( delegate && delegate.toString() === "MouseDelegate" ) {
							var obj = delegate.getThis();
							if( delegate.touchdown.call( obj, event, this.originX, this.originY ) ) {
								delegate.down.call( obj, event );
							}
						}
					} );
				};
				this.touchup = function touchup( event ) {
					this._event = event;
					//console.log("touchup:", Mouse.getTouchX(event), Mouse.getTouchY(event));
					var previousState = this.state;
					this.state = this.DRAW_MOUSE_STATE_UP;
					var isClick = false;
					if( Math.abs( this.originX - this.latestX ) < 5 && Math.abs( this.originY - this.latestY ) < 5 ) {
						isClick = true;
					}
					//Mouse.update(Mouse.getTouchX(event), Mouse.getTouchY(event));
					if( previousState === this.DRAW_MOUSE_STATE_DOWN ) {
						angular.forEach( this.handlers, function( delegate ) {
							if( delegate && delegate.toString() == "MouseDelegate" ) {
								var obj = delegate.getThis();
								if( delegate.touchup.call( obj, event, this.latestX, this.latestY ) ) {
									event.clientX = this.latestX;
									event.clientY = this.latestY;
									delegate.up.call( obj, event );
									delegate.click.call( obj, event );
								}
							}
						} );
					}
				};
				this.touchmove = function touchmove( event ) {
					this._event = event;
					//console.log("touchmove:", Mouse.getTouchX(event), Mouse.getTouchY(event));
					this.latestX = this.getTouchX( event );
					this.latestY = this.getTouchY( event );
					this.update( this.getTouchX( event ), this.getTouchY( event ) );
					if( this.state === this.DRAW_MOUSE_STATE_DOWN ) {
						angular.forEach( this.handlers, function( delegate ) {
							if( delegate && delegate.toString() === "MouseDelegate" ) {
								var obj = delegate.getThis();
								if( delegate.touchmove.call( obj, event, this.latestX, this.latestY ) ) {
									delegate.move.call( obj, event );
								}
							}
						} );
					} else {
						angular.forEach( this.handlers, function( delegate ) {
							if( delegate && delegate.toString() === "MouseDelegate" ) {
								var obj = delegate.getThis();
								delegate.hover.call( obj, event );
							}
						} );
					}
				};
				this.update = function update( clientX, clientY, verbose ) {
					// if( !!verbose ) {
					// 	console.log( "MouseClass.update; clientX:", clientX, "clientY:", clientY, "isMainCanvas:", this.isMainCanvas, "vicarious:", vicarious );
					// }
					//console.log( 'MOUSE COORDINATES' );
					//console.log( 'Orientation:', Page.orientation.getOrientation() );
					//var body = document.getElementsByTagName('body');
					//console.log('Body Scroll Left:', body[0].scrollLeft);
					//console.log('Body Scroll Top:', body[0].scrollTop);
					var $pg = $( "#" + PDFViewer.Canvas.idPage );
					var $doc = $( document );
					var pageScrollX = $pg.scrollLeft();
					var pageScrollY = $pg.scrollTop();
					var canvasOffset = $pg.offset();
					var pageEl = $pg.get( 0 );
					var docScrollX = $doc.scrollLeft();
					var docScrollY = $doc.scrollTop();
					PDFViewer.Canvas.coord.eventX = clientX;
					PDFViewer.Canvas.coord.offsetX = canvasOffset.left;
					PDFViewer.Canvas.coord.scrollX = docScrollX;
					PDFViewer.Canvas.coord.eventY = clientY;
					PDFViewer.Canvas.coord.offsetY = canvasOffset.top;
					PDFViewer.Canvas.coord.scrollY = docScrollY;
					if( !!this._event.vicarious ) {
						docScrollX = 0;
						docScrollY = 0;
					}
					var _zoomScale = Page.getZoomScale();
					var _scaleFactor = PDFViewer.getScaleFactor();
					switch( Page.orientation.getOrientation() ) {
						case 1:
							offsetTop = pageEl.offsetTop - Math.ceil( Page.wrapStyle.marginTop );
							//console.log('Offset Top:', offsetTop);
							offsetLeft = pageEl.offsetLeft - Math.ceil( Page.wrapStyle.marginLeft );
							//console.log('Offset Left:', offsetLeft);
							PDFViewer.Canvas.coord.pixelX = (clientY + docScrollY) - offsetTop;
							//console.log('Canvas Pixel X:', PDFViewer.Canvas.coord.pixelX);
							PDFViewer.Canvas.coord.pixelY = Page.scale.height - ((clientX + docScrollX) - offsetLeft);
							//console.log('Canvas Pixel Y:', PDFViewer.Canvas.coord.pixelY);
							PDFViewer.Canvas.coord.scaleX = PDFViewer.Canvas.coord.pixelX / _zoomScale;
							//console.log('Canvas Scale X:', PDFViewer.Canvas.coord.scaleX);
							PDFViewer.Canvas.coord.scaleY = PDFViewer.Canvas.coord.pixelY / _zoomScale;
							//console.log('Canvas Scale Y:', PDFViewer.Canvas.coord.scaleY);
							PDFViewer.Canvas.coord.pdfX = PDFViewer.Canvas.coord.scaleX / _scaleFactor;
							PDFViewer.Canvas.coord.pdfY = PDFViewer.Canvas.coord.scaleY / _scaleFactor;
							break;
						case 2:
							PDFViewer.Canvas.coord.pixelX = Page.scale.width - ((clientX + docScrollX) - pageEl.offsetLeft);
							//console.log('Canvas Pixel X:', PDFViewer.Canvas.coord.pixelX);
							PDFViewer.Canvas.coord.pixelY = Page.scale.height - ((clientY + docScrollY) - pageEl.offsetTop);
							//console.log('Canvas Pixel Y:', PDFViewer.Canvas.coord.pixelY);
							PDFViewer.Canvas.coord.scaleX = PDFViewer.Canvas.coord.pixelX / _zoomScale;
							//console.log('Canvas Scale X:', PDFViewer.Canvas.coord.scaleX);
							PDFViewer.Canvas.coord.scaleY = PDFViewer.Canvas.coord.pixelY / _zoomScale;
							//console.log('Canvas Scale Y:', PDFViewer.Canvas.coord.scaleY);
							PDFViewer.Canvas.coord.pdfX = PDFViewer.Canvas.coord.scaleX / _scaleFactor;
							PDFViewer.Canvas.coord.pdfY = PDFViewer.Canvas.coord.scaleY / _scaleFactor;
							break;
						case 3:
							offsetTop = pageEl.offsetTop - Math.ceil( Page.wrapStyle.marginTop );
							//console.log('Offset Top:', offsetTop);
							offsetLeft = pageEl.offsetLeft - Math.ceil( Page.wrapStyle.marginLeft );
							//console.log('Offset Left:', offsetLeft);
							PDFViewer.Canvas.coord.pixelX = Page.scale.width - ((clientY + docScrollY) - offsetTop);
							//console.log('Canvas Pixel X:', PDFViewer.Canvas.coord.pixelX);
							PDFViewer.Canvas.coord.pixelY = (clientX + docScrollX) - pageEl.offsetLeft;
							//console.log('Canvas Pixel Y:', PDFViewer.Canvas.coord.pixelY);
							PDFViewer.Canvas.coord.scaleX = PDFViewer.Canvas.coord.pixelX / _zoomScale;
							//console.log('Canvas Scale X:', PDFViewer.Canvas.coord.scaleX);
							PDFViewer.Canvas.coord.scaleY = PDFViewer.Canvas.coord.pixelY / _zoomScale;
							//console.log('Canvas Scale Y:', PDFViewer.Canvas.coord.scaleY);
							PDFViewer.Canvas.coord.pdfX = PDFViewer.Canvas.coord.scaleX / _scaleFactor;
							PDFViewer.Canvas.coord.pdfY = PDFViewer.Canvas.coord.scaleY / _scaleFactor;
							break;
						case 0:
						default:
							PDFViewer.Canvas.coord.pixelX = ((clientX + docScrollX) - pageEl.offsetLeft);
							PDFViewer.Canvas.coord.pixelY = ((clientY + docScrollY) - pageEl.offsetTop);
							PDFViewer.Canvas.coord.scaleX = PDFViewer.Canvas.coord.pixelX / _zoomScale;
							PDFViewer.Canvas.coord.scaleY = PDFViewer.Canvas.coord.pixelY / _zoomScale;
							PDFViewer.Canvas.coord.pdfX = PDFViewer.Canvas.coord.scaleX / _scaleFactor;
							PDFViewer.Canvas.coord.pdfY = PDFViewer.Canvas.coord.scaleY / _scaleFactor;
							break;
					}
					// if( !!verbose ) {
					// 	console.log( "MouseClass.update; vicarious:", !!this._event.vicarious, " _zoomScale:", _zoomScale );
					// 	console.log( "MouseClass.update; clientX:", clientX, "clientY:", clientY );
					// 	console.log( "MouseClass.update; PDFViewer.Canvas.coord.pixelX:", PDFViewer.Canvas.coord.pixelX, "pixelY:", PDFViewer.Canvas.coord.pixelY );
					// 	console.log( "MouseClass.update; PDFViewer.Canvas.coord.scaleX:", PDFViewer.Canvas.coord.scaleX, "scaleY:", PDFViewer.Canvas.coord.scaleY );
					// }
				};
			};
			this.MouseFactory = function MouseFactory() {
				return new MouseClass();
			};
			this.mouse = this.MouseFactory();
			this.mouse.isMainCanvas = true;
			this.mouse.parent = this;
			this.simple = this.MouseFactory(); // generic webapp click handler
			this.simple.parent = this;
			this.simple.click = function click(evt) {
				if(Math.abs(Event.simpleHandlerStatus) === 1) {
					if(typeof Toolbar.activityCallback === "function") {
						Toolbar.activityCallback(false);
					}
				}
				Event.simpleHandlerStatus = -1;
			};
			this.simple.ignore = function ignore(evt) {
				Event.simpleHandlerStatus = 0;
			};
		};
		this.EventFactory = function EventFactory() {
			return new EventClass();
		};
		this.events = this.EventFactory();
		this.events.parent = this;
	};

	function StampClass() {
		var _s = {};
		var Stamp = this;
		var nowdate = new Date();
		Stamp.mode = false;
		Stamp.toolCode = 'draw-stamp';
		Stamp.undoStatus = true;
		Stamp.show = false;
		Stamp.pipeline = DrawPipelineFactory();
		Stamp.DrawObject = undefined;
		Stamp.pointX = -1;
		Stamp.pointY = -1;
		Stamp.stampHeight = -1;
		Stamp.title = {
			myValue: "",
			defaultValue: "",
			oldValue: "",
			myStyle: {"width": 180, "minWidth": 180, "maxWidth": 640},
			inputId: "stampTitleInput",
			shadowId: "stampTitleShadow",
			keydown: function keydown( event ) {
				event = event || window.event;
				window.setTimeout( Stamp.adjustSize, 0 );
				var keyCode = "keyCode" in event ? event.keyCode : event.which;
				if( keyCode === 13 ) {
					if( Stamp.title.myValue === "" && Stamp.subtitle.myValue === "" ) {
						Stamp.title.myValue = Stamp.title.defaultValue;
					}
					$( "#" + Stamp.subtitle.inputId ).focus();
				}
			},
			keyup: function keyup( $event ) {
				window.setTimeout( Stamp.adjustSize, 0 );
			}
		};
		Stamp.subtitle = {
			myValue: "",
			defaultValue: "",
			oldValue: "",
			myStyle: {"width": 180, "minWidth": 180, "maxWidth": 640},
			inputId: "stampSubtitleInput",
			shadowId: "stampSubtitleShadow",
			keydown:function keydown( event ) {
				event = event || window.event;
				window.setTimeout( Stamp.adjustSize, 0 );
				var keyCode = "keyCode" in event ? event.keyCode : event.which;
				if( Stamp.title.myValue === "" && Stamp.subtitle.myValue === "" ) {
					Stamp.subtitle.myValue = Stamp.subtitle.defaultValue;
				}
				if( keyCode === 13 ) {
					Stamp.place();
				}
			},
			keyup: function keyup( $event ) {
				window.setTimeout( Stamp.adjustSize, 0 );
			}
		};
		Stamp.date = {
			myValue:(nowdate.getMonth()+1)+'/'+nowdate.getDate()+'/'+(nowdate.getFullYear().toString().substr(2,2)),
			minimumValue:null,
			defaultValue:'',
			oldValue:''
		};
		Stamp.createDrawObject = function createDrawObject() {
			if(Stamp.title.myValue === '') {
				Stamp.title.myValue = Stamp.title.defaultValue;
			}
			var params = DrawObjectDefaultStampParams();
			canvasWidth = PDFViewer.getCanvasWidth();
			canvasHeight = PDFViewer.getCanvasHeight();
			params.line1 = Stamp.title.myValue;
			params.line2 = Stamp.subtitle.myValue;
			params.line3 = Stamp.date.myValue;
			params.canvasWidth = canvasWidth;
			params.canvasHeight = canvasHeight;
			if(Stamp.pointX === -1)
				Stamp.pointX = canvasWidth-80;
			if(Stamp.pointY === -1)
				Stamp.pointY = 50;
			params.pointList.push([Stamp.pointX,Stamp.pointY]);
			params.margin = [2,2,2,2];
			Stamp.DrawObject = DrawObjectFactory(params);
			if(Stamp.stampHeight !== -1)
				Stamp.DrawObject.stampHeight = Stamp.stampHeight;
			Stamp.DrawObject.select();
			Stamp.pipeline.add(Stamp.DrawObject);
			{ PDFViewer.Canvas.refresh(); Stamp.DrawObject.stampValidateLocation(); }// make sure stamp is on the page
		};
		Stamp.isDrawObject = function isDrawObject() {
			if(typeof Stamp.DrawObject === 'undefined' || Stamp.DrawObject === null) {
				return false;
			}
			if(Stamp.DrawObject.toString() === "DrawObject") {
				return true;
			}
			return false;
		};
		Stamp.clearDrawObject = function clearDrawObject() {
			Stamp.DrawObject = null;
			Stamp.pipeline.clear();
		};
		Stamp.startPlacement = function startPlacement() {
			Stamp.createDrawObject();
			Toolbar.setModalTool(Stamp.toolCode, true);
			var DO = Stamp.DrawObject;
			var x = DO.pointLists[0][0];
			var y = DO.pointLists[0][1];
			PDFViewer.scrollTo(x - DO.width, y - DO.height);// scroll to the position
			PDFViewer.Canvas.refresh();
		};
		Stamp.adjustSize = function adjustSize() {
			var titleShadow = $( "#stampTitleShadow" );
			var subtitleShadow = $( "#stampSubtitleShadow" );
			$( "#stampTitleInput" ).width( Math.max( Math.min( Math.max( titleShadow.width(), subtitleShadow.width() ), 640 ), 180 ) + 20 );
			$( "#stampSubtitleInput" ).width( Math.max( Math.min( Math.max( titleShadow.width(), subtitleShadow.width() ), 640 ), 180 ) + 20 );
		};
		Stamp.start = function start(title, subtitle, date) {
			Stamp.title.myValue = Stamp.title.oldValue = Stamp.title.defaultValue = title;
			Stamp.subtitle.myValue = Stamp.subtitle.oldValue = Stamp.subtitle.myValue = subtitle;
			Stamp.date.myValue = Stamp.date.oldValue = Stamp.date.myValue = date;
			PDFViewer.introduceStart( title, subtitle, date, true );
		};
		Stamp.dialog = function dialog() {
			Stamp.show = true;
		};
		Stamp.place = function place() {
			//console.log("->"+Stamp.title.myValue+"<-");
			if(typeof Stamp.title.myValue === "undefined") {
				Stamp.title.myValue = '';
			}
			if(typeof Stamp.subtitle.myValue === "undefined") {
				Stamp.subtitle.myValue = '';
			}
			if(Stamp.title.myValue === '' && Stamp.subtitle.myValue === '') {
				Stamp.title.myValue = Stamp.title.defaultValue;
			}
			Stamp.show = false;
			Stamp.startPlacement();
		};
		Stamp.undoBtn = function undoBtn(event) {
			$('#stampTitleInput').focus();
			Stamp.undoStatus = !Stamp.undoStatus;
			Stamp.title.oldValue = Stamp.title.myValue;
			Stamp.subtitle.oldValue = Stamp.subtitle.myValue;
			Stamp.title.myValue = '';
			Stamp.subtitle.myValue = '';
		};
		Stamp.redoBtn = function redoBtn(event) {
			$('#stampTitleInput').focus();
			Stamp.undoStatus = !Stamp.undoStatus;
			Stamp.title.myValue = Stamp.title.oldValue;
			Stamp.subtitle.myValue = Stamp.subtitle.oldValue;
			Stamp.title.oldValue = '';
			Stamp.subtitle.oldValue = '';
		};
		Stamp.resetMouseEvent = function resetMouseEvent() {
			var CenterX = 0, CenterY = 0;
			if(Stamp.DrawObject) {
				CenterX = Stamp.DrawObject.centerX;
				CenterY = Stamp.DrawObject.centerY;
			}
			return {mode:'',
					centerX: CenterX,
					centerY: CenterY,
					originX:Canvas.coord.scaleX,
					originY:Canvas.coord.scaleY,
					lastX:Canvas.coord.scaleX,
					lastY:Canvas.coord.scaleY,
					variance:0,
					varianceX:0,
					varianceY:0,
					deltaX:0,
					deltaY:0,
					radians: 0.0,
					reversed: false
				};
		};
		Stamp.mouseEvent = Stamp.resetMouseEvent();
		Stamp.updateMouseEvent = function updateMouseEvent() {
			var varianceX = Math.abs(Stamp.mouseEvent.originX - Canvas.coord.scaleX);
			var varianceY = Math.abs(Stamp.mouseEvent.originY - Canvas.coord.scaleY);
			Stamp.mouseEvent.variance = Math.max(varianceX, varianceY, Stamp.mouseEvent.variance);
			Stamp.mouseEvent.varianceX = Math.max(varianceX, Stamp.mouseEvent.varianceX);
			Stamp.mouseEvent.varianceY = Math.max(varianceY, Stamp.mouseEvent.varianceY);
			if(Stamp.mouseEvent.reversed)
				Stamp.mouseEvent.radians = Math.atan2(Canvas.coord.scaleY - Stamp.mouseEvent.centerY, Canvas.coord.scaleX - Stamp.mouseEvent.centerX);
			else
				Stamp.mouseEvent.radians = Math.atan2(Stamp.mouseEvent.centerY - Canvas.coord.scaleY, Stamp.mouseEvent.centerX - Canvas.coord.scaleX);
			Stamp.mouseEvent.deltaX = Stamp.mouseEvent.lastX - Canvas.coord.scaleX;
			Stamp.mouseEvent.deltaY = Stamp.mouseEvent.lastY - Canvas.coord.scaleY;
			Stamp.mouseEvent.lastX = Canvas.coord.scaleX;
			Stamp.mouseEvent.lastY = Canvas.coord.scaleY;
		};
		/* global M_PI_4, DEFAULT_STAMP_MAX_HEIGHT */
		Stamp.normalizedAngleFromAngle = function normalizedAngleFromAngle(radians) {
			if(Math.abs(radians) < M_PI_4) {				// Left
				return 0;
			}
			if(Math.abs(radians) > (3 * M_PI_4)) {			// Right
				return 2;
			}
			if (radians > 0) {							// Up
				return 1;
			}
			return 3;
		};
		Stamp.setDateMinimum = function setDateMinimum(minimumDate) {
			Stamp.date.minimumValue = minimumDate;
		};
		Stamp.getSaveJson = function getSaveJson() {
			var title = Stamp.title.myValue;
			var subtitle = Stamp.subtitle.myValue;
			var date = Stamp.date.myValue;
			if(!Stamp.DrawObject) {
				Stamp.createDrawObject();
			}
			var pdfScale = 1.5;
			var result = {
				"title": (Stamp.title.myValue !== Stamp.DrawObject.line1) ? Stamp.title.myValue : Stamp.DrawObject.line1,
				"subtitle": (Stamp.subtitle.myValue !== Stamp.DrawObject.line2) ? Stamp.subtitle.myValue : Stamp.DrawObject.line2,
				"date": (Stamp.date.myValue !== Stamp.DrawObject.line3) ? Stamp.date.myValue : Stamp.DrawObject.line3,
				"orientation": Stamp.DrawObject.orientation,
				"stampX": (Stamp.DrawObject.boundingBox[0][0]) / pdfScale,
				"stampY": ((Stamp.DrawObject.canvasHeight - Stamp.DrawObject.boundingBox[0][1] - Stamp.DrawObject.boundingBox[1][1])) / pdfScale,
				"width": (Stamp.DrawObject.width / pdfScale),
				"height": (Stamp.DrawObject.height / pdfScale)
			};
			return result;
		};
		Stamp.delayedGetSaveJson = function delayedGetSaveJson(callback) {
			Stamp.createDrawObject();
			$.when(Stamp.deferredGetSaveJson())
				.then(function(result){
					if(typeof callback === "function") {
						callback(result);
					}
				});
		};
		Stamp.deferredGetSaveJson = function deferredGetSaveJson() {
			var dfd = jQuery.Deferred();
			setTimeout(function(){
				Stamp.DrawObject.stampValidateLocation();
				dfd.resolve(Stamp.getSaveJson());
			}, 250);
			return dfd.promise();
		};
		Stamp.mouseHandler = new Canvas.events.mouse.delegate();
		Stamp.mouseHandler.click = function click(event) {
			Stamp.updateMouseEvent();
			if(Stamp.mouseEvent.variance < 5 && Stamp.mouseEvent.mode === 'move') {
				//Stamp.disableFn();
				if(isset(Stamp.DrawObject) && Stamp.DrawObject.toString() === "DrawObject") {
					Stamp.pointX = Stamp.DrawObject.centerX;
					Stamp.pointY = Stamp.DrawObject.canvasHeight - Stamp.DrawObject.centerY;
					Stamp.stampHeight = Stamp.DrawObject.stampHeight;
					delete Stamp.DrawObject;
					Stamp.DrawObject = null;
				}
				Stamp.pipeline.clear();
				Canvas.refresh();
				Stamp.dialog();
			}
		};
		Stamp.mouseHandler.down = function down(event) {
			// do the click stuff
			event.preventDefault();
			Stamp.mouseEvent = Stamp.resetMouseEvent();
			Stamp.mouseEvent.mode = Stamp.DrawObject.hitStamp(Canvas.coord.scaleX, Canvas.coord.scaleY);
			if(Stamp.mouseEvent.mode == 'move') {
				Stamp.DrawObject.startDrag(Canvas.coord.scaleX, Canvas.coord.scaleY);
			}
		};
		Stamp.mouseHandler.move = function move(event) {
			// do the click stuff
			Stamp.updateMouseEvent();
			event.preventDefault();
			if(Stamp.mouseEvent.mode == 'move') {
				Stamp.DrawObject.drag(Canvas.coord.scaleX, Canvas.coord.scaleY);
				Canvas.refresh();
			}
			else if(Stamp.mouseEvent.mode == 'rotate/resize') {
				if(((Stamp.DrawObject.orientation % 2)==0) && Stamp.mouseEvent.varianceY > 10
					|| ((Stamp.DrawObject.orientation % 2)==1) && Stamp.mouseEvent.varianceX > 10) {
					Stamp.mouseEvent.mode = 'rotate';
				}
				if(((Stamp.DrawObject.orientation % 2)==0) && Stamp.mouseEvent.varianceX > 7
					|| ((Stamp.DrawObject.orientation % 2)==1) && Stamp.mouseEvent.varianceY > 7) {
					Stamp.mouseEvent.mode = 'resize';
				}
				Canvas.refresh();
			}
			else if(Stamp.mouseEvent.mode == 'rotate') {
				Stamp.DrawObject.setRotation(Stamp.mouseEvent.radians);
				Canvas.refresh();
			}
			else if(Stamp.mouseEvent.mode == 'resize') {
				if((Stamp.DrawObject.orientation == 0 && !Stamp.DrawObject.reverse)
					|| (Stamp.DrawObject.orientation == 2 && Stamp.DrawObject.reverse))
					Stamp.DrawObject.stampHeight += Stamp.mouseEvent.deltaX;
				else if((Stamp.DrawObject.orientation == 1 && !Stamp.DrawObject.reverse)
					|| (Stamp.DrawObject.orientation == 3 && Stamp.DrawObject.reverse))
					Stamp.DrawObject.stampHeight += Stamp.mouseEvent.deltaY;
				else if((Stamp.DrawObject.orientation == 2 && !Stamp.DrawObject.reverse)
					|| (Stamp.DrawObject.orientation == 0 && Stamp.DrawObject.reverse))
					Stamp.DrawObject.stampHeight -= Stamp.mouseEvent.deltaX;
				else if((Stamp.DrawObject.orientation == 3 && !Stamp.DrawObject.reverse)
					|| (Stamp.DrawObject.orientation == 1 && Stamp.DrawObject.reverse))
					Stamp.DrawObject.stampHeight -= Stamp.mouseEvent.deltaY;
				Canvas.refresh();
			}
		};
		Stamp.mouseHandler.up = function up(event) {
			Stamp.updateMouseEvent();
			event.preventDefault();
			if(Stamp.mouseEvent.mode == 'move') {
				Stamp.DrawObject.endDrag(Canvas.coord.scaleX, Canvas.coord.scaleY);
				Stamp.DrawObject.stampValidateLocation();
				Canvas.refresh();
			}
			else if(Stamp.mouseEvent.mode == 'rotate') {
				var orientation = Stamp.normalizedAngleFromAngle(Stamp.DrawObject.rotate);
				Stamp.DrawObject.setOrientation(orientation);
				Stamp.DrawObject.stampValidateLocation();
				Canvas.refresh();
			}
			else if(Stamp.mouseEvent.mode == 'resize') {
				if(Stamp.DrawObject.orientation == 0)
					Stamp.DrawObject.stampHeight += Stamp.mouseEvent.deltaX;
				else if(Stamp.DrawObject.orientation == 1)
					Stamp.DrawObject.stampHeight += Stamp.mouseEvent.deltaY;
				else if(Stamp.DrawObject.orientation == 2)
					Stamp.DrawObject.stampHeight -= Stamp.mouseEvent.deltaX;
				else
					Stamp.DrawObject.stampHeight -= Stamp.mouseEvent.deltaY;
				Stamp.DrawObject.stampValidateLocation();
				Canvas.refresh();
			}
		};
		Stamp.mouseHandler.hover = function hover(event) {
			// hover stuff
		};
	};

	function PDFViewerAPIClass() {
		var self = this;
		this.selectPage = function selectPage( pageNum ) {
			var result = PDFViewer.selectPage( pageNum );
			if( result ) {
				$rootScope.$apply();
			}
			return result;
		};

		this.zoomPage = function zoomPage( scale ) {
			console.log( "PDFViewerAPI::zoomPage", scale );
			if( typeof scale === "string" ) {
				for( var sIdx in Page.scaleOptions ) {
					var _scale = Page.scaleOptions[sIdx];
					if( _scale.id === scale ) {
						Page.scaleOption = _scale;
						Page.selectScale();
//						$rootScope.$apply();
						PDFViewer.safeApply();
						return true;
					}
				}
				return false;
			}
			scale = Math.round( parseFloat( scale ) * 100 ) / 100;
			if( isNaN( scale ) ) {
				return false;
			}
			for( var sIdx in Page.scaleOptions ) {
				var _scale = Page.scaleOptions[sIdx];
				if( _scale.scale === scale ) {
					Page.scaleOption = _scale;
					Page.selectScale();
//					$rootScope.$apply();
					PDFViewer.safeApply();
					return true;
				}
			}
			return false;
		};

		this.setOrientation = function setOrientation( orientation ) {
			orientation = parseInt( orientation );
			if( orientation >= 0 && orientation <= 3 ) {
				PDFViewer.setOrientation( orientation );
				$rootScope.$apply();
			}
		};

		this.hasAnnotationChanges = function hasAnnotationChanges() {
			return PDFViewer.annotations.hasChanges();
		};

		this.hasIntroduceChanges = function hasIntroduceChanges() {
			return PDFViewer.annotations.hasChanges(PDF_STATE_INTRODUCE);
		};

		this.getAnnotationChanges = function getAnnotationChanges() {
			return _getAnnotationsGeneral(PDF_STATE_ANNOTATE);
		};

		this.getPresentationAnnotations = function getPresentationAnnotations() {
			return _getAnnotationsGeneral(PDF_STATE_PRESENTATION);
		};

		this.hasCallouts = function hasCallouts() {
			return PDFViewer.annotations.hasCallouts();
		};

		function _getAnnotationsGeneral(requestState) {
			var state = PDFViewer.annotations.getChangeState(requestState);
			return {
				annotations: state.payload,
				callouts: state.callouts,
				then: function() {
					PDFViewer.annotations.catchUp(state);
				}
			};
		};

		this.discardAnnotationChanges = function discardAnnotationChanges() {
			var state = self.getAnnotationChanges();
			state.then();
		};

		this.stopAnnotationTools = function stopAnnotationTools() {
			console.log( 'stopAnnotationTools' );
			if( PDFViewer.Toolbar.draw.arrow.mode ) {
				PDFViewer.Toolbar.draw.arrow.toggleMode();
			}
			if( PDFViewer.Toolbar.draw.highlight.mode ) {
				PDFViewer.Toolbar.draw.highlight.toggleMode();
			}
			if( PDFViewer.Toolbar.draw.pencil.mode ) {
				PDFViewer.Toolbar.draw.pencil.toggleMode();
			}
			if( PDFViewer.Toolbar.draw.note.mode ) {
				PDFViewer.Toolbar.draw.note.toggleMode();
			}
			if( PDFViewer.Toolbar.draw.callout.mode ) {
				PDFViewer.Toolbar.draw.callout.toggleMode();
			}
		};

		this.activateDrawMode = function activateDrawMode() {
			PDFViewer.annotations.setMode('draw');
		};

		this.activateViewMode = function activateViewMode() {
			PDFViewer.annotations.setMode('view');
		};

		this.activatePresenterMode = function activatePresenterMode() {
			PDFViewer.annotations.setMode('presenter');
			PDFViewer.ignoreArrowKeys = false;
		};

		this.activatePresentationMode = function activatePresentationMode() {
			PDFViewer.annotations.setMode('presentation');
			PDFViewer.ignoreArrowKeys = true;
		};

		this.activateAnnotatorMode = function activateAnnotatorMode() {
			PDFViewer.annotations.setMode('annotator');
			PDFViewer.ignoreArrowKeys = false;
		};

		this.introduceStart = function introduceStart(title, subtitle, date, started, first) {
			PDFViewer.Stamp.setDateMinimum(started);
			PDFViewer.introduceStart(title, subtitle, date, first);
		};

		this.introduceStop = function introduceStop() {
			// stop the introduce
			PDFViewer.introduceStop();
		};

		this.introduceSave = function introduceSave(callback) {
			// save the introduce
			return PDFViewer.introduceSave(callback);
		};

		this.introduceClear = function introduceClear() {
			return PDFViewer.introduceClear();
		};

		this.showCallouts = function showCallouts() {
			PDFViewer.annotations.callouts.showAll();
			PDFViewer.safeApply();
		};

		this.hideCallouts = function hideCallouts() {
			PDFViewer.annotations.callouts.hideAll();
			PDFViewer.safeApply();
		};

		this.searchEvents = function searchEvents() {
			this.next = function(search) {
				var dfd = jQuery.Deferred();
				PDFViewer.setSearchPromise(dfd);
				if( PDFViewer.isSearchSame(search) ) {
					PDFViewer.nextSearch();
				} else {
					PDFViewer.search( search, true );
				}
				//setTimeout(function() {dfd.resolve( "next of "+search );}, 1000 );
				return dfd;
			};
			this.prev = function(search) {
				var dfd = jQuery.Deferred();
				PDFViewer.setSearchPromise(dfd);
				if( PDFViewer.isSearchSame(search) ) {
					PDFViewer.prevSearch();
				} else {
					PDFViewer.search( search, true );
				}
				//setTimeout(function() {dfd.resolve( "prev of "+search );}, 1000 );
				return dfd;
			};
			this.clear = function() {
				PDFViewer.clearSearch();
			};
		};

		this.arrowButton = function arrowButton( callback, keepOn ) {
			PDFViewer.Toolbar.draw.arrow.addExitCallback(callback);
			if(keepOn && keepOn === true) {
				PDFViewer.Toolbar.draw.arrow.setMode(true);
			}
			else {
				PDFViewer.Toolbar.draw.arrow.toggleMode();
			}
		};
		this.getArrowDrawColor = function getArrowDrawColor() {
			return PDFViewer.Toolbar.draw.arrow.color;
		};
		this.setArrowDrawColor = function setArrowDrawColor(color) {
			if(typeof color === "string" && color.substring(0,3).toLowerCase() === "rgb") {
				color = DrawColor.cssRgbToHex(color);
			}
			PDFViewer.Toolbar.draw.arrow.setColor(color);
		};
		this.setArrowColorCallback = function setArrowColorCallback(callback) {
			return PDFViewer.setArrowColorCallback(callback);
		};

		this.highlightButton = function highlightButton( callback, keepOn ) {
			PDFViewer.Toolbar.draw.highlight.addExitCallback(callback);
			if(keepOn && keepOn === true) {
				PDFViewer.Toolbar.draw.highlight.setMode(true);
			}
			else {
				PDFViewer.Toolbar.draw.highlight.toggleMode();
			}
		};
		this.getHighlightDrawColor = function getHighlightDrawColor() {
			return PDFViewer.Toolbar.draw.highlight.color;
		};
		this.setHighlightDrawColor = function setHighlightDrawColor(color) {
			if(typeof color === "string" && color.substring(0,3).toLowerCase() === "rgb") {
				color = DrawColor.cssRgbToHex(color);
			}
			PDFViewer.Toolbar.draw.highlight.setColor(color);
		};
		this.setHighlightColorCallback = function setHighlightColorCallback(callback) {
			return PDFViewer.setHighlightColorCallback(callback);
		};

		this.pencilButton = function pencilButton( callback, keepOn ) {
			PDFViewer.Toolbar.draw.pencil.addExitCallback(callback);
			if(keepOn && keepOn === true) {
				PDFViewer.Toolbar.draw.pencil.setMode(keepOn);
			}
			else {
				PDFViewer.Toolbar.draw.pencil.toggleMode();
			}
		};
		this.getPencilDrawColor = function getPencilDrawColor() {
			return PDFViewer.Toolbar.draw.pencil.color;
		};
		this.setPencilDrawColor = function setPencilDrawColor(color) {
			if(typeof color === "string" && color.substring(0,3).toLowerCase() === "rgb") {
				color = DrawColor.cssRgbToHex(color);
			}
			PDFViewer.Toolbar.draw.pencil.setColor(color);
		};
		this.setPencilColorCallback = function setPencilColorCallback(callback) {
			return PDFViewer.setPencilColorCallback(callback);
		};

		this.noteButton = function noteButton( callback, keepOn ) {
			PDFViewer.Toolbar.draw.note.addExitCallback(callback);
			if(keepOn && keepOn === true) {
				PDFViewer.Toolbar.draw.note.setMode(keepOn);
			}
			else {
				PDFViewer.Toolbar.draw.note.toggleMode();
			}
		};
		this.getNoteDrawColor = function getNoteDrawColor() {
			return PDFViewer.Toolbar.draw.note.color;
		};
		this.setNoteDrawColor = function setNoteDrawColor(color) {
			if(typeof color === "string" && color.substring(0,3).toLowerCase() === "rgb") {
				color = DrawColor.cssRgbToHex(color);
			}
			PDFViewer.Toolbar.draw.note.setColor(color);
		};
		this.setNoteColorCallback = function setNoteColorCallback(callback) {
			return PDFViewer.setNoteColorCallback(callback);
		};

		this.calloutButton = function calloutButton( callback, keepOn ) {
			PDFViewer.Toolbar.draw.callout.addExitCallback(callback);
			if(keepOn && keepOn === true) {
				PDFViewer.Toolbar.draw.callout.setMode(true);
			}
			else {
				PDFViewer.Toolbar.draw.callout.toggleMode();
			}
		};
		this.setCalloutCallback = function setCalloutCallback(callback) {
			return PDFViewer.setCalloutCallback(callback);
		};

		this.eraseButton = function eraseButton( callback ) {
			PDFViewer.Toolbar.draw.erase.toggleMode();
			PDFViewer.Toolbar.draw.erase.addExitCallback(callback);
		};

		this.undoButton = function undoButton() {
			PDFViewer.Toolbar.draw.undo.click();
		};

		this.clearAll = function clearAll() {
			PDFViewer.Toolbar.draw.clear(); // just clear
			PDFViewer.safeApply();
		};

		this.clearAllPrompt = function clearAllPrompt() {
			PDFViewer.Toolbar.draw.clearPrompt();
			PDFViewer.safeApply();
		};

		this.setThumbnailEventsHandler = function setThumbnailEventsHandler(thumbnailEvent, pageEvent) {
			//console.log("PDFViewerAPI::setThumbnailEventsHandler()", "thumbnailEvent:", typeof thumbnailEvent, "pageEvent:", typeof pageEvent);
			PDFViewer.thumbnailModeEventHandler = thumbnailEvent;
			PDFViewer.pageModeEventHandler = pageEvent;
		};

		this.viewThumbnails = function viewThumbnails() {
			console.log("PDFViewerAPI::viewThumbnails()");
			PDFViewer.Toolbar.showThumbnails();
		};

		this.viewPage = function viewPage() {
			console.log("PDFViewerAPI::viewPage()");
			PDFViewer.Toolbar.showPage();
		};

		this.hideDuplicateWebAppBtns = function hideDuplicateWebAppBtns() {
			PDFViewer.Toolbar.webAppHide = true;
		};

		this.setActivityCallback = function setActivityCallback(callback) {
			function MyCallback(fn) {
				this.lastTrue = 0;
				this.cbFn = fn;
				this.runit = function runit(activity) {
					if(!!activity) {
						this.lastTrue = Date.now() + 700; // good for 7/10 of a second
						return this.cbFn(activity);
					} else if(this.lastTrue === 0 || this.lastTrue < Date.now()) {
						return this.cbFn(activity);
					}
				};
			}
			var cb = new MyCallback(callback);
			PDFViewer.Toolbar.activityCallback = function(activity){return cb.runit(activity)};
		};

		this.scrollTo = function scrollTo(left, top) {
			PDFViewer.scrollTo(left, top);
		};

		this.onReady = function onReady(callback) {
			PDFViewer.addOnFirstLoadCallback(callback);
		};

		this.onSavable = function onSavable(callback) {
			PDFViewer.addOnSavableCallback(callback);
		};

		this.onPresenterMessage = function onPresenterMessage(callback) {
			PDFViewer.addOnPresenterCallback(callback);
		};

		this.presentationPageAdd = function presentationPageAdd( page ) {
			PDFViewer.presentationPageAdd(page);
		};

		this.presentationAdd = function presentationAdd( addData ) {
			PDFViewer.presentationAdd(addData);
		};

		this.presentationModify = function presentationModify( modifyData ) {
			PDFViewer.presentationModify(modifyData);
		};

		this.presentationDelete = function presentationDelete( deleteData ) {
			PDFViewer.presentationDelete(deleteData);
		};

		this.presentationClearAll = function presentationClearAll( clearData ) {
			PDFViewer.presentationClearAll(clearData);
		};

		this.getPresentationPageState = function getPresentationPageState() {
			var result = {};
			$.each(PDFViewer.presentation.presenterState, function(index, value) {
				if(index === 'firstLoad' || index === 'orientation') {
					return;
				}
				result[index.toString()] = value.toString();
			});
			return result;
		};

		this.getScaleMode = function getScaleMode() {
			PDFViewer.Page.scaleMode;
		};

		this.setScaleMode = function setScaleMode( mode ) {
			console.log( "PDFViewerAPI::setScaleMode", mode );
			// accepts 0 or 1
			mode = Math.max(Math.min(Math.abs(Math.floor(mode)),1),0);
			PDFViewer.Page.setScaleMode( mode );
		};

		this.getPresentationOrientation = function getPresentationOrientation() {
			return appOrientation;
		};

		this.setPresentationOrientation = function setPresentationOrientation(orientation) {
			PDFViewer.Page.setPresentationOrientation(orientation);
		};

		this.getPageIndex = function getPageIndex() {
			return Page.index;
		};

		this.showPauseIndicator = function showPauseIndicator(unpauseFn) {
			PDFViewer.unpauseClick = unpauseFn;
			PDFViewer.pauseIndicator = true;
			PDFViewer.Toolbar.setTool(PDFViewer.Toolbar.noneTool, true);
			PDFViewer.safeApply();
		};

		this.hidePauseIndicator = function hidePauseIndicator() {
			PDFViewer.pauseIndicator = false;
			PDFViewer.Toolbar.setTool(PDFViewer.Toolbar.noneTool, true);
			PDFViewer.safeApply();
		};

		this.setAppOrientationDelegate = function setAppOrientationDelegate( delegate ) {
			if( typeof delegate === "function" || delegate === null ) {
				appOrientationDelegate = delegate;
			}
		};

		this.appOrientationDidChange = function appOrientationDidChange( orientation ) {
			//console.log( "PDFViewerAPI.appOrientationDidChange", appOrientation, orientation );
			if( orientation !== appOrientation && [PORTRAIT, LANDSCAPE].indexOf( orientation ) >= 0 ) {
				appOrientation = orientation;
			}
			PDFViewer.Page.appOrientationClasses();
			PDFViewer.Page.updateDimensions();
		};

		this.getPresentationPageScale = function getPresentationPageScale() {
			//console.log( "getPresentationPageScale;", PDFViewer.presentation.presenterState.pageScale );
			return PDFViewer.presentation.presenterState.pageScale;
		};

		this.getPresentationPageOrientation = function getPresentationPageOrientation() {
			//console.log( "getPresentationPageOrientation;", PDFViewer.presentation.presenterState.pageOrientation );
			return PDFViewer.presentation.presenterState.pageOrientation;
		};

		this.getPresentationPageOffset = function getPresentationPageOffset() {
			//console.log( "getPresentationPageOffset:", PDFViewer.presentation.presenterState.pageOffsetX, PDFViewer.presentation.presenterState.pageOffsetY );
			return {
				"X": PDFViewer.presentation.presenterState.pageOffsetX,
				"Y": PDFViewer.presentation.presenterState.pageOffsetY
			};
		};

		this.setPresenterFirstLoad = function setPresenterFirstLoad( pData ) {
			//console.log( "PDFViewerAPI.setPresentationFirstLoad;", pData );
			PDFViewer.presentation.firstLoad = pData;
			PDFViewer.presentation.presenterState.pageScale = pData.pageScale;
		};

		this.presentationAddCallout = function presentationAddCallout(args) {
			PDFViewer.presentationAddCallout(args);
		};

		this.presentationModifyCallout = function presentationModifyCallout(args) {
			PDFViewer.presentationModifyCallout(args);
		};

		this.presentationDeleteCallout = function presentationDeleteCallout(args) {
			PDFViewer.presentationDeleteCallout(args);
		};

		this.presentationSetCalloutZIndex = function presentationSetCalloutZIndex(args) {
			PDFViewer.presentationSetCalloutZIndex(args);
		};

		this.searchMultiTerms = function searchMultiTerms(termsArray) {
			PDFViewer.search(termsArray, true);
		};
	};



	return PDFViewer;
};

window.onbeforeunload = function onbeforeunload() {
	if( typeof window.top.webApp === "object" && window.top.webApp !== null ) {
		window.top.webApp.bindViewer( null );
	}
};


