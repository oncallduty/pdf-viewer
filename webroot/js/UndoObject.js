/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**********************************************
 * Copyright by eDepoze 2015
 * All rights reserved.
 */


/**
 *
 * @param {type} params | an object of parameters
 * @returns {UndoObjectFactory.UndoObjectClass|UndoObjectFactory.UndoObject}
 */
var UndoObjectFactory = {

	draw: function(DrawObject) {
		var Obj = new UndoObjectFactory.UndoBase();
		Obj.undo = function() {
		};
		return Obj;
	},
	modify: function(DrawObject) {
	},
	erase: function(DrawObject) {
	},
	UndoBase: function() {
	}

};

var UndoObjectClass = function() {
	this.undoType = null;	// "erase", "note", "modify", "new"
	this.page = null;		// Which canvas this object should be drawn on.  This will be the html ID of the canvas.
};
UndoObjectClass.prototype.clone = function() {
	var newObject = new DrawObjectClass();
	newObject.undoType = this.undoType;
	return newObject;
};
UndoObjectClass.prototype.inject = function(injectorObject) {
	if(injectorObject && injectorObject.toString() === 'UndoObject') {
		this.undoType = injectorObject.undoType;
	}
	else {
		throw 'Attempt to inject a non UndoObject into a UndoObject.';
	}
};
UndoObjectClass.prototype.toString = function() {
	return 'UndoObject';
};
UndoObjectClass.prototype.debugShape = function() {
};

// Create a new instance of the DrawClass
var UndoObject = new UndoObjectClass();

// Drawing Parameters
UndoObject.setUndoType(undoType);


//return UndoObject;







