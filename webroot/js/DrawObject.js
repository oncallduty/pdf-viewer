/**********************************************
 * Copyright by eDepoze 2015
 * All rights reserved.
 */

var DEFAULT_LINE_WIDTH = 4;
var DEFAULT_MARKER_WIDTH = 12;
var DEFAULT_ARROWHEAD_LENGTH = 11;
var DEFAULT_ARROWHEAD_ANGLE = Math.PI/6;
var DEFAULT_DRAG_ICON_OFFSET = 15;
var DEFAULT_DRAG_ICON_RADIUS = 4;
var DEFAULT_DEVICE_RATIO = 0.66;
var DEFAULT_BOUNDINGBOX_THICKNESS = 4;
var DEFAULT_BOUNDINGBOX_COLOR = '#000000';
var DEFAULT_BOUNDINGBOX_OPACITY = 0.3;
var DEFAULT_NOTE_ICON_WIDTH = 22;
var DEFAULT_NOTE_ICON_HEIGHT = 22;
var DEFAULT_NOTE_ICON_MAX_WIDTH = 106;
var DEFAULT_NOTE_ICON_MIN_WIDTH = 16;

var DEFAULT_NOTE_ICON_RADIUS = (DEFAULT_NOTE_ICON_HEIGHT >> 2);
var DEFAULT_MOVE_ICON_SIZE = 32;
var DEFAULT_MOVE_ICON_OFFSET = DEFAULT_DRAG_ICON_OFFSET>>1;

var DEFAULT_STAMP_MAX_WIDTH = 640;
var DEFAULT_STAMP_MIN_WIDTH = 160;
var DEFAULT_STAMP_MAX_HEIGHT = 104;
var DEFAULT_STAMP_MIN_HEIGHT = 40;
var DEFAULT_STAMP_MAX_RADIUS = 14;
var DEFAULT_STAMP_MAX_LINEWIDTH = 3;

var DEFAULT_SCALE_FACTOR = 1.5;

var DEFAULT_STAMP_SCALE_ICON_READY = false;
var DEFAULT_STAMP_SCALE_ICON = new Image();
DEFAULT_STAMP_SCALE_ICON.src = '/img/stampScaleLeft.png';
DEFAULT_STAMP_SCALE_ICON.onload = function() {
	DEFAULT_STAMP_SCALE_ICON_READY = true;
};
var M_PI_4 = Math.PI/4;
var M_PI_2 = Math.PI/2;
var M_PI = Math.PI;

var DRAWOBJECT_DRAW_ALWAYS = "all";
var DRAWOBJECT_DRAW_IFSELECTED = "selected";
var DRAWOBJECT_DRAW_IFUNSELECTED = "unselected";

DrawObjectEnv = {
	width: 0,
	height: 0,
	rotation: 0
};

var DrawObjectDefaultRectParams = function DrawObjectDefaultRectParams() {
	return {
		type: 'rect',
		pointLists: [],
		lineWidth: 1,
		isSelected: false,
		canvasWidth: 0,
		canvasHeight: 0,
		scale: 1.0,
		color: '#ffff00',
		opacity: 100,
		borderColor: '#ffff00',
		borderOpacity: 100,
		border: 1
	};
};

var DrawObjectDefaultCalloutParams = function DrawObjectDefaultCalloutParams() {
	return {
		type: 'callout',
		pointList: [],
		lineWidth: 1,
		isSelected: false,
		canvasWidth: 0,
		canvasHeight: 0,
		scale: 1.0,
		color: '#ff0000',
		opacity: 100,
		borderColor: '#ff0000',
		borderOpacity: 100,
		border: 1
	};
};

var DrawObjectDefaultArrowParams = function DrawObjectDefaultArrowParams() {
	return {
		type: 'arrow',
		pointList: [],
		lineWidth: 3,
		isSelected: false,
		canvasWidth: 0,
		canvasHeight: 0,
		scale: 1.0,
		color: '#ff0000',
		opacity: 100
	};
};

var DrawObjectDefaultMarkerParams = function DrawObjectDefaultMarkerParams() {
	return {
		type: 'marker',
		pointList: [],
		lineWidth: 12,
		isSelected: false,
		canvasWidth: 0,
		canvasHeight: 0,
		scale: 1.0,
		color: '#ffff00',
		opacity: 35
	};
};

var DrawObjectDefaultHighlightParams = function DrawObjectDefaultHighlightParams() {
	return {
		type: 'highlight',
		pointList: [],
		lineWidth: 12,
		isSelected: false,
		canvasWidth: 0,
		canvasHeight: 0,
		scale: 1.0,
		color: '#ff0000',
		opacity: 35,
		borderColor: '#ff0000',
		borderOpacity: 100,
		border: 0
	};
};

var DrawObjectDefaultPencilParams = function DrawObjectDefaultPencilParams() {
	return {
		type: 'pencil',
		pointList: [],
		lineWidth: 3,
		isSelected: false,
		canvasWidth: 0,
		canvasHeight: 0,
		scale: 1.0,
		color: '#0000ff',
		opacity: 100
	};
};

var DrawObjectDefaultNoteParams = function DrawObjectDefaultNoteParams() {
	return {
		type: 'note',
		pointList: [],
		lineWidth: 1,
		isSelected: false,
		canvasWidth: 0,
		canvasHeight: 0,
		width: DEFAULT_NOTE_ICON_WIDTH,
		height: DEFAULT_NOTE_ICON_HEIGHT,
		scale: 1.0,
		color: '#ffff00',
		opacity: 100,
		note: '',
		date: new Date(),
		author: ''
	};
};

var DrawObjectDefaultStampParams = function DrawObjectDefaultStampParams() {
	return {
		type: 'stamp',
		pointList: [],
		lineWidth: 3,
		isSelected: false,
		canvasWidth: 0,
		canvasHeight: 0,
		height: DEFAULT_STAMP_MAX_HEIGHT,
		scale: 1.0,
		stampScale: 1.0,
		color: '#000000',
		opacity: 100,
		line1: 'Test Depo',
		line2: 'Exhibit_1',
		line3: new Date().toLocaleString('en-US'),
		date: new Date()
	};
};

CanvasRenderingContext2D.prototype.dashedLine = function dashedLine(x1, y1, x2, y2, tick) {
	//From stack overflow.  Remove when all browsers support dashed lines.
	//dashLen = 3; // 8 pixels, bit 3 for bit shifting
	//dashPixLen = Math.pow(2,dashLen);
	dashPixLen = 6;
	if (typeof tick === 'undefined') tick = 0;
	this.moveTo(x1, y1);

	var dX = x2 - x1;
	var dY = y2 - y1;
	var dashes = Math.floor(Math.sqrt((dX * dX) + (dY * dY)) / dashPixLen);
	var dashX = dX / dashes;
	var dashY = dY / dashes;

	var q = 0;
	//this[q++ % 2 == 0 ? 'moveTo' : 'lineTo'](tick % dashLen, tick % dashLen);
	while (q++ < dashes) {
//		if(q==1) {
//			x1 += dashX * ((tick % dashPixLen));
//			y1 += dashY * ((tick % dashPixLen));
//		}
//		else {
			x1 += dashX;
			y1 += dashY;
//		}
		this[q % 2 === 0 ? 'moveTo' : 'lineTo'](x1, y1);
	}
	this[q % 2 === 0 ? 'moveTo' : 'lineTo'](x2, y2);
};

var DrawObjectFactory = function DrawObjectFactory(params) {

	// Create a new instance of the DrawClass
	var DrawObject = new DrawObjectClass(8);

	// Drawing Parameters
	DrawObject.setDrawType(params.type, params.lineWidth);
	DrawObject.sig = params.sig ? params.sig : "";
	DrawObject.color = params.color ? params.color : 'black';
	DrawObject.opacity = params.opacity ? (params.opacity / 100.0) : 1;
	DrawObject.borderColor = params.borderColor ? params.borderColor : DrawObject.color;
	DrawObject.borderOpacity = params.borderOpacity ? (params.borderOpacity / 100.0) : DrawObject.opacity;
	DrawObject.border = params.border ? params.border : 0;
	DrawObject.op = params.op;
	DrawObject.canvasWidth = params.canvasWidth;
	DrawObject.canvasHeight = params.canvasHeight;
	DrawObject.hostPageRotation = (typeof params.hostPageRotation === "number") ? params.hostPageRotation : 0; // This is the rotation of the page given by pdfdaemon
	DrawObject.width = params.width;
	DrawObject.height = params.height;
	DrawObject.scale = params.scale;
	DrawObject.margin = angular.isArray(params.margin) ? params.margin : [0,0,0,0];
	DrawObject.pointLists = params.pointList ? DrawObject.translate(params.pointList, params.canvasHeight, params.scale) : [];
	DrawObject.lineCount = DrawObject.pointLists.length;		// Current array count in the pointList.
	DrawObject.createBoundingBox();	// Upper left and lower right corners for the bounding box.

	// Other parameters
	DrawObject.page = params.page;		// Which canvas this object should be drawn on.  This will be the html ID of the canvas.
	DrawObject.pageIndex = params.pageIndex ? params.pageIndex : 0;
	DrawObject.setAuthor(params.author);
	DrawObject.setDate(params.date);
	DrawObject.setNote(params.note);
	DrawObject.index = params.index ? params.index : 0;		// Index for sharing in presentations.
	DrawObject.isSelected = !!params.isSelected;	// Is the object currently selected.
	DrawObject.dragPoints = isset(params.dragPoints) ? params.dragPoints : [];	// Drag points for some objects.
	DrawObject.isSegmented = params.isSegmented ? params.isSegmented : 0;	// 0:non-segmented,1:first segment,2:additional segment,3:end of segments

	DrawObject.line1 = params.line1 ? params.line1 : '';
	DrawObject.line2 = params.line2 ? params.line2 : '';
	DrawObject.line3 = params.line3 ? params.line3 : '';
	//DrawObject.setOrientation(0);
	DrawObject.zIndex = -1;

	return DrawObject;
};

var DrawObjectClass = function DrawObjectClass(hitThreshold) {
	var DrawObject = this;
	this.drawType = null;	// "pencil", "highlight", "marker", "arrow", "note", "rect", "callout", or "stamp";
	this.category = 'scribble'; // scribble, note, stamp
	this.sig = "";
	this.visible = true;
	this.origin = DrawObject;
	this.parent = false; // this is to help flatten out the changes into one message
	this.page = null;		// Which canvas this object should be drawn on.  This will be the html ID of the canvas.
	this.pageIndex = 0;
	this.color = "black";	// The color this object should be drawn with.
	this.opacity = 1;		// From 0 to 1.
	this.borderColor = this.color;
	this.borderOpacity = this.opacity;
	this.border = 0;
	this.lineCount = 0;		// Current array count in the pointList.
	this.pointLists = [];	// For lines, this is an array of lines that have an array of points.  For rects, this is an array of rects stored as upper left hand corner with with and height.
	this.boundingBox = [];	// Upper left and lower right corners for the bounding box.
	this.dragBox = [];
	this.margin = [0,0,0,0];
	this.lineWidth = 3;		// How wide is the pencil.
	this.date = null;		// The date of the annotation.
	this.note = null;		// The note text.
	this.author = null;		// Shown on notes.
	this.line1 = null;		// Shown on stamp
	this.line2 = null;		// Shown on stamp
	this.line3 = null;		// Shown on stamp
	this.index = 0;			// Index for sharing in presentations.
	this.isSelected = false;// Is the object currently selected.
	this.dragPoints = [];	// Drag points for some objects.
	this.isSegmented = 0;	// 0:non-segmented,1:first segment,2:additional segment,3:end of segments
	this.canvasWidth = 0;
	this.canvasHeight = 0;
	this.scale = 1.0;
	this.currentTick = 0;
	this.selectedDragPoint = false;
	this.colorArray = [ "#FF0000", "#00FF00", "#0080FF", "#FF00FF", "#FCB643", "#FFFF00", "#000000", "#8000FF" ];
	this.thicknessArray = [1,2,3,4];
	this.opacityArray = [0.25, 0.50, 0.75, 1.00];
	this.unselectable = false;
	this.unhittable = false;
	this.existing = true;
	this.hostPageRotation = 0; // 0-up, 1-right, 2-down, 3-left

	this.stampHeight = 116;
	this.stampScale = 1.0;
	this.orientation = 0;
	this.rotate = 0;

	this.stampSize = [180,116];
	this.stampRotation = 0;
	this.stampIcon = [0,0,44,44];
	this.centerX = 0;
	this.centerY = 0;

	this.initial = false;
	this.zIndex = -1;

	//this.stampScale = 1.0;

	if (typeof hitThreshold !== "number" || isNaN(hitThreshold)) {
		this.hitThreshold = 5;
	} else {
		this.hitThreshold = hitThreshold;
	}
};

DrawObjectClass.prototype.getJsonData = function getJsonData(scale, hostPageRotation) {
	if(this.drawType === "callout")
		return false;

	var jsonStructure = {};
	if(this.parent && this.parent.toString() === this.toString()) {
		jsonStructure = this.parent.getJsonData();
	}

	if(typeof scale === 'undefined') {
		scale = DEFAULT_SCALE_FACTOR;
	}

	var hostPoints = this.outputPointsForHost(hostPageRotation, this.canvasWidth, this.canvasHeight);

	var jsonType = this.drawType;
	if(jsonType === 'highlight') {
		jsonType = 'marker';
	}

	jsonStructure.op = 'non';
	jsonStructure.sig = this.getSig();
	jsonStructure.type = jsonType;
	jsonStructure.color = this.color;
	jsonStructure.lineWidth = this.lineWidth;
	jsonStructure.opacity = Math.ceil(this.opacity*100);
	jsonStructure.pointList = [];
	var value = 0;
	for(var i in hostPoints) {
		jsonStructure.pointList.push([]);
		for(var j in hostPoints[i]) {
			if(parseInt(j.toString())===1 && this.canvasHeight > 0) {
				value = Math.round((this.canvasHeight - hostPoints[i][j])/scale);
			}
			else {
				value = Math.round(hostPoints[i][j]/scale);
			}
			jsonStructure.pointList[i].push(value);
		}
	}
	if(this.drawType === "note") {
		jsonStructure.pointList.push([jsonStructure.pointList[0][0]+(this.width),jsonStructure.pointList[0][1]-(this.height)]);
		jsonStructure.note = this.note;
		jsonStructure.date = this.date.getFullYear()+'-'
							+this.date.getMonth()+'-'
							+this.date.getDate()+' '
							+this.date.getHours()+':'
							+this.date.getMinutes()+':'
							+this.date.getSeconds();
	}
	return jsonStructure;
};

DrawObjectClass.prototype.getOrigin = function getOrigin() {
	return this.origin;
};

DrawObjectClass.prototype.getInitial = function getInitial() {
	return this.initial;
};

DrawObjectClass.prototype.setInitial = function setInitial(value) {
	this.initial = !!value;
};

DrawObjectClass.prototype.getJsonComposition = function getJsonComposition() {
	var result = [];
	if(this.parent && this.parent.toString() === this.toString()) {
		result = this.parent.getJsonComposition();
	}
	result.push(this);
	return result;
};

DrawObjectClass.prototype.setDrawType = function setDrawType( drawType, lineWidth ) {
	if (drawType === "pencil" || drawType === "highlight" || drawType === "marker"
		|| drawType === "arrow" || drawType === 'note' || drawType === 'rect' || drawType === 'callout'
		|| drawType === "stamp") {
		this.drawType = drawType;
		switch(this.drawType) {
			case 'note':
				this.colorArray = [ "#FF0000", "#00FF00", "#0080FF", "#FF00FF", "#FCB643", "#FFFF00", "#000000", "#8000FF" ];
				this.width = DEFAULT_NOTE_ICON_WIDTH;
				this.height = DEFAULT_NOTE_ICON_HEIGHT;
				this.category = 'note';
				break;
			case 'pencil':
				this.colorArray = [ "#FF0000", "#00FF00", "#0080FF", "#FF00FF", "#FCB643", "#FFFF00", "#000000", "#8000FF" ];
				this.thicknessArray = [3,5,8,12];
				this.opacityArray = [0.25, 0.50, 0.75, 1.00];
				this.category = 'scribble';
				break;
			case 'marker':
				this.colorArray = [ "#FF0000", "#00FF00", "#0080FF", "#FF00FF", "#FCB643", "#FFFF00", "#00FFFF", "#8000FF" ];
				this.thicknessArray = [6,8,12,16];
				this.opacityArray = [0.20, 0.35, 0.50, 0.65];
				this.category = 'scribble';
				break;
			case 'arrow':
				this.colorArray = [ "#FF0000", "#00FF00", "#0080FF", "#FF00FF", "#FCB643", "#FFFF00", "#000000", "#8000FF" ];
				this.thicknessArray = [2,3,6,9];
				this.opacityArray = [0.25, 0.50, 0.75, 1.00];
				this.category = 'scribble';
				break;
			case 'rect':
			case 'highlight':
				this.colorArray = [ "#FF0000", "#00FF00", "#0080FF", "#FF00FF", "#FCB643", "#FFFF00", "#00FFFF", "#8000FF" ];
				this.thicknessArray = [4,8,12,16];
				this.opacityArray = [0.20, 0.35, 0.50, 0.65];
				this.category = 'rect';
				break;
			case 'callout':
				this.colorArray = [ "#FF0000", "#00FF00", "#0080FF", "#FF00FF", "#FCB643", "#FFFF00", "#000000", "#8000FF" ];
				this.thicknessArray = [1,1,1,1];
				this.opacityArray = [0.7, 0.8, 0.9, 1.0];
				this.category = 'callout';
				break;
			case 'stamp':
				this.category = 'stamp';
				break;
			default:
				this.category = 'none';
				break;
		}
	} else {
		console.log("Bad drawType: "+drawType);
	}
	if (isset( lineWidth )) {
		this.lineWidth = lineWidth;
	}
};

DrawObjectClass.prototype.addObject = function addObject() {
	this.pointLists[this.lineCount] = [];
	this.lineCount++;
};

DrawObjectClass.prototype.addPoints = function addPoints( x, y, w, h ) {
	if (!isset( w ) && !isset( h ))  {
		this.pointLists.push([x, y]);
	} else {
		this.pointLists.push([x, y, w, h]);
	}
};

DrawObjectClass.prototype.draw = function draw( canvas, tick, drawType ) {
	// console.log( "DrawObject.draw; canvas:", canvas, "tick:", tick, "drawType:", drawType );
	if( !isset( canvas ) || !isset( canvas.context ) ) {
		return true;
	}
	if(tick) {
		this.currentTick = tick;
	}
	if(typeof drawType === "undefined") {
		drawType = DRAWOBJECT_DRAW_ALWAYS;
	}
	if((drawType === DRAWOBJECT_DRAW_IFSELECTED && !this.isSelected)
			|| (drawType === DRAWOBJECT_DRAW_IFUNSELECTED && this.isSelected)) {
		return true;
	}
	if((!this.canvasWidth || !this.canvasHeight) && canvas.isPrimary) {
		this.canvasWidth = canvas.htmlObj.width;
		this.canvasHeight = canvas.htmlObj.height;
	}
	if(this.canvasWidth != DrawObjectEnv.width && canvas.isPrimary) {
		DrawObjectEnv.width = this.canvasWidth;
	}
	if(this.canvasHeight != DrawObjectEnv.height && canvas.isPrimary) {
		DrawObjectEnv.height = this.canvasHeight;
	}
	if(!this.visible) {
		return true;
	}
	switch(this.drawType) {
		case 'rect':
			return this.drawRect(canvas);
			break;
		case 'highlight':
			return this.drawHighlight(canvas);
			break;
		case 'pencil':
			return this.drawPencil(canvas);
			break;
		case 'arrow':
			return this.drawArrow(canvas);
			break;
		case 'marker':
			return this.drawMarker(canvas);
			break;
		case 'note':
			return this.drawNote(canvas);
			break;
		case 'stamp':
			return this.drawStamp(canvas);
			break;
		case 'callout':
			return this.drawCallout(canvas);
			break;
	}
};

DrawObjectClass.prototype.drawRect = function drawRect(canvas) {
	//return this.drawHighlight(canvas);
	var cnvOffsetX = canvas.offsetX;
	var cnvOffsetY = canvas.offsetY;
	var cnvScale = canvas.scale;
	var rgb_value = DrawColor.splitColor( this.color );
	if (rgb_value !== null) {
		canvas.context.fillStyle = 'rgba(' + rgb_value.r + ',' + rgb_value.g + ',' + rgb_value.b + ',' + ( this.opacity / 2 ) + ')';
	} else {
		canvas.context.fillStyle = 'rgba(255, 255, 0, 0.3)';
	}
	xRect = (this.pointLists[0]+cnvOffsetX) * cnvScale;
	yRect = (this.pointLists[1]+cnvOffsetY) * cnvScale;
	wRect = this.pointLists[2] * cnvScale;
	hRect = this.pointLists[3] * cnvScale;
	canvas.context.fillRect(xRect, yRect, wRect, hRect);
	canvas.context.lineWidth = (this.lineWidth * cnvScale) / DEFAULT_DEVICE_RATIO;
	if (this.border) {
		rgb_value = DrawColor.splitColor( this.borderColor );
		if (rgb_value !== null) {
			canvas.context.strokeStyle = 'rgba(' + rgb_value.r + ',' + rgb_value.g + ',' + rgb_value.b + ',' + this.borderOpacity + ')';
		} else {
			canvas.context.strokeStyle = 'rgba(255, 255, 0, ' + this.opacity + ')';
		}
		canvas.context.strokeRect(xRect, yRect, wRect, hRect);
	}
	if (this.isSelected) {
		canvas.context.strokeStyle = 'rgba(169, 169, 169, 1)';
		canvas.context.strokeRect(xRect, yRect, wRect, hRect);
	}

	//Reset the shadowColor
	canvas.context.shadowColor = null;
	canvas.context.shadowBlur = 0;
	canvas.context.shadowOffsetX = 0;
	canvas.context.shadowOffsetY = 0;

	return true;
};

DrawObjectClass.prototype.midPointBtw = function midPointBtw(p1, p2) {
  return [
    p1[0] + ((p2[0] - p1[0]) >> 1),
    p1[1] + ((p2[1] - p1[1]) >> 1)
  ];
};

DrawObjectClass.prototype.drawPencil = function drawPencil(canvas) {
	var cnvOffsetX = canvas.offsetX;
	var cnvOffsetY = canvas.offsetY;
	var cnvScale = canvas.scale;
	canvas.context.beginPath();
	canvas.context.lineCap = 'round';
	canvas.context.lineJoin = 'round';
	rgb_value = DrawColor.splitColor( this.color );
	if (rgb_value !== null) {
		canvas.context.strokeStyle = 'rgba(' + rgb_value.r + ',' + rgb_value.g + ',' + rgb_value.b + ',' + this.opacity + ')';
	} else {
		canvas.context.strokeStyle = 'rgba(255, 255, 0, ' + this.opacity + ')';
	}
	var currX, currY, dot, bezier;
	if (this.isSelected) {
		canvas.context.shadowColor = '#666';
		canvas.context.shadowBlur = 5;
		canvas.context.shadowOffsetX = 5;
		canvas.context.shadowOffsetY = 5;
	}
	// Dot determination
	bezier = true;
	dot = false;
	if(this.pointLists.length <= 4) {
		currX = this.pointLists[0][0];
		currY = this.pointLists[0][1];
		dot = true;
		for (i = 0; i < this.pointLists.length; i++) {
			if(currX !== this.pointLists[i][0] || currY !== this.pointLists[i][1]) {
				dot = false;
				break;
			}
		}
	}
	if(dot) {
		// Draw Point.
		canvas.context.arc(this.pointLists[0][0], this.pointLists[0][1], (this.lineWidth / 4), 0, Math.PI * 2);
	} else if(false && bezier) {
		// Draw Bezier Lines
		var p1 = this.pointLists[0];
		var p2 = this.pointLists[1];
		canvas.context.moveTo(p1[0], p1[1]);
		for (var i = 1, len = this.pointLists.length; i < len; i++) {
			// we pick the point between pi+1 & pi+2 as the
			// end point and p1 as our control point
			var midPoint = this.midPointBtw(p1, p2);
			canvas.context.quadraticCurveTo(p1[0], p1[1], midPoint[0], midPoint[1]);
			p1 = this.pointLists[i];
			p2 = this.pointLists[i+1];
		}
		canvas.context.lineTo(p1[0], p1[1]);
	} else {
		// Draw Pencil (Classic) Drawing.
		for (i = 0; i < this.pointLists.length; i++) {
			currX = ((this.pointLists[i][0]) + cnvOffsetX) * cnvScale;
			currY = ((this.pointLists[i][1]) + cnvOffsetY) * cnvScale;
			if (i === 0) {
				canvas.context.moveTo(currX, currY);
			} else {
				canvas.context.lineTo(currX, currY);
			}
		}
	}
	canvas.context.lineWidth = (this.lineWidth * cnvScale) / DEFAULT_DEVICE_RATIO;
	canvas.context.stroke();

	//Reset the shadowColor
	canvas.context.shadowColor = null;
	canvas.context.shadowBlur = 0;
	canvas.context.shadowOffsetX = 0;
	canvas.context.shadowOffsetY = 0;

	if (this.isSelected) {
		this.drawPencilBox(canvas);
		//this.drawMoveIcon(canvas);
		this.drawDragPoints(canvas);
	}
	return true;
};

DrawObjectClass.prototype.drawArrowEx = function drawArrowEx(ctx,x1,y1,x2,y2,lineWidth,which,angle,d) {
	'use strict';
	// Ceason pointed to a problem when x1 or y1 were a string, and concatenation
	// would happen instead of addition
	if(typeof(x1)=='string') x1=parseInt(x1);
	if(typeof(y1)=='string') y1=parseInt(y1);
	if(typeof(x2)=='string') x2=parseInt(x2);
	if(typeof(y2)=='string') y2=parseInt(y2);
	var style = 2;
	which=typeof(which)!='undefined'? which:1; // end point gets arrow
	angle=typeof(angle)!='undefined'? angle:Math.PI/8;
	d    =typeof(d)    !='undefined'? d    :10;
	// default to using drawHead to draw the head, but if the style
	// argument is a function, use it instead
	var toDrawHead=this.drawArrowHead;

	// For ends with arrow we actually want to stop before we get to the arrow
	// so that wide lines won't put a flat end on the arrow.
	//
	var dist=Math.sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
	var ratio=(dist-lineWidth)/dist; // modified, originally instead of lineWidth.
	// ratio was using the d/3 instead of lineWidth but distance works better when it correlates with the lineWidth.
	var tox, toy,fromx,fromy;
	if(which&1){
		tox=Math.round(x1+(x2-x1)*ratio);
		toy=Math.round(y1+(y2-y1)*ratio);
	}else{
		tox=x2;
		toy=y2;
	}
	if(which&2){
		fromx=x1+(x2-x1)*(1-ratio);
		fromy=y1+(y2-y1)*(1-ratio);
	}else{
		fromx=x1;
		fromy=y1;
	}

	// Draw the shaft of the arrow
	ctx.beginPath();
	ctx.moveTo(fromx,fromy);
	ctx.lineTo(tox,toy);
	//ctx.stroke();

	// calculate the angle of the line
	var lineangle=Math.atan2(y2-y1,x2-x1);
	// h is the line length of a side of the arrow head
	var h=Math.abs(d/Math.cos(angle));

	if(which&1){	// handle far end arrow head
		var angle1=lineangle+Math.PI+angle;
		var topx=x2+Math.cos(angle1)*h;
		var topy=y2+Math.sin(angle1)*h;
		var angle2=lineangle+Math.PI-angle;
		var botx=x2+Math.cos(angle2)*h;
		var boty=y2+Math.sin(angle2)*h;
		toDrawHead(ctx,topx,topy,x2,y2,botx,boty,style);
	}
	if(which&2){ // handle near end arrow head
		var angle1=lineangle+angle;
		var topx=x1+Math.cos(angle1)*h;
		var topy=y1+Math.sin(angle1)*h;
		var angle2=lineangle-angle;
		var botx=x1+Math.cos(angle2)*h;
		var boty=y1+Math.sin(angle2)*h;
		toDrawHead(ctx,topx,topy,x1,y1,botx,boty,style);
	}

	ctx.stroke();
};

DrawObjectClass.prototype.drawArrowHead = function drawArrowHead(ctx,x0,y0,x1,y1,x2,y2,style) {
	'use strict';
	if(typeof(x0)=='string') x0=parseInt(x0);
	if(typeof(y0)=='string') y0=parseInt(y0);
	if(typeof(x1)=='string') x1=parseInt(x1);
	if(typeof(y1)=='string') y1=parseInt(y1);
	if(typeof(x2)=='string') x2=parseInt(x2);
	if(typeof(y2)=='string') y2=parseInt(y2);
	var radius=3;
	var twoPI=2*Math.PI;

	// all cases do this.
	//ctx.save();
	//ctx.beginPath();
	ctx.moveTo(x0,y0);
	ctx.lineTo(x1,y1);
	ctx.lineTo(x2,y2);
	switch(style){
		case 0:
			// curved filled, add the bottom as an arcTo curve and fill
			var backdist=Math.sqrt(((x2-x0)*(x2-x0))+((y2-y0)*(y2-y0)));
			ctx.arcTo(x1,y1,x0,y0,.55*backdist);
			ctx.fill();
			break;
		case 1:
			// straight filled, add the bottom as a line and fill.
			ctx.beginPath();
			ctx.moveTo(x0,y0);
			ctx.lineTo(x1,y1);
			ctx.lineTo(x2,y2);
			ctx.lineTo(x0,y0);
			ctx.fill();
			break;
		case 2:
			// unfilled head, just stroke.
			break;
		case 3:
			//filled head, add the bottom as a quadraticCurveTo curve and fill
			var cpx=(x0+x1+x2)/3;
			var cpy=(y0+y1+y2)/3;
			ctx.quadraticCurveTo(cpx,cpy,x0,y0);
			ctx.fill();
			break;
		case 4:
			//filled head, add the bottom as a bezierCurveTo curve and fill
			var cp1x, cp1y, cp2x, cp2y,backdist;
			var shiftamt=5;
			if(x2==x0){
				// Avoid a divide by zero if x2==x0
				backdist=y2-y0;
				cp1x=(x1+x0)/2;
				cp2x=(x1+x0)/2;
				cp1y=y1+backdist/shiftamt;
				cp2y=y1-backdist/shiftamt;
			}else{
				backdist=Math.sqrt(((x2-x0)*(x2-x0))+((y2-y0)*(y2-y0)));
				var xback=(x0+x2)/2;
				var yback=(y0+y2)/2;
				var xmid=(xback+x1)/2;
				var ymid=(yback+y1)/2;

				var m=(y2-y0)/(x2-x0);
				var dx=(backdist/(2*Math.sqrt(m*m+1)))/shiftamt;
				var dy=m*dx;
				cp1x=xmid-dx;
				cp1y=ymid-dy;
				cp2x=xmid+dx;
				cp2y=ymid+dy;
			}

			ctx.bezierCurveTo(cp1x,cp1y,cp2x,cp2y,x0,y0);
			ctx.fill();
			break;
	}
	ctx.restore();
};

DrawObjectClass.prototype.drawLine = function drawLine(canvas) {
	var cnvOffsetX = canvas.offsetX;
	var cnvOffsetY = canvas.offsetY;
	var cnvScale = canvas.scale;
	color = DrawColor.color2String( this.color, this.opacity );
	faint = DrawColor.color2String("#ffffff", 0.25, "#eeeeee");
	LINEWIDTH = (this.lineWidth * cnvScale) / DEFAULT_DEVICE_RATIO;
	ARROWLENGTH = (DEFAULT_ARROWHEAD_LENGTH + (this.lineWidth*2)) * cnvScale;
	canvas.context.lineWidth = LINEWIDTH;
	canvas.context.lineCap = 'butt';
	canvas.context.lineJoin = 'miter';
	canvas.context.strokeStyle = color;
	if (this.isSelected) {
		//canvas.context.strokeStyle = faint;
		canvas.context.shadowColor = '#666';
		canvas.context.shadowBlur = 5;
		canvas.context.shadowOffsetX = 5;
		canvas.context.shadowOffsetY = 5;
		canvas.context.globalCompositeOperation = "source-over";
	}

	var x1 = (this.pointLists[0][0] + cnvOffsetX) * cnvScale;
	var y1 = (this.pointLists[0][1] + cnvOffsetY) * cnvScale;
	var x2 = (this.pointLists[1][0] + cnvOffsetX) * cnvScale;
	var y2 = (this.pointLists[1][1] + cnvOffsetY) * cnvScale;

	if (this.drawType === "arrow") {
		this.drawArrowEx(canvas.context,x1,y1,x2,y2,LINEWIDTH,1,DEFAULT_ARROWHEAD_ANGLE,ARROWLENGTH);
	} else {
		this.drawArrowEx(canvas.context,x1,y1,x2,y2,LINEWIDTH,0,0,0);
	}

	if (this.isSelected) {
		//Reset the shadowColor
		canvas.context.shadowColor = null;
		canvas.context.shadowBlur = 0;
		canvas.context.shadowOffsetX = 0;
		canvas.context.shadowOffsetY = 0;
		canvas.context.globalCompositeOperation = "source-over";

		this.drawDragPoints(canvas);
	}

	return true;
};

DrawObjectClass.prototype.drawArrow = function drawArrow(canvas) {
	return this.drawLine(canvas);
};

DrawObjectClass.prototype.drawMarker = function drawMarker(canvas) {
	return this.drawLine(canvas);
};

DrawObjectClass.prototype.drawHighlight = function drawHighlight( canvas ) {
	// console.log( "drawHighlight; canvas:", canvas.offsetX, canvas.offsetY, canvas.scale );
	var cnvOffsetX = canvas.offsetX;
	var cnvOffsetY = canvas.offsetY;
	var rgb = DrawColor.splitColor( this.color );
	canvas.context.fillStyle = (isset( rgb ) ? [ "rgba(", rgb.r, ",", rgb.g, ",", rgb.b, ",", this.opacity, ")" ].join( "" ) : 'rgba(255,255,0,0.3)');
	var xRect = (this.pointLists[ 0 ][ 0 ] + cnvOffsetX) * canvas.scale;
	var yRect = (this.pointLists[ 0 ][ 1 ] + cnvOffsetY) * canvas.scale;
	var wRect = this.width = (this.pointLists[ 1 ][ 0 ] - this.pointLists[ 0 ][ 0 ]) * canvas.scale;
	var hRect = this.height = (this.pointLists[ 1 ][ 1 ] - this.pointLists[ 0 ][ 1 ]) * canvas.scale;
	// console.log( "drawHighlight; rect:", {"x": xRect, "y": yRect, "width": wRect, "height": hRect} );
	canvas.context.fillRect( xRect, yRect, wRect, hRect );

	//Reset the shadowColor
	canvas.context.shadowColor = null;
	canvas.context.shadowBlur = 0;
	canvas.context.shadowOffsetX = 0;
	canvas.context.shadowOffsetY = 0;

	if( this.isSelected ) {
		canvas.context.lineWidth = (1 * canvas.scale) / DEFAULT_DEVICE_RATIO;
		canvas.context.strokeStyle = 'rgba(169, 169, 169, 0.7)';
		canvas.context.strokeRect( xRect, yRect, wRect, hRect );
		this.drawDragPoints( canvas );
	}

	return true;
};

DrawObjectClass.prototype.drawNote = function drawNote(canvas) {
	this.drawNoteIcon(canvas);
	canvas.context.globalCompositeOperation = "source-over";
	canvas.context.lineWidth = this.lineWidth;
	//Reset the shadowColor
	canvas.context.shadowColor = null;
	canvas.context.shadowBlur = 0;
	canvas.context.shadowOffsetX = 0;
	canvas.context.shadowOffsetY = 0;
	if (this.isSelected) {
		this.drawNoteDragPoints(canvas);
	}
	return true;
};

DrawObjectClass.prototype.drawPencilBox = function drawPencilBox(canvas) {
	var cnvOffsetX = canvas.offsetX;
	var cnvOffsetY = canvas.offsetY;
	var cnvScale = canvas.scale;
	canvas.context.beginPath();
	var dragBox = [];
	dragBox[0] = [this.boundingBox[0][0] - DEFAULT_DRAG_ICON_OFFSET, this.boundingBox[0][1] - DEFAULT_DRAG_ICON_OFFSET];
	dragBox[1] = [this.boundingBox[1][0] + DEFAULT_DRAG_ICON_OFFSET, this.boundingBox[1][1] + DEFAULT_DRAG_ICON_OFFSET];

	canvas.context.dashedLine( (dragBox[0][0]+cnvOffsetX)*cnvScale, (dragBox[0][1]+cnvOffsetY)*cnvScale, (dragBox[1][0]+cnvOffsetX)*cnvScale, (dragBox[0][1]+cnvOffsetY)*cnvScale, this.currentTick);
	canvas.context.dashedLine( (dragBox[1][0]+cnvOffsetX)*cnvScale, (dragBox[0][1]+cnvOffsetY)*cnvScale, (dragBox[1][0]+cnvOffsetX)*cnvScale, (dragBox[1][1]+cnvOffsetY)*cnvScale, this.currentTick);
	canvas.context.dashedLine( (dragBox[1][0]+cnvOffsetX)*cnvScale, (dragBox[1][1]+cnvOffsetY)*cnvScale, (dragBox[0][0]+cnvOffsetX)*cnvScale, (dragBox[1][1]+cnvOffsetY)*cnvScale, this.currentTick);
	canvas.context.dashedLine( (dragBox[0][0]+cnvOffsetX)*cnvScale, (dragBox[1][1]+cnvOffsetY)*cnvScale, (dragBox[0][0]+cnvOffsetX)*cnvScale, (dragBox[0][1]+cnvOffsetY)*cnvScale, this.currentTick);

	canvas.context.strokeStyle = DrawColor.color2String('#999999',0.8);
	canvas.context.lineWidth = 2;
	canvas.context.stroke();
},

DrawObjectClass.prototype.drawMoveIcon = function drawMoveIcon(canvas) {
	var x = this.boundingBox[0][0]-DEFAULT_MOVE_ICON_OFFSET;
	var y = this.boundingBox[0][1]-DEFAULT_MOVE_ICON_OFFSET;
	var size = DEFAULT_MOVE_ICON_SIZE;
	var mid = size >> 1;
	var qtr = size >> 2;
	var eth = size >> 3;
	var color = DrawColor.color2String('#666666', 0.66);
	var bgcolor = DrawColor.color2String('#cccccc', 0.66);
	//x += mid; // adjust the icon
	//y += mid; // adjust the icon

	canvas.context.globalCompositeOperation = "difference";
	canvas.context.lineWidth = 1;

	canvas.context.fillStyle = bgcolor;
	canvas.context.fillRect(x, y, size, size);
	canvas.context.strokeStyle = color;
	canvas.context.strokeRect(x, y, size, size);

	canvas.context.beginPath();
	canvas.context.moveTo(x+mid+1, y+eth+1);
	canvas.context.lineTo(x+(mid+eth), y+eth+1);
	canvas.context.lineTo(x+mid, y+1);
	canvas.context.lineTo(x+(mid-eth), y+eth+1);
	canvas.context.lineTo(x+mid-1, y+eth+1);
	canvas.context.lineTo(x+mid-1, y+(size-eth-1));
	canvas.context.lineTo(x+(mid-eth), y+(size-eth-1));
	canvas.context.lineTo(x+mid, y+(size-1));
	canvas.context.lineTo(x+(mid+eth), y+(size-eth-1));
	canvas.context.lineTo(x+mid+1, y+(size-eth-1));
	canvas.context.lineTo(x+mid+1, y+eth);
	canvas.context.closePath();
	canvas.context.fillStyle = color;
	canvas.context.fill();
	canvas.context.strokeStyle = color;
	canvas.context.stroke();

	canvas.context.beginPath();
	canvas.context.moveTo(x+eth+1, y+mid+1);
	canvas.context.lineTo(x+eth+1, y+(mid+eth));
	canvas.context.lineTo(x+1, y+mid);
	canvas.context.lineTo(x+eth+1, y+(mid-eth));
	canvas.context.lineTo(x+eth+1, y+mid-1);
	canvas.context.lineTo(x+(size-eth-1), y+mid-1);
	canvas.context.lineTo(x+(size-eth-1), y+(mid-eth));
	canvas.context.lineTo(x+(size-1), y+mid);
	canvas.context.lineTo(x+(size-eth-1), y+(mid+eth));
	canvas.context.lineTo(x+(size-eth-1), y+mid+1);
	canvas.context.lineTo(x+eth, y+mid+1);
	canvas.context.closePath();
	canvas.context.fillStyle = color;
	canvas.context.fill();
	canvas.context.strokeStyle = color;
	canvas.context.stroke();

	canvas.context.globalCompositeOperation = "source-over";
};

DrawObjectClass.prototype.drawNoteIcon = function drawNoteIcon(canvas) {
	var cnvOffsetX = canvas.offsetX;
	var cnvOffsetY = canvas.offsetY;
	var cnvScale = canvas.scale;
	this.width = Math.max(Math.min(this.width, DEFAULT_NOTE_ICON_MAX_WIDTH), DEFAULT_NOTE_ICON_MIN_WIDTH);
	this.height = Math.max(Math.min(this.height, DEFAULT_NOTE_ICON_MAX_WIDTH), DEFAULT_NOTE_ICON_MIN_WIDTH);

	var currWidth = this.width * cnvScale;
	var currHeight = this.height * cnvScale;

	var radius = Math.floor(currWidth*(DEFAULT_NOTE_ICON_RADIUS/DEFAULT_NOTE_ICON_WIDTH));
	var currX = Math.floor(this.pointLists[0][0] + cnvOffsetX) * cnvScale;
	var currY = Math.floor(this.pointLists[0][1] + cnvOffsetY) * cnvScale;
	var strokeColor = DrawColor.color2String( '#888888', this.opacity );
	var fillColor = DrawColor.color2String( this.color, this.opacity );
	canvas.context.globalCompositeOperation = "source-over";

	// paper outline
	if(this.isSelected) {
		canvas.context.shadowColor = '#666';
		canvas.context.shadowBlur = 5;
		canvas.context.shadowOffsetX = 5;
		canvas.context.shadowOffsetY = 5;
	}
	canvas.context.lineWidth = 2 * cnvScale;
	canvas.context.beginPath();
	canvas.context.moveTo(currX, currY);
	canvas.context.lineTo(currX + currWidth, currY);
	canvas.context.lineTo(currX + currWidth, currY + currHeight - radius);
	canvas.context.lineTo(currX + currWidth - radius, currY + currHeight);
	canvas.context.lineTo(currX, currY + currHeight);
	canvas.context.lineTo(currX, currY);
	canvas.context.closePath();
	canvas.context.fillStyle = fillColor;
	canvas.context.fill();

	canvas.context.shadowColor = null;
	canvas.context.shadowBlur = 0;
	canvas.context.shadowOffsetX = 0;
	canvas.context.shadowOffsetY = 0;

	canvas.context.strokeStyle = strokeColor;
	canvas.context.stroke();

	// paper fold
	canvas.context.beginPath();
	canvas.context.moveTo(currX + currWidth - radius, currY + currHeight - 1);
	canvas.context.lineTo(currX + currWidth - radius, currY + currHeight - radius);
	canvas.context.lineTo(currX + currWidth - 1, currY + currHeight - radius);
	canvas.context.stroke();

	// lines
	var radius = currWidth / 9;
	canvas.context.lineWidth = 2 * cnvScale;
	for(var i=1; i < 4; i++) {
		canvas.context.beginPath();
		canvas.context.moveTo(currX + radius, currY + ((i*2)*radius) - (radius>>4));
		canvas.context.lineTo(currX + currWidth - radius, currY + ((i*2)*radius) - (radius>>4));
		canvas.context.closePath();
		canvas.context.stroke();
	}
};

DrawObjectClass.prototype.drawDragPoints = function drawDragPoints(canvas) {
	//console.log('DrawDragPoints', this.currentTick);
	var cnvOffsetX = canvas.offsetX;
	var cnvOffsetY = canvas.offsetY;
	var cnvScale = canvas.scale;
	canvas.context.shadowColor = '#666';
	canvas.context.shadowBlur = 5;
	canvas.context.shadowOffsetX = 5;
	canvas.context.shadowOffsetY = 5;
	canvas.context.lineWidth = ((DEFAULT_DRAG_ICON_RADIUS*cnvScale)>>1);

	for(var p = 0; p < this.dragPoints.length; p++) {
		var x = (this.dragPoints[p][0]+cnvOffsetX)*cnvScale;
		var y = (this.dragPoints[p][1]+cnvOffsetY)*cnvScale;

		canvas.context.beginPath();
		canvas.context.arc(x, y, DEFAULT_DRAG_ICON_RADIUS*cnvScale, 0, 2 * Math.PI, false);
		canvas.context.fillStyle = '#0091F7';
		canvas.context.fill();
		canvas.context.strokeStyle = '#000B6F';
		canvas.context.stroke();
	}

	canvas.context.shadowColor = null;
	canvas.context.shadowBlur = 0;
	canvas.context.shadowOffsetX = 0;
	canvas.context.shadowOffsetY = 0;
};

DrawObjectClass.prototype.drawNoteDragPoints = function drawNoteDragPoints(canvas) {
	// draw circles
	//console.log('DrawDragPoints', this.currentTick);
	canvas.context.shadowColor = '#666';
	canvas.context.shadowBlur = 5;
	canvas.context.shadowOffsetX = 5;
	canvas.context.shadowOffsetY = 5;
	canvas.context.lineWidth = (DEFAULT_DRAG_ICON_RADIUS>>1);

	for(var p = 1; p < this.dragPoints.length; p=p+2) {
		var x = this.dragPoints[p][0];
		var y = this.dragPoints[p][1];

		canvas.context.beginPath();
		canvas.context.arc(x, y, DEFAULT_DRAG_ICON_RADIUS, 0, 2 * Math.PI, false);
		//canvas.context.endPath();
		canvas.context.fillStyle = '#eeeeee';
		canvas.context.fill();
		canvas.context.strokeStyle = '#990000';
		canvas.context.stroke();
	}

	canvas.context.shadowColor = null;
	canvas.context.shadowBlur = 0;
	canvas.context.shadowOffsetX = 0;
	canvas.context.shadowOffsetY = 0;
};

DrawObjectClass.prototype.drawCallout = function drawCallout(canvas) {
	if(!canvas.isPrimary) {
		return true;
	}
	var rgb_value = DrawColor.splitColor( this.color );
	if (rgb_value !== null) {
		canvas.context.strokeStyle = 'rgba(' + rgb_value.r + ',' + rgb_value.g + ',' + rgb_value.b + ',' + ( this.opacity / 2 ) + ')';
		canvas.context.fillStyle = 'rgba(' + rgb_value.r + ',' + rgb_value.g + ',' + rgb_value.b + ',0.05)';
	} else {
		canvas.context.strokeStyle = 'rgba(255, 0, 0, 0.8)';
		canvas.context.fillStyle = 'rgba(0,0,0,0.1)';
	}
	if (this.isSelected || true) {
		canvas.context.shadowColor = '#666';
		canvas.context.shadowBlur = 5;
		canvas.context.shadowOffsetX = 5;
		canvas.context.shadowOffsetY = 5;
	}
	xRect = this.pointLists[0][0];
	yRect = this.pointLists[0][1];
	this.width = wRect = this.pointLists[1][0] - this.pointLists[0][0];
	this.height = hRect = this.pointLists[1][1] - this.pointLists[0][1];
	canvas.context.lineWidth = this.lineWidth;
	canvas.context.fillRect(xRect, yRect, wRect, hRect);
	canvas.context.strokeRect(xRect, yRect, wRect, hRect);

	//Reset the shadowColor
	canvas.context.shadowColor = null;
	canvas.context.shadowBlur = 0;
	canvas.context.shadowOffsetX = 0;
	canvas.context.shadowOffsetY = 0;

	return true;
};

DrawObjectClass.prototype.calcStampDimensions = function calcStampDimensions(canvas) {
	if(this.stampHeight < DEFAULT_STAMP_MIN_HEIGHT)
		this.stampHeight = DEFAULT_STAMP_MIN_HEIGHT;
	if(this.stampHeight > DEFAULT_STAMP_MAX_HEIGHT)
		this.stampHeight = DEFAULT_STAMP_MAX_HEIGHT;
	this.stampScale = this.stampHeight/DEFAULT_STAMP_MAX_HEIGHT;
};

DrawObjectClass.prototype.stampValidateLocation = function stampValidateLocation() {
	x1 = this.boundingBox[0][0] - this.margin[3];
	y1 = this.boundingBox[0][1] - this.margin[0];
	x2 = this.boundingBox[1][0]+this.boundingBox[0][0]+this.margin[1];
	y2 = this.boundingBox[1][1]+this.boundingBox[0][1]+this.margin[2];

	if(this.canvasWidth < x2) {
		this.pointLists[0][0] -= x2 - this.canvasWidth;
		this.boundingBox[0][0] -= x2 - this.canvasWidth;
	}
	if(this.canvasHeight < y2) {
		this.pointLists[0][1] -= y2 - this.canvasHeight;
		this.boundingBox[0][1] -= y2 - this.canvasHeight;
	}

	if(x1 < 0) {
		this.pointLists[0][0] -= x1;
		this.boundingBox[0][0] -= x1;
	}
	if(y1 < 0) {
		this.pointLists[0][1] -= y1;
		this.boundingBox[0][1] -= y1;
	}

	x1 = this.iconNormalPos[0][0];
	y1 = this.iconNormalPos[0][1];
	x2 = this.iconNormalPos[1][0]+this.iconNormalPos[0][0];
	y2 = this.iconNormalPos[1][1]+this.iconNormalPos[0][1];

	this.reverse = false;

	if(this.canvasWidth < x2 && this.orientation === 2) {
		this.reverse = true;
	}
	if(this.canvasHeight < y2 && this.orientation === 1) {
		this.reverse = true;
	}

	if(x1 < 0 && this.orientation === 0) {
		this.reverse = true;
	}
	if(y1 < 0 && this.orientation === 3) {
		this.reverse = true;
	}
};

DrawObjectClass.prototype.setOrientation = function setOrientation(orientation) {
	this.orientation = orientation % 4;
	this.rotate = (this.orientation * Math.PI)/2.0;
};

DrawObjectClass.prototype.setRotation = function setRotation(angle) {
	if(this.reverse) {
		angle += M_PI;
	}
	if(angle >= Math.PI*2.0) {
		this.setRotation(angle - (Math.PI*2.0));
	}
	this.rotate = angle;
};

DrawObjectClass.prototype.startRotate = function startRotate(canvas, centerX, centerY) {
	this.sin = Math.sin(this.rotate);
	this.cos = Math.cos(this.rotate);
	canvas.context.save();
	canvas.context.translate(centerX, centerY);
	canvas.context.transform(this.cos, this.sin, -this.sin, this.cos, 0, 0);
};

DrawObjectClass.prototype.stopRotate = function stopRotate(canvas) {
	canvas.context.restore();
};

DrawObjectClass.prototype.drawStamp = function drawStamp(canvas) {

	this.calcStampDimensions();
	this.height = DEFAULT_STAMP_MAX_HEIGHT * this.stampScale;
	var titlePad = 28 * this.stampScale;
	this.fontSize = (this.height/5.0); // add 2 for smaller font sizes to be larger than usual, helps with shrinking to a more readable size.
	//console.log(fontSize);
	var dateFontSize = (this.height/5.5);
	//console.log(dateFontSize);
	var segment = this.height/3.0;
	this.height_2 = this.height >> 1;
	canvas.context.font = this.fontSize+"px Arial,sans-serif";
	canvas.context.textAlign = "center";
	var width = this.width = Math.max( canvas.context.measureText(this.line1).width + titlePad, canvas.context.measureText(this.line2).width + titlePad, DEFAULT_STAMP_MIN_WIDTH*this.stampScale);

	this.stampSize[0] = this.width;
	var width_2 = this.width >> 1;

	var lineWidth = DEFAULT_STAMP_MAX_LINEWIDTH * this.stampScale;
	var radius = DEFAULT_STAMP_MAX_RADIUS * this.stampScale;

	var segmentPadding = Math.floor(segment-this.fontSize);

	var currX = Math.floor(this.pointLists[0][0] - (this.width>>1));
	var currY = Math.floor(this.pointLists[0][1] - (this.height>>1));

	var centerX = this.centerX = currX + (this.width>>1);
	var centerY = this.centerY = currY + (this.height>>1);

	this.startRotate(canvas, this.centerX, this.centerY);

	var gradient = canvas.context.createLinearGradient(0, -this.height_2, 0, this.height_2);
	gradient.addColorStop(0, '#ffffff');
	gradient.addColorStop(1, '#e3d5bb');

	if (this.isSelected) {
		canvas.context.shadowColor = '#666';
		canvas.context.shadowBlur = 5;
		canvas.context.shadowOffsetX = 5;
		canvas.context.shadowOffsetY = 5;
	}

	var strokeColor = DrawColor.color2String( this.color, 1.0 );
	canvas.context.globalCompositeOperation = "source-over";

	// draw relative to the center coordinates
	canvas.context.lineWidth = lineWidth;
	canvas.context.beginPath();
	canvas.context.moveTo(-width_2 + radius, -this.height_2);
	canvas.context.lineTo(width_2 - radius, -this.height_2);
	canvas.context.quadraticCurveTo(width_2, -this.height_2, width_2, -this.height_2 + radius);
	canvas.context.lineTo(width_2, this.height_2 - radius);
	canvas.context.quadraticCurveTo(width_2, this.height_2, width_2 - radius, this.height_2);
	canvas.context.lineTo(-width_2 + radius, this.height_2);
	canvas.context.quadraticCurveTo(-width_2, this.height_2, -width_2, this.height_2 - radius);
	canvas.context.lineTo(-width_2, -this.height_2 + radius);
	canvas.context.quadraticCurveTo(-width_2, -this.height_2, -width_2 + radius, -this.height_2);
	canvas.context.closePath();
	canvas.context.fillStyle = gradient;
	canvas.context.fill();
	canvas.context.shadowColor = null;
	canvas.context.shadowBlur = 0;
	canvas.context.shadowOffsetX = 0;
	canvas.context.shadowOffsetY = 0;

	canvas.context.strokeStyle = strokeColor;
	canvas.context.stroke();

	canvas.context.font = this.fontSize+"px Helvetica,Arial,sans-serif";
	canvas.context.textAlign = "center";

	canvas.context.lineWidth = (DEFAULT_STAMP_MAX_LINEWIDTH - 1) * this.stampScale;
	canvas.context.beginPath();
	canvas.context.moveTo(-width_2 + radius, -this.height_2+((this.height/3)<<1)+2); // add 2 for smaller resolution appearance.
	canvas.context.lineTo( width_2 - radius, -this.height_2+((this.height/3)<<1)+2); // add 2 for smaller resolution appearance.
	canvas.context.strokeStyle = strokeColor;
	canvas.context.stroke();

	canvas.context.beginPath();
	canvas.context.fillStyle = strokeColor;
	canvas.context.fillText(this.line1,0,-this.height_2+(segment*1)-segmentPadding+(this.fontSize>>2));// TITLE
	canvas.context.fillText(this.line2,0,-this.height_2+(segment*2)-segmentPadding+(this.fontSize>>3));// SUBTITLE
	canvas.context.stroke();
	canvas.context.fill();

	canvas.context.font = dateFontSize+"px Helvetica,Arial,sans-serif";
	canvas.context.fillText(this.line3,0,-this.height_2+(segment*3)-segmentPadding);// DATE
	canvas.context.stroke();
	canvas.context.fill();

	if(DEFAULT_STAMP_SCALE_ICON_READY) {
		this.stampIconPlacement = [-width_2-44-10, -22, -width_2-10, 21];
		if(this.reverse) {
			canvas.context.scale(-1,1);
		}
		canvas.context.drawImage(DEFAULT_STAMP_SCALE_ICON, this.stampIconPlacement[0], this.stampIconPlacement[1], 44, 44);
	}

	this.stopRotate(canvas);

	if((this.orientation % 2) == 1) {
		var tmp = this.width;
		this.width = this.height;
		this.height = tmp;
		tmp = width_2;
		width_2 = this.height_2;
		this.height_2 = tmp;
		currX = centerX - width_2;
		currY = centerY - this.height_2;
	}

	this.boundingBox = [[currX, currY], [this.width+DEFAULT_STAMP_MAX_LINEWIDTH, this.height+DEFAULT_STAMP_MAX_LINEWIDTH]];
	//canvas.context.fillStyle = DrawColor.color2String('#ffff00', 0.40);;
	//canvas.context.fillRect(currX, currY, this.width+DEFAULT_STAMP_MAX_LINEWIDTH, this.height+DEFAULT_STAMP_MAX_LINEWIDTH);
	this.iconNormalPos = [[0,0],[0,0]];
	this.iconScreenPos = [[0,0],[0,0]];
	switch(this.orientation) {
		case 0:
			this.iconScreenPos = this.iconNormalPos = [[currX-DEFAULT_STAMP_MIN_HEIGHT-10, centerY-(DEFAULT_STAMP_MIN_HEIGHT>>1)], [DEFAULT_STAMP_MIN_HEIGHT, DEFAULT_STAMP_MIN_HEIGHT]];
			if(this.reverse)
				this.iconScreenPos = [[currX+this.width+10, centerY-(DEFAULT_STAMP_MIN_HEIGHT>>1)], [DEFAULT_STAMP_MIN_HEIGHT, DEFAULT_STAMP_MIN_HEIGHT]];
			break;
		case 1:
			this.iconScreenPos = this.iconNormalPos = [[centerX-(DEFAULT_STAMP_MIN_HEIGHT>>1), currY-DEFAULT_STAMP_MIN_HEIGHT-10], [DEFAULT_STAMP_MIN_HEIGHT, DEFAULT_STAMP_MIN_HEIGHT]];
			if(this.reverse)
				this.iconScreenPos = [[centerX-(DEFAULT_STAMP_MIN_HEIGHT>>1), currY+this.height+10], [DEFAULT_STAMP_MIN_HEIGHT, DEFAULT_STAMP_MIN_HEIGHT]];
			break;
		case 2:
			//if(!this.reverse)
			this.iconScreenPos = this.iconNormalPos = [[currX+this.width+10, centerY-(DEFAULT_STAMP_MIN_HEIGHT>>1)], [DEFAULT_STAMP_MIN_HEIGHT, DEFAULT_STAMP_MIN_HEIGHT]];
			if(this.reverse)
				this.iconScreenPos = [[currX-DEFAULT_STAMP_MIN_HEIGHT-10, centerY-(DEFAULT_STAMP_MIN_HEIGHT>>1)], [DEFAULT_STAMP_MIN_HEIGHT, DEFAULT_STAMP_MIN_HEIGHT]];
			break;
		case 3:
			this.iconScreenPos = this.iconNormalPos = [[centerX-(DEFAULT_STAMP_MIN_HEIGHT>>1), currY+this.height+10], [DEFAULT_STAMP_MIN_HEIGHT, DEFAULT_STAMP_MIN_HEIGHT]];
			if(this.reverse)
				this.iconScreenPos = [[centerX-(DEFAULT_STAMP_MIN_HEIGHT>>1), currY-DEFAULT_STAMP_MIN_HEIGHT-10], [DEFAULT_STAMP_MIN_HEIGHT, DEFAULT_STAMP_MIN_HEIGHT]];
			break;
	}
};

DrawObjectClass.prototype.createBoundingBox = function createBoundingBox() {
	if (this.drawType === "pencil" || this.drawType === "arrow" || this.drawType === "marker") {
		this.createLineBBox();
	} else if (this.drawType === "highlight" || this.drawType === "rect" || this.drawType === "callout") {
		this.createRectBBox();
	} else if (this.drawType === "note") {
		this.createNoteBBox();
	}
};

DrawObjectClass.prototype.createLineBBox = function createLineBBox() {
	var x1, x2, y1, y2;
	x1 = x2 = this.pointLists[0][0];
	y1 = y2 = this.pointLists[0][1];

	var i = 0, j = 0;
	for(i = 0; i < this.pointLists.length; i++) {
		x1 = Math.min(x1, this.pointLists[i][0]);
		y1 = Math.min(y1, this.pointLists[i][1]);
		x2 = Math.max(x2, this.pointLists[i][0]);
		y2 = Math.max(y2, this.pointLists[i][1]);
	}
	this.storeBoundingBox(x1, y1, x2, y2);
};

DrawObjectClass.prototype.createRectBBox = function createRectBBox() {
	var x1, x2, y1, y2;
	x1 = this.pointLists[0][0];
	y1 = this.pointLists[0][1];
	x2 = this.pointLists[1][0];
	y2 = this.pointLists[1][1];

	var fx1 = Math.min(x1, x2);
	var fy1 = Math.min(y1, y2);
	var fx2 = Math.max(x1, x2);
	var fy2 = Math.max(y1, y2);

	this.storeBoundingBox(fx1, fy1, fx2, fy2);
};

DrawObjectClass.prototype.createNoteBBox = function createNoteBBox() {
	var x1, x2, y1, y2;
	x1 = this.pointLists[0][0] - (this.width>>1);
	y1 = this.pointLists[0][1] - (this.height>>1);
	x2 = x1 + this.width;
	y2 = y1 + this.height;
	this.storeBoundingBox(x1, y1, x2, y2);
};

DrawObjectClass.prototype.storeBoundingBox = function storeBoundingBox(x1, y1, x2, y2) {
	this.boundingBox = [];
	this.boundingBox.push([x1, y1]);
	this.boundingBox.push([x2, y2]);
	this.updateDragPoints();
};

DrawObjectClass.prototype.getBoundingBox = function getBoundingBox() {
	return this.boundingBox;
};

DrawObjectClass.prototype.isPoint = function isPoint() {
	if(Math.abs(this.boundingBox[0][0] - this.boundingBox[1][0]) < 4 &&
			Math.abs(this.boundingBox[0][1] - this.boundingBox[1][1]) < 4) {
		return true;
	}
	if(this.isDot()) {
		return true;
	}
	return false;
};

DrawObjectClass.prototype.isDot = function isDot() {
	var dot = false;
	if(this.pointLists.length <= 4) {
		currX = this.pointLists[0][0];
		currY = this.pointLists[0][1];
		minX = currX - 1;
		minY = currY - 1;
		maxX = currX + 1;
		maxY = currY + 1;
		dot = true;
		for (i = 0; i < this.pointLists.length; i++) {
			if(minX > this.pointLists[i][0]
					|| minY > this.pointLists[i][1]
					|| maxX < this.pointLists[i][0]
					|| maxY < this.pointLists[i][1]) {
				dot = false;
				break;
			}
		}
	}
	return dot;
};

DrawObjectClass.prototype.isResizable = function isResizable() {
	return this.isResizableX() || this.isResizableY();
};

DrawObjectClass.prototype.isResizableX = function isResizableX() {
	var renderWidth = Math.abs(this.boundingBox[1][0] - this.boundingBox[0][0]);
	var pdfWidth = Math.ceil(renderWidth * DEFAULT_DEVICE_RATIO) + this.lineWidth;
	if(pdfWidth < 12) {
		return false;
	}
	return true;
};

DrawObjectClass.prototype.isResizableY = function isResizableY() {
	var renderHeight = Math.abs(this.boundingBox[1][1] - this.boundingBox[0][1]);
	var pdfHeight = Math.ceil(renderHeight * DEFAULT_DEVICE_RATIO) + this.lineWidth;
	if(pdfHeight < 12) {
		return false;
	}
	return true;
};

DrawObjectClass.prototype.isDragResizableX = function isDragResizableX() {
	return !!this.startResizableX;
}

DrawObjectClass.prototype.isDragResizableY = function isDragResizableY() {
	return !!this.startResizableY;
}

DrawObjectClass.prototype.startDrag = function startDrag(x, y) {
	// stuff to start the move
	this.dragUnderway = true;
	this.currX = x;
	this.currY = y;
	this.startResizableX = this.isResizableX();
	this.startResizableY = this.isResizableY();
//		if(this.drawType == 'stamp') {
//			// decide what stamp mode to be in. rotate/resize, or move
//			iconX = currX-30;
//			iconY = (currY+(height>>1))-10;
//			if(x >= iconX && x <= iconX+20
//				&& y >= iconY && y <= iconY+20) {
//				this.stampX = this.currX;
//				this.stampY = this.currY;
//				this.stampMode = 'resize/rotate';
//			}
//			else {
//				this.stampMode = 'move';
//			}
//		}
};

DrawObjectClass.prototype.endDrag = function endDrag() {
	// stuff to end the move
	this.dragUnderway = false;
};

DrawObjectClass.prototype.drag = function drag(x, y) {
	if(!this.dragUnderway) {
		this.startDrag(x, y);
	}
	this.prevX = this.currX;
	this.prevY = this.currY;
	this.currX = x;
	this.currY = y;
	var deltaX = this.currX - this.prevX;
	var deltaY = this.currY - this.prevY;
	if (this.drawType === 'arrow' || this.drawType === 'marker' || this.drawType === 'callout' ) {
		if (this.selectedDragPoint !== false) {
			this.pointLists[this.selectedDragPoint][0] += deltaX;
			this.pointLists[this.selectedDragPoint][1] += deltaY;
		} else {
			this.movePencil( deltaX, deltaY );
		}
	} else if (this.drawType === 'pencil' || this.drawType === 'highlight') {
		if (this.selectedDragPoint !== false
				&& ((this.isDragResizableX() && [0,2,3,4,6,7].indexOf(this.selectedDragPoint) !== false)
					|| (this.isDragResizableY() && [0,1,2,4,5,6].indexOf(this.selectedDragPoint) !== false)) ) {
			var bounds = JSON.parse( JSON.stringify( this.boundingBox ) );
			switch(this.selectedDragPoint) {
				case 0:	// Top left
					bounds[0][0] += deltaX;
					bounds[0][1] += deltaY;
					break;
				case 1: // Top middle
					bounds[0][1] += deltaY;
					break;
				case 2: // Top right
					bounds[1][0] += deltaX;
					bounds[0][1] += deltaY;
					break;
				case 3: // Middle right
					bounds[1][0] += deltaX;
					break;
				case 4: // Bottom right
					bounds[1][0] += deltaX;
					bounds[1][1] += deltaY;
					break;
				case 5: // Bottom middle
					bounds[1][1] += deltaY;
					break;
				case 6: // Bottom left
					bounds[0][0] += deltaX;
					bounds[1][1] += deltaY;
					break;
				case 7: // Left middle
					bounds[0][0] += deltaX;
					break;
			}
			// Convert bounds to message format.
			this.width = Math.abs(bounds[1][0] - bounds[0][0]);
			this.height = Math.abs(bounds[1][1] - bounds[0][1]);

			if(this.drawType === 'highlight') {
				if(this.width < this.lineWidth) {
					bounds[1][0] = bounds[0][0] + this.lineWidth;
				}
				if(this.height < this.lineWidth) {
					bounds[1][1] = bounds[0][1] + this.lineWidth;
				}
			}

			var strBounds = this.formatBounds(bounds, this.canvasWidth, this.canvasHeight);
			this.drawUpdateBounds(strBounds);
		} else {
			this.movePencil( deltaX, deltaY );
		}
	}
	else if(this.drawType === 'note') {
		if (this.selectedDragPoint !== false) {
			var bounds = JSON.parse( JSON.stringify( this.boundingBox ) );
			switch(this.selectedDragPoint) {
				case 1: // Top middle
					deltaY = -deltaY;
				case 5: // Bottom middle
					this.height += deltaY;
					this.height = this.width = Math.max(Math.min(this.height, DEFAULT_NOTE_ICON_MAX_WIDTH), DEFAULT_NOTE_ICON_MIN_WIDTH);
					break;
				case 7: // Left middle
					deltaX = -deltaX;
				case 3: // Middle right
					this.height += deltaX;
					this.height = this.width = Math.max(Math.min(this.height, DEFAULT_NOTE_ICON_MAX_WIDTH), DEFAULT_NOTE_ICON_MIN_WIDTH);
					break;
			}
			this.createBoundingBox();
			this.updateDragPoints();
		} else {
			this.movePencil( deltaX, deltaY );
		}
	}
	else if(this.drawType === 'stamp') {
		this.movePencil( deltaX, deltaY );
	}
	this.createBoundingBox();
	this.updateDragPoints();
};

DrawObjectClass.prototype.dragDeprecated = function dragDeprecated(x, y) {
	if(!this.dragUnderway) {
		this.startDrag(x, y);
	}
	var dx = x - this.currX;
	var dy = y - this.currY;
	switch(this.drawType) {
		case 'rect':
		case 'callout':
		case 'highlight':
			this.moveHighlight(dx, dy);
			break;
		case 'pencil':
		case 'arrow':
		case 'marker':
		case 'note':
			this.movePencil(dx, dy);
			break;
	}
	this.currX = x;
	this.currY = y;
	this.createBoundingBox();
};

DrawObjectClass.prototype.movePencil = function movePencil(dx, dy) {
	for(var idx in this.pointLists) {
		this.pointLists[idx][0] += dx;
		this.pointLists[idx][1] += dy;
	}
};

DrawObjectClass.prototype.getObj = function getObj(x, y) {
	if(this.hit(x, y)) {
		return this;
	}
	return undefined;
};

DrawObjectClass.prototype.select = function select() {
	if(!this.unselectable) {
		this.isSelected = true;
		this.createBoundingBox();
	}
};

DrawObjectClass.prototype.deselect = function deselect() {
	this.isSelected = false;
	this.endDrag();
};

DrawObjectClass.prototype.hit = function hit(x, y) {
	if(this.unselectable) {
		return false;
	}

	if(this.unhittable) {
		return false;
	}

	if(!this.visible) {
		return false;
	}

	//Check bounding box first.
	if (( this.boundingBox[0][0] - this.hitThreshold <= x && x <= this.boundingBox[1][0] + this.hitThreshold ) &&
		( this.boundingBox[0][1] - this.hitThreshold <= y && y <= this.boundingBox[1][1] + this.hitThreshold )) {

		if (this.drawType === "pencil") {
			return this.hitLine( x, y );
		} else if (this.drawType === "arrow" || this.drawType === "marker") {
			return this.hitArrow( x, y ); // marker is same as arrow but no arrowhead
		} else if (this.drawType === "highlight") {
			return this.hitRect( x, y );
		} else if (this.drawType === "note") {
			return true;
		}
	}

	return false;
};

DrawObjectClass.prototype.hitSelected = function hitSelected(x, y) {
	//Check bounding box first.
	if( this.isSelected && !this.unselectable ) {
		this.selectedDragPoint = this.hitDragPoint(x, y);
		if(this.selectedDragPoint!==false)
			return true;
		//if(this.hitMove(x,y))
		//	return true;
		return this.hit(x,y);
	}
	return false;
};

DrawObjectClass.prototype.hitLine = function hitLine( x, y ) {
	var i = 0;
	var x1 = 0;
	var y1 = 0;
	var x2 = 0;
	var y2 = 0;
	//var slope = 0;
	var threshold = this.hitThreshold + this.lineWidth;
	var thresholdHalf = threshold >> 1;
	for(i = 1; i < this.pointLists.length; i++) {
		x1 = this.pointLists[i-1][0];
		y1 = this.pointLists[i-1][1];
		x2 = this.pointLists[i][0];
		y2 = this.pointLists[i][1];

		var dx = x2 - x1; // change in x
		var dy = y2 - y1; // change in y

		var bx1 = x1 < x2 ? x1 : x2;
		var by1 = y1 < y2 ? y1 : y2;
		var bx2 = bx1 === x1 ? x2 : x1;
		var by2 = by1 === y1 ? y2 : y1;

		bx1 -= thresholdHalf;
		by1 -= thresholdHalf;
		bx2 += thresholdHalf;
		by2 += thresholdHalf;

		if(bx2 < x || bx1 > x)
			continue;
		if(by2 < y || by1 > y)
			continue;

		var stepsX = Math.abs(dx / thresholdHalf);
		var stepsY = Math.abs(dy / thresholdHalf);

		var steps = stepsX > stepsY ? stepsX : stepsY;
		if(steps === 0) {
			steps = 1;
		}

		var stepX = dx / steps;
		var stepY = dy / steps;

		// step through 1/2 threshold size
		for(var j = 0; j < steps; j++) {
			var cx = x1 + (j*stepX);
			var cy = y1 + (j*stepY);
			var cx1 = cx - thresholdHalf;
			var cx2 = cx + thresholdHalf;
			var cy1 = cy - thresholdHalf;
			var cy2 = cy + thresholdHalf;

			if(cx2 >= x && cx1 <= x && cy2 >= y && cy1 <= y)
				return true;
		}

	}
	return false;
};

DrawObjectClass.prototype.hitArrow = function hitArrow(x, y) {

	var x1 = this.pointLists[0][0];
	var y1 = this.pointLists[0][1];
	var x2 = this.pointLists[1][0];
	var y2 = this.pointLists[1][1];
	var threshold = this.hitThreshold + this.lineWidth;
	var thresholdHalf = threshold >> 1;

	var dx = x2 - x1; // change in x
	var dy = y2 - y1; // change in y

	var bx1 = x1 < x2 ? x1 : x2;
	var by1 = y1 < y2 ? y1 : y2;
	var bx2 = bx1 === x1 ? x2 : x1;
	var by2 = by1 === y1 ? y2 : y1;

	bx1 -= thresholdHalf;
	by1 -= thresholdHalf;
	bx2 += thresholdHalf;
	by2 += thresholdHalf;

	if(bx2 < x || bx1 > x)
		return false;
	if(by2 < y || by1 > y)
		return false;

	var stepsX = Math.abs(dx / thresholdHalf);
	var stepsY = Math.abs(dy / thresholdHalf);

	var steps = stepsX > stepsY ? stepsX : stepsY;
	if(steps === 0) {
		steps = 1;
	}

	var stepX = dx / steps;
	var stepY = dy / steps;

	// step through 1/2 threshold size
	for(var j = 0; j < steps; j++) {
		var cx = x1 + (j*stepX);
		var cy = y1 + (j*stepY);
		var cx1 = cx - thresholdHalf;
		var cx2 = cx + thresholdHalf;
		var cy1 = cy - thresholdHalf;
		var cy2 = cy + thresholdHalf;

		if(cx2 >= x && cx1 <= x && cy2 >= y && cy1 <= y)
			return true;
	}

	return false;
};

DrawObjectClass.prototype.hitRect = function hitRect(x, y) {
	var x1, x2, y1, y2;
	x1 = this.pointLists[0][0];
	y1 = this.pointLists[0][1];
	x2 = this.pointLists[1][0];
	y2 = this.pointLists[1][1];

	var fx1 = Math.min(x1, x2);
	var fy1 = Math.min(y1, y2);
	var fx2 = Math.max(x1, x2);
	var fy2 = Math.max(y1, y2);

	if(x >= fx1 && x <= fx2 && y >= fy1 && y <= fy2)
		return true;

	return false;
};

DrawObjectClass.prototype.hitMove = function hitMove(x, y) {
	var x1 = this.boundingBox[0][0]-DEFAULT_MOVE_ICON_OFFSET;
	var y1 = this.boundingBox[0][1]-DEFAULT_MOVE_ICON_OFFSET;
	var x2 = x1+DEFAULT_MOVE_ICON_SIZE;
	var y2 = y1+DEFAULT_MOVE_ICON_SIZE;
	if(x1 <= x && x2 >= x && y1 <= y && y2 >= y)
		return true;

	return false;
};

DrawObjectClass.prototype.hitStamp = function hitStamp(x, y) {
	if(x >= this.boundingBox[0][0] && x <= this.boundingBox[0][0]+this.boundingBox[1][0]
		&& y >= this.boundingBox[0][1] && y <= this.boundingBox[0][1]+this.boundingBox[1][1]) {
		return 'move';
	}
	if(x >= this.iconScreenPos[0][0] && x <= this.iconScreenPos[0][0]+this.iconScreenPos[1][0]
		&& y >= this.iconScreenPos[0][1] && y <= this.iconScreenPos[0][1]+this.iconScreenPos[1][1]) {
		return 'rotate/resize';
	}
	return '';
};

DrawObjectClass.prototype.addNoteMouseOver = function addNoteMouseOver() {
	//Add annotation layer
	return 0;
	var pageContainer = $('.pdf-canvas-wrap');
	if ($(pageContainer).children( '.annotationLayer').length < 1) {
		$(pageContainer).append( '<div class="annotationLayer"><section class="annotText"><div class="annotTextContentWrapper" style="left: 27px; top: -10px;"><div class="annotTextContent" style="display: none;"></div></div></section></div>' );
	}
	var annotationLayer = $(pageContainer).children( '.annotationLayer');
	//console.log('addNoteMouseOver', annotationLayer);
	if(typeof this.date === 'undefined')
		this.date = new Date().getTime();
	var noteID = 'note_' + this.date;

	//Add note hit spot and note.
	$(annotationLayer).append( '');
	var annotText = $(annotationLayer).children( '#' + noteID );
	var scale = $(pageContainer).width() * 1.54 / 947;
	var scaleAdjustment = 0;
	// Ajust the click box due to matrix scaling.
	if (scale < 1) {
		scaleAdjustment = 0;
	} else {
		scaleAdjustment = (scale - 1) * 10;
	}
	$(annotText).css({'width': '20px', 'height': '20px', 'position': 'absolute', 'left': ( this.pointLists[0][0] + scaleAdjustment )  + 'px',
						'top': ( this.pointLists[0][1] + scaleAdjustment ) + 'px' } );
	$(annotText).css('-webkit-transform', 'matrix(' + scale + ', 0, 0, ' + scale + ', 0, 0)');
	$(annotText).append( '');
	var annotTextContent = $(annotText).find( '.annotTextContent' );
	$(annotTextContent).css("background-color", this.color);
	$(annotTextContent).append( '<h1>' + this.author + '</h1>' );
	var text = '<p><span>';
	if(this.note) {
		var lines = this.note.split(/\r\n|\r|\n/g);
		for (var l in lines) {
			text += lines[l] + '<br />';
		}
	}
	text += '</span></p>';
	$(annotTextContent).append( text );

	//Add note mouse over.
	$(annotText).mouseenter(function(e) {
		var x = $(e.target).find( '.annotTextContent');
		$(e.target).find( '.annotTextContent').show();
	});
	$(annotText).mouseleave(function(e) {
		$(e.target).find( '.annotTextContent').hide();
	});
};

DrawObjectClass.prototype.updateDragPoints = function updateDragPoints() {
	if (this.drawType === 'arrow' || this.drawType === 'marker' ) {
		var currX, currY, prevX, prevY;
		prevX = this.pointLists[0][0];
		prevY = this.pointLists[0][1];
		currX = this.pointLists[1][0];
		currY = this.pointLists[1][1];
		var lineAngle = Math.atan2( currY - prevY, currX - prevX );

		var x, y;
		x = -DEFAULT_DRAG_ICON_OFFSET * Math.cos( lineAngle ) + prevX;
		y = -DEFAULT_DRAG_ICON_OFFSET * Math.sin( lineAngle ) + prevY;
		this.dragPoints[0] = [x, y];
		x = DEFAULT_DRAG_ICON_OFFSET * Math.cos( lineAngle ) + currX;
		y = DEFAULT_DRAG_ICON_OFFSET * Math.sin( lineAngle ) + currY;
		this.dragPoints[1] = [x, y];
	} else if (this.drawType === 'pencil' || this.drawType === 'note' || this.drawType === 'highlight') {
		// Create the 8 drag points, starting in the top left and going clockwise.
		this.dragBox = [];
		this.dragBox[0] = [this.boundingBox[0][0] - DEFAULT_DRAG_ICON_OFFSET, this.boundingBox[0][1] - DEFAULT_DRAG_ICON_OFFSET];
		this.dragBox[1] = [this.boundingBox[1][0] + DEFAULT_DRAG_ICON_OFFSET, this.boundingBox[1][1] + DEFAULT_DRAG_ICON_OFFSET];
		this.dragPoints[0] = [this.dragBox[0][0], this.dragBox[0][1]];
		this.dragPoints[1] = [this.dragBox[0][0] + ( this.dragBox[1][0] - this.dragBox[0][0] ) / 2, this.dragBox[0][1]];
		this.dragPoints[2] = [this.dragBox[1][0], this.dragBox[0][1]];
		this.dragPoints[3] = [this.dragBox[1][0], this.dragBox[0][1] + ( this.dragBox[1][1] - this.dragBox[0][1] ) / 2];
		this.dragPoints[4] = [this.dragBox[1][0], this.dragBox[1][1]];
		this.dragPoints[5] = [this.dragBox[0][0] + ( this.dragBox[1][0] - this.dragBox[0][0] ) / 2, this.dragBox[1][1]];
		this.dragPoints[6] = [this.dragBox[0][0], this.dragBox[1][1]];
		this.dragPoints[7] = [this.dragBox[0][0], this.dragBox[0][1] + ( this.dragBox[1][1] - this.dragBox[0][1] ) / 2];
	}
};

DrawObjectClass.prototype.hitDragPoint = function hitDragPoint( x, y) {
	for (var i = 0; i < this.dragPoints.length; i++) {
		var x1 = this.dragPoints[i][0];
		var y1 = this.dragPoints[i][1];
		if (this.hitThreshold >= Math.sqrt(Math.pow(x1 - x, 2) + Math.pow(y1 - y, 2))) {
			return i;
		}
	}
	return false;
};

DrawObjectClass.prototype.setLineWidth = function setLineWidth(width) {
	this.lineWidth = width;
};

DrawObjectClass.prototype.getColor = function getColor() {
	return this.color;
};

DrawObjectClass.prototype.setColor = function setColor(color) {
	this.color = color;
};

DrawObjectClass.prototype.setOpacity = function setOpacity(opacity) {
	this.opacity = opacity;
};

DrawObjectClass.prototype.setBorderColor = function setBorderColor(color) {
	this.borderColor = color;
};

DrawObjectClass.prototype.setBorderOpacity = function setBorderOpacity(opacity) {
	this.borderOpacity = opacity;
};

DrawObjectClass.prototype.translate = function translate(pointLists, height, scale) {
	var xIdx = 0;
	var yIdx = 1;
	var wIdx = 2;
	var hIdx = 3;
	if(this.drawType === 'rect') {
		pointLists[xIdx] = pointLists[0] * scale;
		if(height > 0) {
			pointLists[yIdx] = height - (pointLists[1] * scale);
		}
		else {
			pointLists[yIdx] = pointLists[1] * scale;
		}
		pointLists[wIdx] = pointLists[2] * scale;
		pointLists[hIdx] = pointLists[3] * scale;
	}
	else {
		for(var i = 0; i < pointLists.length; i++) {
			pointLists[i][xIdx] = pointLists[i][0] * scale;
			if(height > 0) {
				pointLists[i][yIdx] = height - (pointLists[i][1] * scale);
			}
			else {
				pointLists[i][yIdx] = pointLists[i][1] * scale;
			}
		}
	}
	return pointLists;
};

DrawObjectClass.prototype.changeLineEnd = function changeLineEnd( x, y ) {
	// console.log( "changeLineEnd;", {"x":x, "y":y} );
	this.pointLists[ 1 ][ 0 ] = x; // scaling?
	this.pointLists[ 1 ][ 1 ] = y; // scaling?
};

DrawObjectClass.prototype.changeLineStart = function changeLineStart( x, y ) {
	// console.log( "changeLineStart;", {"x":x, "y":y} );
	this.pointLists[ 0 ][ 0 ] = x;
	this.pointLists[ 0 ][ 1 ] = y;
};

DrawObjectClass.prototype.generateSig = function generateSig(scale) {
	if(typeof scale === 'undefined') {
		scale = DEFAULT_SCALE_FACTOR;
	}

	var result = "";
	var pointCount = this.pointLists.length;
	var firstPoint = this.pointLists[0];
	var lastPoint = this.pointLists[pointCount-1];

	//enum ANNOTATION_TYPE {
	//  0 ANNO_NONE,
	//  1 ANNO_PENCIL,
	//  2 ANNO_STAMP,
	//  3 ANNO_ARROW,
	//  4 ANNO_NOTE,
	//  5 ANNO_MARKER
	//};

	var type = (this.drawType==="pencil"?1:(this.drawType==="stamp"?2:(this.drawType==="arrow"?3:(this.drawType==="note"?4:(this.drawType==="marker"?5:(this.drawType==="highlight"?5:0))))));

	if(this.drawType === "note" && pointCount === 1) {
		pointCount++;
		lastPoint = [firstPoint[0]+(DEFAULT_NOTE_ICON_WIDTH*scale),firstPoint[1]-(DEFAULT_NOTE_ICON_HEIGHT*scale)];
	}

	//ss << std::hex << std::setfill('0') << std::uppercase;

	result = result + __pad(type.toString(16),1,'0');
	//ss << setw(1) << annoPtr->m_type;

	result = result + __pad(this.lineWidth.toString(16),2,'0');
	//ss << setw(1) << annoPtr->m_lineWidth;

	result = result + __pad((this.opacity*100).toString(16),2,'0');
	//ss << setw(2) << annoPtr->m_opacity;

	result = result + __pad(this.color.substring(1).toString(16),6,'0');
	//ss << setw(6) << annoPtr->m_color;

	result = result + __pad(pointCount.toString(16),4,'0');
	//ss << setw(4) << pointCount;

	result = result + __pad(Math.abs(Math.round(firstPoint[0]/scale)).toString(16),4,'0');
	//ss << setw(4) << (int) annoPtr->m_linePoints[0].x;

	result = result + __pad(Math.abs(Math.round((this.canvasHeight-firstPoint[1])/scale)).toString(16),4,'0');
	//ss << setw(4) << (int) annoPtr->m_linePoints[0].y;

	result = result + __pad(Math.abs(Math.round(lastPoint[0]/scale)).toString(16),4,'0');
	//ss << setw(4) << (int) annoPtr->m_linePoints[pointCount-1].x;

	result = result + __pad(Math.abs(Math.round((this.canvasHeight-lastPoint[1])/scale)).toString(16),4,'0');
	//ss << setw(4) << (int) annoPtr->m_linePoints[pointCount-1].y;

	//console.log('Create annotation Signature', result.toUpperCase());
	//LOGMSG( LOG_DEBUG, "Created annotation Signature %s", ss.str().c_str() );

	if(this.sig === "") {
		this.sig = result.toUpperCase();
	}

	return result.toUpperCase();
	//return ss.str();

	function __pad(n, width, z) {
		z = z || '0';
		n = n + '';
		return n.length > width ? n.substring(n.length-width) : (n.length < width ? new Array(width - n.length + 1).join(z) + n : n);
	};
};

DrawObjectClass.prototype.clone = function clone() {
	var newObject = new DrawObjectClass( this.hitThreshold );
	newObject.setDrawType(this.drawType, this.lineWidth);
	newObject.category = this.category;
	newObject.author = this.author;
	newObject.boundingBox = JSON.parse( JSON.stringify( this.boundingBox ) );
	newObject.color = this.color;
	newObject.opacity = this.opacity;
	newObject.borderColor = this.borderColor;
	newObject.borderOpacity = this.borderOpacity;
	newObject.border = this.border;
	newObject.date = this.date;
	newObject.index = this.index;
	newObject.lineCount = this.lineCount;
	newObject.note = this.note;
	newObject.page = this.page;
	newObject.pageIndex = this.pageIndex;
	newObject.pointLists = JSON.parse( JSON.stringify( this.pointLists ) );
	newObject.dragPoints = JSON.parse( JSON.stringify( this.dragPoints ) );
	newObject.width = this.width;
	newObject.height = this.height;
	newObject.canvasWidth = this.canvasWidth;
	newObject.canvasHeight = this.canvasHeight;
	newObject.scale = this.scale;
	newObject.sig = this.getSig();
	newObject.origin = this.origin;
	newObject.unselectable = this.unselectable;
	newObject.unhittable = this.unhittable;
	newObject.existing = this.existing;
	newObject.hostPageRotation = this.hostPageRotation;
	newObject.visible = this.visible;
	newObject.parent = this;

	return newObject;
};

DrawObjectClass.prototype.inject = function inject(injectorObject) {
	if(injectorObject && injectorObject.toString() === 'DrawObject') {
		this.category = injectorObject.category;
		this.hitThreshold = injectorObject.hitThreshold;
		this.author = injectorObject.author;
		this.boundingBox = JSON.parse( JSON.stringify( injectorObject.boundingBox ) );
		this.color = injectorObject.color;
		this.opacity = injectorObject.opacity;
		this.borderColor = injectorObject.borderColor;
		this.borderOpacity = injectorObject.borderOpacity;
		this.border = injectorObject.border;
		this.date = injectorObject.date;
		this.drawType = injectorObject.drawType;
		this.index = injectorObject.index;
		this.lineCount = injectorObject.lineCount;
		this.lineWidth = injectorObject.lineWidth;
		this.note = injectorObject.note;
		this.page = injectorObject.page;
		this.pageIndex = injectorObject.pageIndex;
		this.pointLists = JSON.parse( JSON.stringify( injectorObject.pointLists ) );
		this.dragPoints = JSON.parse( JSON.stringify( injectorObject.dragPoints ) );
		this.width = injectorObject.width;
		this.height = injectorObject.height;
		this.canvasWidth = injectorObject.canvasWidth;
		this.canvasHeight = injectorObject.canvasHeight;
		this.scale = injectorObject.scale;
		this.sig = injectorObject.getSig();
		this.unselectable = injectorObject.unselectable;
		this.unhittable = injectorObject.unhittable;
		this.existing = injectorObject.existing;
		this.hostPageRotation = injectorObject.hostPageRotation;
		this.visible = injectorObject.visible;
	}
	else {
		throw 'Attempt to inject a non DrawObject into a DrawObject.';
	}
};

DrawObjectClass.prototype.formatPoint = function formatPoint(point, width, height) {
	var newX = point[0];
	var newY = point[1];
	var newWidth = width ? width : DrawObjectEnv.width;
	var newHeight = height ? height : DrawObjectEnv.height;
	var pointString = '{' + ( newX / newWidth ).toFixed(6) + ', ' + ( 1.0 - newY / newHeight ).toFixed(6) + '}';
	//console.log( 'annotate::formatPoint() --> pointString', pointString );
	return pointString;
};

DrawObjectClass.prototype.formatBoundPoint = function formatBoundPoint(point, width, height) {
	var newX = point[0];
	var newY = point[1];
	var newWidth = width ? width : DrawObjectEnv.width;
	var newHeight = height ? height : DrawObjectEnv.height;
	return '{' + ( newX / newWidth ).toFixed(6) + ', ' + ( newY / newHeight ).toFixed(6) + '}';
};

DrawObjectClass.prototype.formatBounds = function formatBounds( boundPoints, width, height ) {
//		console.log( 'annotate::formatBounds( {{', boundPoints[0][0], boundPoints[0][1],'}{', boundPoints[1][0], boundPoints[1][1], '}}, ', canvasObj.width, ', ', canvasObj.height, ', ', canvasObj.rotation, ')' );
	var padding = 0; //(this.lineWidth << 1);
	var boundWidth = Math.abs(boundPoints[1][0] - boundPoints[0][0]) + padding;
	var boundHeight = Math.abs(boundPoints[1][1] - boundPoints[0][1]) + padding;
	var newX = boundPoints[0][0];
	var newY = boundPoints[0][1] + Math.abs(boundPoints[1][1] - boundPoints[0][1]);
	var newWidth = width ? width : DrawObjectEnv.width;
	var newHeight = height ? height : DrawObjectEnv.height;
//		console.log( 'annotate::formatBounds() --> newX = ', newX, ' newY = ', newY, ' newWidth = ', newWidth, ' newHeight = ', newHeight );
	var pointString = '{{' + ( newX / newWidth ).toFixed(6) + ', ' + ( 1.0 - (newY / newHeight) ).toFixed(6) + '}, {' + (boundWidth / newWidth).toFixed(6) + ', ' + (boundHeight / newHeight).toFixed(6) + '}}';
//		console.log( 'annotate::formatBounds() --> pointString', pointString );
	return pointString;
};

DrawObjectClass.prototype.formatPresentationBounds = function formatPresentationBounds( boundPoints, width, height ) {
//		console.log( 'annotate::formatBounds( {{', boundPoints[0][0], boundPoints[0][1],'}{', boundPoints[1][0], boundPoints[1][1], '}}, ', canvasObj.width, ', ', canvasObj.height, ', ', canvasObj.rotation, ')' );
	var padding = (this.lineWidth / DEFAULT_DEVICE_RATIO) * 2; //(this.lineWidth << 1);
	var boundWidth = Math.abs(boundPoints[1][0] - boundPoints[0][0]) + padding;
	var boundHeight = Math.abs(boundPoints[1][1] - boundPoints[0][1]) + padding;
	var boundMinimum = 24 * DEFAULT_DEVICE_RATIO;
	var newX = boundPoints[0][0];
	if(boundMinimum > boundWidth) {
		boundWidth = boundMinimum;
		newX -= (boundMinimum - boundWidth) / 4;
	} else {
		newX -= padding / 2;
	}
	var newY = boundPoints[0][1] + Math.abs(boundPoints[1][1] - boundPoints[0][1]);
	if(boundMinimum > boundHeight) {
		boundHeight = boundMinimum;
		newY += (boundMinimum - boundHeight) / 4;
	} else {
		newY += padding / 2;
	}
	var newWidth = width ? width : DrawObjectEnv.width;
	var newHeight = height ? height : DrawObjectEnv.height;
//		console.log( 'annotate::formatBounds() --> newX = ', newX, ' newY = ', newY, ' newWidth = ', newWidth, ' newHeight = ', newHeight );
	var pointString = '{{' + ( newX / newWidth ).toFixed(6) + ', ' + ( 1.0 - (newY / newHeight) ).toFixed(6) + '}, {' + (boundWidth / newWidth).toFixed(6) + ', ' + (boundHeight / newHeight).toFixed(6) + '}}';
//		console.log( 'annotate::formatBounds() --> pointString', pointString );
	return this.outputBoundsForHost(pointString, this.hostPageRotation);
};

DrawObjectClass.prototype.drawUpdateBounds = function drawUpdateBounds(bounds) {
	var parseBounds = bounds.replace( '{{', '{' ).replace( '},', '}X').replace( '}}', '}' );
	var splitPoints = parseBounds.split( 'X' );
	// Height and Width become the scale
	var scale = this.stringToBoundPoint(splitPoints[1]); // stringToBoundPoint
	if((scale[0]===0 && (this.isResizableX() || this.isDragResizableX()))
		|| (scale[1]===0 && (this.isResizableY() || this.isDragResizableY()))) {
		// avoid situations where the scale is zero and the annotation disappears.
		return;
	}
	// X, Y point of new bounding box
	var point = this.stringToPoint(splitPoints[0]); // stringToPoint
	// Y point top side instead of bottom side
	point[1] -= scale[1];
	// Calculate X scale
	var scaleX = (this.isResizableX() || this.isDragResizableX()) ? (scale[0] / (this.boundingBox[1][0] - this.boundingBox[0][0])) : 1.0;
	// Calculate Y scale
	var scaleY = (this.isResizableY() || this.isDragResizableY()) ? (scale[1] / (this.boundingBox[1][1] - this.boundingBox[0][1])) : 1.0;
	// Calculate X position change
	var deltaX = this.boundingBox[0][0] - point[0];
	// Calculate Y position change
	var deltaY = this.boundingBox[0][1] - point[1];
	// determine the bounding box upper left value
	var newBoundingBoxPoint = [ this.boundingBox[0][0] - deltaX, this.boundingBox[0][1] - deltaY ];
	for(var i = 0; i < this.pointLists.length; i++) {
		// translate the x coordinate
		this.pointLists[i][0] -= deltaX;
		// translate the y coordinate
		this.pointLists[i][1] -= deltaY;
		// resize the x value
		this.pointLists[i][0] = newBoundingBoxPoint[0] + ( this.pointLists[i][0] - newBoundingBoxPoint[0] ) * scaleX;
		// resize the y value
		this.pointLists[i][1] = newBoundingBoxPoint[1] + ( this.pointLists[i][1] - newBoundingBoxPoint[1] ) * scaleY;
	}
	this.createBoundingBox();
	this.updateDragPoints();
};

DrawObjectClass.prototype.drawPresentationBounds = function drawPresentationBounds(bounds) {
	var parseBounds = bounds.replace( '{{', '{' ).replace( '},', '}X').replace( '}}', '}' );
	var splitPoints = parseBounds.split( 'X' );
	var coords = this.transformHostBoundPoints(splitPoints, this.hostPageRotation); // stringToBoundPoints
	// Padding value = LineWidth
	var padding = (this.lineWidth / DEFAULT_DEVICE_RATIO) * 2;
	if(this.isPoint() && this.lineWidth < 8) {
		padding = (12 / DEFAULT_DEVICE_RATIO);
	}
	// Margin value = 1/2 Padding
	var margin = padding / 2;
	// Height and Width become the scale
	//console.log("--4", "xR:", coords[0][0], "yR:", coords[0][1], "wR:", coords[1][0], "hR:", coords[1][1]);
	// Get the scale from points
	var scale = coords[1];
	if((scale[0]===0 && (this.isResizableX() || this.isDragResizableX()))
		|| (scale[1]===0 && (this.isResizableY() || this.isDragResizableY()))) {
		// avoid situations where the scale is zero and the annotation disappears.
		return;
	}
	// X, Y point of new bounding box
	var point = coords[0];
	// Remove the padding from the scale
	scale[0] -= padding;
	scale[1] -= padding;
	// Remove the margin from the point
	point[0] += margin;
	point[1] -= margin;
	// Y point top side instead of bottom side
	point[1] -= scale[1];
	// Calculate X scale
	var scaleX = (!this.isPoint()) ? (scale[0] / (this.boundingBox[1][0] - this.boundingBox[0][0])) : 1.0;
	// Calculate Y scale
	var scaleY = (!this.isPoint()) ? (scale[1] / (this.boundingBox[1][1] - this.boundingBox[0][1])) : 1.0;
	// Calculate X position change
	var deltaX = this.boundingBox[0][0] - point[0];
	// Calculate Y position change
	var deltaY = this.boundingBox[0][1] - point[1];
	// determine the bounding box upper left value
	var newBoundingBoxPoint = [ this.boundingBox[0][0] - deltaX, this.boundingBox[0][1] - deltaY ];
	for(var i = 0; i < this.pointLists.length; i++) {
		// translate the x coordinate
		this.pointLists[i][0] -= deltaX;
		// translate the y coordinate
		this.pointLists[i][1] -= deltaY;
		// resize the x value
		this.pointLists[i][0] = newBoundingBoxPoint[0] + ( this.pointLists[i][0] - newBoundingBoxPoint[0] ) * scaleX;
		// resize the y value
		this.pointLists[i][1] = newBoundingBoxPoint[1] + ( this.pointLists[i][1] - newBoundingBoxPoint[1] ) * scaleY;
	}
	this.createBoundingBox();
	this.updateDragPoints();
};

DrawObjectClass.prototype.stringToPoint = function stringToPoint(stringPoint) {
	var splitPoint = stringPoint.replace( /{/g, '' ).replace( /}/g, '' ).split( ',' );
	var point = [ parseFloat( splitPoint[0] ) * this.canvasWidth, ( 1 - parseFloat( splitPoint[1] ) ) * this.canvasHeight];
	return point;
};

DrawObjectClass.prototype.stringToBoundPoint = function stringToBoundPoint(stringPoint) {
	var padding = 0;//(this.lineWidth << 1);
	var splitPoint = stringPoint.replace( /{/g, '' ).replace( /}/g, '' ).split( ',' );
	var point = [ (parseFloat( splitPoint[0] ) * this.canvasWidth) - padding, (parseFloat( splitPoint[1] ) * this.canvasHeight) - padding];
	return point;
};

DrawObjectClass.prototype.sendInitial = function sendInitial(pageIndex) {
	var pageValue = typeof pageIndex !== "undefined" ? pageIndex : -1;
	if(this.getInitial()) {
		var drawData = this.sendDraw(pageValue);
		this.setInitial(false);
		//drawData.Operation = "add";
		return drawData;
	}
	else {
		return false;
	}
};

DrawObjectClass.prototype.sendDraw = function sendDraw(pageIndex) {
	if(this.drawType === 'callout') {
		return false;
	}

	var pageValue = typeof pageIndex !== "undefined" ? pageIndex : -1;
	var type = 3; // arrow
	if(this.drawType === 'arrow') {
		type = 3;
		var workingPoints = this.outputPointsForHost( this.hostPageRotation, this.canvasWidth, this.canvasHeight);
		return {
			"Operation": "add",
			"Type": type,
			"Page": pageValue,
			"Opacity": Math.floor( this.opacity * 100 ),
			"LineWidth": this.lineWidth,
			"Color": DrawColor.colorToInt( this.color ),
			"StartPoint": this.formatPoint( workingPoints[0], this.canvasWidth, this.canvasHeight ),
			"EndPoint": this.formatPoint( workingPoints[1], this.canvasWidth, this.canvasHeight ),
			"Index": this.getSig(),
			"isSegmented": this.isSegmented
		};
	}
	else if(this.drawType === 'marker' || this.drawType === 'highlight') {
		type = 5;
		var workingPoints = this.outputPointsForHost( this.hostPageRotation, this.canvasWidth, this.canvasHeight);
		return {
			"Operation": "add",
			"Type": type,
			"Page": pageValue,
			"Opacity": Math.floor( this.opacity * 100 ),
			"LineWidth": this.lineWidth,
			"Color": DrawColor.colorToInt( this.color ),
			"Bounds": this.formatPresentationBounds( this.pointLists, this.canvasWidth, this.canvasHeight ),
			"StartPoint": this.formatPoint( workingPoints[0], this.canvasWidth, this.canvasHeight ),
			"EndPoint": this.formatPoint( workingPoints[1], this.canvasWidth, this.canvasHeight ),
			"Index": this.getSig(),
			"isSegmented": this.isSegmented
		};
	}
	else if(this.drawType === 'pencil') {
		type = 1;
		var points = [];
		var workingPoints = this.outputPointsForHost( this.hostPageRotation, this.canvasWidth, this.canvasHeight);
		for( var i=0; i<workingPoints.length; ++i ) {
			points.push( this.formatPoint( workingPoints[i], this.canvasWidth, this.canvasHeight ) );
		}
		return {
			"Operation": "add",
			"Type": type,
			"Page": pageValue,
			"Opacity": Math.floor( this.opacity * 100 ),
			"LineWidth": this.lineWidth,
			"Color": DrawColor.colorToInt( this.color ),
			"Points": points,
			"Index": this.getSig(),
			"isSegmented": this.isSegmented
		};
	}
	else if(this.drawType === 'note') {
		type = 8;
		var workingPoints = this.outputPointsForHost( this.hostPageRotation, this.canvasWidth, this.canvasHeight);
		return {
			"Operation": "add",
			"Type": type,
			"Page": pageValue,
			"Opacity": Math.floor( this.opacity * 100 ),
			"LineWidth": this.lineWidth,
			"Color": DrawColor.colorToInt( this.color ),
			"Bounds": this.formatPoint( workingPoints[0], this.canvasWidth, this.canvasHeight ),
			"Index": this.getSig(),
			"Content": this.note,
			"isSegmented": this.isSegmented
		};
	}
	else {
		drawData = {};
	}

	return drawData;
};

DrawObjectClass.prototype.sendModify = function sendModify(pageIndex) {
	if(this.drawType === 'callout') {
		return false;
	}

	var pageValue = typeof pageIndex !== "undefined" ? pageIndex : -1;
	var result = {"Operation":"mod", "Page":pageValue, "Index":this.getSig()};

	switch(this.drawType) {
		case 'arrow':
		case 'marker':
		case 'highlight':
			var workingPoints = this.outputPointsForHost( this.hostPageRotation, this.canvasWidth, this.canvasHeight);
			result["StartPoint"] = this.formatPoint(workingPoints[0], this.canvasWidth, this.canvasHeight);
			result["EndPoint"] = this.formatPoint(workingPoints[1], this.canvasWidth, this.canvasHeight);
			result["Bounds"] = this.formatPresentationBounds(this.pointLists, this.canvasWidth, this.canvasHeight);
			break;
		case 'pencil':
			result["StartPoint"] = "{0,0}";
			result["EndPoint"] = "{0,0}";
			result["Bounds"] = this.formatPresentationBounds(this.boundingBox, this.canvasWidth, this.canvasHeight);
			this.createBoundingBox();
			break;
	}

	result["Opacity"] = this.opacity * 100;
	result["LineWidth"] = this.lineWidth;
	result["Color"] = DrawColor.colorToInt(this.color);

	result["Initial"] = false;
	if(this.getInitial()) {
		result["Initial"] = this.sendInitial(page);
	}

	return result;
};

DrawObjectClass.prototype.sendDelete = function sendDelete(pageIndex) {
	if(this.drawType === 'callout') {
		return false;
	}
	var pageValue = typeof pageIndex !== "undefined" ? pageIndex : -1;
	var Initial = false;
	if(this.getInitial()) {
		Initial = this.sendInitial(pageValue);
	}
	var result = {"Operation":"del", "Page":pageValue, "Index":this.getSig(), "Initial":Initial};
	return result;
};

DrawObjectClass.prototype.getNote = function getNote() {
	return this.note;
};

DrawObjectClass.prototype.setNote = function setNote(note) {
	this.note = note;
};

DrawObjectClass.prototype.getAuthor = function getAuthor() {
	return this.author;
};

DrawObjectClass.prototype.setAuthor = function setAuthor(author) {
	this.author = author;
};

DrawObjectClass.prototype.getDate = function getDate() {
	return this.date;
};

DrawObjectClass.prototype.setDate = function setDate(date) {
	if(typeof date === "string") {
		adate = date.replace(/(.+) (.+)/, "$1T$2Z");
		this.date = new Date(adate);
		if( !isNaN( this.date.getTime() ) ) {
			return;
		}
		this.date = new Date(date);
		if( !isNaN( this.date.getTime() ) ) {
			return;
		}
	}
	else if(Object.prototype.toString.call(date) === '[object Date]') {
		if( !isNaN( date.getTime() ) ) {
			this.date = date;
			return;
		}
	}
	this.date = new Date();
};

DrawObjectClass.prototype.getSig = function getSig() {
	if(typeof this.sig !== "string" || this.sig.length < 31) {
		this.sig = this.generateSig();
	}
	return this.sig;
};

DrawObjectClass.prototype.setSig = function setSig(sig) {
	this.sig = sig;
};

DrawObjectClass.prototype.getUnselectable = function getUnselectable() {
	return this.unselectable;
};

DrawObjectClass.prototype.setUnselectable = function setUnselectable(unselectable) {
	this.unselectable = unselectable;
};

DrawObjectClass.prototype.getUnhittable = function getUnhittable() {
	return this.unhittable;
};

DrawObjectClass.prototype.setUnhittable = function setUnhittable(unhittable) {
	this.unhittable = unhittable;
};

DrawObjectClass.prototype.refresh = function refresh() {
	this.createBoundingBox();
	this.updateDragPoints();
};

DrawObjectClass.prototype.toString = function toString() {
	return 'DrawObject';
};

DrawObjectClass.prototype.debugShape = function debugShape() {
	console.log("Page: "+this.page);
	console.log("Color: "+this.color);
	console.log("LineCount: "+this.lineCount);
	console.log("DrawType: "+this.drawType);
	console.log("BoundingBox: "+this.boundingBox.toString());
	console.log("Hit Threshold: "+this.hitThreshold.toString());
	console.log("Index: "+this.getSig());

	var i = 0;
	for(i = 0; i < this.pointLists.length; i++) {
		if (this.drawType === "pencil") {
			console.log("{ "+this.pointLists[i][0]+", "+this.pointLists[i][1]+" },");
		}
		if (this.drawType === "arrow" || this.drawType === "marker") {
			console.log("{ "+this.pointLists[i][0]+", "+this.pointLists[i][1]+" },");
		}
		if (this.drawType === "highlight") {
			console.log("{ "+this.pointLists[i][0]+", "+this.pointLists[i][1]+", "+this.pointLists[i][2]+", "+this.pointLists[i][3]+" },");
		}
	}
};

DrawObjectClass.prototype.getCategory = function getCategory() {
	return this.category;
};

DrawObjectClass.prototype.assureDotFoxitCompatible = function assureDotFoxitCompatible() {
	if(this.pointLists.length <= 4) {
		currX = this.pointLists[0][0];
		currY = this.pointLists[0][1];
		dot = true;
		for (i = 0; i < this.pointLists.length; i++) {
			if(currX !== this.pointLists[i][0] || currY !== this.pointLists[i][1]) {
				dot = false;
				break;
			}
		}
		if(dot) {
			this.pointLists[1] = [this.pointLists[0][0], this.pointLists[0][1]+1];
			this.pointLists[2] = [this.pointLists[0][0]+1, this.pointLists[0][1]];
			this.pointLists[3] = [this.pointLists[0][0]-1, this.pointLists[0][1]];
			this.pointLists[0] = [this.pointLists[0][0], this.pointLists[0][1]-1];
		}
	}
};

DrawObjectClass.prototype.getHostPageRotation = function getHostPageRotation() {
	return (typeof this.hostPageRotation === "number") ? this.hostPageRotation : 0;
};

DrawObjectClass.prototype.setTargetHostPageRotation = function setTargetHostPageRotation(targetRotation) {
	this.hostPageRotation = targetRotation;
};

DrawObjectClass.prototype.transformHostPageRotation = function transformHostPageRotation(rotation, width, height) {
	this.hostPageRotation = rotation % 4;

	if(this.hostPageRotation === 0)
		return true;

	var xIdx = 0; // X Destination Index
	var xLoc = 0; // X Source Index
	var xInv = false; // X Inverse
	var xTmp = 0; // X Temporary Storage

	var yIdx = 1; // Y Destination Index
	var yLoc = 1; // Y Source Index
	var yInv = false; // Y Inverse
	var yTmp = 0; // Y Temporary Storage

	switch(this.hostPageRotation) {
		case 0:
			break;
		case 1:
			xLoc = 1;
			yLoc = 0;
			xInv = true;
			t = height;
			height = width;
			width = t;
			break;
		case 2:
			xInv = true;
			yInv = true;
			break;
		case 3:
			xLoc = 1;
			yLoc = 0;
			yInv = true;
//			t = height;
//			height = width;
//			width = t;
			for(var i = 0; i < this.pointLists.length; i++) {
				xTmp = !!xInv ? width - this.pointLists[i][xLoc] : this.pointLists[i][xLoc];
				yTmp = !!yInv ? height - this.pointLists[i][yLoc] : this.pointLists[i][yLoc];
				this.pointLists[i][xIdx] = xTmp + (Math.abs(width-height));;
				this.pointLists[i][yIdx] = yTmp;
			}
			this.createBoundingBox();
			return;
//			xLoc = 1;
//			yLoc = 0;
//			xInv = true;
//			for(var i = 0; i < this.pointLists.length; i++) {
//				resultPoints[i] = this.pointLists[i].slice(0); // make a copy
//				resultPoints[i][xIdx] = !!xInv ? height - this.pointLists[i][xLoc] : this.pointLists[i][xLoc];
//				resultPoints[i][yIdx] = !!yInv ? width - this.pointLists[i][yLoc] : this.pointLists[i][yLoc] - (Math.abs(width-height));
//			}
			break;
	}
	for(var i = 0; i < this.pointLists.length; i++) {
		xTmp = !!xInv ? width - this.pointLists[i][xLoc] : this.pointLists[i][xLoc];
		yTmp = !!yInv ? height - this.pointLists[i][yLoc] : this.pointLists[i][yLoc];
		this.pointLists[i][xIdx] = xTmp;
		this.pointLists[i][yIdx] = yTmp;
	}
	this.createBoundingBox();
};

DrawObjectClass.prototype.transformHostBoundPoints = function transformHostBoundPoints(stringPoints, rotation) {
	var splitPoint = stringPoints[0].replace( /{/g, '' ).replace( /}/g, '' ).split( ',' );
	var splitScale = stringPoints[1].replace( /{/g, '' ).replace( /}/g, '' ).split( ',' );
	var point = [(parseFloat(splitPoint[0])), (parseFloat(splitPoint[1]))];
	var scale = [(parseFloat(splitScale[0])), (parseFloat(splitScale[1]))];
	//console.log("--1", "(x:", (point[0]*100.0)+"", "y:", (point[1]*100.0)+")", "(w:", (scale[0]*100.0)+"", "h:", (scale[1]*100.0)+")");

	rotation = rotation % 4;

	var xIdx = 0; // X Destination Index
	var xPoint = 0;
	var xScale = 0;

	var yIdx = 1; // Y Destination Index
	var yPoint = 0;
	var yScale = 0;

	var pointIndex = 0;
	var scaleIndex = 1;
	var resultPoint = [[0,0],[0,0]];

	var width = this.canvasWidth;
	var height = this.canvasHeight;

	switch(rotation) {
		case 0:
			// (x,y) -> (x,y) around the center of rotation
			// center of rotation? bottom left
			//console.log("--2", "(x:", (point[0]*100.0)+"", "y:", (point[1]*100.0)+")", "(w:", (scale[0]*100.0)+"", "h:", (scale[1]*100.0)+")");
			xScale = resultPoint[scaleIndex][xIdx] = (scale[0] * width);
			yScale = resultPoint[scaleIndex][yIdx] = (scale[1] * height);
			xPoint = resultPoint[pointIndex][xIdx] = (point[0] * width);
			yPoint = resultPoint[pointIndex][yIdx] = ((1.0-point[1]) * height);
			//console.log("--3", "(x:", xPoint+"", "y:", yPoint+")", "(w:", xScale+"", "h:", yScale+")");
			break;
		case 1:
			// (x,y) -> (y,-x) around the center of rotation (clockwise 90)
			// center of rotation? bottom left
			//console.log("--2", "(x:", (point[1]*100.0)+"", "y:", (-point[0]*100.0)+")", "(w:", (scale[1]*100.0)+"", "h:", (scale[0]*100.0)+")");
			xScale = resultPoint[scaleIndex][xIdx] = (scale[1] * height);
			yScale = resultPoint[scaleIndex][yIdx] = (scale[0] * width);
			xPoint = resultPoint[pointIndex][xIdx] = (point[1] * height);
			yPoint = resultPoint[pointIndex][yIdx] = width - ((1.0-point[0]) * width) + yScale;
			//console.log("--3", "(x:", xPoint+"", "y:", yPoint+")", "(w:", xScale+"", "h:", yScale+")");
			break;
		case 2:
			// (x,y) -> (-x,-y) around the center of rotation
			// center of rotation? bottom left
			//console.log("--2", "(x:", (-point[0]*100.0)+"", "y:", (-point[1]*100.0)+")", "(w:", (scale[0]*100.0)+"", "h:", (scale[1]*100.0)+")");
			xScale = resultPoint[scaleIndex][xIdx] = (scale[0] * width);
			yScale = resultPoint[scaleIndex][yIdx] = (scale[1] * height);
			xPoint = resultPoint[pointIndex][xIdx] = ((1.0-point[0]) * width) - xScale;
			yPoint = resultPoint[pointIndex][yIdx] = ((point[1]) * height) + yScale;
			//console.log("--3", "(x:", xPoint+"", "y:", yPoint+")", "(w:", xScale+"", "h:", yScale+")");
			break;
		case 3:
			// (x,y) -> (-y,x) around the center of rotation (counter-clockwise 90)
			// center of rotation? bottom left
			//console.log("--2", "(x:", (-point[1]*100.0)+"", "y:", (point[0]*100.0)+")", "(w:", (scale[1]*100.0)+"", "h:", (scale[0]*100.0)+")");
			xScale = resultPoint[scaleIndex][xIdx] = (scale[1] * height);
			yScale = resultPoint[scaleIndex][yIdx] = (scale[0] * width);
			xPoint = resultPoint[pointIndex][xIdx] = width - (point[1] * height) - xScale;
			yPoint = resultPoint[pointIndex][yIdx] = height - (point[0] * width);
			//console.log("--3", "(x:", xPoint+"", "y:", yPoint+")", "(w:", xScale+"", "h:", yScale+")");
			break;
	}
	return resultPoint;
};

DrawObjectClass.prototype.outputPointsForHost = function outputPointsForHost(rotation, width, height) {
	rotation = rotation % 4;

	var xIdx = 0; // X Destination Index
	var xLoc = 0; // X Source Index
	var xInv = false; // X Inverse

	var yIdx = 1; // Y Destination Index
	var yLoc = 1; // Y Source Index
	var yInv = false; // Y Inverse

	var resultPoints = [];

	switch(rotation) {
		case 0:
			break;
		case 1:
			xLoc = 1;
			yLoc = 0;
			yInv = true;
			break;
		case 2:
			xInv = true;
			yInv = true;
			break;
		case 3:
			xLoc = 1;
			yLoc = 0;
			xInv = true;
			for(var i = 0; i < this.pointLists.length; i++) {
				if(this.pointLists[i] && this.pointLists[i].constructor === Array) {
					resultPoints[i] = this.pointLists[i].slice(0); // make a copy
					resultPoints[i][xIdx] = !!xInv ? height - this.pointLists[i][xLoc] : this.pointLists[i][xLoc];
					resultPoints[i][yIdx] = !!yInv ? width - this.pointLists[i][yLoc] : this.pointLists[i][yLoc] - (Math.abs(width-height));
				}
			}
			return resultPoints;
			break;
	}
	for(var i = 0; i < this.pointLists.length; i++) {
		if(this.pointLists[i] && this.pointLists[i].constructor === Array) {
			resultPoints[i] = this.pointLists[i].slice(0); // make a copy
			resultPoints[i][xIdx] = !!xInv ? width - this.pointLists[i][xLoc] : this.pointLists[i][xLoc];
			resultPoints[i][yIdx] = !!yInv ? height - this.pointLists[i][yLoc] : this.pointLists[i][yLoc];
		}
	}
	return resultPoints;
};

DrawObjectClass.prototype.outputBoundsForHost = function outputBoundsForHost(bounds, rotation) {
	var parseBounds = bounds.replace( '{{', '{' ).replace( '},', '}X').replace( '}}', '}' );
	var splitPoints = parseBounds.split( 'X' );
	var splitPoint = splitPoints[0].replace( /{/g, '' ).replace( /}/g, '' ).split( ',' );
	var splitScale = splitPoints[1].replace( /{/g, '' ).replace( /}/g, '' ).split( ',' );
	var point = [(parseFloat(splitPoint[0])), (parseFloat(splitPoint[1]))];
	var scale = [(parseFloat(splitScale[0])), (parseFloat(splitScale[1]))];
	//console.log("--1", "(x:", (point[0]*100.0)+"", "y:", (point[1]*100.0)+")", "(w:", (scale[0]*100.0)+"", "h:", (scale[1]*100.0)+")");

	rotation = rotation % 4;

	var xIdx = 0; // X Destination Index
	var xPoint = 0;
	var xScale = 0;

	var yIdx = 1; // Y Destination Index
	var yPoint = 0;
	var yScale = 0;

	var width = this.canvasWidth;
	var height = this.canvasHeight;

	var wOverH = width/height;
	var hOverW = height/width;

	switch(rotation) {
		case 0:
			// (x,y) -> (x,y) around the center of rotation
			// center of rotation? bottom left
			//console.log("--2", "(x:", (point[0]*100.0)+"", "y:", (point[1]*100.0)+")", "(w:", (scale[0]*100.0)+"", "h:", (scale[1]*100.0)+")");
			xScale = (scale[0]);
			yScale = (scale[1]);
			xPoint = (point[0]);
			yPoint = (point[1]);
			//console.log("--3", "(x:", xPoint+"", "y:", yPoint+")", "(w:", xScale+"", "h:", yScale+")");
			break;
		case 1:
			// (x,y) -> (-y,x) around the center of rotation (counter-clockwise 90)
			// center of rotation? bottom left
			//console.log("--2", "(x:", (-point[1]*100.0)+"", "y:", (point[0]*100.0)+")", "(w:", (scale[1]*100.0)+"", "h:", (scale[0]*100.0)+")");
			xScale = (scale[1]) * hOverW;
			yScale = (scale[0]) * wOverH;
			xPoint = ((1.0 - point[1]) * hOverW) - xScale;
			yPoint = ((point[0]) * wOverH);
			//console.log("--3", "(x:", xPoint+"", "y:", yPoint+")", "(w:", xScale+"", "h:", yScale+")");
			break;
		case 2:
			// (x,y) -> (-x,-y) around the center of rotation
			// center of rotation? bottom left
			//console.log("--2", "(x:", (-point[0]*100.0)+"", "y:", (-point[1]*100.0)+")", "(w:", (scale[0]*100.0)+"", "h:", (scale[1]*100.0)+")");
			xScale = (scale[0]);
			yScale = (scale[1]);
			xPoint = (1.0-point[0]) - xScale;
			yPoint = (1.0-point[1]) - yScale;
			//console.log("--3", "(x:", xPoint+"", "y:", yPoint+")", "(w:", xScale+"", "h:", yScale+")");
			break;
		case 3:
			// (x,y) -> (y,-x) around the center of rotation (clockwise 90)
			// center of rotation? bottom left
			//console.log("--2", "(x:", (point[1]*100.0)+"", "y:", (-point[0]*100.0)+")", "(w:", (scale[1]*100.0)+"", "h:", (scale[0]*100.0)+")");
			xScale = (scale[1]) * hOverW;
			yScale = (scale[0]) * wOverH;
			xPoint = (point[1]) * hOverW;
			yPoint = ((1.0-point[0]) * wOverH) - yScale;
			//console.log("--3", "(x:", xPoint+"", "y:", yPoint+")", "(w:", xScale+"", "h:", yScale+")");
			break;
	}
	var pointString = '{{' + xPoint.toFixed(6) + ', ' + yPoint.toFixed(6) + '}, {' + xScale.toFixed(6) + ', ' + yScale.toFixed(6) + '}}';

	return pointString;
};

DrawObjectClass.prototype.setCallout = function setCallout(DrawCallout) {
	this.Callout = DrawCallout;
};

DrawObjectClass.prototype.getCallout = function getCallout() {
	return this.Callout;
};

DrawObjectClass.prototype.convertMarker2Highlight = function convertMarker2Highlight() {
	if(this.drawType === 'marker') {
		var adjustment = this.lineWidth * DEFAULT_DEVICE_RATIO;
		this.drawType = 'highlight';
		this.category = 'rect';
		var x1 = Math.min(this.pointLists[0][0], this.pointLists[1][0]);
		var y1 = Math.min(this.pointLists[0][1], this.pointLists[1][1]);
		var x2 = Math.max(this.pointLists[0][0], this.pointLists[1][0]);
		var y2 = Math.max(this.pointLists[0][1], this.pointLists[1][1]);
		this.pointLists[0][0] = x1;
		this.pointLists[0][1] = y1;
		this.pointLists[1][0] = x2;
		this.pointLists[1][1] = y2;
		if(Math.abs(this.pointLists[1][0] - this.pointLists[0][0]) < this.lineWidth) {
			this.pointLists[0][0] -= adjustment;
			this.pointLists[1][0] += adjustment;
		}
		if(Math.abs(this.pointLists[1][1] - this.pointLists[0][1]) < this.lineWidth) {
			this.pointLists[0][1] -= adjustment;
			this.pointLists[1][1] += adjustment;
		}
		this.createRectBBox();
		this.updateDragPoints();
	}
};
