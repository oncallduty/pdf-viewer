/**
 * Drawing color manipulation object
 * @type type
 */
var DrawColor = {
	color2String : function(color, opacity, backupColor) {
		var rgb_value = DrawColor.splitColor( color );
		var rgb_backup = {r:0,g:0,b:0};
		if(backupColor != undefined) {
			rgb_backup = DrawColor.splitColor( backupColor );
		}
		if(!angular.isDefined(opacity))
			opacity = 1.0;
		var resultString = null;
		if (rgb_value !== null) {
			resultString = 'rgba(' + rgb_value.r + ',' + rgb_value.g + ',' + rgb_value.b + ',' + opacity + ')';
		} else {
			resultString = 'rgba(' + rgb_backup.r + ',' + rgb_backup.g + ',' + rgb_backup.b + ',' + opacity + ')';
		}
		return resultString;
	},

	splitColor : function(color) {
		if (typeof color === "string") {
			if (color[0] === "#") {
				return DrawColor.hexToRgb(color);
			} else if (color[0] === "r") {
				return DrawColor.splitRgb(color);
			}
		}
		return null;
	},

	hexToRgb : function(hex) {
		var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
		return result ? {
			r: parseInt(result[1], 16),
			g: parseInt(result[2], 16),
			b: parseInt(result[3], 16)
		} : null;
	},

	rgbToHex : function (r, g, b) {
		return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
	},

	rgbaToHex : function (r, g, b, a) {
		return "#" + (a * 255).toString(16) + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
	},

	intToHex : function ( i ) {
		var hex = i.toString( 16 );
		while (hex.length < 6) hex = "0" + hex;
		if (hex.length === 8) {
			// Remove alpha at the front.
			hex = hex.substring(2);
		}
		return '#' + hex;
	},

	hexToInt : function( h ) {
		if (h[0] === '#') {
			h = h.substring( 1 );
		}
		return parseInt( h, 16 );
	},

	colorToInt : function( c ) {
		// This will convert any color to an Int value.
		if (c[0] === '#') {
			return DrawColor.hexToInt( c );
		}
		var splitColor;
		if (c.substring(0, "rgba".length) == "rgba") {
			splitColor = DrawColor.splitRgb( c );
			return DrawColor.hexToInt( DrawColor.rgbToHex( splitColor.r, splitColor.g, splitColor.b ) );
		}
		if (c.substring(0, "rgb".length) == "rgb") {
			splitColor = DrawColor.splitRgb( c );
			return DrawColor.hexToInt( DrawColor.rgbToHex( splitColor.r, splitColor.g, splitColor.b ) );
		}
		return null;
	},

	splitRgb : function(color) {
        var colors;
		if (color.substring(0, "rgba".length) == "rgba") {
			colors = color.substring(5, color.length - 1 ).split(",");
			return {
				r: parseInt(colors[0].trim(), 10),
				g: parseInt(colors[1].trim(), 10),
				b: parseInt(colors[2].trim(), 10),
				a: parseFloat(colors[3].trim())
				};
		}
        colors = color.substring(4, color.length - 1 ).split(",");
        return {
            r: parseInt(colors[0].trim(), 10),
            g: parseInt(colors[1].trim(), 10),
            b: parseInt(colors[2].trim(), 10)
            };
	},

	cssRgbToHex : function(rgb){
		rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
		return (rgb && rgb.length === 4) ? "#" +
			("0" + parseInt(rgb[1],10).toString(16)).slice(-2) +
			("0" + parseInt(rgb[2],10).toString(16)).slice(-2) +
			("0" + parseInt(rgb[3],10).toString(16)).slice(-2) : '';
	},

	colorToPastel : function(color, opacity) {
		var rgb_value = DrawColor.splitColor( color );
		if(!angular.isDefined(opacity))
			opacity = 1.0;
		var resultString = null;
		rgb_value.r = Math.min(255,Math.ceil((255-rgb_value.r)*0.9)+rgb_value.r);
		rgb_value.g = Math.min(255,Math.ceil((255-rgb_value.g)*0.9)+rgb_value.g);
		rgb_value.b = Math.min(255,Math.ceil((255-rgb_value.b)*0.9)+rgb_value.b);
		if (rgb_value !== null) {
			resultString = 'rgba(' + rgb_value.r + ',' + rgb_value.g + ',' + rgb_value.b + ',' + opacity + ')';
		}
		return resultString;
	}
};
