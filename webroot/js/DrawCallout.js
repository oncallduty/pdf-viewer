/**********************************************
 * Copyright by eDepoze 2016
 * All rights reserved.
 */

/**
 * Function Factory DrawCalloutFactory
 * @param {Object} params
 * @returns {DrawCalloutClass}
 */
var DrawCalloutFactory = function DrawCalloutFactory( params, normCallout ) {
	// console.log( "===", "DrawCalloutFactory; params:", params );
	this.last = new DrawCalloutClass( params, normCallout );
	return this.last;
};

/****************************************************************
 * Function Class DrawCalloutCanvasFactory
 * @param {Array} params
 * @returns {DrawCalloutCanvas}
 */
var DrawCalloutCanvasFactory = function DrawCalloutCanvasFactory(params) {
	//console.log("===CO","DrawCalloutCanvasFactory()");
	var self = {};
	self._canvas = document.getElementById( "callout_canvas_" + params.Index );
	self.drawCanvasObj = {
		"context": null,
		"htmlObj": self._canvas,
		"isPrimary": false,
		"scale": 2.0,
		"offsetX": 0,
		"offsetY": 0,
		"orientation": 0
	};
	self._id = "callout_element_" + params.Index;
	return self;
};

/****************************************************************
 * Function Class DrawCalloutCoord
 * @param {Array} params
 * @returns {DrawCalloutCoord}
 */
var DrawCalloutCoordFactory = function DrawCalloutCoordFactory(params) {
	//console.log("===CO","DrawCalloutCoordFactory()");
	var self = {};
	// pixels
	self.x1 = 0;
	self.y1 = 0;
	self.width = 0;
	self.height = 0;
	self.pageWidth = 0;
	self.pageHeight = 0;
	self.pdfWidth = 0;
	self.pdfHeight = 0;
	self.totalWidth = 0;
	self.totalHeight = 0;
	// initial
	self.Initial = {};
	return self;
};

/****************************************************************
 * Function Class DrawCalloutState
 * @param {Array} params
 * @returns {DrawCalloutState}
 */
var DrawCalloutStateFactory = function DrawCalloutStateFactory( params ) {
	// console.log( "===", "DrawCalloutStateFactory(); params:", params );
	var self = {};
	self.pageIndex = typeof params.page !== "undefined" ? params.page : params.PDFViewer.Page.index;
	self.visible = typeof params.visible !== "undefined" ? params.visible : true;
	self.zIndex = typeof params.zIndex !== "undefined" ? params.zIndex : params.PDFViewer.calloutZIndex++;
	self.zTop = typeof params.zTop !== "undefined" ? params.zTop : false;
	self.scale = typeof params.scale !== "undefined" ? params.scale : 1.0;
	self.baseScale = typeof params.baseScale !== "undefined" ? params.baseScale : 2.0;
	self.orientation = typeof params.pageOrientation !== "undefined" ? params.pageOrientation : 0;
	self.drawable = typeof params.drawable !== "undefined" ? params.drawable : true;
	self.resizable = typeof params.resizable !== "undefined" ? params.resizable : true;
	self.moveable = typeof params.moveable !== "undefined" ? params.moveable : true;
	self.closeable = typeof params.closeable !== "undefined" ? params.closeable : true;
	self.destroyed = typeof params.destroyed !== "undefined" ? params.destroyed : false;
	self.origin = typeof params.origin !== "undefined" ? params.origin : "draw"; // draw, disk
	//self.dirty = typeof params.dirty !== "undefined" ? params.dirty : true;
	self.draw = {
		"drawready": false,
		"drawInProgress": false
	};
	self.resize = {
		"mode": "",
		"centerX": 0,
		"centerY": 0,
		"transformX": 0,
		"transformY": 0,
		"deltaX": 0, // used
		"deltaY": 0, // used
		"originX": 0, // used
		"originY": 0, // used
		"lastX": 0, // used
		"lastY": 0, // used
		"variance": 0, // used
		"varianceX": 0, // used
		"varianceY": 0, // used
		"radians": 0.0, // used
		"degrees": 0.0, // used
		"reversed": false,
		"isResizing": false // used
	};
	self.saved = {};
	return self;
};

/****************************************************************
 * Function Class DrawCalloutClass
 * @param {type} params
 * @returns {DrawCalloutClass}
 */
function DrawCalloutClass( params, nCallout ) {
	// console.log( "--", "DrawCalloutClass()", Object.assign( {}, params ), Object.assign( {}, nCallout ) );
	if( !isset( params ) ) {
		params = {};
	}
	var self = this;
	var PDFViewer = params.PDFViewer;
	var DrawObject = params.DrawObject;
	params.Index = PDFViewer.getValidCalloutIndex( params.Index );//calloutNextIndex++;
	this.Index = params.Index;
	this.State = DrawCalloutStateFactory( params );
	this.Focus = DrawCalloutCoordFactory( params ); // what are the coordinates on the page that this callout is focused on
	this.View = DrawCalloutCoordFactory( params ); // what are the view coordinates we are displaying our canvas at
	this.Canvas = DrawCalloutCanvasFactory( params );
	this.Style = {
		"element": {
			"position": "absolute",
			"display": "block",
			"margin": 0,
			"padding": 0
		},
		"canvas": {},
		"resize": {
			"position": "absolute",
			"width": 50,
			"height": 50,
			"left": -50,
			"top": "50%",
			"margin": "-25px 0 0",
			"cursor": "pointer",
			"font-size": "30px",
			"line-height": "49px",
			"text-align": "center",
			"vertical-align": "middle"},
		"resizeImg": {
			"display": "block",
			"width": 50,
			"height": 50
		},
		"close": {
			"position": "absolute",
			"width": 32,
			"height": 32,
			"right": 0,
			"top": 0,
			"margin": "-18px -14.5px 0 0",
			"cursor": "pointer",
			"font-size": "30px",
			"line-height": "30px",
			"text-align": "right"
		},
		"closeImg": {}
	};
	this.Event = PDFViewer.Canvas.EventFactory();
	this.Event.canvas = this.Event.MouseFactory();
	this.Event.resize = this.Event.MouseFactory();
	this.setupEventHandlers();
	this.setDrawObject( DrawObject );
	DrawObject.visible = false;
	this.setPdfViewer( PDFViewer );
	if( isset( nCallout ) ) {
		this.nCallout = nCallout;
		this.dCallout = this.denormalizeCallout( nCallout );
		this.DrawObject.baseScale = this.dCallout.baseScale;
	}
	this.applyToView( params.viewX, params.viewY, params.viewW, params.viewH, params.viewUrl, params.pageOrientation );
	this.setDirty( typeof params.dirty !== "undefined" ? !!params.dirty : false );
};

DrawCalloutClass.prototype.toString = function toString() {
	//console.log( "--", "DrawCalloutClass.toString();" );
	return "DrawCallout";
};

DrawCalloutClass.prototype.getCanvasObj = function getCanvasObj() {
	var pwScale = this.getPageWidthScale();
	this.Canvas.drawCanvasObj.scale = ((this.State.scale * this.dCallout.baseScale) * pwScale);
	this.Canvas._canvas = document.getElementById( "callout_canvas_" + this.Index );
	if( !isset( this.Canvas._canvas ) ) {
		return null;
	}
	this.Canvas.drawCanvasObj.context = this.Canvas._canvas.getContext( "2d" );
	if( isset( this.Canvas.drawCanvasObj.context ) ) {
		return this.Canvas.drawCanvasObj;
	} else {
		return null;
	}
};

DrawCalloutClass.prototype.elevate = function elevate( isTop ) {
	//console.log( "--", "DrawCallout::elevate()", isTop );
	if(this.State.zTop !== !!isTop && !!isTop) {
		this.State.zIndex += 300; // top position
	} else if(this.State.zTop !== !!isTop && !isTop) {
		this.State.zIndex -= 300; // normal position
	}
 	this.Style.element.zIndex = this.State.zIndex;
	this.State.zTop = !!isTop;
};

DrawCalloutClass.prototype.toggle = function toggle() {
	//console.log("===CO","DrawCallout::toggle()");
	if(this.isVisible()) {
		this.hide();
	} else {
		this.show();
	}
};

DrawCalloutClass.prototype.isVisible = function isVisible() {
	//console.log("===CO","DrawCallout::isVisible()");
	return this.State.visible;
};

DrawCalloutClass.prototype.hide = function hide() {
	//console.log("===CO","DrawCallout::hide()");
	this.State.visible = false;
	this.DrawObject.visible = false;
};

DrawCalloutClass.prototype.show = function show() {
	//console.log("===CO","DrawCallout::show()");
	this.State.visible = true;
	this.DrawObject.visible = true;
};

DrawCalloutClass.prototype.setDrawObject = function setDrawObject(DrawObject) {
	//console.log("===CO","DrawCallout::setDrawObject()", DrawObject);
	if(typeof DrawObject !== "undefined" && DrawObject !== null && DrawObject.toString() === "DrawObject") {
		DrawObject.setCallout(this);
		this.DrawObject = DrawObject;
		this.Focus.width = DrawObject.width;
		this.Focus.height = DrawObject.height;
	}
};

DrawCalloutClass.prototype.setPdfViewer = function setPdfViewer(PDFViewer) {
	//console.log("===CO","DrawCallout::setPDFViewer()", PDFViewer);
	this.PDFViewer = PDFViewer;
	this.State.zoomScale = this.PDFViewer.Page.getZoomScale();
};

DrawCalloutClass.prototype.applyToView = function applyToView( x, y, w, h, url, orientation ) {
	// console.log( "applyToView; x:", x, "y:", y, "w:", w, "h:", h, "url:", url, "orientation:", orientation );
	var bounds = this.DrawObject.getBoundingBox();
	var pwScale = this.getPageWidthScale();
	var baseScale = (isset( this.dCallout ) ? this.dCallout.baseScale : this.State.baseScale);
	var scale = (this.State.scale * baseScale);
	var viewScale = (scale * pwScale);
	// console.log( "DrawCallout.applyToView; scale:", scale, "viewScale:", viewScale, "this.State.scale:", this.State.scale, "baseScale:", baseScale, "page-width-scale:", pwScale );

	var pageSize = this.getPageSize();
	var pageScale = this.getPageScale();
	var canvasOffset = this.PDFViewer.Canvas.getCanvasOffset();

	this.Focus.x1 = (bounds[ 0 ][ 0 ]);
	this.Focus.y1 = (bounds[ 0 ][ 1 ]);
	this.Focus.width = Math.round( this.DrawObject.width );
	this.Focus.height = Math.round( this.DrawObject.height );
	this.Focus.pageWidth = pageSize.width;
	this.Focus.pageHeight = pageSize.height;
	this.Focus.pdfWidth = Math.round( this.Focus.pageWidth / 1.5 );
	this.Focus.pdfHeight = Math.round( this.Focus.pageHeight / 1.5 );
	this.Focus.totalWidth = this.Focus.pageWidth;
	this.Focus.totalHeight = this.Focus.pageHeight;
	this.saveInitialCoord( this.Focus );

	this.View.width = Math.round( w );
	this.View.height = Math.round( h );
	this.View.x1 = Math.round( this.dCallout.Bounds.left + canvasOffset.left );
	this.View.y1 = Math.round( this.dCallout.Bounds.top + canvasOffset.top );
	this.View.pageWidth = this.Focus.pageWidth;
	this.View.pageHeight = this.Focus.pageHeight;
	this.View.pdfWidth = this.Focus.pdfWidth;
	this.View.pdfHeight = this.Focus.pdfHeight;
	this.View.totalWidth = Math.round( pageScale.width * baseScale );
	this.View.totalHeight = Math.round( pageScale.height * baseScale );
	this.saveInitialCoord( this.View );

	this.Canvas.drawCanvasObj.offsetX = -(bounds[ 0 ][ 0 ]); // tell the draw objects what offsetX they are for this canvas
	this.Canvas.drawCanvasObj.offsetY = -(bounds[ 0 ][ 1 ]); // tell the draw objects what offsetY they are for this canvas
	this.View.Initial.width = this.View.width;
	this.View.Initial.height = this.View.height;

	this.Style.canvas.position = "absolute";
	this.Style.canvas.width = this.View.width;
	this.Style.canvas.height = this.View.height;
	this.Style.canvas.backgroundImage = "url('" + url + "')";
	this.Style.canvas.backgroundRepeat = "no-repeat";
	this.Style.canvas.backgroundSize = ("" + Math.ceil( this.View.totalWidth ) + "px " + Math.ceil( this.View.totalHeight ) + "px");
	this.Style.canvas.backgroundPosition = ("-" + Math.floor( this.Focus.x1 * viewScale ) + "px -" + Math.floor( this.Focus.y1 * viewScale ) + "px");
	this.Style.canvas.border = "1px solid #000";
	this.Style.canvas.cursor = "pointer";
	this.Style.canvas.margin = 0;
	this.Style.canvas.padding = 0;
	this.Style.canvas.left = 0;
	this.Style.canvas.top = 0;
	this.Style.canvas.transform = "rotate(" + (orientation * 90) + "deg)";
	switch( orientation ) {
		case 1:
		case 3:
			this.Style.canvas.height = this.View.width;
			this.Style.canvas.width = this.View.height;
			this.Style.canvas.transform = "translate" + (orientation === 3 ? "X" : "Y") + "(-100%) " + this.Style.canvas.transform;
			this.Style.canvas.transformOrigin = (orientation === 3 ? "right top" : "left bottom") + " 0px";
			break;
		case 2:
			this.Style.canvas.transformOrigin = "50% 50%";
			break;
		case 0:
		default:
			break;
	}
	this.Style.element.width = this.View.width;
	this.Style.element.height = this.View.height;
	this.Style.element.left = this.View.x1;
	this.Style.element.top = this.View.y1;
};

DrawCalloutClass.prototype.clearCanvas = function clearCanvas() {
	var scale = this.State.scale * this.State.baseScale * this.State.zoomScale;
	var width = this.View.pageWidth*scale;
	var height = this.View.pageHeight*scale;
	if(isset(this.getCanvasObj())) {
		this.getCanvasObj().context.clearRect(0,0,width,height);
	}
};

DrawCalloutClass.prototype.close = function close() {
	this.hide();
	this.State.destroyed = true;
	this.PDFViewer.Canvas.refresh();
	this.PDFViewer.safeApply();
	this.sendDelete();
	this.PDFViewer.triggerViewerAPISavable(this.PDFViewer.annotations.hasChanges());
};

DrawCalloutClass.prototype.setPosition = function setPosition( x, y ) {
	this.View.x1 = x;
	this.View.y1 = y;
	this.Style.element.left = x;
	this.Style.element.top = y;
	// console.log( "setPosition; this.Style.element:", Object.assign( {}, this.Style.element ) );
};

DrawCalloutClass.prototype.moveDown = function moveDown(event) {
	if(!this.State.draw.drawready && this.State.moveable) {
		//console.log("===CO", "-- DrawCallout::moveDown()");
		var clientX = this.getEventX(event, true);
		var clientY = this.getEventY(event, true);
		this.State.saved.clientX = clientX;
		this.State.saved.clientY = clientY;
		//console.log("===CO", "-- DrawCallout::moveDown()", "x:", clientX, "y:", clientY);
		this.State.dragStarted = true;
		this.State.validDrag = false;
		this.State.dragStartX = this.State.dragLatestX = clientX;
		this.State.dragStartY = this.State.dragLatestY = clientY;
		this.State.dragOffsetX = clientX - this.View.x1;
		this.State.dragOffsetY = clientY - this.View.y1;
		this.State.saved.closeable = this.State.closeable;
		this.State.saved.resizable = this.State.resizable;
		this.PDFViewer.calloutContainerCss({'left':0,'top':0,'width':'100%','height':'100%','zIndex':499});
		this.elevate(true);
	}
};

DrawCalloutClass.prototype.moveMove = function moveMove(event) {
	if(!this.State.draw.drawready && this.State.moveable && this.State.dragStarted) {
		//console.log("===CO", "-- DrawCallout::moveMove()");
		event.preventDefault();
		var clientX = this.getEventX(event, true);
		var clientY = this.getEventY(event, true);
		if(clientX===0 && clientY===0) {
			clientX = this.State.saved.clientX;
			clientY = this.State.saved.clientY;
		} else {
			this.State.saved.clientX = clientX;
			this.State.saved.clientY = clientY;
		}
		//console.log("===CO", "-- DrawCallout::moveMove()", "x:", clientX, "y:", clientY);
		this.State.dragLatestX = clientX;
		this.State.dragLatestY = clientY;
		if(!this.State.validDrag && ((Math.abs(this.State.dragStartX-this.State.dragLatestX) >= 5) || (Math.abs(this.State.dragStartY-this.State.dragLatestY) >= 5))) {
			this.State.validDrag = true;
			this.State.closeable = false;
			this.State.resizable = false;
		}
		this.State.prevDragX = this.State.dragX;
		this.State.prevDragY = this.State.dragY;
		if(isset(clientX) && isset(clientY)) {
			this.State.dragX = Math.min(Math.max(clientX - this.State.dragOffsetX,-(this.View.width-80)),(this.getViewerWidth()+this.getViewerOffsetX())-80);
			this.State.dragY = Math.min(Math.max(clientY - this.State.dragOffsetY,-(this.View.height-80)),(this.getViewerHeight()+this.getViewerOffsetY())-80);
		}
		this.setPosition(this.State.dragX, this.State.dragY);
	}
};

DrawCalloutClass.prototype.moveUp = function moveUp(event) {
	if(!this.State.draw.drawready && this.State.moveable) {
		//console.log("-- DrawCallout::dragend()", event.originalEvent.clientX, event.originalEvent.clientY, event.originalEvent.offsetX, event.originalEvent.offsetY);
		event.preventDefault();
		this.State.dragStarted = false;
		this.State.closeable = this.State.saved.closeable;
		this.State.resizable = this.State.saved.resizable;
		this.PDFViewer.calloutContainerCss({'left':0,'top':0,'width':0,'height':0,'zIndex':''});
		this.elevate(false);
		if(this.State.validDrag) {
			//console.log("===CO", "-- DrawCallout::moveUp()", "x:", this.State.dragX, "y:", this.State.dragY);
			this.View.x1 = this.State.dragX;
			this.View.y1 = this.State.dragY;
			this.setDirty(true);
			if(this.View.x1 < this.getViewerWidth()+this.getViewerOffsetX()
					&& this.View.y1 < this.getViewerHeight()+this.getViewerOffsetY()) {
				this.setPosition(this.View.x1, this.View.y1);
				this.sendModify();
			}
			else {
				this.close();
			}
		}
	}
};

DrawCalloutClass.prototype.moveClick = function moveClick(event) {
	if(!this.State.draw.drawready && this.State.moveable) {
		event.preventDefault();
		this.State.closeable = this.State.saved.closeable;
		this.State.resizable = this.State.saved.resizable;
		this.PDFViewer.calloutContainerCss({'left':0,'top':0,'width':0,'height':0,'zIndex':''});
		if(!this.State.validDrag) {
			//console.log("===CO", "-- DrawCallout::moveClick()", "Index:", this.Index);
			if(isset(this.State.lastClickTimeout) && Date.now() < this.State.lastClickTimeout) {
				// double click - doing it this way for regular and touch support
				this.moveDblClick(event);
				this.sendModify();
			}
			else {
				this.PDFViewer.annotations.callouts.lift(this);
				this.clickPassAlong(event);
				this.State.lastClickTimeout = Date.now()+300;
				this.PDFViewer.sendCalloutsZIndexes();
			}
		}
	}
};

DrawCalloutClass.prototype.moveDblClick = function moveDblClick(event) {
//  ED-3750 - remove the resizing of callouts
};

DrawCalloutClass.prototype.isOriginalView = function resetViewSize() {
	var initialWidth = this.View.Initial.width;
	var isOrientationDifferent = (Math.abs(this.View.Initial.orientation - this.State.orientation) % 2 === 1);
	if(isOrientationDifferent) {
		initialWidth = this.View.Initial.height; // doesn't rotate, so need to account for rotation
	}
	var result = false;
	if(Math.abs(this.View.width - initialWidth) < 2) {
		result = true;
	}
	return result;
};

DrawCalloutClass.prototype.applyOriginalView = function applyOriginalView() {
	this.resize(1/this.State.scale);
};

DrawCalloutClass.prototype.applyDoubleView = function applyDoubleView() {
	this.resize(2/this.State.scale);
};

DrawCalloutClass.prototype.getEventX = function getEventX( event, includeScrollOffset ) {
	var docScrollX = 0;//includeScrollOffset ? $(document).scrollLeft() : 0;
	if( isset( event ) && isset( event.originalEvent ) && isset( event.originalEvent.touches ) && isset( event.originalEvent.touches[ 0 ] ) ) {
		return event.originalEvent.touches[0].clientX + docScrollX;
	} else if( event.vicarious && event.preClientX ) {
		return event.preClientX + docScrollX;
	} else if( event.clientX ) {
		return event.clientX + docScrollX;
	}
	return this.Event.canvas.latestX + docScrollX;
};

DrawCalloutClass.prototype.getEventY = function getEventY( event, includeScrollOffset ) {
	var docScrollY = 0;//includeScrollOffset ? $(document).scrollTop() : 0;
	if( isset( event ) && isset( event.originalEvent ) && isset( event.originalEvent.touches ) && isset( event.originalEvent.touches[ 0 ] ) ) {
		return event.originalEvent.touches[0].clientY + docScrollY;
	} else if( event.vicarious && event.preClientY ) {
		return event.preClientY + docScrollY;
	} else if( event.clientX ) {
		return event.clientY + docScrollY;
	}
	return this.Event.canvas.latestY + docScrollY;
};

DrawCalloutClass.prototype.resizeDown = function resizeDown(event) {
	if(this.State.resizable) {
		event.preventDefault();
		// handle the resize
		console.log("-- DrawCallout.resizeDown", this.View);
		var x = this.getEventX(event, true), y = this.getEventY(event, true);
		//var docScrollX = $(document).scrollLeft();
		//var docScrollY = $(document).scrollTop();

		this.resetResizeState();
		this.State.resize.lastX = this.State.resize.originX = x;
		this.State.resize.lastY = this.State.resize.originY = y;
		this.State.saved.closeable = this.State.closeable;
		this.State.closeable = false;
		//this.Canvas._jResizeImg.css({"position":"fixed","left":(x-15)-docScrollX,"top":(y-15)-docScrollY});
		this.Style.resizeImg.position = 'fixed';
		this.Style.resizeImg.left = (x-25);//-docScrollX;
		this.Style.resizeImg.top = (y-25);//-docScrollY;
	}
};

DrawCalloutClass.prototype.resizeMove = function resizeMove(event) {
	if(this.State.resizable) {
		event.preventDefault();
		if(this.State.resize.isResizing) {
			var x = this.getEventX(event, true), y = this.getEventY(event, true);
			this.updateMouseEvent(x, y);
			//var docScrollX = $(document).scrollLeft();
			//var docScrollY = $(document).scrollTop();
			//this.Canvas._jResizeImg.css({"left":(x-15)-docScrollX,"top":(y-15)-docScrollY});
			this.Style.resizeImg.left = (x-25);//-docScrollX;
			this.Style.resizeImg.top = (y-25);//-docScrollY;
			if(this.State.resize.mode === 'rotate/resize') {
				if(this.State.resize.varianceY > 7) {
					this.State.resize.mode = 'rotate';
				}
				if(this.State.resize.varianceX > 7) {
					this.State.resize.mode = 'resize';
				}
			}
			if(this.State.resize.mode === 'rotate') {
				//this.Canvas._jCanvas.css({'transform':'rotate('+this.State.resize.degrees+'deg)'});
				this.Style.canvas.transform = 'rotate('+this.State.resize.degrees+'deg)';
			}
			else if(this.State.resize.mode === 'resize') {
				var origWidth = this.View.width;
				var proportion = 1.0 + ((this.State.resize.deltaX*2) / origWidth);
				this.resize(proportion);
			}
		}
	}
};

DrawCalloutClass.prototype.resize = function resize( proportion ) {
	// console.log( "resize; proportion:", proportion );
	var origWidth = this.View.width;
	var origHeight = this.View.height;
	this.applyProportionToView( proportion );
	var pageSize = this.getPageSize();
	var pwScale = this.getPageWidthScale();
	var canvasOffset = this.PDFViewer.Canvas.getCanvasOffset();
	this.View.x1 = Math.floor( this.dCallout.Bounds.left + canvasOffset.left );
	this.View.y1 = Math.floor( this.dCallout.Bounds.top + canvasOffset.top );
	var viewScale = (this.State.scale * this.dCallout.baseScale * pwScale);
	// console.log( "resize; viewScale:", viewScale, "this.State.scale:", this.State.scale, "baseScale:", this.dCallout.baseScale, "pwScale:", pwScale );
	this.Style.canvas.backgroundSize = Math.round( pageSize.width * viewScale ) + "px " + Math.round( pageSize.height * viewScale ) + "px";
	this.Style.canvas.backgroundPosition = ("-" + Math.floor( this.Focus.x1 * viewScale ) + "px -" + Math.floor( this.Focus.y1 * viewScale ) + "px");
	// console.log( "resize; this.Style.canvas.backgroundSize:", this.Style.canvas.backgroundSize, "this.Style.canvas.backgroundPosition:", this.Style.canvas.backgroundPosition, "State.orientation:", this.State.orientation );
	this.Style.canvas.width = this.View.width;
	this.Style.canvas.height = this.View.height;
	this.Style.canvas.left = 0;
	this.Style.canvas.top = 0;
	switch( this.State.orientation ) {
		case 1:
			this.Style.canvas.width = this.View.height;
			this.Style.canvas.height = this.View.width;
			this.Style.canvas.left = -(this.View.width + 10);
			this.Style.canvas.top = this.View.height;
			break;
		case 3:
			this.Style.canvas.width = this.View.height;
			this.Style.canvas.height = this.View.width;
			this.Style.canvas.left = this.View.width;
			this.Style.canvas.top = (this.View.height - this.View.width);
			break;
		case 2:
			this.Style.canvas.left = -2;
			break;
		case 0:
		default:
			break;
	}
	this.Style.element.width = this.View.width;
	this.Style.element.height = this.View.height;
	this.Style.element.left = this.View.x1;
	this.Style.element.top = this.View.y1;
};

DrawCalloutClass.prototype.resizeUp = function resizeUp(event) {
	if(this.State.resizable) {
		event.preventDefault();
		this.State.closeable = this.State.saved.closeable;
		if(this.State.resize.isResizing) {
			//this.Canvas._jResizeImg.css({"position":"relative","left":0,"top":0});
			this.Style.resizeImg.position = "relative";
			this.Style.resizeImg.left = 0;
			this.Style.resizeImg.top = 0;
			//console.log("-- DrawCallout.resizeUp", event);
			if(this.State.resize.mode === 'rotate') {
				// snap into place
				var normalized = this.resizeNormalizedAngle(this.State.resize.radians);
				this.setOrientation(normalized);
				this.sendModify();
				this.setDirty(true);
			}
			else if(this.State.resize.mode === 'resize') {
				this.setOrientation(this.State.orientation);
				this.sendModify();
				this.setDirty(true);
			}
			this.State.resize.isResizing = false;
			//this.Canvas._jCloseBtn.show();
			this.PDFViewer.Canvas.refresh();
		}
	}
};

DrawCalloutClass.prototype.resizeClick = function resizeClick(event) {
	if(this.State.resizable) {
		event.preventDefault();
	};
};

DrawCalloutClass.prototype.rotateAnimateCanvas = function rotateAnimateCanvas(degrees) {
	//this.Canvas._jCanvas.css({'transform':'rotate('+(degrees)+'deg)'});
	this.Style.canvas.transform = 'rotate('+(degrees)+'deg)';
};

DrawCalloutClass.prototype.updateMouseEvent = function updateMouseEvent(x, y) {
	var varianceX = Math.abs(this.State.resize.originX - x);
	var varianceY = Math.abs(this.State.resize.originY - y);
	this.State.resize.variance = Math.max(varianceX, varianceY, this.State.resize.variance);
	this.State.resize.varianceX = Math.max(varianceX, this.State.resize.varianceX);
	this.State.resize.varianceY = Math.max(varianceY, this.State.resize.varianceY);
	this.State.resize.radians = Math.atan2(this.State.resize.centerY - y, this.State.resize.centerX - x) + (this.State.orientation * M_PI_2);
	this.State.resize.degrees = this.State.resize.radians * (180.0/Math.PI);
	this.State.resize.deltaX = this.State.resize.lastX - x;
	this.State.resize.deltaY = this.State.resize.lastY - y;
	this.State.resize.lastX = x;
	this.State.resize.lastY = y;
};

DrawCalloutClass.prototype.applyProportionToView = function applyProportionToView( proportion ) {
	// console.log( "applyProportionToView; proportion:", proportion, this.View.width, this.View.height );
	var minimumWidth = this.View.Initial.width;
	var viewWidth = this.View.width;
	var viewHeight = this.View.height;
	var isOrientationDifferent = ((Math.abs( this.View.Initial.orientation - this.State.orientation ) % 2) === 1);
	if( isOrientationDifferent ) {
		minimumWidth = this.View.Initial.height; // doesn't rotate, so need to account for rotation
	}
	if( minimumWidth >= Math.floor( viewWidth * proportion ) ) {
		if( isOrientationDifferent ) {
			this.View.width = this.View.Initial.height;
			this.View.height = this.View.Initial.width;
		} else {
			this.View.width = this.View.Initial.width;
			this.View.height = this.View.Initial.height;
		}
		this.View.totalWidth = this.View.Initial.totalWidth;
		this.View.totalHeight = this.View.Initial.totalHeight;
	} else {
		if( this.getViewerWidth() < viewWidth * proportion || this.getViewerHeight() < viewHeight * proportion ) {
			var xDiff = (viewWidth * proportion) - this.getViewerWidth();
			var yDiff = (viewHeight * proportion) - this.getViewerHeight();
			if( xDiff > yDiff ) {
				// x boundry
				proportion = this.getViewerWidth() / this.View.width;
			} else {
				// y boundry
				proportion = this.getViewerHeight() / this.View.height;
			}
		}
		this.View.width *= proportion; // TODO: Math.round()
		this.View.height *= proportion;
		this.View.totalWidth *= proportion;
		this.View.totalHeight *= proportion;
		this.State.scale *= proportion;
		console.log( "applyProportionToView; adjusting to proportion:", proportion );
	}
};

DrawCalloutClass.prototype.resetResizeState = function resetResizeState() {
	this.State.resize.isResizing = true;
	this.State.resize.mode = 'rotate/resize';

	this.State.resize.transformX = Math.ceil(this.View.width) >> 1;
	this.State.resize.transformY = Math.ceil(this.View.height) >> 1;
	this.State.resize.centerX = this.State.resize.transformX + this.View.x1;
	this.State.resize.centerY = this.State.resize.transformY + this.View.y1;

	this.State.resize.variance = 0;
	this.State.resize.varianceX = 0;
	this.State.resize.varianceY = 0;
};

DrawCalloutClass.prototype.resizeNormalizedAngle = function resizeNormalizedAngle(radians) {
	console.log("-- DrawCallout.resizeNormalizedAngle", radians);
	var TwoPi = Math.PI * 2.0;
	var PiFourths = Math.PI / 4.0;
	while(radians < 0.0) {
		radians += TwoPi;
	}
	while(radians > TwoPi) {
		radians -= TwoPi;
	}
	if(radians >= (7.0*PiFourths) || radians < PiFourths) {// Left
		return 0;
	}
	if(radians >= (3.0*PiFourths) && radians < (5.0*PiFourths)) {// Right
		return 2;
	}
	if (radians >= PiFourths && radians < (3.0*PiFourths)) {// Up
		return 1;
	}
	return 3;
};

DrawCalloutClass.prototype.setRotation = function setRotation(angle) {
	console.log("-- DrawCallout.setRotation", angle);
	if(this.reverse) {
		angle += M_PI;
	}
	if(angle >= Math.PI*2.0) {
		this.setRotation(angle - (Math.PI*2.0));
	}
	this.State.resize.radians = angle;
};

DrawCalloutClass.prototype.setOrientation = function setOrientation( orientation ) {
	var different = (orientation !== this.State.orientation && Math.abs(orientation - this.State.orientation) !== 2);
	this.State.orientation = orientation;
	var left = (this.View.width - this.View.height)/2;
	var top = (this.View.height - this.View.width)/2;
	if( different ) {
		this.View.x1 += left;
		this.View.y1 += top;
		this.View.saveWidth = this.View.width;
		this.View.saveHeight = this.View.height;
		this.View.width = this.View.saveHeight;
		this.View.height = this.View.saveWidth;
		left = (this.View.width - this.View.height)/2;
		top = (this.View.height - this.View.width)/2;
		this.setDirty(true);
	}
	if( this.State.orientation % 2 === 1 ) {
		//this.Canvas._jCanvas.css({left:left, top:top, 'transform':'rotate('+(this.State.orientation*90)+'deg)'});
		this.Style.canvas.left = left;
		this.Style.canvas.top = top;
	} else {
		//this.Canvas._jCanvas.css({left:0, top:0, 'transform':'rotate('+(this.State.orientation*90)+'deg)'});
		this.Style.canvas.left = 0;
		this.Style.canvas.top = 0;
	}
	this.Style.canvas.transform = "rotate(" + (this.State.orientation * 90) + "deg)";
	//this.Canvas._jElement.css({width:this.View.width,height:this.View.height,left:this.View.x1,top:this.View.y1});
	this.Style.element.width = this.View.width;
	this.Style.element.height = this.View.height;
	this.Style.element.left = this.View.x1;
	this.Style.element.top = this.View.y1;
};

DrawCalloutClass.prototype.setDrawReady = function setDrawReady(drawready) {
	//console.log("-- DrawCallout.setDrawReady", drawready);
	this.State.draw.drawready = !!drawready;
};

DrawCalloutClass.prototype.drawUpdate = function drawUpdate( event, verbose ) {
	event.vicarious = true;
	event.preClientX = event.clientX;
	event.preClientY = event.clientY;
	var docScrollX = 0; // $( document ).scrollLeft();
	var docScrollY = 0; // $( document ).scrollTop();
	var x2 = this.View.x1 + this.View.width;
	var y2 = this.View.y1 + this.View.height;
	var viewCX = 0;
	var viewCY = 0;
	var focusCX = 0;
	var focusCY = 0;
	var relativeX = 0;
	var relativeY = 0;
	var currentZoomScale = this.PDFViewer.Page.getZoomScale();
	var scale = (this.State.scale * this.dCallout.baseScale) / (currentZoomScale / this.State.zoomScale);
	// console.log( "DrawCallout.drawUpdate; currentZoomScale:", currentZoomScale, "State.zoomScale:", this.State.zoomScale );
	var clientX = this.getEventX( event, false );
	var clientY = this.getEventY( event, false );
	var coord = Object.assign( {}, this.PDFViewer.Canvas.coord );
	// console.log( "Coord:", "x:", clientX, "y:", clientY, "x1:", this.View.x1, "y1:", this.View.y1, "scrollX:", docScrollX, "scrollY:", docScrollY );
	var canvasOffsetX = this.PDFViewer.Canvas.getCanvasOffsetX();
	var canvasOffsetY = this.PDFViewer.Canvas.getCanvasOffsetY();
	switch( this.State.orientation ) {
		case 0:
			event.clientX = ((((clientX - this.View.x1) / scale) + (this.Focus.x1 * currentZoomScale)) + canvasOffsetX);
			event.clientY = ((((clientY - this.View.y1) / scale) + (this.Focus.y1 * currentZoomScale)) + canvasOffsetY);
			break;
		case 1:
			console.warn( "TODO: fix clientX/clientY calculations for this orientation" );
			//console.log("1", [this.View.width, this.View.height]);
			viewCX = this.View.x1 + (Math.floor( this.View.width ) >> 1);
			viewCY = this.View.y1 + (Math.floor( this.View.height ) >> 1);
			focusCX = this.Focus.x1 + (Math.floor( this.Focus.width ) >> 1);
			focusCY = this.Focus.y1 + (Math.floor( this.Focus.height ) >> 1);
			relativeX = clientX - viewCX;
			relativeY = clientY - viewCY;
			// event.clientX = ((relativeY / scale) + (focusCX * this.PDFViewer.Page.getZoomScale()) - docScrollX) + canvasOffsetX;
			// event.clientY = (((-relativeX) / scale) + (focusCY * this.PDFViewer.Page.getZoomScale()) - docScrollY) + canvasOffsetY;
			event.clientX = ((relativeY / scale) + (focusCX * this.PDFViewer.Page.getZoomScale())) + canvasOffsetX;
			event.clientY = (((-relativeX) / scale) + (focusCY * this.PDFViewer.Page.getZoomScale())) + canvasOffsetY;
			break;
		case 2:
			console.warn( "TODO: fix clientX/clientY calculations for this orientation" );
			x2 = (this.View.x1-docScrollX) + this.View.width;
			y2 = (this.View.y1-docScrollY) + this.View.height;
			event.clientX = (((x2 - clientX) / scale) + (this.Focus.x1 * currentZoomScale) - docScrollX) + canvasOffsetX;
			event.clientY = (((y2 - clientY) / scale) + (this.Focus.y1 * currentZoomScale) - docScrollY) + canvasOffsetY;
			break;
		case 3:
			console.warn( "TODO: fix clientX/clientY calculations for this orientation" );
			//console.log("3", [this.View.width, this.View.height]);
			viewCX = this.View.x1 + (Math.floor( this.View.width ) >> 1);
			viewCY = this.View.y1 + (Math.floor( this.View.height ) >> 1);
			focusCX = this.Focus.x1 + (Math.floor( this.Focus.width ) >> 1);
			focusCY = this.Focus.y1 + (Math.floor( this.Focus.height ) >> 1);
			relativeX = clientX - viewCX;
			relativeY = clientY - viewCY;
			event.clientX = (((-relativeY) / scale) + (focusCX * currentZoomScale) - docScrollX) + canvasOffsetX;
			event.clientY = ((relativeX / scale) + (focusCY * currentZoomScale) - docScrollY) + canvasOffsetY;
			break;
	}
	if( !!verbose ) {
		console.log( "drawUpdate; event.clientX:", event.clientX, "event.clientY:", event.clientY, "scaleX:", coord.scaleX, "scrollX:", coord.scrollX, "scaleY:", coord.scaleY, "scrollY:", coord.scrollY );
	}
	return event;
};

DrawCalloutClass.prototype.drawDown = function drawDown( event ) {
	if( this.State.drawable ) {
		event.preventDefault();
		if( this.State.draw.drawready ) {
			// console.log( "===", "-- DrawCallout::drawDown()" );
			this.State.draw.drawInProgress = true;
			var pEvent = this.drawUpdate( event );
			this.PDFViewer.Canvas.events.mouse.down( pEvent );
		}
	}
};

DrawCalloutClass.prototype.drawMove = function drawMove(event) {
	if(this.State.drawable) {
		event.preventDefault();
		if(this.State.draw.drawready && this.State.draw.drawInProgress) {
			//console.log("===CO", "-- DrawCallout::drawMove()");
			this.PDFViewer.Canvas.events.mouse.move( this.drawUpdate( event ) );
		}
	}
};

DrawCalloutClass.prototype.drawUp = function drawUp(event) {
	if(this.State.drawable) {
		event.preventDefault();
		if(this.State.draw.drawready && this.State.draw.drawInProgress) {
			//console.log("===CO", "-- DrawCallout::drawUp()");
			this.PDFViewer.Canvas.events.mouse.up( this.drawUpdate( event ) );
			this.State.draw.drawInProgress = false;
		}
	}
};

DrawCalloutClass.prototype.drawClick = function drawClick(event) {
	if(this.State.drawable) {
		event.preventDefault();
		if(this.State.draw.drawready) {
			// console.log( "===", "-- DrawCallout::drawClick()" );
			this.PDFViewer.Toolbar.draw.modify.reset();
			this.clickPassAlong(event);
		}
	}
};

DrawCalloutClass.prototype.clickPassAlong = function clickPassAlong(event) {
	//console.log( "clickPassAlong; event:", event );
	var myevent = this.drawUpdate(event);
	//this.PDFViewer.Canvas.events.mouse.down(myevent);
	//this.PDFViewer.Canvas.events.mouse.move(myevent);
	this.PDFViewer.Canvas.events.mouse.click(myevent);
	//this.PDFViewer.safeApply();
};

DrawCalloutClass.prototype.setupEventHandlers = function setupEventHandlers() {
	//console.log("-- DrawCallout.setupEventHandlers");
	if(this.State.drawable) {
		this.setupDrawHandlers();
	}
	if(this.State.closeable) {
		this.setupCloseHandlers();
	}
	if(this.State.resizable) {
		this.setupResizeHandlers();
	}
	if(this.State.moveable) {
		this.setupMoveHandlers();
	}
};

DrawCalloutClass.prototype.setupDrawHandlers = function setupDrawHandlers() {
	//console.log("-- DrawCallout.setupDrawHandlers");
	if(!isset(this.Event.canvas.mydraw)) {
		this.Event.canvas.mydraw = new this.Event.canvas.delegate(this);
		this.Event.canvas.mydraw.down = this.drawDown;
		this.Event.canvas.mydraw.move = this.drawMove;
		this.Event.canvas.mydraw.up = this.drawUp;
		this.Event.canvas.mydraw.click = this.drawClick;
	}
	this.Event.canvas.addHandler(this.Event.canvas.mydraw);
};

DrawCalloutClass.prototype.setupMoveHandlers = function setupMoveHandlers() {
	//console.log("-- DrawCallout.setupMoveHandlers");
	if(!isset(this.Event.canvas.mymove)) {
		this.Event.canvas.mymove = new this.Event.canvas.delegate(this);
		this.Event.canvas.mymove.down = this.moveDown;
		this.Event.canvas.mymove.move = this.moveMove;
		this.Event.canvas.mymove.up = this.moveUp;
		this.Event.canvas.mymove.click = this.moveClick;
		//this.Event.canvas.mymove.dblclick = this.moveDblClick;
	}
	this.Event.canvas.addHandler(this.Event.canvas.mymove);
};

DrawCalloutClass.prototype.setupResizeHandlers = function setupResizeHandlers() {
	//console.log("-- DrawCallout.setupResizeHandlers");
	if(!isset(this.Event.resize.myresize)) {
		this.Event.resize.myresize = new this.Event.resize.delegate(this);
		this.Event.resize.myresize.down = this.resizeDown;
		this.Event.resize.myresize.move = this.resizeMove;
		this.Event.resize.myresize.up = this.resizeUp;
		this.Event.resize.myresize.click = this.resizeClick;
	}
	this.Event.resize.addHandler(this.Event.resize.myresize);
};

DrawCalloutClass.prototype.setupCloseHandlers = function setupCloseHandlers() {
	//console.log("-- DrawCallout.setupCloseHandlers");
	//var DrawCalloutClass = this;
	//var close_clik = function(event) { return DrawCalloutClass.close(event); };
	//this.Canvas._jCloseBtn.on('click', close_clik);
	//this.Canvas._jCloseBtn.show();
};

DrawCalloutClass.prototype.disableEventHandlers = function disableEventHandlers() {
	//console.log("-- DrawCallout.disableEventHandlers");
	this.disableDrawHandlers();
	this.disableMoveHandlers();
	this.disableResizeHandlers();
	this.disableCloseHandlers();
};

DrawCalloutClass.prototype.disableDrawHandlers = function disableDrawHandlers() {
	//console.log("-- DrawCallout.disableDrawHandlers");
	if(isset(this.Event.canvas.mydraw)) {
		this.Event.canvas.removeHandler(this.Event.canvas.mydraw);
	}
};

DrawCalloutClass.prototype.disableMoveHandlers = function disableMoveHandlers() {
	//console.log("-- DrawCallout.disableMoveHandlers");
	if(isset(this.Event.canvas.mymove)) {
		this.Event.canvas.removeHandler(this.Event.canvas.mymove);
	}
};

DrawCalloutClass.prototype.disableResizeHandlers = function disableResizeHandlers() {
	//console.log("-- DrawCallout.disableResizeHandlers");
	if(isset(this.Event.resize.myresize)) {
		this.Event.resize.removeHandler(this.Event.resize.myresize);
	}
};

DrawCalloutClass.prototype.disableCloseHandlers = function disableCloseHandlers() {
	//console.log("-- DrawCallout.disableCloseHandlers");
};

DrawCalloutClass.prototype.saveInitialCoord = function saveInitialCoord(Coord) {
	Coord.Initial.x1 = Coord.x1;
	Coord.Initial.y1 = Coord.y1;
	Coord.Initial.width = Coord.width;
	Coord.Initial.height = Coord.height;
	Coord.Initial.pageWidth = Coord.pageWidth;
	Coord.Initial.pageHeight = Coord.pageHeight;
	Coord.Initial.pdfWidth = Coord.pdfWidth;
	Coord.Initial.pdfHeight = Coord.pdfHeight;
	Coord.Initial.totalWidth = Coord.totalWidth;
	Coord.Initial.totalHeight = Coord.totalHeight;
	Coord.Initial.orientation = this.State.orientation;
};

DrawCalloutClass.prototype.getViewerWidth = function getViewerWidth() {
	return this.PDFViewer.Page.scale.width;
};

DrawCalloutClass.prototype.getViewerHeight = function getViewerHeight() {
	return this.PDFViewer.Page.scale.height;
};

DrawCalloutClass.prototype.getViewerOffsetX = function getViewerOffsetX() {
	return this.PDFViewer.Canvas.getCanvasOffsetX();
};

DrawCalloutClass.prototype.getViewerOffsetY = function getViewerOffsetY() {
	return this.PDFViewer.Canvas.getCanvasOffsetY();
};

DrawCalloutClass.prototype.getPageSize = function getPageSize() {
	return Object.assign( {}, this.PDFViewer.Page.size );
};

DrawCalloutClass.prototype.getPageScale = function getPageScale() {
	return Object.assign( {}, this.PDFViewer.Page.scale );
};

DrawCalloutClass.prototype.getPageWidthScale = function getPageWidthScale() {
	return this.PDFViewer.Page.scaleOptionWidth.scale;
};

DrawCalloutClass.prototype.composeAddJson = function composeAddJson() {
	//console.log("-- DrawCallout.composeAddJson");
	var pageOrientation = 0;
	if( this.PDFViewer.Page.index === this.State.pageIndex ) {
		pageOrientation = this.PDFViewer.Page.orientation.getOrientation();
	}
	var width = this.getViewerWidth();
	var height = this.getViewerHeight();
	var left = (this.View.x1-this.PDFViewer.Canvas.getCanvasOffsetX()) / width;
	var top = (this.View.y1-this.PDFViewer.Canvas.getCanvasOffsetY()) / height;
	var right = (this.View.width) / width;
	var bottom = (this.View.height) / height;
	var result = {
		"zIndex": this.State.zIndex,
		"Initial": {
			"bottom": (this.View.Initial.height) / height,
			"top": (this.View.Initial.y1) / height,
			"right": (this.View.Initial.width) / width,
			"left": (this.View.Initial.x1) / width
		},
		"Bounds": {
			"bottom": bottom,
			"top": top,
			"right": right,
			"left": left
		},
		"Origin": {
			"x": this.Focus.x1 / this.DrawObject.canvasWidth,
			"y": this.Focus.y1 / this.DrawObject.canvasHeight
		},
		"Index": this.Index,
		"viewOrientation": (4-this.State.orientation)%4,
		"drag": {
			"left": this.Focus.Initial.x1 / this.DrawObject.canvasWidth,
			"top": this.Focus.Initial.y1 / this.DrawObject.canvasHeight,
			"bottom": this.Focus.Initial.height / this.DrawObject.canvasHeight,
			"right": this.Focus.Initial.width / this.DrawObject.canvasWidth
		},
		"pageOrientation": pageOrientation,
		"Page": this.State.pageIndex,
		"scale": this.State.scale
	};
	return result;
};

DrawCalloutClass.prototype.composeModifyJson = function composeModifyJson() {
	//console.log("-- DrawCallout.composeModifyJson");
	return this.composeAddJson();
};

DrawCalloutClass.prototype.composeDeleteJson = function composeDeleteJson() {
	//console.log("-- DrawCallout.composeDeleteJson");
	var result = {Page:this.State.pageIndex, Index: this.Index};
	return result;
};

DrawCalloutClass.prototype.sendAdd = function sendAdd() {
	//console.log("-- DrawCallout.sendAdd");
	this.PDFViewer.triggerViewerAPIPresenter('AddCallout', this.composeAddJson());
};

DrawCalloutClass.prototype.sendModify = function sendModify() {
	//console.log("-- DrawCallout.sendModify");
	this.PDFViewer.triggerViewerAPIPresenter('ModifyCallout', this.composeModifyJson());
};

DrawCalloutClass.prototype.sendDelete = function sendDelete() {
	//console.log("-- DrawCallout.sendDelete");
	this.PDFViewer.triggerViewerAPIPresenter('DeleteCallout', this.composeDeleteJson());
};

DrawCalloutClass.prototype.sendZIndex = function sendZIndex() {
	//console.log("-- DrawCallout.sendZIndex");
	this.PDFViewer.sendCalloutsZIndexes();
};

DrawCalloutClass.prototype.receiveAdd = function receiveAdd(args) {
	this.receiveModify(args);
	this.saveInitialCoord(this.View);
	this.saveInitialCoord(this.Focus);
};

DrawCalloutClass.prototype.receiveModify = function receiveModify( args ) {
	//console.log( "receiveModify; args:", args );
	var orientation = (4 - args.pageOrientation) % 4;
	this.setOrientation( orientation );
	var origWidth = this.View.width;
	var pageScale = this.getPageScale();
	var canvasOffset = this.PDFViewer.Canvas.getCanvasOffset();
	var proportion = ((args.Bounds.right * pageScale.width) / origWidth) / args.scale;
	this.resize( proportion );
	var newX = Math.round( (args.Bounds.left * pageScale.width) + canvasOffset.left );
	var newY = Math.round( (args.Bounds.top * pageScale.height) + canvasOffset.top );
	this.setPosition( newX, newY );
	this.setZIndex( args.zIndex );
};

DrawCalloutClass.prototype.receiveDelete = function receiveModify(args) {
	this.hide();
	this.State.destroyed = true;
	this.PDFViewer.Canvas.refresh();
	this.PDFViewer.safeApply();
};

DrawCalloutClass.prototype.setZIndex = function setZIndex(zIndex) {
	//console.log("===CO", "-- DrawCallout::setZIndex()", "Callout:", this.Index, "zIndex:", zIndex);
	this.State.zTop = false;
	this.State.zIndex = zIndex;
	this.Style.element.zIndex = zIndex;
};

DrawCalloutClass.prototype.getZIndex = function getZIndex() {
	return this.State.zIndex;
};

DrawCalloutClass.prototype.isDestroyed = function isDestroyed() {
	return !!this.State.destroyed;
};

DrawCalloutClass.prototype.setDirty = function setDirty(dirty) {
	var origDirty = this.State.dirty;
	this.State.dirty = !!dirty;
	if(origDirty !== this.State.dirty) {
		//console.log("State dirty:", "changed", this.PDFViewer.annotations.hasChanges());
		this.PDFViewer.triggerViewerAPISavable(this.PDFViewer.annotations.hasChanges());
	}
};

DrawCalloutClass.prototype.isDirty = function isDirty() {
	return !!this.State.dirty;
};

DrawCalloutClass.prototype.getOrigin = function getOrigin() {
	return this.State.origin;
};

DrawCalloutClass.prototype.setOrigin = function setOrigin(origin) {
	this.State.origin = origin;
};

DrawCalloutClass.prototype.denormalizeCallout = function denormalizeCallout( calloutData ) {
	if( !isset( calloutData ) || !calloutData ) {
		console.error( "DrawCallout.denormalizeCallout; Unable to denormalize callout data:", calloutData );
		return;
	}
	var pageSize = this.getPageSize();
	var pwScale = this.getPageWidthScale();
	var pageScale = {
		"width": (pageSize.width * pwScale),
		"height": (pageSize.height * pwScale)
	};
	var canvasOffset = this.PDFViewer.Canvas.getCanvasOffset();
	var denormalizedCallout = new DrawCalloutStruct();
	try {
		denormalizedCallout.Bounds.left = (calloutData.Bounds.left * pageScale.width);
		denormalizedCallout.Bounds.top = (calloutData.Bounds.top * pageScale.height);
		denormalizedCallout.Bounds.right = (calloutData.Bounds.right * pageScale.width);
		denormalizedCallout.Bounds.bottom = (calloutData.Bounds.bottom * pageScale.height);
		denormalizedCallout.Initial.left = (calloutData.Initial.left * pageScale.width);
		denormalizedCallout.Initial.top = (calloutData.Initial.top * pageScale.height);
		denormalizedCallout.Initial.right = (calloutData.Initial.right * pageScale.width);
		denormalizedCallout.Initial.bottom = (calloutData.Initial.bottom * pageScale.height);
		denormalizedCallout.drag.left = (calloutData.drag.left * pageScale.width);
		denormalizedCallout.drag.top = (calloutData.drag.top * pageScale.height);
		denormalizedCallout.drag.right = (calloutData.drag.right * pageScale.width);
		denormalizedCallout.drag.bottom = (calloutData.drag.bottom * pageScale.height);
		denormalizedCallout.Origin.x = (calloutData.Origin.x * pageScale.width);
		denormalizedCallout.Origin.y = (calloutData.Origin.y * pageScale.height);
		denormalizedCallout.Index = calloutData.Index;
		denormalizedCallout.Page = calloutData.Page;
		denormalizedCallout.pageOrientation = calloutData.pageOrientation;
		denormalizedCallout.viewOrientation = calloutData.viewOrientation;
		denormalizedCallout.scale = calloutData.scale;
		denormalizedCallout.zIndex = calloutData.zIndex;
		denormalizedCallout.baseScale = this.State.baseScale;
		var hr = (denormalizedCallout.Initial.right / denormalizedCallout.drag.right);
		var vr = (denormalizedCallout.Initial.bottom / denormalizedCallout.drag.bottom);
		if( hr + vr < 4 ) {
			denormalizedCallout.baseScale = Math.max( hr, vr );
		}
	} catch( e ) {
		console.error( "DrawCallout.denormalizeCallout; invalid callout data:", calloutData );
	}
	// console.log( "DrawCallout.denormalizeCallout; callout:", denormalizedCallout );
	return denormalizedCallout;
};

function DrawCalloutStruct() {
	return {
		"Bounds": {"left": 0, "top": 0, "right": 0, "bottom": 0},
		"Initial": {"left": 0, "top": 0, "right": 0, "bottom": 0},
		"drag": {"left": 0, "top": 0, "right": 0, "bottom": 0},
		"Origin": {"x": 0, "y": 0},
		"Page": 0,
		"pageOrientation": 0,
		"viewOrientation": 0,
		"scale": 1,
		"baseScale": 2,
		"Index": 0,
		"zIndex": 0
	};
}


/***********************************************************
 * Function Class DrawCalloutList
 * @param {string} containerSelector
 * @returns {DrawCalloutList}
 */
var DrawCalloutList = function DrawCalloutList() {
	//console.log("===CL","DrawCalloutList()");
	this.callouts = [];
	this.unordered = [];
	this.map = {};
	this.drawable = true;
	this.moveable = true;
	this.closeable = true;
	this.resizable = true;
};
var DrawCalloutListFactory = function DrawCalloutListFactory() {
	//console.log("===CL","DrawCalloutListFactory()");
	var DrawCOList = new DrawCalloutList();
	return DrawCOList;
};

DrawCalloutList.prototype.add = function add(DrawCallout) {
	//console.log("===CL","DrawCalloutList.add()", DrawCallout);
	if(typeof DrawCallout !== "undefined" && DrawCallout !== null && DrawCallout.toString()==='DrawCallout') {
		if(!DrawCallout.isDestroyed()) {
			this.map[DrawCallout.Index] = this.callouts.length;
			this.callouts.push(DrawCallout);
			this.unordered.push(DrawCallout);
			DrawCallout.setZIndex(this.callouts.length + 4);
		}
	}
};

DrawCalloutList.prototype.indexOf = function indexOf(DrawCallout) {
	//console.log("===CL","DrawCalloutList.indexOf()", DrawCallout);
	if(typeof DrawCallout === "object" && DrawCallout !== null && DrawCallout.toString()==='DrawCallout') {
		return this.callouts.indexOf(DrawCallout);
	}
	return -1;
};

DrawCalloutList.prototype.getByIndex = function getByIndex(lookupIndex) {
	//console.log("===CL","DrawCalloutList.getByIndex()");
	if(typeof lookupIndex === "string") {
		lookupIndex = parseInt(lookupIndex);
	}
	var arrayIndex = this.map[lookupIndex];
	if(typeof arrayIndex === "undefined" || arrayIndex === null) {
		return null;
	}
	if(arrayIndex < 0 && arrayIndex >= this.callouts.length ) {
		return null;
	}
	return this.callouts[arrayIndex];
};

DrawCalloutList.prototype.setDrawReady = function setDrawReady(drawready) {
	//console.log("===CL","DrawCalloutList.setDrawReady()", drawready);
	angular.forEach(this.callouts, function(value, key) {
		value.setDrawReady(drawready);
	});
};

DrawCalloutList.prototype.descend = function descend() {
	//console.log("===CL","DrawCalloutList.descend()");
	angular.forEach(this.callouts, function(value) {
		value.elevate(false);
	});
};

DrawCalloutList.prototype.lift = function lift(DrawCallout) {
	//console.log("===CL","DrawCalloutList.lift()", DrawCallout);
	var liftIndex = this.indexOf(DrawCallout);
	if(liftIndex !== -1) {
		this.callouts.splice(liftIndex,1);
		this.callouts.push(DrawCallout);
		this.remap();
		var next = 4;
		angular.forEach(this.callouts, function(value) {
			value.setZIndex(++next);
		});
	}
};

DrawCalloutList.prototype.remap = function remap() {
	var map = {};
	angular.forEach(this.callouts, function(value, key) {
		map[value.Index] = key;
	});
	this.map = map;
};

DrawCalloutList.prototype.remove = function remove(DrawCallout) {
	//console.log("===CL","DrawCalloutList.remove()", DrawCallout);
	var index = jQuery.inArray(DrawCallout, this.callouts);
	if(index > -1) {
		this.callouts.splice(index, 1);
		this.remap();
		return true;
	}
	return false;
};

DrawCalloutList.prototype.clone = function clone() {
	//console.log("===CL","DrawCalloutList.clone()", this.map);
	var DC = DrawCalloutListFactory();
	DC.drawable = this.drawable;
	DC.moveable = this.moveable;
	DC.closeable = this.closeable;
	DC.resizable = this.resizable;
	DC.callouts = this.callouts.slice();
	DC.unordered = this.unordered.slice();
	DC.map = this.map;
	return DC;
};

DrawCalloutList.prototype.inject = function inject(CL) {
	//console.log("===CL","DrawCalloutList.inject()", this.map);
	this.drawable = CL.drawable;
	this.moveable = CL.moveable;
	this.closeable = CL.closeable;
	this.resizable = CL.resizable;
	this.unordered = [];
	this.callouts = [];
	if(CL && CL.toString() === this.toString()) {
		for(var idx in CL.callouts) {
			this.add(CL.callouts[idx]);
		};
		this.remap();
	}
};

DrawCalloutList.prototype.setDrawable = function setDrawable(drawable) {
	//console.log("===CL","DrawCalloutList.setDrawable()", !!moveable);
	this.drawable = !!drawable;
	for(var idx in this.callouts) {
		if(this.drawable) {
			this.callouts[idx].State.drawable = true;
			this.callouts[idx].setupMoveHandlers();
		} else {
			this.callouts[idx].State.drawable = false;
			this.callouts[idx].disableMoveHandlers();
		}
	}
};

DrawCalloutList.prototype.setMoveable = function setMoveable(moveable) {
	//console.log("===CL","DrawCalloutList.setMoveable()", !!moveable);
	this.moveable = !!moveable;
	for(var idx in this.callouts) {
		if(this.moveable) {
			this.callouts[idx].State.moveable = true;
			this.callouts[idx].setupMoveHandlers();
		} else {
			this.callouts[idx].State.moveable = true;
			this.callouts[idx].disableMoveHandlers();
		}
	}
};

DrawCalloutList.prototype.setResizable = function setResizable(resizable) {
	//console.log("===CL","DrawCalloutList.setResizable()", !!resizable);
	this.resizable = !!resizable;
	for(var idx in this.callouts) {
		if(this.resizable) {
			this.callouts[idx].State.resizable = true;
			this.callouts[idx].setupResizeHandlers();
		} else {
			this.callouts[idx].State.resizable = false;
			this.callouts[idx].disableResizeHandlers();
		}
	}
};

DrawCalloutList.prototype.setCloseable = function setCloseable(closeable) {
	//console.log("===CL","DrawCalloutList.setCloseable()", !!closeable);
	this.closeable = !!closeable;
	for(var idx in this.callouts) {
		if(this.closeable) {
			this.callouts[idx].State.closeable = true;
			this.callouts[idx].setupCloseHandlers();
		} else {
			this.callouts[idx].State.closeable = false;
			this.callouts[idx].disableCloseHandlers();
		}
	}
};

DrawCalloutList.prototype.setZIndexes = function setZIndexes(args) {
	console.log("DrawCalloutList.setZIndexes()", args);
	var Indices = args.Indices;
	var self = this;
	angular.forEach(Indices, function(value, key) {
		var Callout = self.getByIndex(key);
		if(isset(Callout) && Callout.toString() === 'DrawCallout') {
			Callout.setZIndex(value);
		}
	});
};

DrawCalloutList.prototype.getZIndexes = function getZIndexes() {
	var Indices = {};
	angular.forEach(this.callouts, function(value) {
		if(isset(value) && value.toString() === 'DrawCallout') {
			Indices[value.Index] = value.getZIndex();
		}
	});
	return Indices;
};

DrawCalloutList.prototype.showAll = function showAll() {
	//console.log("===CL","DrawCalloutList.setDrawable()", !!moveable);
	for(var idx in this.callouts) {
		this.callouts[idx].show();
	}
};

DrawCalloutList.prototype.hideAll = function hideAll() {
	//console.log("===CL","DrawCalloutList.setDrawable()", !!moveable);
	for(var idx in this.callouts) {
		this.callouts[idx].hide();
	}
};
