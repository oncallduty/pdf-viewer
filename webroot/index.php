<?php

error_reporting( E_ALL );
ini_set( 'display_errors', 1 );

$app = function() {
	$fwPath = realpath( pathinfo( __FILE__, PATHINFO_DIRNAME ) . '/../framework' );
	require_once( "{$fwPath}/bootstrap.php" );

	$routeWhitelist = ['login','logout']; // these routes don't require authentication

	$route = 'viewer';	//default route
	$URI = Config::getInstance()->URI;
	if( property_exists( $URI, 'route' ) ) {
		$route = mb_strtolower( $URI->route );
	}

	//authenticate -- user
	if( !in_array( $route, $routeWhitelist ) ) { // these routes don't require authentication
//		Log::add( "[Router] authenticate user for route: {$route}" );
		if( !Auth::validateUser() ) {
			Log::add( '[Router] user authentication failed' );
			$route = 'login';
		}
	}

	$session =& $_SESSION[$URI->platform];

	//authenticate -- file
	if( in_array( $route, ['viewer','presenter','presentation','image','search','annotations'] ) ) {
		Log::add( $URI->request );
		if( $URI->platform === 'webapp' && ((in_array( $route, ['presenter','presentation'] ) && $URI->sessionID) || (isset( $session['inPresentation'] ) && $session['inPresentation'] == $URI->routeID)) ) {
			if( !Auth::validatePresentation( $route ) ) {
				$user = $session['user'];
				Log::add( "userID: {$user['ID']} role: '{$user['role']}', does not have access to session: {$URI->sessionID}" );
				$route = '404';
			}
		} else {
			if( !Auth::validateFile( $route ) ) {
				if( isset( $session['user'] ) && isset( $session['user'] ) ) {
					$user = $session['user'];
					Log::add( "userID: {$user['ID']} role: '{$user['role']}', does not have access to file: {$URI->routeID}" );
				} elseif( isset( $session['backendType'] ) && isset( $session['email'] ) ) {
					Log::add( "user: {$session['email']}, does not have access to file: {$session['fileID']}" );
				} else {
					Log::add( "user does not have access to file: {$session['fileID']}" );
				}
				$route = '404';
			}
		}
	}

	switch( $route ) {
		case 'login':
			require_once( "{$fwPath}/views/login.php" );
			break;
		case 'logout':
			session_destroy();
			session_unset();
			exit;
			break;
		case 'image':
			require_once( "{$fwPath}/views/image.php" );
			break;
		case 'annotations':
			require_once( "{$fwPath}/views/annotations.php" );
			break;
		case 'search':
			require_once( "{$fwPath}/views/search.php" );
			break;
		case 'presenter':
		case 'presentation':
		case 'viewer':
			require_once( "{$fwPath}/views/viewer.php" );
			break;
		default:
		case '404':
			require_once( "{$fwPath}/views/404.php" );
			break;
	}
};

$app();
unset( $app );
